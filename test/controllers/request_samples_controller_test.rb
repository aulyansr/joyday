require 'test_helper'

class RequestSamplesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @request_sample = request_samples(:one)
  end

  test "should get index" do
    get request_samples_url
    assert_response :success
  end

  test "should get new" do
    get new_request_sample_url
    assert_response :success
  end

  test "should create request_sample" do
    assert_difference('RequestSample.count') do
      post request_samples_url, params: { request_sample: { address: @request_sample.address, city: @request_sample.city, email: @request_sample.email, name: @request_sample.name, phone_number: @request_sample.phone_number, province: @request_sample.province, variant: @request_sample.variant } }
    end

    assert_redirected_to request_sample_url(RequestSample.last)
  end

  test "should show request_sample" do
    get request_sample_url(@request_sample)
    assert_response :success
  end

  test "should get edit" do
    get edit_request_sample_url(@request_sample)
    assert_response :success
  end

  test "should update request_sample" do
    patch request_sample_url(@request_sample), params: { request_sample: { address: @request_sample.address, city: @request_sample.city, email: @request_sample.email, name: @request_sample.name, phone_number: @request_sample.phone_number, province: @request_sample.province, variant: @request_sample.variant } }
    assert_redirected_to request_sample_url(@request_sample)
  end

  test "should destroy request_sample" do
    assert_difference('RequestSample.count', -1) do
      delete request_sample_url(@request_sample)
    end

    assert_redirected_to request_samples_url
  end
end
