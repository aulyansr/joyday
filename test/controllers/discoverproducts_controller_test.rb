require 'test_helper'

class DiscoverproductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @discoverproduct = discoverproducts(:one)
  end

  test "should get index" do
    get discoverproducts_url
    assert_response :success
  end

  test "should get new" do
    get new_discoverproduct_url
    assert_response :success
  end

  test "should create discoverproduct" do
    assert_difference('Discoverproduct.count') do
      post discoverproducts_url, params: { discoverproduct: { content_en: @discoverproduct.content_en, content_id: @discoverproduct.content_id, custom: @discoverproduct.custom, full_image: @discoverproduct.full_image, link: @discoverproduct.link, title_en: @discoverproduct.title_en, title_id: @discoverproduct.title_id, tp_1_image_1: @discoverproduct.tp_1_image_1, tp_1_image_2: @discoverproduct.tp_1_image_2, tp_1_image_3: @discoverproduct.tp_1_image_3, tp_1_image_4: @discoverproduct.tp_1_image_4, tp_1_image_5: @discoverproduct.tp_1_image_5, tp_2_image_1: @discoverproduct.tp_2_image_1, tp_2_image_3: @discoverproduct.tp_2_image_3, tp_3_image_3: @discoverproduct.tp_3_image_3, tp_3_image_4: @discoverproduct.tp_3_image_4, tp_image_6: @discoverproduct.tp_image_6 } }
    end

    assert_redirected_to discoverproduct_url(Discoverproduct.last)
  end

  test "should show discoverproduct" do
    get discoverproduct_url(@discoverproduct)
    assert_response :success
  end

  test "should get edit" do
    get edit_discoverproduct_url(@discoverproduct)
    assert_response :success
  end

  test "should update discoverproduct" do
    patch discoverproduct_url(@discoverproduct), params: { discoverproduct: { content_en: @discoverproduct.content_en, content_id: @discoverproduct.content_id, custom: @discoverproduct.custom, full_image: @discoverproduct.full_image, link: @discoverproduct.link, title_en: @discoverproduct.title_en, title_id: @discoverproduct.title_id, tp_1_image_1: @discoverproduct.tp_1_image_1, tp_1_image_2: @discoverproduct.tp_1_image_2, tp_1_image_3: @discoverproduct.tp_1_image_3, tp_1_image_4: @discoverproduct.tp_1_image_4, tp_1_image_5: @discoverproduct.tp_1_image_5, tp_2_image_1: @discoverproduct.tp_2_image_1, tp_2_image_3: @discoverproduct.tp_2_image_3, tp_3_image_3: @discoverproduct.tp_3_image_3, tp_3_image_4: @discoverproduct.tp_3_image_4, tp_image_6: @discoverproduct.tp_image_6 } }
    assert_redirected_to discoverproduct_url(@discoverproduct)
  end

  test "should destroy discoverproduct" do
    assert_difference('Discoverproduct.count', -1) do
      delete discoverproduct_url(@discoverproduct)
    end

    assert_redirected_to discoverproducts_url
  end
end
