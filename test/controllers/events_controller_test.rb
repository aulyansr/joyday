require 'test_helper'

class EventsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @event = events(:one)
  end

  test "should get index" do
    get events_url
    assert_response :success
  end

  test "should get new" do
    get new_event_url
    assert_response :success
  end

  test "should create event" do
    assert_difference('Event.count') do
      post events_url, params: { event: { content_en: @event.content_en, content_id: @event.content_id, featured_image: @event.featured_image, meta_description_en: @event.meta_description_en, meta_description_id: @event.meta_description_id, title_en: @event.title_en, title_id: @event.title_id } }
    end

    assert_redirected_to event_url(Event.last)
  end

  test "should show event" do
    get event_url(@event)
    assert_response :success
  end

  test "should get edit" do
    get edit_event_url(@event)
    assert_response :success
  end

  test "should update event" do
    patch event_url(@event), params: { event: { content_en: @event.content_en, content_id: @event.content_id, featured_image: @event.featured_image, meta_description_en: @event.meta_description_en, meta_description_id: @event.meta_description_id, title_en: @event.title_en, title_id: @event.title_id } }
    assert_redirected_to event_url(@event)
  end

  test "should destroy event" do
    assert_difference('Event.count', -1) do
      delete event_url(@event)
    end

    assert_redirected_to events_url
  end
end
