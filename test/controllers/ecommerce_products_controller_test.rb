require 'test_helper'

class EcommerceProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ecommerce_product = ecommerce_products(:one)
  end

  test "should get index" do
    get ecommerce_products_url
    assert_response :success
  end

  test "should get new" do
    get new_ecommerce_product_url
    assert_response :success
  end

  test "should create ecommerce_product" do
    assert_difference('EcommerceProduct.count') do
      post ecommerce_products_url, params: { ecommerce_product: { ecommerce_id: @ecommerce_product.ecommerce_id, link: @ecommerce_product.link, product_id: @ecommerce_product.product_id } }
    end

    assert_redirected_to ecommerce_product_url(EcommerceProduct.last)
  end

  test "should show ecommerce_product" do
    get ecommerce_product_url(@ecommerce_product)
    assert_response :success
  end

  test "should get edit" do
    get edit_ecommerce_product_url(@ecommerce_product)
    assert_response :success
  end

  test "should update ecommerce_product" do
    patch ecommerce_product_url(@ecommerce_product), params: { ecommerce_product: { ecommerce_id: @ecommerce_product.ecommerce_id, link: @ecommerce_product.link, product_id: @ecommerce_product.product_id } }
    assert_redirected_to ecommerce_product_url(@ecommerce_product)
  end

  test "should destroy ecommerce_product" do
    assert_difference('EcommerceProduct.count', -1) do
      delete ecommerce_product_url(@ecommerce_product)
    end

    assert_redirected_to ecommerce_products_url
  end
end
