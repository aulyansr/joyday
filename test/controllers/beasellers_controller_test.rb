require 'test_helper'

class BeasellersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @beaseller = beasellers(:one)
  end

  test "should get index" do
    get beasellers_url
    assert_response :success
  end

  test "should get new" do
    get new_beaseller_url
    assert_response :success
  end

  test "should create beaseller" do
    assert_difference('Beaseller.count') do
      post beasellers_url, params: { beaseller: { address: @beaseller.address, city: @beaseller.city, email: @beaseller.email, name: @beaseller.name, phone_number: @beaseller.phone_number, province: @beaseller.province } }
    end

    assert_redirected_to beaseller_url(Beaseller.last)
  end

  test "should show beaseller" do
    get beaseller_url(@beaseller)
    assert_response :success
  end

  test "should get edit" do
    get edit_beaseller_url(@beaseller)
    assert_response :success
  end

  test "should update beaseller" do
    patch beaseller_url(@beaseller), params: { beaseller: { address: @beaseller.address, city: @beaseller.city, email: @beaseller.email, name: @beaseller.name, phone_number: @beaseller.phone_number, province: @beaseller.province } }
    assert_redirected_to beaseller_url(@beaseller)
  end

  test "should destroy beaseller" do
    assert_difference('Beaseller.count', -1) do
      delete beaseller_url(@beaseller)
    end

    assert_redirected_to beasellers_url
  end
end
