require 'test_helper'

class ImageProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @image_product = image_products(:one)
  end

  test "should get index" do
    get image_products_url
    assert_response :success
  end

  test "should get new" do
    get new_image_product_url
    assert_response :success
  end

  test "should create image_product" do
    assert_difference('ImageProduct.count') do
      post image_products_url, params: { image_product: { image: @image_product.image, product_id: @image_product.product_id } }
    end

    assert_redirected_to image_product_url(ImageProduct.last)
  end

  test "should show image_product" do
    get image_product_url(@image_product)
    assert_response :success
  end

  test "should get edit" do
    get edit_image_product_url(@image_product)
    assert_response :success
  end

  test "should update image_product" do
    patch image_product_url(@image_product), params: { image_product: { image: @image_product.image, product_id: @image_product.product_id } }
    assert_redirected_to image_product_url(@image_product)
  end

  test "should destroy image_product" do
    assert_difference('ImageProduct.count', -1) do
      delete image_product_url(@image_product)
    end

    assert_redirected_to image_products_url
  end
end
