require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @page = pages(:one)
  end

  test "should get index" do
    get pages_url
    assert_response :success
  end

  test "should get new" do
    get new_page_url
    assert_response :success
  end

  test "should create page" do
    assert_difference('Page.count') do
      post pages_url, params: { page: { background: @page.background, content_en: @page.content_en, content_id: @page.content_id, meta_description_en: @page.meta_description_en, meta_description_id: @page.meta_description_id, status: @page.status, title_en: @page.title_en, title_id: @page.title_id } }
    end

    assert_redirected_to page_url(Page.last)
  end

  test "should show page" do
    get page_url(@page)
    assert_response :success
  end

  test "should get edit" do
    get edit_page_url(@page)
    assert_response :success
  end

  test "should update page" do
    patch page_url(@page), params: { page: { background: @page.background, content_en: @page.content_en, content_id: @page.content_id, meta_description_en: @page.meta_description_en, meta_description_id: @page.meta_description_id, status: @page.status, title_en: @page.title_en, title_id: @page.title_id } }
    assert_redirected_to page_url(@page)
  end

  test "should destroy page" do
    assert_difference('Page.count', -1) do
      delete page_url(@page)
    end

    assert_redirected_to pages_url
  end
end
