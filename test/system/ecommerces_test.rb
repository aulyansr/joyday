require "application_system_test_case"

class EcommercesTest < ApplicationSystemTestCase
  setup do
    @ecommerce = ecommerces(:one)
  end

  test "visiting the index" do
    visit ecommerces_url
    assert_selector "h1", text: "Ecommerces"
  end

  test "creating a Ecommerce" do
    visit ecommerces_url
    click_on "New Ecommerce"

    fill_in "Logo", with: @ecommerce.logo
    fill_in "Title", with: @ecommerce.title
    click_on "Create Ecommerce"

    assert_text "Ecommerce was successfully created"
    click_on "Back"
  end

  test "updating a Ecommerce" do
    visit ecommerces_url
    click_on "Edit", match: :first

    fill_in "Logo", with: @ecommerce.logo
    fill_in "Title", with: @ecommerce.title
    click_on "Update Ecommerce"

    assert_text "Ecommerce was successfully updated"
    click_on "Back"
  end

  test "destroying a Ecommerce" do
    visit ecommerces_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Ecommerce was successfully destroyed"
  end
end
