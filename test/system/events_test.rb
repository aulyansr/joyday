require "application_system_test_case"

class EventsTest < ApplicationSystemTestCase
  setup do
    @event = events(:one)
  end

  test "visiting the index" do
    visit events_url
    assert_selector "h1", text: "Events"
  end

  test "creating a Event" do
    visit events_url
    click_on "New Event"

    fill_in "Content en", with: @event.content_en
    fill_in "Content", with: @event.content_id
    fill_in "Featured image", with: @event.featured_image
    fill_in "Meta description en", with: @event.meta_description_en
    fill_in "Meta description", with: @event.meta_description_id
    fill_in "Title en", with: @event.title_en
    fill_in "Title", with: @event.title_id
    click_on "Create Event"

    assert_text "Event was successfully created"
    click_on "Back"
  end

  test "updating a Event" do
    visit events_url
    click_on "Edit", match: :first

    fill_in "Content en", with: @event.content_en
    fill_in "Content", with: @event.content_id
    fill_in "Featured image", with: @event.featured_image
    fill_in "Meta description en", with: @event.meta_description_en
    fill_in "Meta description", with: @event.meta_description_id
    fill_in "Title en", with: @event.title_en
    fill_in "Title", with: @event.title_id
    click_on "Update Event"

    assert_text "Event was successfully updated"
    click_on "Back"
  end

  test "destroying a Event" do
    visit events_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Event was successfully destroyed"
  end
end
