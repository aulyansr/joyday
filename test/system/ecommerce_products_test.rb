require "application_system_test_case"

class EcommerceProductsTest < ApplicationSystemTestCase
  setup do
    @ecommerce_product = ecommerce_products(:one)
  end

  test "visiting the index" do
    visit ecommerce_products_url
    assert_selector "h1", text: "Ecommerce Products"
  end

  test "creating a Ecommerce product" do
    visit ecommerce_products_url
    click_on "New Ecommerce Product"

    fill_in "Ecommerce", with: @ecommerce_product.ecommerce_id
    fill_in "Link", with: @ecommerce_product.link
    fill_in "Product", with: @ecommerce_product.product_id
    click_on "Create Ecommerce product"

    assert_text "Ecommerce product was successfully created"
    click_on "Back"
  end

  test "updating a Ecommerce product" do
    visit ecommerce_products_url
    click_on "Edit", match: :first

    fill_in "Ecommerce", with: @ecommerce_product.ecommerce_id
    fill_in "Link", with: @ecommerce_product.link
    fill_in "Product", with: @ecommerce_product.product_id
    click_on "Update Ecommerce product"

    assert_text "Ecommerce product was successfully updated"
    click_on "Back"
  end

  test "destroying a Ecommerce product" do
    visit ecommerce_products_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Ecommerce product was successfully destroyed"
  end
end
