require "application_system_test_case"

class ImageProductsTest < ApplicationSystemTestCase
  setup do
    @image_product = image_products(:one)
  end

  test "visiting the index" do
    visit image_products_url
    assert_selector "h1", text: "Image Products"
  end

  test "creating a Image product" do
    visit image_products_url
    click_on "New Image Product"

    fill_in "Image", with: @image_product.image
    fill_in "Product", with: @image_product.product_id
    click_on "Create Image product"

    assert_text "Image product was successfully created"
    click_on "Back"
  end

  test "updating a Image product" do
    visit image_products_url
    click_on "Edit", match: :first

    fill_in "Image", with: @image_product.image
    fill_in "Product", with: @image_product.product_id
    click_on "Update Image product"

    assert_text "Image product was successfully updated"
    click_on "Back"
  end

  test "destroying a Image product" do
    visit image_products_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Image product was successfully destroyed"
  end
end
