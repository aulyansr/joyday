require "application_system_test_case"

class DiscoverproductsTest < ApplicationSystemTestCase
  setup do
    @discoverproduct = discoverproducts(:one)
  end

  test "visiting the index" do
    visit discoverproducts_url
    assert_selector "h1", text: "Discoverproducts"
  end

  test "creating a Discoverproduct" do
    visit discoverproducts_url
    click_on "New Discoverproduct"

    fill_in "Content en", with: @discoverproduct.content_en
    fill_in "Content", with: @discoverproduct.content_id
    fill_in "Custom", with: @discoverproduct.custom
    fill_in "Full image", with: @discoverproduct.full_image
    fill_in "Link", with: @discoverproduct.link
    fill_in "Title en", with: @discoverproduct.title_en
    fill_in "Title", with: @discoverproduct.title_id
    fill_in "Tp 1 image 1", with: @discoverproduct.tp_1_image_1
    fill_in "Tp 1 image 2", with: @discoverproduct.tp_1_image_2
    fill_in "Tp 1 image 3", with: @discoverproduct.tp_1_image_3
    fill_in "Tp 1 image 4", with: @discoverproduct.tp_1_image_4
    fill_in "Tp 1 image 5", with: @discoverproduct.tp_1_image_5
    fill_in "Tp 2 image 1", with: @discoverproduct.tp_2_image_1
    fill_in "Tp 2 image 3", with: @discoverproduct.tp_2_image_3
    fill_in "Tp 3 image 3", with: @discoverproduct.tp_3_image_3
    fill_in "Tp 3 image 4", with: @discoverproduct.tp_3_image_4
    fill_in "Tp image 6", with: @discoverproduct.tp_image_6
    click_on "Create Discoverproduct"

    assert_text "Discoverproduct was successfully created"
    click_on "Back"
  end

  test "updating a Discoverproduct" do
    visit discoverproducts_url
    click_on "Edit", match: :first

    fill_in "Content en", with: @discoverproduct.content_en
    fill_in "Content", with: @discoverproduct.content_id
    fill_in "Custom", with: @discoverproduct.custom
    fill_in "Full image", with: @discoverproduct.full_image
    fill_in "Link", with: @discoverproduct.link
    fill_in "Title en", with: @discoverproduct.title_en
    fill_in "Title", with: @discoverproduct.title_id
    fill_in "Tp 1 image 1", with: @discoverproduct.tp_1_image_1
    fill_in "Tp 1 image 2", with: @discoverproduct.tp_1_image_2
    fill_in "Tp 1 image 3", with: @discoverproduct.tp_1_image_3
    fill_in "Tp 1 image 4", with: @discoverproduct.tp_1_image_4
    fill_in "Tp 1 image 5", with: @discoverproduct.tp_1_image_5
    fill_in "Tp 2 image 1", with: @discoverproduct.tp_2_image_1
    fill_in "Tp 2 image 3", with: @discoverproduct.tp_2_image_3
    fill_in "Tp 3 image 3", with: @discoverproduct.tp_3_image_3
    fill_in "Tp 3 image 4", with: @discoverproduct.tp_3_image_4
    fill_in "Tp image 6", with: @discoverproduct.tp_image_6
    click_on "Update Discoverproduct"

    assert_text "Discoverproduct was successfully updated"
    click_on "Back"
  end

  test "destroying a Discoverproduct" do
    visit discoverproducts_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Discoverproduct was successfully destroyed"
  end
end
