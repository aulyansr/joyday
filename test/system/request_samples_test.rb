require "application_system_test_case"

class RequestSamplesTest < ApplicationSystemTestCase
  setup do
    @request_sample = request_samples(:one)
  end

  test "visiting the index" do
    visit request_samples_url
    assert_selector "h1", text: "Request Samples"
  end

  test "creating a Request sample" do
    visit request_samples_url
    click_on "New Request Sample"

    fill_in "Address", with: @request_sample.address
    fill_in "City", with: @request_sample.city
    fill_in "Email", with: @request_sample.email
    fill_in "Name", with: @request_sample.name
    fill_in "Phone number", with: @request_sample.phone_number
    fill_in "Province", with: @request_sample.province
    fill_in "Variant", with: @request_sample.variant
    click_on "Create Request sample"

    assert_text "Request sample was successfully created"
    click_on "Back"
  end

  test "updating a Request sample" do
    visit request_samples_url
    click_on "Edit", match: :first

    fill_in "Address", with: @request_sample.address
    fill_in "City", with: @request_sample.city
    fill_in "Email", with: @request_sample.email
    fill_in "Name", with: @request_sample.name
    fill_in "Phone number", with: @request_sample.phone_number
    fill_in "Province", with: @request_sample.province
    fill_in "Variant", with: @request_sample.variant
    click_on "Update Request sample"

    assert_text "Request sample was successfully updated"
    click_on "Back"
  end

  test "destroying a Request sample" do
    visit request_samples_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Request sample was successfully destroyed"
  end
end
