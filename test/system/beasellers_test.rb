require "application_system_test_case"

class BeasellersTest < ApplicationSystemTestCase
  setup do
    @beaseller = beasellers(:one)
  end

  test "visiting the index" do
    visit beasellers_url
    assert_selector "h1", text: "Beasellers"
  end

  test "creating a Beaseller" do
    visit beasellers_url
    click_on "New Beaseller"

    fill_in "Address", with: @beaseller.address
    fill_in "City", with: @beaseller.city
    fill_in "Email", with: @beaseller.email
    fill_in "Name", with: @beaseller.name
    fill_in "Phone number", with: @beaseller.phone_number
    fill_in "Province", with: @beaseller.province
    click_on "Create Beaseller"

    assert_text "Beaseller was successfully created"
    click_on "Back"
  end

  test "updating a Beaseller" do
    visit beasellers_url
    click_on "Edit", match: :first

    fill_in "Address", with: @beaseller.address
    fill_in "City", with: @beaseller.city
    fill_in "Email", with: @beaseller.email
    fill_in "Name", with: @beaseller.name
    fill_in "Phone number", with: @beaseller.phone_number
    fill_in "Province", with: @beaseller.province
    click_on "Update Beaseller"

    assert_text "Beaseller was successfully updated"
    click_on "Back"
  end

  test "destroying a Beaseller" do
    visit beasellers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Beaseller was successfully destroyed"
  end
end
