require "application_system_test_case"

class PagesTest < ApplicationSystemTestCase
  setup do
    @page = pages(:one)
  end

  test "visiting the index" do
    visit pages_url
    assert_selector "h1", text: "Pages"
  end

  test "creating a Page" do
    visit pages_url
    click_on "New Page"

    fill_in "Background", with: @page.background
    fill_in "Content en", with: @page.content_en
    fill_in "Content", with: @page.content_id
    fill_in "Meta description en", with: @page.meta_description_en
    fill_in "Meta description", with: @page.meta_description_id
    fill_in "Status", with: @page.status
    fill_in "Title en", with: @page.title_en
    fill_in "Title", with: @page.title_id
    click_on "Create Page"

    assert_text "Page was successfully created"
    click_on "Back"
  end

  test "updating a Page" do
    visit pages_url
    click_on "Edit", match: :first

    fill_in "Background", with: @page.background
    fill_in "Content en", with: @page.content_en
    fill_in "Content", with: @page.content_id
    fill_in "Meta description en", with: @page.meta_description_en
    fill_in "Meta description", with: @page.meta_description_id
    fill_in "Status", with: @page.status
    fill_in "Title en", with: @page.title_en
    fill_in "Title", with: @page.title_id
    click_on "Update Page"

    assert_text "Page was successfully updated"
    click_on "Back"
  end

  test "destroying a Page" do
    visit pages_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Page was successfully destroyed"
  end
end
