require "application_system_test_case"

class ProductsTest < ApplicationSystemTestCase
  setup do
    @product = products(:one)
  end

  test "visiting the index" do
    visit products_url
    assert_selector "h1", text: "Products"
  end

  test "creating a Product" do
    visit products_url
    click_on "New Product"

    fill_in "Bukalapak", with: @product.bukalapak
    fill_in "Content en", with: @product.content_en
    fill_in "Content", with: @product.content_id
    fill_in "Featured image", with: @product.featured_image
    fill_in "Netto", with: @product.netto
    fill_in "Product category", with: @product.product_category_id
    fill_in "Shopee", with: @product.shopee
    fill_in "Sku", with: @product.sku
    fill_in "Title en", with: @product.title_en
    fill_in "Tokopedia", with: @product.tokopedia
    click_on "Create Product"

    assert_text "Product was successfully created"
    click_on "Back"
  end

  test "updating a Product" do
    visit products_url
    click_on "Edit", match: :first

    fill_in "Bukalapak", with: @product.bukalapak
    fill_in "Content en", with: @product.content_en
    fill_in "Content", with: @product.content_id
    fill_in "Featured image", with: @product.featured_image
    fill_in "Netto", with: @product.netto
    fill_in "Product category", with: @product.product_category_id
    fill_in "Shopee", with: @product.shopee
    fill_in "Sku", with: @product.sku
    fill_in "Title en", with: @product.title_en
    fill_in "Tokopedia", with: @product.tokopedia
    click_on "Update Product"

    assert_text "Product was successfully updated"
    click_on "Back"
  end

  test "destroying a Product" do
    visit products_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Product was successfully destroyed"
  end
end
