<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<style type="text/css">
.coklat1{
	position: absolute;
    height: 180px;
    top: 12vh;
    right:0;
    z-index:10;
}
.coklat2{
	position: absolute;
    height: 180px;
    bottom: 3vh;
    right:1vw;
    z-index:5;
}
.coklat3{
	position: absolute;
    right: 0;
    bottom:20vh;
    width:200px;
}
.titlepage{
	font-size: 2rem;
	text-transform: uppercase;
	text-align: center;
	margin-bottom: 2rem;
	color:#0060af;
}
.paper p{
	text-transform: uppercase;
	letter-spacing: .2em;
	color:#0060af;
	font-weight: 300;
	font-size: .7em;
	line-height: 2;
	margin-bottom:1em;
}
.sidebare{
	max-height: 80%;
}
.sidebare .wrapmenusidebar{
	max-width: 170px;
	margin:0;
}
.sidebare h2{
	font-size: 2rem;
	margin:2rem 0;
	color: #0060af;
}
.item-blog{
	position: relative;
	letter-spacing: .1em;
	line-height: 1.5;
	padding:15px 2.5rem;
	font-size:13px;
	/* text-transform: uppercase; */
	opacity: .5;
	color: #0060af;
	-webkit-transition: all 1s ease;
    -moz-transition: all 1s ease;
    -o-transition: all 1s ease;
    -ms-transition: all 1s ease;
    transition: all 1s ease;
}
.item-blog:hover{
	padding-left: 6rem;
	opacity: 1;
	cursor: pointer;
}
.title-blog{
	font-size:2rem;
	margin:0;
	padding:0;
}
.meta-title-blog{
	padding:5px 0 20px 0;
}
.content-item-blog{
	max-width: 650px;
	line-height: 2;
	margin-bottom: 25px;
}
.readmore{
	display: inline-block;
	position: relative;
	border:1px solid #0060af;
	padding:10px 15px;
	padding-right:35px;
	border-radius: 4px;
	font-size: 12px;
	margin-bottom: 15px;
}
.readmore span{
	position: absolute;
	top:10px;
	right:10px;
	left:auto;
	font-size: 15px;
}
.item-blog:nth-of-type(odd) {

}

.item-blog:nth-of-type(even) {

}
.papaerblog{
	padding-bottom: 50px;
}
.fancybox-slide--iframe .fancybox-close-small{
	right:0;
}
.fancybox-slide--iframe .fancybox-content::-webkit-scrollbar {
    width: 10px;
}

/* Track */
.fancybox-slide--iframe .fancybox-content::-webkit-scrollbar-track {
	background: rgba(255,255,255,0.7);
    border: 4px solid transparent;
    background-clip: content-box;
    border-radius: 10px;
}

/* Handle */
.fancybox-slide--iframe .fancybox-content::-webkit-scrollbar-thumb {
    background: rgb(255,255,255);
    border-radius: 10px;
}
.fancybox-slide--iframe .fancybox-content::-webkit-scrollbar-thumb:vertical {
    height:15px;
}
/* Handle on hover */
.fancybox-slide--iframe .fancybox-content::-webkit-scrollbar-thumb:hover {
    background: #555;
}
.fancybox-bg{
	background: rgba(255,255,255,0.3);
}
.close-smalle{
	position: absolute;
	top: 0;
	right: 0;
	width: 44px;
	height: 44px;
	padding: 0;
	margin: 0;
	border: 0;
	border-radius: 0;
	outline: none;
	background: transparent;
	z-index: 104;
	cursor: pointer;
}
.close-smalle:after {
    content: '×';
    position: absolute;
    top: 5px;
    right: 5px;
    width: 30px;
    height: 30px;
    font: 40px/50px Arial,"Helvetica Neue",Helvetica,sans-serif;
    color: #0060af;
    font-weight: 300;
    text-align: center;
    border-radius: 50%;
    border-width: 0;
    background: transparent;
    transition: background .25s;
    box-sizing: border-box;
    z-index: 26;
}
.detail-blog-item{
	position: absolute;
	top:100%;
	left:0rem;
	width: 100%;
	background: #fff;
	padding-bottom:12rem;
	height: 100vh;
	overflow: hidden;
	overflow-y: auto;
	z-index: 102;
	background: transparent;
}
.detail-blog-item > .wrappadding{
	position: relative;
	padding: 4rem 2.5rem;
	padding-right:1rem;
	margin:0 3rem;
	background: rgba(255,255,255,.98);
	border-radius: 5px;
	overflow: hidden;
	overflow-y: auto;
	box-shadow:0 10px 20px 0px #555;
}
.detail-blog-item > .wrappadding{
	/* text-transform: uppercase; */
    letter-spacing: .1em;
    color: #0060af;
    font-weight: 300;
    font-size: 13px;
    line-height: 2;
	min-height:90vh;
    margin-bottom: 10rem;
    padding-bottom: 8rem;
}
.detail-blog-item > .wrappadding p{
    margin-bottom: 1em;
}
.nanas.hasContent .blogwrap{
	z-index:20;
}
.nanas.hasContent .blogwrap .ss-container{
	overflow-y: hidden;
}
.content-description{
	max-width: 90%;
}
.content-socmed{
	max-width: 10%;
}
.sosmed-product-detaile{
	padding-right:0rem;
	font-size:2rem;
}
.sosmed-product-detaile a{
	text-decoration: none;
	color: #005b9c;
	cursor: pointer;
	opacity: .9;
}
.headerhome,.socmed{
	opacity: 1;
}
.detail-blog-item ul li,.detail-blog-item ol li{
	margin-bottom:10px;
}
<?php
	if($this->input->get('date') != ''){
		echo '[data-date]:not([data-date="'. $this->input->get('date') .'"]){
			display:none;
		}';
	}
?>
@media (min-width: 1279.99px) {
    .blogging{
		margin:0 10px 4rem 10px;
        padding-bottom: 4rem;
	}
}
@media (max-width: 769px){
	.ss-container {
	    max-height: 78vh;
	}
	.paper{
		margin: 0 0px;
	}
	.coklat1,.coklat2{
		right:-6vw;
	}
	.paper > .wrappadding{
		min-height: 75vh;
	}
	.nanas8 {
	    height: 80px;
	    top: 18vh;
	    left: 20vw;
	}
	.detail-blog-item{
		max-height: 80vh;
		padding-bottom: 5rem;
	}
	.blogging{
		background-color: rgba(255,255,255,.9);
		margin:0 10px 4rem 10px;
        padding-bottom: 4rem;
	}
	.item-blog{
		padding:2rem 2.5rem;
	}
	.title-blog {
	    font-size: 1.5rem;
	}
	.item-blog:hover{
		padding-left: 2rem;
	}
	.detail-blog-item > .wrappadding{
		padding: 2rem 2.5rem
		margin:0 1rem 0 1.5rem;
	}
	.titlepage{
		margin-bottom: 1rem;
	}
}
@media (max-width: 740px)
{
	.detail-blog-item{
		max-height: 88vh;
		padding-bottom: 5rem;
	}
	.coklat1,.coklat2{
		right:0vw;
		max-height: 100px;
	}
	.coklat3 {
	    max-width: 120px;
	}
	.nanas1{
		max-height: 100px;
	}
	.nanas2{
		max-height: 100px;
	}
	.nanas2{
		max-height: 120px;
		bottom: 10vh;
	}
	.contentterms{
		height: 70vh;
	}
	.blogging{
		background-color: rgba(255,255,255,.9);
		margin:0 10px 4rem 10px;
	}
	.item-blog{
		padding:1rem 1.5rem;
	}
	.title-blog {
	    font-size: 1.5rem;
	}
	.item-blog:hover{
		padding-left: 2rem;
	}
	.detail-blog-item > .wrappadding{
		padding: 2rem 1.5rem;
		margin:0 1rem 0 1.5rem;
	}
	.titlepage{
		margin-bottom: 1rem;
	}
}
@media (max-width: 500px)
{
	.detail-blog-item{
		max-height: 85vh;
		padding-bottom: 5rem;
	}
	.contentterms{
		height: 80vh;
	}
	.titlepage{
		font-size:1.8rem;
	}
	.coklat2{
		bottom:10vh;
	}
	.coklat1, .coklat2{
		right: -7vw;
	}
	.coklat3{
		bottom: 30vh;
	}
}
table, th, td {
    border: 1px solid black;
    text-align: center;
}

</style>
<?php
		if($this->input->get('content') != 'inload'){
	?>
	<section id="contentsection" class="contentsection">
		<div id="load" class="contenthomeslide contenview">
	<?php }?>
			<div id="wrappingshow">
				<div class="nanas">
					<div class="overlay-sidebar animated fadeInLeft"></div>
					<img class="img-fluid nanas1 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas1.png');?>">
					<img class="img-fluid nanas2 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas2.png');?>">
					<img class="img-fluid nanas8 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas6.png');?>">
					<img class="img-fluid coklat1 naanimation animated opacity0" src="<?php echo base_url('asset/static/coklat/coklat4.png');?>">
					<img class="img-fluid nanas5 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas5.png');?>">
					<img class="img-fluid coklat3 naanimation animated opacity0" src="<?php echo base_url('asset/static/coklat/coklat3.png');?>">
					<div class="contentterms blogwrap animated opacity0">
						<div class="contentterm height100percent">
							<div class="container height100percent">
								<div class="row height100percent">
									<div ss-container class="col-md-3 ss-container sidebare height100percent ">
										<div class="wrapmenusidebar">
											<h2>BLOG</h2>
											<ul class="menusidebarblog">
												<li><a href="<?php echo base_url('blog?date=05-2019');?>" data-url="group-title-1" data-title="Group Title 1" class="sidemenu active"><span class="en">May 2019</span><span class="id">Mei 2019</span></a></li>
                                                <li><a href="<?php echo base_url('blog?date=12-2018');?>" data-url="group-title-1" data-title="Group Title 1" class="sidemenu active"><span class="en">December 2018</span><span class="id">Desember 2018</span></a></li>
												<li><a href="<?php echo base_url('blog?date=11-2018');?>" data-url="group-title-1" data-title="Group Title 1" class="sidemenu active"><span class="en">November 2018</span><span class="id">Nopember 2018</span></a></li>
												<li><a href="<?php echo base_url('blog?date=06-2018');?>" data-url="group-title-1" data-title="Group Title 1" class="sidemenu active"><span class="en">June 2018</span><span class="id">Juni 2018</span></a></li>
												<li><a href="<?php echo base_url('blog?date=05-2018');?>" data-url="group-title-1" data-title="Group Title 1" class="sidemenu active"><span class="en">May 2018</span><span class="id">Mei 2018</span></a></li>
											</ul>
										</div>
									</div>
									<div id="main-blog" class="col-lg-9 col-md-12 height100percent ">
										<div ss-container class="papaerblog ss-container">
											<div class="blogging">

                                                <div class="item-blog" data-date="05-2019">
                                                    <h2 class="title-blog">                                                    
                                                        <span class="id">Joyday Mung Bean</span>
                                                        <span class="en">Joyday Mung Bean</span>
                                                    </h2>
                                                    <div class="meta-title-blog en">24 May 2019</div>
                                                    <div class="meta-title-blog id">24 Mei 2019</div>
                                                    <div class="content-item-blog">
                                                        <div class="id">
															Joyday Mung Bean varian baru es krim Joyday di Indonesia yang menawarkan paduan rasa kacang hijau dan susu. Es krim susu kacang hijau ini mengandung sari kacang hijau asli untuk memberi kesegaran terutama ketika cuaca sedang terik.
															<br><br>
															Tidak hanya lezat, kacang hijau juga sangat bermanfaat bagi tubuh. Antara lain baik untuk menjaga sistem kekebalan tubuh, mengatasi anemia dan sembelit, termasuk membantu menjaga berat badan. Asik kan? Kamu bisa tetap menikmati es krim tanpa khawatir timbangan naik.
														</div>
                                                        <div class="en">
															Joyday Mung Bean varian baru es krim Joyday di Indonesia yang menawarkan paduan rasa kacang hijau dan susu. Es krim susu kacang hijau ini mengandung sari kacang hijau asli untuk memberi kesegaran terutama ketika cuaca sedang terik.
															<br><br>
															Tidak hanya lezat, kacang hijau juga sangat bermanfaat bagi tubuh. Antara lain baik untuk menjaga sistem kekebalan tubuh, mengatasi anemia dan sembelit, termasuk membantu menjaga berat badan. Asik kan? Kamu bisa tetap menikmati es krim tanpa khawatir timbangan naik.
                                                        </div>
                                                    </div>
                                                    <a class="readmore" data-target="#readmore_11"><div class="propin" data-id="Selengkapnya" data-en="Read More"><?php
                                                        if(get_cookie('currentlang') == 'id'){
                                                            echo 'Selengkapnya';
                                                        }else{
                                                            echo 'Read more';
                                                        }
                                                    ?></div> <span><i class="ti-arrow-right"></i></span></a>				
                                                </div>
                                                <div class="item-blog" data-date="05-2019">
                                                    <h2 class="title-blog">                                                    
                                                        <span class="id">Joyday Mocha Crispy</span>
                                                        <span class="en">Joyday Mocha Crispy</span>
                                                    </h2>
                                                    <div class="meta-title-blog en">24 May 2019</div>
                                                    <div class="meta-title-blog id">24 Mei 2019</div>
                                                    <div class="content-item-blog">
                                                        <div class="id">
															Ada varian es krim baru dari Joyday Indonesia: Joyday Mocha Crispy. Es krim ini merupakan paduan rasa susu, kopi, dan cokelat. Paduan ketiga rasa ini biasanya dikenal sebagai rasa moccachino. Es krim Mocha Crispy, juga berlapis cokelat renyah untuk menambah cita rasa manis yang lezat.
															<br><br>
															Es krim ini tidak hanya memanjakan lidah. Wangi kopi dan cokelatnya begitu memanjakan; paduan rasa susu, kopi, cokelat yang lumer di mulut, sampai tekstur lapisan cokelat yang renyah, memberi kenikmatan tiada tara ketika dikunyah.
                                                        </div>
                                                        <div class="en">
															Ada varian es krim baru dari Joyday Indonesia: Joyday Mocha Crispy. Es krim ini merupakan paduan rasa susu, kopi, dan cokelat. Paduan ketiga rasa ini biasanya dikenal sebagai rasa moccachino. Es krim Mocha Crispy, juga berlapis cokelat renyah untuk menambah cita rasa manis yang lezat.
															<br><br>
															Es krim ini tidak hanya memanjakan lidah. Wangi kopi dan cokelatnya begitu memanjakan; paduan rasa susu, kopi, cokelat yang lumer di mulut, sampai tekstur lapisan cokelat yang renyah, memberi kenikmatan tiada tara ketika dikunyah.
                                                        </div>
                                                    </div>
                                                    <a class="readmore" data-target="#readmore_12"><div class="propin" data-id="Selengkapnya" data-en="Read More"><?php
                                                        if(get_cookie('currentlang') == 'id'){
                                                            echo 'Selengkapnya';
                                                        }else{
                                                            echo 'Read more';
                                                        }
                                                    ?></div> <span><i class="ti-arrow-right"></i></span></a>				
                                                </div>
												<div class="item-blog" data-date="05-2019">
                                                    <h2 class="title-blog">                                                    
                                                        <span class="id">Joyday Milky Milk</span>
                                                        <span class="en">Joyday Milky Milk</span>
                                                    </h2>
                                                    <div class="meta-title-blog en">24 May 2019</div>
                                                    <div class="meta-title-blog id">24 Mei 2019</div>
                                                    <div class="content-item-blog">
                                                        <div class="id">
															Milky milk varian baru rasa susu untuk es krim Joyday di Indonesia. Inilah es krim susu klasik dari Joyday dengan susu asli yang berkualitas dan kaya rasa
															<br><br>
															Sejak dulu susu menjadi asupan nutrisi yang menyempurnakan komposisi makanan 4 sehat dan 5 sempurna. Karenanya sangat dianjurkan untuk mengkonsumsi susu setiap hari. Selain mengandung banyak protein, susu juga mengandung mineral yang berguna bagi tubuh.
                                                        </div>
                                                        <div class="en">														
															Milky milk varian baru rasa susu untuk es krim Joyday di Indonesia. Inilah es krim susu klasik dari Joyday dengan susu asli yang berkualitas dan kaya rasa
															<br><br>
															Sejak dulu susu menjadi asupan nutrisi yang menyempurnakan komposisi makanan 4 sehat dan 5 sempurna. Karenanya sangat dianjurkan untuk mengkonsumsi susu setiap hari. Selain mengandung banyak protein, susu juga mengandung mineral yang berguna bagi tubuh.
                                                        </div>
                                                    </div>
                                                    <a class="readmore" data-target="#readmore_13"><div class="propin" data-id="Selengkapnya" data-en="Read More"><?php
                                                        if(get_cookie('currentlang') == 'id'){
                                                            echo 'Selengkapnya';
                                                        }else{
                                                            echo 'Read more';
                                                        }
                                                    ?></div> <span><i class="ti-arrow-right"></i></span></a>				
                                                </div>

                                                <div class="item-blog" data-date="12-2018">
                                                    <h2 class="title-blog">                                                    
                                                        <span class="id">Joyday Media Launch</span>
                                                        <span class="en">Joyday Media Launch</span>
                                                    </h2>
                                                    <div class="meta-title-blog en">7 December 2018</div>
                                                    <div class="meta-title-blog id">7 December 2018</div>
                                                    <div class="content-item-blog">
                                                        <div class="id">
                                                            Perusahaan susu asal Cina Yili Group baru saja meluncurkan produk es krim Joyday di Hotel Four Season, Jakarta 23 Oktober 2018. Peluncuran ini dihadiri langsung oleh CEO Yili Group, Zhang Jianqiu, Walikota Hohhot city, Inner Mongolia, Feng Yuzhen, Vice President Director PT Green Asia Food Indonesia, Satria Bakti, Psikolog Ayoe Sutomo dan dimeriahkan oleh HIVI yang menyanyikan jingle lagu Joyday.
                                                        </div>
                                                        <div class="en">
                                                            Perusahaan susu asal Cina Yili Group baru saja meluncurkan produk es krim Joyday di Hotel Four Season, Jakarta 23 Oktober 2018. Peluncuran ini dihadiri langsung oleh CEO Yili Group, Zhang Jianqiu, Walikota Hohhot city, Inner Mongolia, Feng Yuzhen, Vice President Director PT Green Asia Food Indonesia, Satria Bakti, Psikolog Ayoe Sutomo dan dimeriahkan oleh HIVI yang menyanyikan jingle lagu Joyday.
                                                        </div>
                                                    </div>
                                                    <a class="readmore" data-target="#readmore_9"><div class="propin" data-id="Selengkapnya" data-en="Read More"><?php
                                                        if(get_cookie('currentlang') == 'id'){
                                                            echo 'Selengkapnya';
                                                        }else{
                                                            echo 'Read more';
                                                        }
                                                    ?></div> <span><i class="ti-arrow-right"></i></span></a>				
                                                </div>
                                                <div class="item-blog" data-date="12-2018">
                                                    <h2 class="title-blog">                                                    
                                                        <span class="id">Joyday School to School Roadshow</span>
                                                        <span class="en">Joyday School to School Roadshow</span>
                                                    </h2>
                                                    <div class="meta-title-blog en">7 December 2018</div>
                                                    <div class="meta-title-blog id">7 December 2018</div>
                                                    <div class="content-item-blog">
                                                        <div class="id">
                                                            Hey semua! Joyday telah melakukan serangkaian acara dari sekolah ke sekolah nih sekitar Jakarta, Tangerang dan Jawa Barat untuk memperkenalkan Es krim Joyday.
                                                        </div>
                                                        <div class="en">
                                                            Hey semua! Joyday telah melakukan serangkaian acara dari sekolah ke sekolah nih sekitar Jakarta, Tangerang dan Jawa Barat untuk memperkenalkan Es krim Joyday.
                                                        </div>
                                                    </div>
                                                    <a class="readmore" data-target="#readmore_10"><div class="propin" data-id="Selengkapnya" data-en="Read More"><?php
                                                        if(get_cookie('currentlang') == 'id'){
                                                            echo 'Selengkapnya';
                                                        }else{
                                                            echo 'Read more';
                                                        }
                                                    ?></div> <span><i class="ti-arrow-right"></i></span></a>				
                                                </div>

                                                <div class="item-blog" data-date="11-2018">
                                                    <h2 class="title-blog">                                                    
                                                        <span class="id">Yili Group Secara Resmi Meluncurkan Produk “Joyday Ice Cream” dengan 11 Varian Rasa</span>
                                                        <span class="en">Yili Group Secara Resmi Meluncurkan Produk “Joyday Ice Cream” dengan 11 Varian Rasa</span>
                                                    </h2>
                                                    <div class="meta-title-blog en">21 November 2018</div>
                                                    <div class="meta-title-blog id">21 November 2018</div>
                                                    <div class="content-item-blog">
														<div class="id">
                                                            Jakarta, Jawapers –  PT Green Asia Food Indonesia adalah anak perusahaan Yili Group, produsen susu terbesar di Cina yang memegang peringkat nomor 1 di Asia berdasarkan 2018 Rabobank Global Dairy Top 20 Report. Joyday Ice Cream diproduksi dengan standar pengendalian mutu terbaik yang diterapkan oleh Yili Group, produsen susu ternama dunia.
														</div>
														<div class="en">
                                                            Jakarta, Jawapers –  PT Green Asia Food Indonesia adalah anak perusahaan Yili Group, produsen susu terbesar di Cina yang memegang peringkat nomor 1 di Asia berdasarkan 2018 Rabobank Global Dairy Top 20 Report. Joyday Ice Cream diproduksi dengan standar pengendalian mutu terbaik yang diterapkan oleh Yili Group, produsen susu ternama dunia.
														</div>
													</div>
													<a class="readmore" data-target="#readmore_4"><div class="propin" data-id="Selengkapnya" data-en="Read More"><?php
														if(get_cookie('currentlang') == 'id'){
															echo 'Selengkapnya';
														}else{
															echo 'Read more';
														}
													?></div> <span><i class="ti-arrow-right"></i></span></a>				
                                                </div>                                                
                                                <div class="item-blog" data-date="11-2018">
                                                    <h2 class="title-blog">
                                                        <span class="id">Fakta: Es Krim Dapat Memperbaiki Mood</span>
                                                        <span class="en">Fakta: Es Krim Dapat Memperbaiki Mood</span>
                                                    </h2>
                                                    <div class="meta-title-blog en">20 November 2018</div>
                                                    <div class="meta-title-blog id">20 November 2018</div>
                                                    <div class="content-item-blog">
														<div class="id">
                                                        TRIBUNJAKARTA.COM - Rasanya tidak mungkin bila seseorang tidak menyukai es krim yang manis dan dingin. Es Krim ternyata dapat menghasilkan serangkaian respons fisik dan emosional pada sebagian besar orang. Pasti kamu pernah merasakan perasaan bahagia saat menghabiskan satu es krim.<br><br>
                                                        Beberapa peneliti mengatakan, <a href="http://jakarta.tribunnews.com/tag/mood" target="_blank">mood</a> dapat dengan sendirinya meningkat dengan atau tanpa makanan, misalnya beberapa saat setelah kita merasakan amarah. Es krim dapat dikatakan sebagai pereda emosi, terutama bagi anak usia remaja, atau kamu yang seringkali moody.
														</div>
														<div class="en">
                                                        TRIBUNJAKARTA.COM - Rasanya tidak mungkin bila seseorang tidak menyukai es krim yang manis dan dingin. Es Krim ternyata dapat menghasilkan serangkaian respons fisik dan emosional pada sebagian besar orang. Pasti kamu pernah merasakan perasaan bahagia saat menghabiskan satu es krim.<br><br>
                                                        Beberapa peneliti mengatakan, <a href="http://jakarta.tribunnews.com/tag/mood" target="_blank">mood</a> dapat dengan sendirinya meningkat dengan atau tanpa makanan, misalnya beberapa saat setelah kita merasakan amarah. Es krim dapat dikatakan sebagai pereda emosi, terutama bagi anak usia remaja, atau kamu yang seringkali moody.
														</div>
													</div>
													<a class="readmore" data-target="#readmore_5"><div class="propin" data-id="Selengkapnya" data-en="Read More"><?php
														if(get_cookie('currentlang') == 'id'){
															echo 'Selengkapnya';
														}else{
															echo 'Read more';
														}
													?></div> <span><i class="ti-arrow-right"></i></span></a>
                                                </div>      
                                                <div class="item-blog" data-date="11-2018">
                                                    <h2 class="title-blog">
                                                        <span class="id">Ubah Mood Negatif Menjadi Positif Melalui Es Krim</span>
                                                        <span class="en">Ubah Mood Negatif Menjadi Positif Melalui Es Krim</span>
                                                    </h2>
                                                    <div class="meta-title-blog en">21 November 2018</div>
                                                    <div class="meta-title-blog id">21 November 2018</div>
                                                    <div class="content-item-blog">
														<div class="id">
                                                            JAKARTA - Es krim secara ilmiah bisa membuat seseorang merasa senang dan mengubah mood yang negatif menjadi positif, serta membuat mood yang sudah positif menjadi lebih senang dan bahagia.<br><br>
                                                            Sensasi es krim yang dingin dan menyegarkan, rasa yang lezat dan tampilan penuh warna dapat membuat senang dan bahkan menjadi lebih bahagia saat suasana hati sudah baik.
														</div>
														<div class="en">
                                                            JAKARTA - Es krim secara ilmiah bisa membuat seseorang merasa senang dan mengubah mood yang negatif menjadi positif, serta membuat mood yang sudah positif menjadi lebih senang dan bahagia.<br><br>
                                                            Sensasi es krim yang dingin dan menyegarkan, rasa yang lezat dan tampilan penuh warna dapat membuat senang dan bahkan menjadi lebih bahagia saat suasana hati sudah baik.
														</div>
													</div>
													<a class="readmore" data-target="#readmore_6"><div class="propin" data-id="Selengkapnya" data-en="Read More"><?php
														if(get_cookie('currentlang') == 'id'){
															echo 'Selengkapnya';
														}else{
															echo 'Read more';
														}
													?></div> <span><i class="ti-arrow-right"></i></span></a>
                                                </div>
                                                <div class="item-blog" data-date="11-2018">
                                                    <h2 class="title-blog">
                                                        <span class="id">HIVI! Usir Bad Mood dengan Es Krim</span>
                                                        <span class="en">HIVI! Usir Bad Mood dengan Es Krim</span>
                                                    </h2>
                                                    <div class="meta-title-blog en">21 November 2018</div>
                                                    <div class="meta-title-blog id">21 November 2018</div>
                                                    <div class="content-item-blog">
														<div class="id">
                                                            Femme.id, Jakarta – Setiap orang punya cara mengubah suasana hati yang buruk.<br><br>
                                                            Bagi grup musik HIVI! yang beranggotakan Nadhia Aleida (vokal), Ilham Aditama (vokal), Febrian Nindyo (vokal/gitar) dan Ezra Mandira (vokal/gitar), es krim berperan besar dalam membuat suasana kerja makin menyenangkan.
														</div>
														<div class="en">
                                                            Femme.id, Jakarta – Setiap orang punya cara mengubah suasana hati yang buruk.<br><br>
                                                            Bagi grup musik HIVI! yang beranggotakan Nadhia Aleida (vokal), Ilham Aditama (vokal), Febrian Nindyo (vokal/gitar) dan Ezra Mandira (vokal/gitar), es krim berperan besar dalam membuat suasana kerja makin menyenangkan.
														</div>
													</div>
													<a class="readmore" data-target="#readmore_7"><div class="propin" data-id="Selengkapnya" data-en="Read More"><?php
														if(get_cookie('currentlang') == 'id'){
															echo 'Selengkapnya';
														}else{
															echo 'Read more';
														}
													?></div> <span><i class="ti-arrow-right"></i></span></a>
                                                </div>
                                                <div class="item-blog" data-date="11-2018">
                                                    <h2 class="title-blog">
                                                        <span class="id">Cara Atasi Anak Pilih-pilih Makanan</span>
                                                        <span class="en">Cara Atasi Anak Pilih-pilih Makanan</span>
                                                    </h2>
                                                    <div class="meta-title-blog en">20 November 2018</div>
                                                    <div class="meta-title-blog id">20 November 2018</div>
                                                    <div class="content-item-blog">
														<div class="id">
                                                        Jakarta (HN) - Buah hati yang terlalu pemilih dalam mengisi perut bisa membuat orangtua pusing tujuh keliling karena anak cuma mau mengonsumsi makanan tertentu. Psikolog Ayoe Sutomo mengatakan, ada banyak faktor yang menyebabkan anak jadi pilih-pilih soal makanan, termasuk dari sisi psikologi hingga pola makan ibu saat mengandung.
														</div>
														<div class="en">
                                                        Jakarta (HN) - Buah hati yang terlalu pemilih dalam mengisi perut bisa membuat orangtua pusing tujuh keliling karena anak cuma mau mengonsumsi makanan tertentu. Psikolog Ayoe Sutomo mengatakan, ada banyak faktor yang menyebabkan anak jadi pilih-pilih soal makanan, termasuk dari sisi psikologi hingga pola makan ibu saat mengandung.
														</div>
													</div>
													<a class="readmore" data-target="#readmore_8"><div class="propin" data-id="Selengkapnya" data-en="Read More"><?php
														if(get_cookie('currentlang') == 'id'){
															echo 'Selengkapnya';
														}else{
															echo 'Read more';
														}
													?></div> <span><i class="ti-arrow-right"></i></span></a>
                                                </div>

												<div class="item-blog" data-date="06-2018">
													<h2 class="title-blog"><span class="id">5 Makanan Manis untuk Kembalikan Energi saat Berbuka Puasa</span><span class="en">5 Sweet Foods For Restore Energy During Fasting</span></h2>
													<div class="meta-title-blog en">13 June 2018</div>
													<div class="meta-title-blog id">13 Juni 2018</div>
													<div class="content-item-blog">
														<div class="id">
															Di bulan suci Ramadan, umat Muslim menjalankan ibadah puasa selama satu bulan. Kita seringkali disarankan untuk berbuka dengan yang manis seperti buah-buahan atau aneka kudapan ringan. Hidangan manis yang dikonsumsi saat berbuka puasa menjadi penting karena dapat membantu menggantikan kekurangan kadar gula dalam darah selama menjalankan ibadah puasa.
														</div>
														<div class="en">
															On Holy Month, Moslems run fasting for one month. We often advised break fasting with sweet foods like fruits or various dishes. Sweet dishes that consumed when break fasting being important because can help to replace deficiency sugar in blood during fasting.
														</div>
													</div>
													<a class="readmore" data-target="#readmore_3"><div class="propin" data-id="Selengkapnya" data-en="Read More">
														<?php
															if(get_cookie('currentlang') == 'id'){
																echo 'Selengkapnya';
															}else{
																echo 'Read more';
															}
														?>
													</div><span><i class="ti-arrow-right"></i></span></a>
												</div>

												<div class="item-blog" data-date="05-2018">
													<h2 class="title-blog"><span class="id">Sertifikasi & Sistem Jaminan Halal Joyday</span><span class="en">Halal Warranty Certification & Systems of Joyday</span></h2>
													<div class="meta-title-blog en">8 May 2018</div>
													<div class="meta-title-blog id">8 Mei 2018</div>
													<div class="content-item-blog">
														<div class="id">
															Selain banyak rasa dan nikmat, eskrim Joyday telah memperoleh Sertifikat Halal dan Implementasi Sistem Jaminan Halal dengan kategori nilai A (Sangat Baik) yang dikeluarkan secara resmi oleh Lembaga Pengkajian Pangan, Obat-obatan dan Kosmetika – Majelis Ulama Indonesia (LPPOM MUI).
														</div>
														<div class="en">
                                                        Aside its variants and delicious taste. Joyday ice cream has achieved Halal Certificate and Warranty System implementation with value a (very good) which is expended by authorised institutional agency of food, drugs and cosmetics - Indonesian Moslem Scholar Assembly (LPPOM MUI).
														</div>
													</div>
													<a class="readmore" data-target="#readmore_1"><div class="propin" data-id="Selengkapnya" data-en="Read More">
														<?php
															if(get_cookie('currentlang') == 'id'){
																echo 'Selengkapnya';
															}else{
																echo 'Read more';
															}
														?>
													</div><span><i class="ti-arrow-right"></i></span></a>
												</div>
												<div class="item-blog" data-date="05-2018">
													<h2 class="title-blog"><span class="id">Lulus Uji BPOM</span><span class="en">Passed test from BPOM (Drug and Food Control Agency)</span></h2>
													<div class="meta-title-blog en">7 May 2018</div>
													<div class="meta-title-blog id">7 Mei 2018</div>
													<div class="content-item-blog">
														<div class="id">
															Kini kamu tidak perlu khawatir, karena es krim Joyday sudah terbukti lulus uji dari BPOM (Badan Pengawas Obat dan Makanan). Jadi selain nikmat, Joyday sudah terbukti aman loh untuk dikonsumsi. Yuk, tunggu apalagi? Cobain eskrim Joyday sekarang dan ceriakan harimu karena Happy Day, Joyday!
														</div>
														<div class="en">
                                                        Now you do not need to worry, because ice cream Joyday have proven passed test from BPOM (Drug and Food Control Agency). So enjoy this, Joyday is proven safe for consumption. Come on, what are you waiting for? let's try Joyday ice cream now and add happiness in your day because happy day, Joyday!
														</div>
													</div>
													<a class="readmore" data-target="#readmore_2"><div class="propin" data-id="Selengkapnya" data-en="Read More"><?php
														if(get_cookie('currentlang') == 'id'){
															echo 'Selengkapnya';
														}else{
															echo 'Read more';
														}
													?></div> <span><i class="ti-arrow-right"></i></span></a>
												</div>

											</div>
										</div>

										<!-- BLOG DETAIL 1 -->
										<div id="readmore_1" ss-container class="detail-blog-item animated">
											<div class="wrappadding animated fadeInUp">
												<div class="closecontent close-smalle"></div>
												<h1 class="titlepage"><span class="id">Sertifikasi & Sistem Jaminan Halal Joyday</span><span class="en">Halal Warranty Certification & Systems of Joyday</span></h1>
												<div class="is-clearfix">
													<div class="is-pulled-left content-description">
														<div class="id">
														<p>Selain banyak rasa dan nikmat, es krim Joyday telah memperoleh Sertifikat Halal dan Implementasi Sistem Jaminan Halal dengan kategori nilai A (Sangat Baik) yang dikeluarkan secara resmi oleh Lembaga Pengkajian Pangan, Obat-obatan dan Kosmetika – Majelis Ulama Indonesia (LPPOM MUI). Yuk, cobain es krim Joyday sekarang dan rasakan nikmatnya. Kapan pun dimana pun, Joyday siap menambah keceriaan harimu karena Happy Day, Joyday!</p>
														<a href="https://www.joyday.com/asset/pdf/Halal-Certificate-Meizhou.pdf" target="_blank"><p>Download sertifikat halal disini.</p></a><br />
														</div>
														<div class="en">
														<p>Aside its variants and delicious taste. Joyday ice cream has achieved halal certificate and warranty system implementation with value a (very good) which is expended by authorised institutional agency of food, drugs and cosmetics - Indonesian moslem scholar assembly (LPPOM MUI). Come on, let’s try Joyday ice cream now and taste it’s delicious taste. anywhere anytime, Joyday is ready to add happiness in your day because happy day, Joyday!</p>
														<a href="https://www.joyday.com/asset/pdf/Halal-Certificate-Meizhou.pdf" target="_blank"><p>Download Halal Certificate Here.</p></a><br />
														</div>
														<center>
														<img src="https://www.joyday.com/asset/static/sertifikat-halal-1.png" alt="sertifikat halal 1"><br /><br/>
														<img src="https://www.joyday.com/asset/static/sertifikat-halal-2.png" alt="sertifikat halal 2"><br /><br/><br /><br /><br />
														</center>
													</div>
													<div class="is-pulled-right content-socmed">
														<ul class="sosmed-product-detaile">
															<li><a href="//www.facebook.com/JoydayIceCream/" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
															<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=<?php echo substr('Selain banyak rasa dan nikmat, es krim Joyday telah memperoleh Sertifikat Halal dan Implementasi Sistem Jaminan Halal dengan kategori nilai A (Sangat Baik) yang dikeluarkan secara resmi oleh Lembaga Pengkajian Pangan',0,20)?>" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
															<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
															<li><a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?php echo current_url();?>."><i class="ti-email"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>

										<!-- BLOG DETAIL 2 -->
										<div id="readmore_2" ss-container class="detail-blog-item animated">
											<div class="wrappadding animated fadeInUp">
												<div class="closecontent close-smalle"></div>
												<h1 class="titlepage"><span class="id">Lulus Uji BPOM</span><span class="en">PASSED BPOM TEST</span></h1>
												<div class="is-clearfix">
													<div class="is-pulled-left content-description">
														<table class="table">
  															<tr style="text-align:center">
    															<th>NO</th>
    															<th><span class="id">NAMA PRODUK</span><span class="en">PRODUCT</span></th>
    															<th>NO BPOM</th>
  															</tr>
  															<tr>
    															<td>1.</td>
    															<td>Joyday - Crunchy Chocolate Malt</td>
    															<td>204209024726</td>
  															</tr>
    														<tr>
    															<td>2.</td>
    															<td>Joyday - Crunchy Chocolate Blueberry </td>
    															<td>204209029726</td>
  															</tr>
    														<tr>
    															<td>3.</td>
    															<td>Joyday - Tiramisu Chocolate Mint</td>
    															<td>204109014726</td>
  															</tr>
    														<tr>
    															<td>4.</td>
    															<td>Joyday - Classic Belgian Chocolate</td>
    															<td>204209019726</td>
  															</tr>
    														<tr>
    															<td>5.</td>
    															<td>Joyday - Crunchy Chocolate Vanila</td>
    															<td>304209016726</td>
  															</tr>
    														<tr>
    															<td>6.</td>
    															<td>Joyday - Crunchy Honeydew Melon</td>
    															<td>304209017726</td>
  															</tr>
    														<tr>
    															<td>7.</td>
    															<td>Joyday - Vanilla Milkshake</td>
    															<td>304209013726</td>
  															</tr>
    														<tr>
   	 															<td>8.</td>
    															<td>Joyday - Sweet Corn </td>
    															<td>204209012726</td>
  															</tr>
    														<tr>
    															<td>9.</td>
    															<td>Joyday - Jujube White Chocolate</td>
    															<td>204209018726</td>
  															</tr>
    														<tr>
    															<td>10.</td>
    															<td>Joyday - Cool Blueberry</td>
    															<td>214209015726</td>
  															</tr>
  															<tr>
    															<td>11.</td>
    															<td>Joyday - Cool Pineapple</td>
    															<td>214209011726</td>
    														</tr>
														</table>
														<div class="id">
															<p>Kini kamu tidak perlu khawatir, karena es krim Joyday sudah terbukti lulus uji dari BPOM (Badan Pengawas Obat dan Makanan). Jadi selain nikmat, Joyday sudah terbukti aman loh untuk dikonsumsi. Yuk, tunggu apalagi? Cobain es krim Joyday sekarang dan ceriakan harimu karena Happy Day, Joyday!</p>
															<p>&nbsp;<br/></p>
															<p>&nbsp;<br/></p>
														</div>
														<div class="en">
															<p>Now you do not need to worry, because ice cream Joyday have proven passed test from BPOM (Drug and Food Control Agency). So enjoy this, Joyday is proven safe for consumption. Come on, what are you waiting for? let's try Joyday ice cream now and add happiness in your day because happy day, Joyday!</p>
															<p>&nbsp;<br/></p>
															<p>&nbsp;<br/></p>
														</div>
													</div>
													<div class="is-pulled-right content-socmed">
														<ul class="sosmed-product-detaile">
															<li><a href="//www.facebook.com/JoydayIceCream/" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
															<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=<?php echo substr('Kini kamu tidak perlu khawatir, karena es krim Joyday sudah terbukti lulus uji dari BPOM',0,20)?>" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
															<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
															<li><a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?php echo current_url();?>."><i class="ti-email"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>

										<!-- BLOG DETAIL 3 -->
										<div id="readmore_3" ss-container class="detail-blog-item animated">
											<div class="wrappadding animated fadeInUp">
												<div class="closecontent close-smalle"></div>
												<h1 class="titlepage"><span class="id">5 Makanan Manis untuk Kembalikan Energi saat Berbuka Puasa</span><span class="en">5 Sweet Foods For Restore Energy During Fasting</span></h1>
												<div class="is-clearfix">
													<div class="is-pulled-left content-description">
														<div class="id">
															<p><strong style="color:##0060af;">TRIBUNNERS</strong> - Di bulan suci Ramadan, umat Muslim menjalankan ibadah puasa selama satu bulan.</p>
															<p>Kita seringkali disarankan untuk berbuka dengan yang manis seperti buah-buahan atau aneka kudapan ringan.</p>
															<p>Hidangan manis yang dikonsumsi saat berbuka puasa menjadi penting karena dapat membantu menggantikan kekurangan kadar gula dalam darah selama menjalankan ibadah puasa.</p>
															<p>Dr. Kaseem Halmar dari University of Warwick menjelaskan bahwa tubuh membutuhkan sumber energi yang dihasilkan dari asupan karbohidrat yang dikonversi menjadi gula oleh insulin.</p>
															<p>Tetapi juga perlu diketahui adanya batasan dalam mengonsumsi makanan dan minuman manis, yaitu 5 % dari total jumlah asupan kalori.*</p>
															<p>Berikut adalah beberapa beberapa hidangan manis yang lezat untuk dikonsumsi pada saat berbuka:</p>
															<ol>
																<li>Kolak<br/>
																	Hidangan manis yang satu ini paling sering dihidangkan dengan pisang di dalamnya. Tetapi tidak jarang ada yang menambahkan variasi buah kedalamnya seperti ubi, kelapa muda bahkan biji salak.
																</li>
																<li>Cendol<br/>
																	Cendol merupakan minuman khas Sunda yang terbuat dari tepung beras, disajikan dengan es parut serta gula merah cair dan santan. Disukai karena rasanya yang manis dan gurih, cendol seringkali menjadi pilihan untuk berbuka puasa.
																</li>
																<li>Bubur Candil<br/>
																	Bubur candil terbuat dari tepung ketan yang direbus dengan gula merah dan disajikan dengan kuah santan.
																</li>
																<li>Bubur Kacang Hijau<br/>
																	Bubur Kacang Hijau seringkali dijadikan hidangan pembuka puasa karena kadar asam amino yang tinggi, dapat menjadi alternatif sumber protein. Selain itu, rasanya yang manis dan khas juga menjadi nilai tambah.
																</li>
																<li>Es Krim<br/>
																	<p>Es krim dapat menjadi pilihan hidangan untuk berbuka puasa. Selain rasanya yang manis, efek dingin yang menyegarkan akan menggugah selera kita.</p>
																	<p>Dr. Matthew Lantz Blaylock, PhD, seorang nutritional scientist, menjelaskan bahwa tidak ada dampak negatif mengonsumsi es krim untuk berbuka puasa.</p>
																	<p>Dari kandungan gizi, es krim mengandung gula, krim lemak dan protein susu, yang seperti makanan lainnya dapat membantu memenuhi kebutuhan gizi selama berpuasa.</p>
																	<p>Salah satu es krim yang dapat menjadi pilihan adalah Joyday Jujube White Chocolate. Es krim yang lezat dari Joyday ini unik karena mengandung buah jujube (kurma merah), dalam balutan coklat putih yang lezat sehingga sangat cocok sebagai hidangan pembuka puasa.</p>
																</li>
															</ol>
															<p>Artikel ini telah tayang di Tribunnews.com dengan judul 5 Makanan Manis untuk Kembalikan Energi saat Berbuka Puasa, <a href="http://www.tribunnews.com/tribunners/2018/06/13/5-makanan-manis-untuk-kembalikan-energi-saat-berbuka-puasa?page=2." target="_blank">http://www.tribunnews.com/tribunners/2018/06/13/5-makanan-manis-untuk-kembalikan-energi-saat-berbuka-puasa?page=2.</a></p>
															<p>Oleh:  Malvyandie Haryadi (editor)</p>
															<p>&nbsp;<br/></p>
															<p>&nbsp;<br/></p>
														</div>
														<div class="en">
															<p><strong style="color:##0060af;">TRIBUNNERS</strong> - On Holy Month, Moslems run fasting for one month.</p>
															<p>We often advised break fasting with sweet foods like fruits or various dishes.</p>
															<p>Sweet dishes that consumed when break fasting being important because can help to replace deficiency sugar in blood during fasting.</p>
															<p>Dr.Kaseem Halmar from University of Warwick explained that body needs source of energy that resulting carbohydrate intake that convertion being sugar by insulin.</p>
															<p>But also need to know the limitations in consume sweet foods and drinks, that is 5% from total calory intake.</p>
															<p>Here are some delicious sweet dishes for consume when break fasting :</p>
															<ol>
																<li>Compote<br/>
																	One of this sweet dishes most often served with banana inside. But it is not uncommon to add variaety of fruits in to it like sweet potatoes, coconut also zalacca seeds.
																</li>
																<li>Cendol<br/>
																	Cendol is kind of typical Sunda drink that made from rice flour, served with ice also liquid brown sugar and coconut milk. Peoples like the sweetnest and tasteful taste, cendol being most choice for break fasting.
																</li>
																<li>Porrige Candil<br/>
																	Porrige candil made from gluitinous rice flour that boiled with brown sugar and served with coconut milk.
																</li>
																<li>Porrige green bean<br/>
																	Porrige green bean most often being appetizer dishes because high level of amino acid, that can be as alternative protein source. Bised that, the sweet and unique flavor can being add value
																</li>
																<li>Ice Cream<br/>
																	<p>Ice cream can be a option dishes for break fasting. Beside that the sweet flavor, a refreshing cold effect will inspire our taste.</p>
																	<p>Dr. Matthew Lantz Blaylock, PhD, a nutritional scientist, explained that there are no negative affect consume ice cream for break fasting.</p>
																	<p>From nutritional content, ice cream contained sugar, fat cream, milk protein, that likes the other foods can help fulfill nutritional content during fasting.</p>
																	<p>One of ice cream that can be the option is Joyday Jujube White Chocolate. This delicious ice cream from Joyday is unique because contained Jujube fruit (Red Dates), in bandage delicious white chocolate so that fits as break fasting dishes.</p>
																</li>
															</ol>
															<p>This articles has aired on  Tribunnews.com with title 5 sweet food for restore energy during fasting, <a href="http://www.tribunnews.com/tribunners/2018/06/13/5-makanan-manis-untuk-kembalikan-energi-saat-berbuka-puasa?page=2." target="_blank">http://www.tribunnews.com/tribunners/2018/06/13/5-makanan-manis-untuk-kembalikan-energi-saat-berbuka-puasa?page=2.</a></p>
															<p>By:  Malvyandie Haryadi (editor)</p>
															<p>&nbsp;<br/></p>
															<p>&nbsp;<br/></p>
														</div>
													</div>
													<div class="is-pulled-right content-socmed">
														<ul class="sosmed-product-detaile">
															<li><a href="//www.facebook.com/JoydayIceCream/" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
															<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=<?php echo substr('Di bulan suci Ramadan, umat Muslim menjalankan ibadah puasa selama satu bulan.',0,20)?>" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
															<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
															<li><a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?php echo current_url();?>."><i class="ti-email"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>

										<!-- BLOG DETAIL 4 -->
										<div id="readmore_4" ss-container class="detail-blog-item animated">
											<div class="wrappadding animated fadeInUp">
												<div class="closecontent close-smalle"></div>
												<h1 class="titlepage">
                                                    <span class="id">Yili Group Secara Resmi Meluncurkan Produk "Joyday Ice Cream" dengan 11 Varian Rasa</span>
                                                    <span class="en">Yili Group Secara Resmi Meluncurkan Produk "Joyday Ice Cream" dengan 11 Varian Rasa</span>
                                                </h1>
												<div class="is-clearfix">
													<div class="is-pulled-left content-description">
														<div class="id">
                                                            <b>Jakarta, Jawapers</b> – PT Green Asia Food Indonesia adalah anak perusahaan Yili Group, produsen susu terbesar di Cina yang memegang peringkat nomor 1 di Asia berdasarkan 2018 Rabobank Global Dairy Top 20 Report. Joyday Ice Cream diproduksi dengan standar pengendalian mutu terbaik yang diterapkan oleh Yili Group, produsen susu ternama dunia.<br><br>
                                                            Joyday Ice Cream telah mendapatkan sertifikasi keamanan pangan dari BPOM RI dan sertifikasi halal dari LPPOM MUI. Joyday Ice Cream menawarkan 11 rasa Iezat untuk beragam selera, seperti Joyday Crunchy Chocolate Malt, Joyday Tiramisu Chocolate Mint, Joyday Cool Blueberry dan Joyday Cool Pineapple.<br><br>
                                                            Joyday Ice Cream membantu menyemangati anak remaja untuk bertanggung-jawab secara penuh terhadap perasaan bahagia yang mereka rasakan, mendorong remaja melakukan yang terbaik dalam hidup mereka, melakukan eksplorasi, dan menemukan keseruan dalam keseharian.<br><br>
                                                            Dengan tujuan membawa keseruan dalam hidup anak remaja dan menyemangati mereka melakukan eksplorasi untuk menemukan kebahagiaan dalam keseharian, PT Green Asia Food Indonesia pada hari ini meluncurkan Joyday Ice Cream dengan 11 rasa Iezat untuk beragam selera, seperti Joyday Crunchy Chocolate Malt, Joyday Tiramisu Chocolate Mint, Joyday Cool Blueberry dan Joyday Cool Pineapple. Hadir dalam acara peluncuran tersebut adalah grup musik HIVI, psikolog Ayoe Soetomo, walikota Hohhot City, Inner Mongolia, Feng Yuzhen, CEO Yili Group, Zhang Jianqiu, dan Vice President Director PT Green Asia Food Indonesia, Satria Bakti.<br><br>
                                                            Dalam kata sambutannya, Satria Bakti, Vice President Director dari PT Green Asia Food Indonesia berkata: ”Berfokus pada anak remaja, Joyday Ice Cream hadir dengan 11 rasa yang Iezat, tampil penuh warna-warni, dan sangat mudah diperoleh. Kami ingin memenuhi kebutuhan anak remaja yang ingin menikmati santapan Iezat dengan harga bersahabat dan standar keamanan makanan dari BPOM RI serta sertifikasi halal dari LPPOM MUI untuk memastikan selalu ada keseruan dan kebahagiaan di setiap gigitan. Kami menyemangati anak remaja untuk menjalani hidup dengan maksimal dan menjelajahi hidup dengan mood yang positif bersama Joyday ice Cream. Turut hadir dalam acara peluncuran adalah Walikota Hohhot City, Inner Mongolia, Feng Yuzhen, yang berkata: ‘Kami mengucapkan selamat kepada Yili Group atas peluncuran Joyday Ice Cream yang ke-66 di Indonesia. Sebagai bagian penting dari Hohhot City, Yili Juga merupakan perusahaan perwakilan terkemuka dari City of China Dairy. Saya berharap Yili akan menyediakan nutrisi dan kesehatan bagi lebih banyak konsumen lagi dengan menggunakan produk dan layanan terbaik di dunia.”
                                                            <br><br>Harga Joyday Ice Cream berkisar dari Rp. 12.000,- hingga Rp. 3.000,- dan tersedia dalam 11 rasa yang Iezat: Joyday Vanilla Milkshake, Joyday Juju be White Chocolate, Joyday Sweet Corn, Joyday Crunchy Chocolate Vanilla, Joyday Crunchy Honeydew Melon, Joyday Cool Pineapple, Joyday Cool Blueberry, Joyday Classic Belgian Chocolate, Joyday Tiramisu Chocolate Mint, Joyday Crunchy Chocolate Malt dan Joyday Crunchy Chocolate Blueberry.<br><br>
                                                            Tujuan kami adalah untuk memberikan senyum di wajah sebanyak mungkin anak remaja, menyemangati mereka untuk mengendalikan kebahagiaan dalam hidup mereka, dan menjelajahi hidup dengan maksimal dengan penuh kesenangan, mengetahui bahwa Happy Day itu Joyday. Bagi anak remaja di Indonesia, keseruan dan kesenangan dimulai sekarang. Kapan pun kamu merasa sedih, kapan pun kamu memerlukan pendorong semangat, nikmatilah Joyday Ice Cream dan temukan kembali keseruan dan kesenangan kamu,” tutup Satria Bakti.
                                                            <br><br>
                                                            <b>Tentang Joyday Ice Cream</b>
                                                            <br>
                                                            Joyday Ice Cream diproduksi oleh PT. Green Asia Food Indonesia dan menggunakan susu serta bahan-bahan berkualitas yang berstandar internasional, aman dan bersertifikasi halal dari BPOM RI dan LPPOM MUI untuk memastikan selalu ada kebahagiaan pada setiap gigitan. Joyday ingin siapa pun yang menikmatinya untuk selalu merasa senang kapan pun dan pada situasi apa pun HappyDayJoyday. Melalui pilihan rasa es krim yang lezat, Joyday ingin memastikan kita semua dapat merasakan kebahagiaan setiap harinya. (Yanti)
                                                            <br><br>
                                                            <img src="asset/static/blog/blog-yili.jpg" class="img-fluid" alt="Joyday Launched">
                                                            <br>
                                                            Source : <a href="https://jawapers.co.id/2018/10/26/yili-group-secara-resmi-meluncurkan-produk-joyday-ice-cream-dengan-11-varian-rasa/" target="_blank">Link</a><br><br>
														</div>
														<div class="en">
                                                            <b>Jakarta, Jawapers</b> – PT Green Asia Food Indonesia adalah anak perusahaan Yili Group, produsen susu terbesar di Cina yang memegang peringkat nomor 1 di Asia berdasarkan 2018 Rabobank Global Dairy Top 20 Report. Joyday Ice Cream diproduksi dengan standar pengendalian mutu terbaik yang diterapkan oleh Yili Group, produsen susu ternama dunia.<br><br>
                                                            Joyday Ice Cream telah mendapatkan sertifikasi keamanan pangan dari BPOM RI dan sertifikasi halal dari LPPOM MUI. Joyday Ice Cream menawarkan 11 rasa Iezat untuk beragam selera, seperti Joyday Crunchy Chocolate Malt, Joyday Tiramisu Chocolate Mint, Joyday Cool Blueberry dan Joyday Cool Pineapple.<br><br>
                                                            Joyday Ice Cream membantu menyemangati anak remaja untuk bertanggung-jawab secara penuh terhadap perasaan bahagia yang mereka rasakan, mendorong remaja melakukan yang terbaik dalam hidup mereka, melakukan eksplorasi, dan menemukan keseruan dalam keseharian.<br><br>
                                                            Dengan tujuan membawa keseruan dalam hidup anak remaja dan menyemangati mereka melakukan eksplorasi untuk menemukan kebahagiaan dalam keseharian, PT Green Asia Food Indonesia pada hari ini meluncurkan Joyday Ice Cream dengan 11 rasa Iezat untuk beragam selera, seperti Joyday Crunchy Chocolate Malt, Joyday Tiramisu Chocolate Mint, Joyday Cool Blueberry dan Joyday Cool Pineapple. Hadir dalam acara peluncuran tersebut adalah grup musik HIVI, psikolog Ayoe Soetomo, walikota Hohhot City, Inner Mongolia, Feng Yuzhen, CEO Yili Group, Zhang Jianqiu, dan Vice President Director PT Green Asia Food Indonesia, Satria Bakti.<br><br>
                                                            Dalam kata sambutannya, Satria Bakti, Vice President Director dari PT Green Asia Food Indonesia berkata: ”Berfokus pada anak remaja, Joyday Ice Cream hadir dengan 11 rasa yang Iezat, tampil penuh warna-warni, dan sangat mudah diperoleh. Kami ingin memenuhi kebutuhan anak remaja yang ingin menikmati santapan Iezat dengan harga bersahabat dan standar keamanan makanan dari BPOM RI serta sertifikasi halal dari LPPOM MUI untuk memastikan selalu ada keseruan dan kebahagiaan di setiap gigitan. Kami menyemangati anak remaja untuk menjalani hidup dengan maksimal dan menjelajahi hidup dengan mood yang positif bersama Joyday ice Cream. Turut hadir dalam acara peluncuran adalah Walikota Hohhot City, Inner Mongolia, Feng Yuzhen, yang berkata: ‘Kami mengucapkan selamat kepada Yili Group atas peluncuran Joyday Ice Cream yang ke-66 di Indonesia. Sebagai bagian penting dari Hohhot City, Yili Juga merupakan perusahaan perwakilan terkemuka dari City of China Dairy. Saya berharap Yili akan menyediakan nutrisi dan kesehatan bagi lebih banyak konsumen lagi dengan menggunakan produk dan layanan terbaik di dunia.”
                                                            <br><br>Harga Joyday Ice Cream berkisar dari Rp. 12.000,- hingga Rp. 3.000,- dan tersedia dalam 11 rasa yang Iezat: Joyday Vanilla Milkshake, Joyday Juju be White Chocolate, Joyday Sweet Corn, Joyday Crunchy Chocolate Vanilla, Joyday Crunchy Honeydew Melon, Joyday Cool Pineapple, Joyday Cool Blueberry, Joyday Classic Belgian Chocolate, Joyday Tiramisu Chocolate Mint, Joyday Crunchy Chocolate Malt dan Joyday Crunchy Chocolate Blueberry.<br><br>
                                                            Tujuan kami adalah untuk memberikan senyum di wajah sebanyak mungkin anak remaja, menyemangati mereka untuk mengendalikan kebahagiaan dalam hidup mereka, dan menjelajahi hidup dengan maksimal dengan penuh kesenangan, mengetahui bahwa Happy Day itu Joyday. Bagi anak remaja di Indonesia, keseruan dan kesenangan dimulai sekarang. Kapan pun kamu merasa sedih, kapan pun kamu memerlukan pendorong semangat, nikmatilah Joyday Ice Cream dan temukan kembali keseruan dan kesenangan kamu,” tutup Satria Bakti.
                                                            <br><br>
                                                            <b>Tentang Joyday Ice Cream</b>
                                                            <br>
                                                            Joyday Ice Cream diproduksi oleh PT. Green Asia Food Indonesia dan menggunakan susu serta bahan-bahan berkualitas yang berstandar internasional, aman dan bersertifikasi halal dari BPOM RI dan LPPOM MUI untuk memastikan selalu ada kebahagiaan pada setiap gigitan. Joyday ingin siapa pun yang menikmatinya untuk selalu merasa senang kapan pun dan pada situasi apa pun HappyDayJoyday. Melalui pilihan rasa es krim yang lezat, Joyday ingin memastikan kita semua dapat merasakan kebahagiaan setiap harinya. (Yanti)
                                                            <br><br>
                                                            <img src="asset/static/blog/blog-yili.jpg" class="img-fluid" alt="Joyday Launched">
                                                            <br>
                                                            Source : <a href="https://jawapers.co.id/2018/10/26/yili-group-secara-resmi-meluncurkan-produk-joyday-ice-cream-dengan-11-varian-rasa/" target="_blank">Link</a><br><br>
														</div>
													</div>
													<div class="is-pulled-right content-socmed">
														<ul class="sosmed-product-detaile">
															<li><a href="//www.facebook.com/JoydayIceCream/" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
															<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=<?php echo substr('Yili Group Secara Resmi Meluncurkan Produk "Joyday Ice Cream" dengan 11 Varian Rasa',0,20)?>" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
															<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
															<li><a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?php echo current_url();?>."><i class="ti-email"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
										<!-- BLOG DETAIL 5 -->
										<div id="readmore_5" ss-container class="detail-blog-item animated">
											<div class="wrappadding animated fadeInUp">
												<div class="closecontent close-smalle"></div>
												<h1 class="titlepage">
                                                    <span class="id">Fakta: Es Krim Dapat Memperbaiki Mood</span>
                                                    <span class="en">Fakta: Es Krim Dapat Memperbaiki Mood</span>
                                                </h1>
												<div class="is-clearfix">
													<div class="is-pulled-left content-description">
														<div class="id">
                                                            TRIBUNJAKARTA.COM - Rasanya tidak mungkin bila seseorang tidak menyukai es krim yang manis dan dingin. Es Krim ternyata dapat menghasilkan serangkaian respons fisik dan emosional pada sebagian besar orang. Pasti kamu pernah merasakan perasaan bahagia saat menghabiskan satu es krim.<br><br>
                                                            Beberapa peneliti mengatakan, <a href="http://jakarta.tribunnews.com/tag/mood" target="_blank">mood</a> dapat dengan sendirinya meningkat dengan atau tanpa makanan, misalnya beberapa saat setelah kita merasakan amarah. Es krim dapat dikatakan sebagai pereda emosi, terutama bagi anak usia remaja, atau kamu yang seringkali moody.<br><br>
                                                            Menurut Psikolog Ayoe Soetomo, dalam acara Peluncuran Joyday Ice Cream beberapa waktu lalu di Jakarta Selatan, usia remaja sangat rentan mengalami <a href="http://jakarta.tribunnews.com/tag/mood" target="_blank">mood</a> swing atau perubahan <a href="http://jakarta.tribunnews.com/tag/mood" target="_blank">mood</a> mendadak.<br><br>
                                                            "Kalau berbicara mengenai <a href="http://jakarta.tribunnews.com/tag/mood" target="_blank">mood</a> anak remaja, perubahan fisik hormon tidak stabil kemudian perubahan peran sosial, mereka bukan anak kecil lagi tapi dibilang sudah dewasa juga belum sampai kesana, sehingga berpengaruh terhadap emosi dan itu sangat wajar terjadi," ungkap Ayoe.<br><br>
                                                            Menurut Ayoe, bila ingin mencari kebahagiaan dan meningkatkan <a href="http://jakarta.tribunnews.com/tag/mood" target="_blank">mood</a>, carilah apa yang kita sukai.<br><br>
                                                            "Kalau membahas bagaimana mencari happines atau kebahagiaan kemudian meningkatkan <a href="http://jakarta.tribunnews.com/tag/mood" target="_blank">mood</a>, memang level pertama mencari kenikmatan dan kenyamanan saat beraktivitas, itu salah satunya makan makanan yang kita suka atau aktivitas yang kita suka," tandasnya.<br><br>
                                                            Saat mengonsumsi apa yang kita suka, misalnya es krim, bagian otak tertentu merespon emosi positif dengan sangat baik.<br><br>
                                                            "Ada bagian otak yang bertanggung jawab terhadap emosi positif yang kemudian berpengaruh pada saat intake es krim, kemudian bagian otak itu kemudian bekerja dengan lebih aktif atau lebih baik," ungkapnya.<br><br>
                                                            Berikut alasan mengapa es krim dapat meningkatkan <a href="http://jakarta.tribunnews.com/tag/mood" target="_blank">mood</a>, dikutip dari Livestrong.
                                                            <br><br>
                                                            
                                                            Tubuh jadi lebih berenergi<br>
                                                            Es krim mengandung protein, yang terdiri dari asam amino. Asam amino yang kita dapatkan ketika makan es krim dapat mempengaruhi suasana hati. Es krim yang kaya protein meningkatkan kemungkinan kadar tirosin di otak. Tirosin adalah neurotransmitter yang meningkatkan kadar dopamin dan norepinefrin di dalam tubuh kita, menurut Franklin Institute. Hasilnya ialah kita lebih waspada dan lebih berenergi.<br><br>

                                                            Memiliki efek menenangkan<br>
                                                            Produk susu dalam es krim merupakan neurotransmitter: Pembawa pesan kimia yang sensitif yang hanya membutuhkan lingkungan yang tepat untuk memicu pelepasannya. Tryptophanadalah asam amino dalam produk susu yang mengarah ke suasana hati yang tenang dengan meningkatkan produksi serotonin, menurut University of Maryland Medical Center. Hal ini diketahui dapat membantu kita rileks dan tidur lebih baik.<br><br>

                                                            Punya beragam nutrisi<br>
                                                            Mengonsumsi es krim juga dapat memberikan manfaat lain bagi tubuh, lebih dari sekedar memperbaiki suasana hati. Es krim dapat menjadi sumber karbohidrat, protein, dan lemak yang dapat membantu proses sintesis energi.<br>
                                                            Seperti yang dikutip dari situs Newsweek, sebuah penelitian yang dilakukan Yoshihiko Koga, seorang profesor dari Universitas Kyorin, Tokyo, membuktikan jika mengonsumsi makanan dingin seperti es krim sesaat setelah terbangun dapat memberikan stimulus pada otak menjadi lebih aktif, meningkatkan kewaspadaan, dan kapasitas mental.<br>
                                                            Bagi kamu yang gemar es krim, manfaat-manfaat tersebut bisa diperoleh asalkan mengonsumsinya dengan bijaksana, alias dalam porsi yang sehat dan wajar.<br>
                                                            Idealnya es krim dikonsumsi dalam porsi setengah cup atau dengan ukuran separuh bola baseball. Konsumsi juga dalam frekuensi yang wajar. (Nakita.id/Fadhila Afifah)<br><br>
                                                            <!-- Editor: Erlina Fury Santika  -->
                                                            <br><br>
                                                            <img src="asset/static/blog/blog-moods-2.jpg" class="img-fluid" alt="Joyday Launched">
                                                            <br>
                                                            <br>
                                                            Source : <a href="http://nakita.grid.id/read/02963379/inilah-alasannya-mengapa-ice-cream-dapat-membuat-mood-lebih-baik?page=all" target="_blank">Link</a><br><br>
														</div>
														<div class="en">
                                                        TRIBUNJAKARTA.COM - Rasanya tidak mungkin bila seseorang tidak menyukai es krim yang manis dan dingin. Es Krim ternyata dapat menghasilkan serangkaian respons fisik dan emosional pada sebagian besar orang. Pasti kamu pernah merasakan perasaan bahagia saat menghabiskan satu es krim.<br><br>
                                                            Beberapa peneliti mengatakan, <a href="http://jakarta.tribunnews.com/tag/mood" target="_blank">mood</a> dapat dengan sendirinya meningkat dengan atau tanpa makanan, misalnya beberapa saat setelah kita merasakan amarah. Es krim dapat dikatakan sebagai pereda emosi, terutama bagi anak usia remaja, atau kamu yang seringkali moody.<br><br>
                                                            Menurut Psikolog Ayoe Soetomo, dalam acara Peluncuran Joyday Ice Cream beberapa waktu lalu di Jakarta Selatan, usia remaja sangat rentan mengalami <a href="http://jakarta.tribunnews.com/tag/mood" target="_blank">mood</a> swing atau perubahan <a href="http://jakarta.tribunnews.com/tag/mood" target="_blank">mood</a> mendadak.<br><br>
                                                            "Kalau berbicara mengenai <a href="http://jakarta.tribunnews.com/tag/mood" target="_blank">mood</a> anak remaja, perubahan fisik hormon tidak stabil kemudian perubahan peran sosial, mereka bukan anak kecil lagi tapi dibilang sudah dewasa juga belum sampai kesana, sehingga berpengaruh terhadap emosi dan itu sangat wajar terjadi," ungkap Ayoe.<br><br>
                                                            Menurut Ayoe, bila ingin mencari kebahagiaan dan meningkatkan <a href="http://jakarta.tribunnews.com/tag/mood" target="_blank">mood</a>, carilah apa yang kita sukai.<br><br>
                                                            "Kalau membahas bagaimana mencari happines atau kebahagiaan kemudian meningkatkan <a href="http://jakarta.tribunnews.com/tag/mood" target="_blank">mood</a>, memang level pertama mencari kenikmatan dan kenyamanan saat beraktivitas, itu salah satunya makan makanan yang kita suka atau aktivitas yang kita suka," tandasnya.<br><br>
                                                            Saat mengonsumsi apa yang kita suka, misalnya es krim, bagian otak tertentu merespon emosi positif dengan sangat baik.<br><br>
                                                            "Ada bagian otak yang bertanggung jawab terhadap emosi positif yang kemudian berpengaruh pada saat intake es krim, kemudian bagian otak itu kemudian bekerja dengan lebih aktif atau lebih baik," ungkapnya.<br><br>
                                                            Berikut alasan mengapa es krim dapat meningkatkan <a href="http://jakarta.tribunnews.com/tag/mood" target="_blank">mood</a>, dikutip dari Livestrong.
                                                            <br><br>
                                                            
                                                            Tubuh jadi lebih berenergi<br>
                                                            Es krim mengandung protein, yang terdiri dari asam amino. Asam amino yang kita dapatkan ketika makan es krim dapat mempengaruhi suasana hati. Es krim yang kaya protein meningkatkan kemungkinan kadar tirosin di otak. Tirosin adalah neurotransmitter yang meningkatkan kadar dopamin dan norepinefrin di dalam tubuh kita, menurut Franklin Institute. Hasilnya ialah kita lebih waspada dan lebih berenergi.<br><br>

                                                            Memiliki efek menenangkan<br>
                                                            Produk susu dalam es krim merupakan neurotransmitter: Pembawa pesan kimia yang sensitif yang hanya membutuhkan lingkungan yang tepat untuk memicu pelepasannya. Tryptophanadalah asam amino dalam produk susu yang mengarah ke suasana hati yang tenang dengan meningkatkan produksi serotonin, menurut University of Maryland Medical Center. Hal ini diketahui dapat membantu kita rileks dan tidur lebih baik.<br><br>

                                                            Punya beragam nutrisi<br>
                                                            Mengonsumsi es krim juga dapat memberikan manfaat lain bagi tubuh, lebih dari sekedar memperbaiki suasana hati. Es krim dapat menjadi sumber karbohidrat, protein, dan lemak yang dapat membantu proses sintesis energi.<br>
                                                            Seperti yang dikutip dari situs Newsweek, sebuah penelitian yang dilakukan Yoshihiko Koga, seorang profesor dari Universitas Kyorin, Tokyo, membuktikan jika mengonsumsi makanan dingin seperti es krim sesaat setelah terbangun dapat memberikan stimulus pada otak menjadi lebih aktif, meningkatkan kewaspadaan, dan kapasitas mental.<br>
                                                            Bagi kamu yang gemar es krim, manfaat-manfaat tersebut bisa diperoleh asalkan mengonsumsinya dengan bijaksana, alias dalam porsi yang sehat dan wajar.<br>
                                                            Idealnya es krim dikonsumsi dalam porsi setengah cup atau dengan ukuran separuh bola baseball. Konsumsi juga dalam frekuensi yang wajar. (Nakita.id/Fadhila Afifah)<br><br>
                                                            <!-- Editor: Erlina Fury Santika  -->
                                                            <br><br>
                                                            <img src="asset/static/blog/blog-moods-2.jpg" class="img-fluid" alt="Joyday Launched">
                                                            <br>
                                                            <br>
                                                            Source : <a href="http://nakita.grid.id/read/02963379/inilah-alasannya-mengapa-ice-cream-dapat-membuat-mood-lebih-baik?page=all" target="_blank">Link</a><br><br>
														</div>
													</div>
													<div class="is-pulled-right content-socmed">
														<ul class="sosmed-product-detaile">
															<li><a href="//www.facebook.com/JoydayIceCream/" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
															<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=<?php echo substr('Fakta: Es Krim Dapat Memperbaiki Mood',0,20)?>" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
															<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
															<li><a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?php echo current_url();?>."><i class="ti-email"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
										<!-- BLOG DETAIL 6 -->
										<div id="readmore_6" ss-container class="detail-blog-item animated">
											<div class="wrappadding animated fadeInUp">
												<div class="closecontent close-smalle"></div>
												<h1 class="titlepage">
                                                    <span class="id">Ubah Mood Negatif Menjadi Positif Melalui Es Krim</span>
                                                    <span class="en">Ubah Mood Negatif Menjadi Positif Melalui Es Krim</span>
                                                </h1>
												<div class="is-clearfix">
													<div class="is-pulled-left content-description">
														<div class="id">
                                                            JAKARTA - Es krim secara ilmiah bisa membuat seseorang merasa senang dan mengubah mood yang negatif menjadi positif, serta membuat mood yang sudah positif menjadi lebih senang dan bahagia.<br><br>
                                                            Sensasi es krim yang dingin dan menyegarkan, rasa yang lezat dan tampilan penuh warna dapat membuat senang dan bahkan menjadi lebih bahagia saat suasana hati sudah baik.<br><br>
                                                            &quot;Indra pengecap kita sensitif terhadap rasa manis. Ketika lidah mengecap rasa manis, tubuh melepaskan stres dan menciptakan rasa senang. Itulah sebabnya ketika memakan es krim, anak-anak selalu melakukannya dengan senyum yang merekah di wajah mereka,&quot; ujar psikolog Ayoe Sutomo di Jakarta,
                                                            Rabu (24/10).<br><br>
                                                            Berdasarkan fakta tersebut, PT Green Asia Food Indonesia pun meluncurkan Joyday Ice Cream dengan 11 rasa lezat untuk beragam selera, di antaranya Joyday Crunchy Chocolate Malt, Joyday Tiramisu Chocolate Mint, Joyday Cool Blueberry dan Joyday Cool Pineapple. Menyasar segmen remaja, Joyday Ice Cream hadir dengan varian rasa yang banyak dan tampil warna-warni serta sangat mudah diperoleh.<br><br>
                                                            &quot;Kami ingin memenuhi kebutuhan anak remaja yang ingin menikmati santapan lezat dengan harga bersahabat dan standar keamanan makanan dari BPOM RI serta sertifikasi halal dari LPPOM MUI untuk memastikan selalu ada keseruan dan kebahagiaan di setiap gigitan. Kami menyemangati anak remaja untuk menjalani hidup dengan maksimal dan menjelajahi hidup dengan mood yang positif bersama Joyday Ice Cream,&quot; jelas Vice President Director PT Green Asia Food Indonesia, Satria Bakti.<br><br>
                                                            &quot;Remaja sangat dikenal dengan mood yang mudah berubah atau mood swings. Hal-hal sederhana dapat mengubah mood mereka dari positif menjadi negatif maupun sebaliknya. Semudah anak remaja mendapatkan bad mood, terdapat juga hal-hal sederhana yang dapat mengembalikan mood mereka menjadi positif kembali,&quot; tambahnya.<br><br>
                                                            Dengan aneka macam rasa yang dijajakan, Joyday Ice Cream dibanderol dengan harga yang terjangkau bagi para remaja. &quot;Tujuan kami adalah untuk memberikan senyum mereka di wajah sebanyak mungkin, menyemangati mereka untuk mengendalikan kebahagiaan dalam hidup mereka dan menjelajahi hidup dengan maksimal dengan penuh kesenangan,&quot; tutup Satria Bakti. 
                                                            (nug)
                                                            <br><br>
                                                            <img src="asset/static/blog/blog-mood-change.jpg" class="img-fluid" alt="Joyday Launched">
                                                            <br>
                                                            <br>
                                                            Source : <a href="https://lifestyle.sindonews.com/read/1348977/166/ubah-mood-negatif-menjadi-positif-melalui-es-krim-1540391743" target="_blank">Link</a><br><br>
														</div>
														<div class="en">
                                                            JAKARTA - Es krim secara ilmiah bisa membuat seseorang merasa senang dan mengubah mood yang negatif menjadi positif, serta membuat mood yang sudah positif menjadi lebih senang dan bahagia.<br><br>
                                                            Sensasi es krim yang dingin dan menyegarkan, rasa yang lezat dan tampilan penuh warna dapat membuat senang dan bahkan menjadi lebih bahagia saat suasana hati sudah baik.<br><br>
                                                            &quot;Indra pengecap kita sensitif terhadap rasa manis. Ketika lidah mengecap rasa manis, tubuh melepaskan stres dan menciptakan rasa senang. Itulah sebabnya ketika memakan es krim, anak-anak selalu melakukannya dengan senyum yang merekah di wajah mereka,&quot; ujar psikolog Ayoe Sutomo di Jakarta,
                                                            Rabu (24/10).<br><br>
                                                            Berdasarkan fakta tersebut, PT Green Asia Food Indonesia pun meluncurkan Joyday Ice Cream dengan 11 rasa lezat untuk beragam selera, di antaranya Joyday Crunchy Chocolate Malt, Joyday Tiramisu Chocolate Mint, Joyday Cool Blueberry dan Joyday Cool Pineapple. Menyasar segmen remaja, Joyday Ice Cream hadir dengan varian rasa yang banyak dan tampil warna-warni serta sangat mudah diperoleh.<br><br>
                                                            &quot;Kami ingin memenuhi kebutuhan anak remaja yang ingin menikmati santapan lezat dengan harga bersahabat dan standar keamanan makanan dari BPOM RI serta sertifikasi halal dari LPPOM MUI untuk memastikan selalu ada keseruan dan kebahagiaan di setiap gigitan. Kami menyemangati anak remaja untuk menjalani hidup dengan maksimal dan menjelajahi hidup dengan mood yang positif bersama Joyday Ice Cream,&quot; jelas Vice President Director PT Green Asia Food Indonesia, Satria Bakti.<br><br>
                                                            &quot;Remaja sangat dikenal dengan mood yang mudah berubah atau mood swings. Hal-hal sederhana dapat mengubah mood mereka dari positif menjadi negatif maupun sebaliknya. Semudah anak remaja mendapatkan bad mood, terdapat juga hal-hal sederhana yang dapat mengembalikan mood mereka menjadi positif kembali,&quot; tambahnya.<br><br>
                                                            Dengan aneka macam rasa yang dijajakan, Joyday Ice Cream dibanderol dengan harga yang terjangkau bagi para remaja. &quot;Tujuan kami adalah untuk memberikan senyum mereka di wajah sebanyak mungkin, menyemangati mereka untuk mengendalikan kebahagiaan dalam hidup mereka dan menjelajahi hidup dengan maksimal dengan penuh kesenangan,&quot; tutup Satria Bakti. 
                                                            (nug)
                                                            <br><br>
                                                            <img src="asset/static/blog/blog-mood-change.jpg" class="img-fluid" alt="Joyday Launched">
                                                            <br>
                                                            <br>
                                                            Source : <a href="https://lifestyle.sindonews.com/read/1348977/166/ubah-mood-negatif-menjadi-positif-melalui-es-krim-1540391743" target="_blank">Link</a><br><br>
														</div>
													</div>
													<div class="is-pulled-right content-socmed">
														<ul class="sosmed-product-detaile">
															<li><a href="//www.facebook.com/JoydayIceCream/" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
															<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=<?php echo substr('Ubah Mood Negatif Menjadi Positif Melalui Es Krim',0,20)?>" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
															<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
															<li><a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?php echo current_url();?>."><i class="ti-email"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
										<!-- BLOG DETAIL 7 -->
										<div id="readmore_7" ss-container class="detail-blog-item animated">
											<div class="wrappadding animated fadeInUp">
												<div class="closecontent close-smalle"></div>
												<h1 class="titlepage">
                                                    <span class="id">HIVI! Usir Bad Mood dengan Es Krim</span>
                                                    <span class="en">HIVI! Usir Bad Mood dengan Es Krim</span>
                                                </h1>
												<div class="is-clearfix">
													<div class="is-pulled-left content-description">
														<div class="id">
                                                            Femme.id, Jakarta – Setiap orang punya cara mengubah suasana hati yang buruk.<br><br>
                                                            Bagi grup musik HIVI! yang beranggotakan Nadhia Aleida (vokal), Ilham Aditama (vokal), Febrian Nindyo (vokal/gitar) dan Ezra Mandira (vokal/gitar), es krim berperan besar dalam membuat suasana kerja makin menyenangkan.<br><br>
                                                            “Makan es krim itu meningkatkan mood,” kata Ilham di peluncuran es krim Joyday, Jakarta, Selasa (23/10/2018).<br><br>
                                                            Menurut Ezra, biasanya dia menyimpan banyak es krim dalam kulkas di kantor mereka. Seraya bercanda, ia menunjuk teman-temannya sebagai orang yang menghabiskan stok es krim. <br><br>
                                                            Grup musik asal Jakarta yang berdiri sejak 2009 ketika para anggotanya masih duduk di bangku SMA, kecuali Nadhia yang bergabung belakangan, membawakan lagu jingle Joyday di peluncuran es krim asal China tersebut untuk pertama kalinya di Jakarta.<br><br>
                                                            Menurut psikolog Ayoe Sutomo, secara ilmiah es krim memang bisa memperbaiki suasana hati yang buruk.<br><br>
                                                            “Es krim membuat bagian otak yang membuat emosi positif jadi meningkat,” jelas Ayoe. (nan)
                                                            <br><br>
                                                            <img src="asset/static/blog/blog-hivi.jpg" class="img-fluid" alt="Joyday Launched">
                                                            <br>
                                                            Source : <a href="https://femme.id/index.php/2018/10/24/hivi-usir-bad-mood-dengan-es-krim/" target="_blank">Link</a><br><br>
                                                        </div>
														<div class="en">
                                                            Femme.id, Jakarta – Setiap orang punya cara mengubah suasana hati yang buruk.<br><br>
                                                            Bagi grup musik HIVI! yang beranggotakan Nadhia Aleida (vokal), Ilham Aditama (vokal), Febrian Nindyo (vokal/gitar) dan Ezra Mandira (vokal/gitar), es krim berperan besar dalam membuat suasana kerja makin menyenangkan.<br><br>
                                                            “Makan es krim itu meningkatkan mood,” kata Ilham di peluncuran es krim Joyday, Jakarta, Selasa (23/10/2018).<br><br>
                                                            Menurut Ezra, biasanya dia menyimpan banyak es krim dalam kulkas di kantor mereka. Seraya bercanda, ia menunjuk teman-temannya sebagai orang yang menghabiskan stok es krim. <br><br>
                                                            Grup musik asal Jakarta yang berdiri sejak 2009 ketika para anggotanya masih duduk di bangku SMA, kecuali Nadhia yang bergabung belakangan, membawakan lagu jingle Joyday di peluncuran es krim asal China tersebut untuk pertama kalinya di Jakarta.<br><br>
                                                            Menurut psikolog Ayoe Sutomo, secara ilmiah es krim memang bisa memperbaiki suasana hati yang buruk.<br><br>
                                                            “Es krim membuat bagian otak yang membuat emosi positif jadi meningkat,” jelas Ayoe. (nan)
                                                            <br><br>
                                                            <img src="asset/static/blog/blog-hivi.jpg" class="img-fluid" alt="Joyday Launched">
                                                            <br>
                                                            Source : <a href="https://femme.id/index.php/2018/10/24/hivi-usir-bad-mood-dengan-es-krim/" target="_blank">Link</a><br><br>
														</div>
													</div>
													<div class="is-pulled-right content-socmed">
														<ul class="sosmed-product-detaile">
															<li><a href="//www.facebook.com/JoydayIceCream/" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
															<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=<?php echo substr('HIVI! Usir Bad Mood dengan Es Krim',0,20)?>" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
															<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
															<li><a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?php echo current_url();?>."><i class="ti-email"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
										<!-- BLOG DETAIL 8 -->
										<div id="readmore_8" ss-container class="detail-blog-item animated">
											<div class="wrappadding animated fadeInUp">
												<div class="closecontent close-smalle"></div>
												<h1 class="titlepage">
                                                    <span class="id">Cara Atasi Anak Pilih-pilih Makanan</span>
                                                    <span class="en">Cara Atasi Anak Pilih-pilih Makanan</span>
                                                </h1>
												<div class="is-clearfix">
													<div class="is-pulled-left content-description">
														<div class="id">
                                                            Jakarta (HN) - Buah hati yang terlalu pemilih dalam mengisi perut bisa membuat orangtua pusing tujuh keliling karena anak cuma mau mengonsumsi makanan tertentu. Psikolog Ayoe Sutomo mengatakan, ada banyak faktor yang menyebabkan anak jadi pilih-pilih soal makanan, termasuk dari sisi psikologi hingga pola makan ibu saat mengandung.<br><br>
                                                            "Ada kalanya kondisi tertentu yang membuat dia tidak nyaman saat makan makanan tertentu. Misalnya saat makan sesuatu dia dipaksa, jadi trauma," tutur Ayoe usai peluncuran es krim Joyday di Jakarta, Selasa (23/10), dikutip Antara.
                                                            Menurut Ayoe, ada juga kemungkinan sang ibu kurang banyak menyantap variasi makanan saat hamil sehingga anak tidak tidak mengenal banyak rasa. Variasi banyaknya makanan yang dimakan ibunya saat hamil, kata dia, mempengaruhi memori rasa pada anak.
                                                            Oleh karena itu, orangtua sebaiknya mengenalkan beragam jenis makanan baru sesering mungkin pada anak. Jangan ragu mengeksplorasi resep dan bentuk agar makanan terlihat menggiurkan. Misalnya bento di mana bahan makanan diubah menjadi bentuk binatang-binatang lucu.<br><br>
                                                            Dia menyarankan libatkan anak dalam memasak. Berikan tugas-tugas sederhana seperti memotong atau mengupas sayur. Keterlibatannya dalam proses memasak akan membuat anak lebih tergerak untuk mencicipi makanan tersebut.
                                                            <br><br>
                                                            <img src="asset/static/blog/blog-kids.jpg" class="img-fluid" alt="Joyday Launched">
                                                            <br>
                                                            <br>
                                                            Source : <a href="http://harnas.co/2018/10/23/cara-atasi-anak-pilih-pilih-makanan">Link</a><br><br>
														</div>
														<div class="en">
                                                            Jakarta (HN) - Buah hati yang terlalu pemilih dalam mengisi perut bisa membuat orangtua pusing tujuh keliling karena anak cuma mau mengonsumsi makanan tertentu. Psikolog Ayoe Sutomo mengatakan, ada banyak faktor yang menyebabkan anak jadi pilih-pilih soal makanan, termasuk dari sisi psikologi hingga pola makan ibu saat mengandung.<br><br>
                                                            "Ada kalanya kondisi tertentu yang membuat dia tidak nyaman saat makan makanan tertentu. Misalnya saat makan sesuatu dia dipaksa, jadi trauma," tutur Ayoe usai peluncuran es krim Joyday di Jakarta, Selasa (23/10), dikutip Antara.
                                                            Menurut Ayoe, ada juga kemungkinan sang ibu kurang banyak menyantap variasi makanan saat hamil sehingga anak tidak tidak mengenal banyak rasa. Variasi banyaknya makanan yang dimakan ibunya saat hamil, kata dia, mempengaruhi memori rasa pada anak.
                                                            Oleh karena itu, orangtua sebaiknya mengenalkan beragam jenis makanan baru sesering mungkin pada anak. Jangan ragu mengeksplorasi resep dan bentuk agar makanan terlihat menggiurkan. Misalnya bento di mana bahan makanan diubah menjadi bentuk binatang-binatang lucu.<br><br>
                                                            Dia menyarankan libatkan anak dalam memasak. Berikan tugas-tugas sederhana seperti memotong atau mengupas sayur. Keterlibatannya dalam proses memasak akan membuat anak lebih tergerak untuk mencicipi makanan tersebut.
                                                            <br><br>
                                                            <img src="asset/static/blog/blog-kids.jpg" class="img-fluid" alt="Joyday Launched">
                                                            <br>
                                                            <br>
                                                            Source : <a href="http://harnas.co/2018/10/23/cara-atasi-anak-pilih-pilih-makanan">Link</a><br><br>
														</div>
													</div>
													<div class="is-pulled-right content-socmed">
														<ul class="sosmed-product-detaile">
															<li><a href="//www.facebook.com/JoydayIceCream/" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
															<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=<?php echo substr('Cara Atasi Anak Pilih-pilih Makanan',0,20)?>" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
															<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
															<li><a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?php echo current_url();?>."><i class="ti-email"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>

                                        <!-- BLOG DETAIL 9 -->
										<div id="readmore_9" ss-container class="detail-blog-item animated">
											<div class="wrappadding animated fadeInUp">
												<div class="closecontent close-smalle"></div>
												<h1 class="titlepage">
                                                    <span class="id">Joyday Media Launch</span>
                                                    <span class="en">Joyday Media Launch</span>
                                                </h1>
												<div class="is-clearfix">
													<div class="is-pulled-left content-description">
														<div class="id">
                                                            Perusahaan susu asal Cina Yili Group baru saja meluncurkan produk es krim Joyday di Hotel Four Season, Jakarta 23 Oktober 2018. Peluncuran ini dihadiri langsung oleh CEO Yili Group, Zhang Jianqiu, Walikota Hohhot city, Inner Mongolia, Feng Yuzhen, Vice President Director PT Green Asia Food Indonesia, Satria Bakti, Psikolog Ayoe Sutomo dan dimeriahkan oleh HIVI yang menyanyikan jingle lagu Joyday. Momen yang paling asik tentunya ketika dapat menikmati kesebelas varian rasa es krim-nya. Tapi, jangan khawatir karena ada yang ga kalah asik yaitu Joyday sudah mulai dipasarkan di wilayah Jabodetabek mulai bulan Juni 2018 loh!
                                                            <br><br>
                                                            Selamat datang di Indonesia, Joyday! dan Selamat menikmati!<br><br>
                                                            <iframe width="100%" height="524" src="https://www.youtube.com/embed/bR0jK2r4X2w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
														</div>
														<div class="en">
                                                            Perusahaan susu asal Cina Yili Group baru saja meluncurkan produk es krim Joyday di Hotel Four Season, Jakarta 23 Oktober 2018. Peluncuran ini dihadiri langsung oleh CEO Yili Group, Zhang Jianqiu, Walikota Hohhot city, Inner Mongolia, Feng Yuzhen, Vice President Director PT Green Asia Food Indonesia, Satria Bakti, Psikolog Ayoe Sutomo dan dimeriahkan oleh HIVI yang menyanyikan jingle lagu Joyday. Momen yang paling asik tentunya ketika dapat menikmati kesebelas varian rasa es krim-nya. Tapi, jangan khawatir karena ada yang ga kalah asik yaitu Joyday sudah mulai dipasarkan di wilayah Jabodetabek mulai bulan Juni 2018 loh!
                                                            <br><br>
                                                            Selamat datang di Indonesia, Joyday! dan Selamat menikmati!<br><br>
                                                            <iframe width="100%" height="324" src="https://www.youtube.com/embed/bR0jK2r4X2w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
														</div>
													</div>
													<div class="is-pulled-right content-socmed">
														<ul class="sosmed-product-detaile">
															<li><a href="//www.facebook.com/JoydayIceCream/" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
															<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=<?php echo substr('Joyday Media Launch',0,20)?>" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
															<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
															<li><a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?php echo current_url();?>."><i class="ti-email"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
                                        <!-- BLOG DETAIL 10 -->
										<div id="readmore_10" ss-container class="detail-blog-item animated">
											<div class="wrappadding animated fadeInUp">
												<div class="closecontent close-smalle"></div>
												<h1 class="titlepage">
                                                    <span class="id">Joyday School to School Roadshow</span>
                                                    <span class="en">Joyday School to School Roadshow</span>
                                                </h1>
												<div class="is-clearfix">
													<div class="is-pulled-left content-description">
														<div class="id">
                                                            Hey semua! Joyday telah melakukan serangkaian acara dari sekolah ke sekolah nih sekitar Jakarta, Tangerang dan Jawa Barat untuk memperkenalkan Es krim Joyday.
                                                            <br><br>
                                                            Dan ternyata, respon yang diberikan sungguh  antusias! Anak-anak sekolah sangat menyukai rasa es krim dengan bentuknya yang unik. Jadi, jangan mau ketinggalan! Cobain sensasi dari Joyday yang bikin harimu jadi Joy terus!<br><br>
                                                            <iframe width="100%" height="524" src="https://www.youtube.com/embed/cL_2SyfqNQQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
														</div>
														<div class="en">
                                                            Hey semua! Joyday telah melakukan serangkaian acara dari sekolah ke sekolah nih sekitar Jakarta, Tangerang dan Jawa Barat untuk memperkenalkan Es krim Joyday.
                                                            <br><br>
                                                            Dan ternyata, respon yang diberikan sungguh  antusias! Anak-anak sekolah sangat menyukai rasa es krim dengan bentuknya yang unik. Jadi, jangan mau ketinggalan! Cobain sensasi dari Joyday yang bikin harimu jadi Joy terus!<br><br>
                                                            <iframe width="100%" height="524" src="https://www.youtube.com/embed/cL_2SyfqNQQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
														</div>
													</div>
													<div class="is-pulled-right content-socmed">
														<ul class="sosmed-product-detaile">
															<li><a href="//www.facebook.com/JoydayIceCream/" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
															<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=<?php echo substr('Joyday School to School Roadshow',0,20)?>" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
															<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
															<li><a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?php echo current_url();?>."><i class="ti-email"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>

                                        <!-- BLOG DETAIL 11 -->
										<div id="readmore_11" ss-container class="detail-blog-item animated">
											<div class="wrappadding animated fadeInUp">
												<div class="closecontent close-smalle"></div>
												<h1 class="titlepage">
                                                    <span class="id">Joyday Mung Bean</span>
                                                    <span class="en">Joyday Mung Bean</span>
                                                </h1>
												<div class="is-clearfix">
													<div class="is-pulled-left content-description">
														<div class="id">														
															Joyday Mung Bean varian baru es krim Joyday di Indonesia yang menawarkan paduan rasa kacang hijau dan susu. Es krim susu kacang hijau ini mengandung sari kacang hijau asli untuk memberi kesegaran terutama ketika cuaca sedang terik.
															<br><br>
															Tidak hanya lezat, kacang hijau juga sangat bermanfaat bagi tubuh. Antara lain baik untuk menjaga sistem kekebalan tubuh, mengatasi anemia dan sembelit, termasuk membantu menjaga berat badan. Asik kan? Kamu bisa tetap menikmati es krim tanpa khawatir timbangan naik.
															<br><br>
															Rasa segar kacang hijau dengan paduan rasa susu yang lembut pada es krim Joyday seketika akan membuat kita &quot; bangkit &quot; kembali sejak gigitan pertama.
															<div class="row mt-5 d-none">
																<div class="col-md-3">
																	<h1 class="h4 text-center">Joyday Mung Bean</h1>
																	<img src="asset/static/joyday-product/MungBean.png" class="img-fluid mx-auto" />
																</div>
																<div class="col-md-9">
																	<div class="d-block h6">Name of Product（产品名称)	: Mung Bean</div>
																	<div class="d-block h6">Taste of Product（产品口味)	: Mung Beans and milk（牛奶绿豆</div>
																	<div class="d-block h6">Net content（净含量)	  	: 70gr</div>
																	<div class="d-block h6">Pcs / Carton			: 50 pcs</div>
																	<div class="d-block h6">Price				: Rp.3,500</div>
																	<div class="my-4 h6">
																		Product features(产品特色）: <br>
																		Mung bean ice milk, contains of natural mung bean paste soothing your day and bringing more freshness on summer heat.
																	</div>
																</div>
															</div>
															<br><br><br><br>
														</div>
														<div class="en">
															Joyday Mung Bean varian baru es krim Joyday di Indonesia yang menawarkan paduan rasa kacang hijau dan susu. Es krim susu kacang hijau ini mengandung sari kacang hijau asli untuk memberi kesegaran terutama ketika cuaca sedang terik.
															<br><br>
															Tidak hanya lezat, kacang hijau juga sangat bermanfaat bagi tubuh. Antara lain baik untuk menjaga sistem kekebalan tubuh, mengatasi anemia dan sembelit, termasuk membantu menjaga berat badan. Asik kan? Kamu bisa tetap menikmati es krim tanpa khawatir timbangan naik.
															<br><br>
															Rasa segar kacang hijau dengan paduan rasa susu yang lembut pada es krim Joyday seketika akan membuat kita &quot; bangkit &quot; kembali sejak gigitan pertama.
															<div class="row mt-5 d-none">
																<div class="col-md-3">
																	<h1 class="h4 text-center">Joyday Mung Bean</h1>
																	<img src="asset/static/joyday-product/MungBean.png" class="img-fluid mx-auto" />
																</div>
																<div class="col-md-9">
																	<div class="d-block h6">Name of Product（产品名称)	: Mung Bean</div>
																	<div class="d-block h6">Taste of Product（产品口味)	: Mung Beans and milk（牛奶绿豆</div>
																	<div class="d-block h6">Net content（净含量)	  	: 70gr</div>
																	<div class="d-block h6">Pcs / Carton			: 50 pcs</div>
																	<div class="d-block h6">Price				: Rp.3,500</div>
																	<div class="my-4 h6">
																		Product features(产品特色）: <br>
																		Mung bean ice milk, contains of natural mung bean paste soothing your day and bringing more freshness on summer heat.
																	</div>
																</div>
															</div>
															<br><br><br><br>
														</div>
													</div>
													<div class="is-pulled-right content-socmed">
														<ul class="sosmed-product-detaile">
															<li><a href="//www.facebook.com/JoydayIceCream/" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
															<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=<?php echo substr('Joyday Mung Bean',0,20)?>" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
															<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
															<li><a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?php echo current_url();?>."><i class="ti-email"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
                                        <!-- BLOG DETAIL 12 -->
										<div id="readmore_12" ss-container class="detail-blog-item animated">
											<div class="wrappadding animated fadeInUp">
												<div class="closecontent close-smalle"></div>
												<h1 class="titlepage">
                                                    <span class="id">Joyday Mocha Crispy</span>
                                                    <span class="en">Joyday Mocha Crispy</span>
                                                </h1>
												<div class="is-clearfix">
													<div class="is-pulled-left content-description">
														<div class="id">														
															Ada varian es krim baru dari Joyday Indonesia: Joyday Mocha Crispy. Es krim ini merupakan paduan rasa susu, kopi, dan cokelat. Paduan ketiga rasa ini biasanya dikenal sebagai rasa moccachino. Es krim Mocha Crispy, juga berlapis cokelat renyah untuk menambah cita rasa manis yang lezat.
															<br><br>
															Es krim ini tidak hanya memanjakan lidah. Wangi kopi dan cokelatnya begitu memanjakan; paduan rasa susu, kopi, cokelat yang lumer di mulut, sampai tekstur lapisan cokelat yang renyah, memberi kenikmatan tiada tara ketika dikunyah.
															<br><br>
															Saat mulai mengantuk atau merasa bosan di tengah kesibukan rutin, es krim ini akan membantu kita untuk tetap terjaga dan segar kembali melanjutkan aktivitas.
															<div class="row mt-5 d-none">
																<div class="col-md-3">
																	<h1 class="h4 text-center">Joyday Mung Bean</h1>
																	<img src="asset/static/joyday-product/MochaCrispy.png" class="img-fluid mx-auto" />
																</div>
																<div class="col-md-9">
																	<div class="d-block h6">Name of Product（产品名称)	: Mocha Crispy</div>
																	<div class="d-block h6">Taste of Product（产品口味)	: Milk, Coffee & Chocolate</div>
																	<div class="d-block h6">Net content（净含量)	  	: 70gr</div>
																	<div class="d-block h6">Pcs/ Carton			: 35 pcs</div>
																	<div class="d-block h6">Price				: Rp.4500</div>
																	<div class="my-4 h6">
																		Product features(产品特色）: <br>
																		Mocha crispy ice milk, the combination of coffee, chocolate and milk flavored, a famous mochaccino taste coating with a delicious crispy brown chocolate for your sweet enjoyment.
																	</div>
																</div>
															</div>
															<br><br><br><br>
														</div>
														<div class="en">
															Ada varian es krim baru dari Joyday Indonesia: Joyday Mocha Crispy. Es krim ini merupakan paduan rasa susu, kopi, dan cokelat. Paduan ketiga rasa ini biasanya dikenal sebagai rasa moccachino. Es krim Mocha Crispy, juga berlapis cokelat renyah untuk menambah cita rasa manis yang lezat.
															<br><br>
															Es krim ini tidak hanya memanjakan lidah. Wangi kopi dan cokelatnya begitu memanjakan; paduan rasa susu, kopi, cokelat yang lumer di mulut, sampai tekstur lapisan cokelat yang renyah, memberi kenikmatan tiada tara ketika dikunyah.
															<br><br>
															Saat mulai mengantuk atau merasa bosan di tengah kesibukan rutin, es krim ini akan membantu kita untuk tetap terjaga dan segar kembali melanjutkan aktivitas.
															<div class="row mt-5 d-none">
																<div class="col-md-3">
																	<h1 class="h4 text-center">Joyday Mung Bean</h1>
																	<img src="asset/static/joyday-product/MochaCrispy.png" class="img-fluid mx-auto" />
																</div>
																<div class="col-md-9">
																	<div class="d-block h6">Name of Product（产品名称)	: Mocha Crispy</div>
																	<div class="d-block h6">Taste of Product（产品口味)	: Milk, Coffee & Chocolate</div>
																	<div class="d-block h6">Net content（净含量)	  	: 70gr</div>
																	<div class="d-block h6">Pcs/ Carton			: 35 pcs</div>
																	<div class="d-block h6">Price				: Rp.4500</div>
																	<div class="my-4 h6">
																		Product features(产品特色）: <br>
																		Mocha crispy ice milk, the combination of coffee, chocolate and milk flavored, a famous mochaccino taste coating with a delicious crispy brown chocolate for your sweet enjoyment.
																	</div>
																</div>
															</div>															
															<br><br><br><br>
														</div>
													</div>
													<div class="is-pulled-right content-socmed">
														<ul class="sosmed-product-detaile">
															<li><a href="//www.facebook.com/JoydayIceCream/" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
															<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=<?php echo substr('Joyday Mocha Crispy',0,20)?>" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
															<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
															<li><a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?php echo current_url();?>."><i class="ti-email"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
                                        <!-- BLOG DETAIL 13 -->
										<div id="readmore_13" ss-container class="detail-blog-item animated">
											<div class="wrappadding animated fadeInUp">
												<div class="closecontent close-smalle"></div>
												<h1 class="titlepage">
                                                    <span class="id">Joyday Milky Milk</span>
                                                    <span class="en">Joyday Milky Milk</span>
                                                </h1>
												<div class="is-clearfix">
													<div class="is-pulled-left content-description">
														<div class="id">														
															Milky milk varian baru rasa susu untuk es krim Joyday di Indonesia. Inilah es krim susu klasik dari Joyday dengan susu asli yang berkualitas dan kaya rasa
															<br><br>
															Sejak dulu susu menjadi asupan nutrisi yang menyempurnakan komposisi makanan 4 sehat dan 5 sempurna. Karenanya sangat dianjurkan untuk mengkonsumsi susu setiap hari. Selain mengandung banyak protein, susu juga mengandung mineral yang berguna bagi tubuh.
															<br><br>
															Sangat menyenangkan menikmati es krim Joyday Milky Milk terutama ketika kita ingin bersantai sejenak setelah melalui berbagai aktivitas yang sangat padat.
															<div class="row mt-5 d-none">
																<div class="col-md-3">
																	<h1 class="h4 text-center">Joyday Milky Milk</h1>
																	<img src="asset/static/joyday-product/MilkyMilk.png" class="img-fluid mx-auto" />
																</div>
																<div class="col-md-9">
																	<div class="d-block h6">Name of Product（产品名称)	: Milky Milk</div>
																	<div class="d-block h6">Taste of Product（产品口味)	: Milk（牛奶</div>
																	<div class="d-block h6">Net content（净含量)	  	: 48gr</div>
																	<div class="d-block h6">Pcs/ Carton			: 50 pcs</div>
																	<div class="d-block h6">Price				: Rp.2,500</div>
																	<div class="my-4 h6">
																		Product features(产品特色）: <br>
																		Milky milk is the most classic ice milk of Joyday with a newly enhanced milk flavor for you, more tasty, more enjoyment. 
																	</div>
																</div>
															</div>
															<br><br><br><br>
														</div>
														<div class="en">
															Milky milk varian baru rasa susu untuk es krim Joyday di Indonesia. Inilah es krim susu klasik dari Joyday dengan susu asli yang berkualitas dan kaya rasa
															<br><br>
															Sejak dulu susu menjadi asupan nutrisi yang menyempurnakan komposisi makanan 4 sehat dan 5 sempurna. Karenanya sangat dianjurkan untuk mengkonsumsi susu setiap hari. Selain mengandung banyak protein, susu juga mengandung mineral yang berguna bagi tubuh.
															<br><br>
															Sangat menyenangkan menikmati es krim Joyday Milky Milk terutama ketika kita ingin bersantai sejenak setelah melalui berbagai aktivitas yang sangat padat.
															<div class="row mt-5 d-none">
																<div class="col-md-3">
																	<h1 class="h4 text-center">Joyday Milky Milk</h1>
																	<img src="asset/static/joyday-product/MilkyMilk.png" class="img-fluid mx-auto" />
																</div>
																<div class="col-md-9">
																	<div class="d-block h6">Name of Product（产品名称)	: Milky Milk</div>
																	<div class="d-block h6">Taste of Product（产品口味)	: Milk（牛奶</div>
																	<div class="d-block h6">Net content（净含量)	  	: 48gr</div>
																	<div class="d-block h6">Pcs/ Carton			: 50 pcs</div>
																	<div class="d-block h6">Price				: Rp.2,500</div>
																	<div class="my-4 h6">
																		Product features(产品特色）: <br>
																		Milky milk is the most classic ice milk of Joyday with a newly enhanced milk flavor for you, more tasty, more enjoyment. 
																	</div>
																</div>
															</div>
															<br><br><br><br>
														</div>
													</div>
													<div class="is-pulled-right content-socmed">
														<ul class="sosmed-product-detaile">
															<li><a href="//www.facebook.com/JoydayIceCream/" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
															<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=<?php echo substr('Joyday Mocha Crispy',0,20)?>" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
															<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
															<li><a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?php echo current_url();?>."><i class="ti-email"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<script type="text/javascript">
					$(document).ready(function(){
						/*$('.sidemenu').on('click',function(){
						  	var $this = $(this);
						  		//urlload = $this.attr('data-href');
						  		$('.sidemenu').removeClass('active');
						  		$this.addClass('active');
						  		$('.closecontent').trigger('click');
						  		//history.pushState({urlPath:'/index.php'}, $(this).attr('data-title'), $(this).attr('data-url'));
						  		/*setTimeout(function(){
						  			$('#main-blog').load(urlload);
						  		},1200);

						  });*/
						$(document).on('click','.readmore',function(){
							var target = $(this).attr('data-target');
							$('.nanas').addClass('hasContent')
							$(target).animate({
								'opacity' : 1,
								'top'  : '-1rem',
								'z-index':'105'
							},1000,function(){
								setTimeout(function(){
									$(target).removeClass('fadeInUp');
								},1000);
							}).addClass('fadeInUp');
						});
						$(document).on('click','.closecontent',function(){
							var $this = $(this);
							$this.closest('.detail-blog-item').animate({
								'opacity' : 0,
								'top'	  : '-1rem',
								'z-index' : '-1'
							},1000,function(){
								$('.nanas').removeClass('hasContent');
								// setTimeout(function(){ $('.detail-blog-item').removeClass('fadeOutDown animated'); },1000);
							}).addClass('fadeOutDown');
						});
						function resizeSidebarOverlay()
						{
							var locationside = $(".sidebare");
				        	var locright = ($(window).width() - (locationside.offset().left + locationside.outerWidth()));
				        	return locright;
					    }

					  $(window).resize(function(){
					  	$('.overlay-sidebar').css({'right' : resizeSidebarOverlay()});
					  });
					  $('.firstmenu').removeClass('active');
					  $('[blog]').addClass('active');
						$('.imgfooter').animate({'opacity':1, 'bottom' : 0},100,function(){
							$('.nanas8').animate({'opacity':1},100,function(){
								<?php if($this->input->get('content') != 'inload'){?>
									$('.socmed, .socmedmobile, .headerhome').animate({'opacity':1},1000).removeClass('fadeIn');
								<?php }?>
								$('.coklat1').animate({'opacity': 1},100,function(){
									setTimeout(function(){
										$('.nanas1').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
										$('.nanas2').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
									},300);
								}).addClass('fadeInUp');
									$('.coklat3').animate({'opacity': 1},100,function(){
										setTimeout(function(){
											$('.contentterms').animate({'opacity' : 1},1000,function(){

											}).addClass('fadeInUp');
										},1000);
									}).addClass('fadeInUp');
							}).addClass('fadeInUp');
						}).addClass('fadeInUp').prev('.copyright').delay(1000).animate({opacity:1},1000);
					});
				</script>
<?php
		if($this->input->get('content') != 'inload'){
	?>
			</div>
		</section>
		<div class="copyright absolute animated opacity0">
			<div class="footermenu">
				<a href="#contentsection" data-href="<?php echo base_url('home/terms');?>" data-url="home/terms" data-title="terms of use Joyday" class="firstmenu menufoot"><span class="en">Terms of Use</span><span class="id">Syarat &amp; Ketentuan</span></a>
				<a href="#contentsection" data-href="<?php echo base_url('home/privacy');?>" data-url="home/privacy" data-title="Privacy Policy Joyday" class="firstmenu menufoot"><span class="en">Privacy Policy</span><span class="id">Kebijakan Privasi</span></a>
				<a href="#contentsection" data-href="<?php echo base_url('home/contact');?>" data-url="home/contact" data-title="Contact Joyday" class="firstmenu menufoot"><span class="en">Contact Us</span><span class="id">Hubungi Kami</span></a>
			</div>
		</div>
		<div class="imgfooter imgfooter1 opacity0 animated">
			<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer.png')?>" alt="shadowyellow"/>
		</div>
		<div class="imgfooter imgfooter2 opacity0 animated">
			<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer2.png')?>" alt="shadowyellow"/>
		</div>
		<div class="imgfooter imgfooter3 opacity0 animated">
			<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer3.png')?>" alt="shadowyellow"/>
		</div>
	<?php }?>
