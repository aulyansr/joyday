<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<style type="text/css">
.coklat1{
  position: absolute;
  height: 180px;
  top: 12vh;
  right:3vw;
  z-index:10;
}
.coklat2{
  position: absolute;
  height: 180px;
  bottom: 3vh;
  right:1vw;
  z-index:5;
}
.coklat3{
  position: absolute;
  right: 0;
  bottom:20vh;
  width:200px;
}
.titlepage{
  font-size: 2rem;
  text-transform: uppercase;
  text-align: center;
  margin-bottom: 2rem;
  color:#0060af;
}
.paper p{
  text-transform: uppercase;
  letter-spacing: .2em;
  color:#0060af;
  font-weight: 300;
  font-size: .7em;
  line-height: 2;
  margin-bottom:1em;
}
.headerhome,.socmed{
  opacity: 1;
}

.text-benefit{
  font-weight:300;
  color: #1864ae;
  font-size:20px;
  line-height: 1;
}
.text-ds{
  font-weight:300;margin-top:20px;color: #1864ae;font-size:26px;
}

button:focus{
  outline: none;
}
.fancybox-close-small:focus:after {
  outline: none;
}
.fancybox-is-open .fancybox-bg{
  opacity: .5;
}
.fancybox-close-small:after{
  font:40px/30px Arial,"Helvetica Neue",Helvetica,sans-serif;
  color:#0060af;
}
.fancybox-slide > div{
  width:690px;
  max-width: 95%;
}
.hidden-content{
  color: #0060af;
}
.hidden-content h1{
  font-size: 2rem;
  text-align: center;
  margin-bottom: 20px;
  color: #0060af;
}
.hidden-content .columns{
  margin-bottom: 0;
}
.notification{
  font-family: 'COCOGOOSE';
  text-transform: uppercase;
  color: #0060af !important;
  letter-spacing: .4em;
  font-size: 11px;
}
.notification p{
  font-family: 'COCOGOOSE';
  text-transform: uppercase;
  color: #0060af !important;
  letter-spacing: .4em;
  font-size: 11px;
  margin-bottom:0;
}
.p-section{
  padding-top: 40vh !important;
}

.nanas8 {
  height: 80px;
  top: 0 !important;
  left: 20vw;
}
@media (max-width: 769px){
  
  .paper{
    margin: 0 0px;
  }
  .coklat1,.coklat2{
    right:-6vw;
  }
  .paper > .wrappadding{
    min-height: 75vh;
  }
  .nanas8 {
    height: 80px;
    top: 0;
    left: 20vw;
  }
}
@media (max-width: 740px)
{
  .coklat1,.coklat2{
    right:0vw;
    max-height: 100px;
  }
  .coklat3 {
    max-width: 120px;
  }
  .nanas1{
    max-height: 100px;
  }
  .nanas2{
    max-height: 100px;
  }
  .nanas2{
    max-height: 120px;
    bottom: 10vh;
  }
  .contentterms{
    height: 70vh;
  }
  .paper > .wrappadding{
    padding-bottom: 5rem;
  }
  .variantpros{
    height: 0;
  }
}
@media (max-width: 500px)
{
  .contentsection{
    padding-bottom: 27vh !important;
  }
  .text-ds{
    font-weight:300;margin-top:20px;color: #1864ae;font-size:19px;
  }
  .p-section{
    padding: 0 !important;
  }
  .text-benefit{
    font-size: 15px;
  }
  .contentterms{
    height: 80vh;
  }
  .titlepage{
    font-size:1.8rem;
  }
  .coklat2{
    bottom:10vh;
  }
  .coklat1, .coklat2{
    right: -7vw;
  }
  .coklat3{
    bottom: 30vh;
  }
  .paper > .wrappadding{
    padding:2rem 1.5rem;
    padding-bottom: 7rem;
  }
  .titlepage{
    font-size: 1.2rem;
    margin-bottom: 1.3rem;
  }
  .pb-0{
    padding-bottom: 0;
  }
  .hidden-content h1{
    font-size: 1.5rem;
    padding-top: 1rem;
  }
}
@media (max-width: 390px)
{
  .hidden-content{
    font-size: .9rem;
  }
}
@media (max-width: 330px)
{
  .hidden-content{
    font-size: .8rem;
  }
}
</style>
<?php
if($this->input->get('content') != 'inload'){
  ?>
  <div class="d-sm-block d-md-none" style="background: #fcd400;padding-top: 13vh;">
    <img class="w-100 " src="<?php echo base_url('asset/static/image-banner-mobile.png');?>" >
  </div>
  <section id="contentsection" class="contentsection d-none d-md-block" style="overflow:visible;">
    <div id="load" class="contenthomeslide contenview">
    <?php }?>
    <div id="wrappingshow">
      <div class="nanas">
        <img class="img-fluid nanas1 naanimation animated <?php echo $this->input->get('status') != '' ? '':'opacity0'; ?>" src="<?php echo base_url('asset/static/aeskrimbox.png');?>" style=" position: absolute;height: 45vh;top: 25vh;left: 50vw;right: 0;" >
        <img class="img-fluid nanas2 naanimation animated <?php echo $this->input->get('status') != '' ? '':'opacity0'; ?>" src="<?php echo base_url('asset/static/avatar1.png');?>"style="height:90vh;bottom:0;left:10vw;">
        <img class="img-fluid coklat3 naanimation animated <?php echo $this->input->get('status') != '' ? '':'opacity0'; ?>" src="<?php echo base_url('asset/static/borderbawah.png');?>"style="bottom:0;width:100%;bottom: 0px;width: 100%;height: auto;top: 67%;left: 0;z-index:0;">
        <img class="img-fluid coklat2 naanimation animated <?php echo $this->input->get('status') != '' ? '':'opacity0'; ?>" src="<?php echo base_url('asset/static/icecream-bottom.png');?>" style="     position: absolute;height: 35vh;bottom: 0;left: 70%;right: 10vh;" >
      </div>
      <script type="text/javascript">
      $(document).ready(function(){
        function updateVariant() {
          var allVals = [];
          $('#hidden-content :checked').each(function() {
            allVals.push($(this).val());
          });
          $('#variant').val(allVals);
        }
        $('.btn-selected').on('click',function(){
          updateVariant();
        });
        var width = $('.g-recaptcha').parent().width();
        if (width < 302) {
          var scale = width / 302;
          $('.g-recaptcha').css('transform', 'scale(' + scale + ')');
          $('.g-recaptcha').css('-webkit-transform', 'scale(' + scale + ')');
          $('.g-recaptcha').css('transform-origin', '0 0');
          $('.g-recaptcha').css('-webkit-transform-origin', '0 0');
        }
        $(window).resize(function(){
          width = $('.g-recaptcha').parent().width();
          if (width < 302) {
            var scale = width / 302;
            $('.g-recaptcha').css('transform', 'scale(' + scale + ')');
            $('.g-recaptcha').css('-webkit-transform', 'scale(' + scale + ')');
            $('.g-recaptcha').css('transform-origin', '0 0');
            $('.g-recaptcha').css('-webkit-transform-origin', '0 0');
          }
        });
        $('.firstmenu').removeClass('active');
        $('[beaseller]').addClass('active');
        <?php
        if($this->input->get('status') != ''){
          echo "$('.imgfooter').animate({'opacity':1, 'bottom' : 0},100,function(){}).prev('.copyright').animate({opacity:1},100);";
        }else{
          ?>
          $('.imgfooter').animate({'opacity':1, 'bottom' : 0},100,function(){
            $('.nanas8').animate({'opacity':1},100,function(){
              <?php if($this->input->get('content') != 'inload'){?>
                $('.socmed,.socmedmobile,.headerhome').animate({'opacity':1},1000).removeClass('fadeIn');
                <?php }?>
                $('.coklat1').animate({'opacity': 1},100,function(){
                  setTimeout(function(){
                    $('.nanas1').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
                    $('.nanas2').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
                  },300);
                }).addClass('fadeInUp');
                $('.coklat2').animate({'opacity': 1},100,function(){
                  $('.coklat3').animate({'opacity': 1},100,function(){
                    $('.socmed').animate({'opacity':1},1000).removeClass('fadeIn');
                    setTimeout(function(){
                      $('.contentterms').animate({'opacity' : 1},1000,function(){
                        
                      }).addClass('fadeInUp');
                    },1000);
                  }).addClass('fadeInUp');
                }).addClass('fadeInUp');
              }).addClass('fadeInUp');
            }).addClass('fadeInUp').prev('.copyright').delay(1000).animate({opacity:1},1000);
            <?php } ?>
            $('#propinsi_id').on('change',function(){
              var propinsi_id = $('#propinsi_id option:selected').val();
              $('#kabkota_id').load('<?php echo base_url('home/findcity');?>',{propinsi_id:propinsi_id});
            });
            
            $('.delete').on('click',function(){
              $(this).parent().remove();
            });
          });
          </script>
        </div>
        <?php
        if($this->input->get('content') != 'inload'){
          ?>
        </div>
      </section>
      <section class="p-section" style="background-color: #ffd400;background-repeat: no-repeat;background-size: 100%;background-position: center bottom;margin: 0;padding: 0;position: relative;overflow: visible;-webkit-transition: all 1s ease;-moz-transition: all 1s ease;-o-transition: all 1s ease;-ms-transition: all 1s ease;transition: all 1s ease;padding-top:10vh;padding-top:40vh;" >
        <div class="container">
          <img src="<?php echo base_url('asset/static/title-reseler.png');?>" alt="joyday become reseller" style="max-width:100%;display:block;margin:auto;">
          <div class="d-block mt-5">
            <div class="row " style="align-items:center;justify-content:center;">
              <div class="col-md-6">
                <div class="row" style="align-items:center;margin-bottom:15px;">
                  <div class="col col-md-1">
                    <img src="<?php echo base_url('asset/static/iocn1.png');?>" alt="resmi 1" style="width:40px;">
                  </div>
                  <div class="col-10">
                    <p class="text-benefit">
                      Memiliki hak jual eskrim Joyday secara resmi
                    </p>
                  </div>
                </div>
                <div class="row" style="align-items:center;margin-bottom:15px;">
                  <div class="col col-md-1">
                    <img src="<?php echo base_url('asset/static/iocn2.png');?>" alt="resmi 1" style="width:40px;">
                  </div>
                  <div class="col-10">
                    <p class="text-benefit">
                      Difasilitasi Freezer berteknologi  hemat energi dan ramah lingkungan
                    </p>
                  </div>
                </div>
                <div class="row" style="align-items:center;margin-bottom:15px;">
                  <div class="col col-md-1">
                    <img src="<?php echo base_url('asset/static/iocn3.png');?>" alt="resmi 1" style="width:40px;">
                  </div>
                  <div class="col-10">
                    <p class="text-benefit">
                      Bisa menambah penghasilan di toko atau rumah
                    </p>
                  </div>
                </div>
                <div class="row" style="align-items:center;margin-bottom:15px;">
                  <div class="col-1">
                    
                  </div>
                  
                  <div class="col-10 p-0 text-center text-md-left">
                    <a href="<?php echo base_url('home/simulasikeuntungan');?>" class="btn " style="background: #005b9c;color: white;margin-top:10px">
                      Lihat Simulasi
                    </a>
                    <p style="" class="text-ds">
                      Yuk langsung scroll ke bawah!
                    </p>
                  </div>
                </div>
              </div>
              <div class="col-md-2 d-none d-md-block">
                <img src="<?php echo base_url('asset/static/iabal.png');?>" alt="iqbal ramadhan joyday" style="width:100%;">
              </div>
            </div>
            
          </div>
          
        </section>
      </div>
      <div style="background-color: #ffd400">
        <img src="<?php echo base_url('asset/static/banner bawah.png');?>" alt="joyday 2019" style="width:100%;">
      </div>
      <section class="contentsection" style="background-color: #f8f5bd;background-repeat: no-repeat;background-size: 100%;background-position: center bottom;padding-bottom: 25vh;position: relative;overflow: hidden;-webkit-transition: all 1s ease;-moz-transition: all 1s ease;-o-transition: all 1s ease;-ms-transition: all 1s ease;transition: all 1s ease;">
        
        <img class="img-fluid nanas1 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas1.png');?>">
        <img class="img-fluid nanas2 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas2.png');?>" style="z-index:2;top:100%;">
        <img class="img-fluid nanas8 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas6.png');?>">
        <img class="img-fluid coklat1 naanimation animated opacity0" src="<?php echo base_url('asset/static/coklat/coklat1.png');?>" style="top: 100%;">
        <img class="img-fluid nanas5 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas5.png');?>">
        <img class="img-fluid coklat2 naanimation animated opacity0" src="<?php echo base_url('asset/static/coklat/coklat2.png');?>">
        <img class="img-fluid coklat3 naanimation animated opacity0" src="<?php echo base_url('asset/static/coklat/coklat3.png');?>" style="top:100%;">
        <div class="contentterms animated <?php echo $this->input->get('status') != '' ? '':'opacity0'; ?>" style="position:relative; height: 100%;">
          <div class="contentterm height100percent" style="background: #f8f5bd;">
            <div class="container height100percent container1000" style="">
              <h1 class="titlepage"><span class="en"> Ayo isi data berikut untuk jadi Mitra Joyday!</span><span class="id"> Ayo isi data berikut untuk jadi Mitra Joyday!</span></h1>
              <div class="row height100percent">
                <div class="col-md-12 height100percent ">
                  <div ss-container class="paper ss-container" style="max-height:100%;">
                    <div class="wrappadding" style="background-color: transparent;box-shadow: none;height: 100%;padding: 0;min-height: unset;">
                      <?php if(validation_errors()){?>
                        <div class="notification is-warning">
                          <button class="delete"></button>
                          <?php echo validation_errors();?>
                        </div>
                      <?php }?>
                      <?php if($this->session->flashdata('message')){?>
                        <div class="notification is-success">
                          <button class="delete"></button>
                          <?php echo $this->session->flashdata('message');?>
                        </div>
                      <?php }?>
                      
                      <form action="<?php echo base_url('home/send_beaseller')?>" method="post" id="dataform">
                        <div class="columns">
                          <div class="column">
                            <div class="field">
                              <div class="field-body">
                                <div class="field">
                                  <label class="label labelform mb-2" for="name"><span class="en">Nama Lengkap</span><span class="id">Nama Lengkap</span></label>
                                  <p class="control is-expanded">
                                    <input type="text" id="name" class="input name" name="name" value="<?php echo set_value('name')?>" required placeholder="Nama Lengkap" />
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="columns">
                          <div class="column">
                            <div class="field">
                              <div class="field-body">
                                <div class="field">
                                  <label class="label labelform mb-2" for="email">Email</label>
                                  <p class="control is-expanded">
                                    <input type="email" id="email" required class="input email" name="email" value="<?php echo set_value('email')?>" required placeholder="Email" />
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                          
                          <div class="column">
                            <div class="field">
                              <div class="field-body">
                                <div class="field">
                                  <label class="label labelform mb-2" for="phone"><span class="en">Nomor Handphone</span><span class="id">Nomor Handphone</span></label>
                                  <p class="control is-expanded">
                                    <input type="text" id="phone" onkeypress="return event.charCode >= 48 && event.charCode <= 57" class="input phone" name="phone" value="<?php echo set_value('phone')?>" required placeholder="Phone" />
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="columns">
                          <div class="column">
                            <div class="field">
                              <div class="field-body">
                                <div class="field">
                                  <label class="label labelform mb-2" for="address"><span class="en">Alamat lengkap</span><span class="id">Alamat lengkap</span></label>
                                  <p class="control is-expanded">
                                    <textarea class="textarea" rows="3" required name="address" placeholder="Address"><?php echo set_value('address')?></textarea>
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="column">
                            <div class="field">
                              <div class="field-body mb-15">
                                <div class="field">
                                  <label class="label labelform" for="propinsi_id"><span class="en">Propinsi</span><span class="id">Propinsi</span></label>
                                  <div class="control is-expanded">
                                    <div class="select is-fullwidth">
                                      <select name="propinsi_id" required id="propinsi_id" class="form-control">
                                        <option value="" class="propin" data-id="Pilih Propinsi" data-en="Choose State">
                                          <?php
                                            if(get_cookie('currentlang') == 'id'){
                                              echo 'Pilih Propinsi';
                                            }else{
                                              echo 'Choose State';
                                            }
                                          ?>
                                        </option>
                                        <?php
                                          $this->db->order_by('propinsi_name','asc');
                                          $pro = $this->db->get('propinsi');
                                          if(count($pro->result()) > 0){
                                            foreach ($pro->result() as $p) {
                                              echo '<option value="'. $p->propinsi_id .'">'. $p->propinsi_name .'</option>';
                                            }
                                          }
                                        ?>
                                      </select>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="field-body">
                                <div class="field">
                                  <label class="label labelform" required for="kabkota_id"><span class="en">Kota</span><span class="id">Kota</span></label>
                                  <div class="control is-expanded">
                                    <div class="select is-fullwidth">
                                      <select name="kabkota_id" id="kabkota_id" class="form-control">
                                        <option value="" class="kotas" data-id="Pilih Kota" data-en="Choose City">
                                          <?php
                                            if(get_cookie('currentlang') == 'id'){
                                              echo 'Pilih Kota';
                                            }else{
                                              echo 'Choose City';
                                            }
                                          ?>
                                        </option>
                                      </select>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="columns">
                          <div class="column">
                            <div class="field">
                              <div class="field-body mb-10">
                                <div class="field">
                                  <label class="label labelform mb-2"><span class="en">Please Check Captcha</span><span class="id">Mohon periksa Captcha</span></label>
                                  <script src='https://www.google.com/recaptcha/api.js'></script>
                                  <div class="g-recaptcha"  data-sitekey="6LdsblMUAAAAALd-yaO4tNjuKu86UdjXRCdDyrI_" style="transform:scale(1);-webkit-transform:scale(1);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="column">
                            <button class="button is-link is-outlined float-right btnform" style="height: 2.5rem; width:100px;text-align:center;">Kirim</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
        </section>
        <div class="copyright absolute animated opacity0">
          <div class="footermenu">
            <a href="#contentsection" data-href="<?php echo base_url('home/terms');?>" data-url="home/terms" data-title="terms of use Joyday" class="firstmenu menufoot"><span class="en">Terms of Use</span><span class="id">Syarat &amp; Ketentuan</span></a>
            <a href="#contentsection" data-href="<?php echo base_url('home/privacy');?>" data-url="home/privacy" data-title="Privacy Policy Joyday" class="firstmenu menufoot"><span class="en">Privacy Policy</span><span class="id">Kebijakan Privasi</span></a>
            <a href="#contentsection" data-href="<?php echo base_url('home/contact');?>" data-url="home/contact" data-title="Contact Joyday" class="firstmenu menufoot"><span class="en">Contact Us</span><span class="id">Hubungi Kami</span></a>
          </div>
        </div>
        <div class="imgfooter imgfooter1 opacity0 animated">
          <img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer.png')?>" alt="shadowyellow"/>
        </div>
        <div class="imgfooter imgfooter2 opacity0 animated">
          <img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer2.png')?>" alt="shadowyellow"/>
        </div>
        <div class="imgfooter imgfooter3 opacity0 animated">
          <img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer3.png')?>" alt="shadowyellow"/>
        </div>
      <?php }?>
      
