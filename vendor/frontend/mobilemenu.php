<div class="sidebarmenu">
<div class="close togglemenu">&times;</div>
<a class="logosidebar" href="<?php echo base_url();?>">
	<img class="img-fluid" src="<?php echo base_url('asset/static/logo.png');?>" alt="logo"/>
</a>
<ul>
	<li><a href="#contentsection" data-href="<?php echo base_url('home/about');?>" data-url="home/about" data-title="About Joyday" class="menuitem closesidebar">About</a></li>
	<li><a product href="javascript:void(0);" class="product-menu has-arrow">Product</a>
	<ul>
	  <li><a islink href="<?php echo base_url('product');?>" class="firstmenu closesidebar">Sticks</a></li>
	  <li><a islink href="<?php echo base_url('product/cone');?>" class="firstmenu closesidebar">Cone</a></li>
	  <li><a islink href="<?php echo base_url('product/cup');?>" class="firstmenu closesidebar">Cup</a></li>
	</ul>
	</li>
	<li><a islink href="<?php echo base_url('blog');?>" class="firstmenu closesidebar">BLOG</a></li>
	<!--
	<li><a islink href="<?php echo base_url('home/requestsample');?>" requestsample  class="firstmenu closesidebar">Request Sample</a></li>
	<li><a islink href="<?php echo base_url('home/beaseller');?>" beaseller class="firstmenu closesidebar">Be a Seller</a></li>
	-->
	<li><a islink href="<?php echo base_url('home/contact');?>" requestsample  class="firstmenu closesidebar">Request Sample</a></li>
	<li><a islink href="<?php echo base_url('home/beaseller');?>" beaseller class="firstmenu closesidebar">Be a Seller</a></li>
</ul>
<div class="wrapsocmedmob" style="max-width:220px; margin:0 auto; padding:15px; margin-top:15px;">
	<div class="socmedmobile animated opacity0">
		<div class="d-table height100percent">
			<div class="d-table-cell middle">
				<a href="//www.facebook.com/JoydayIceCream/" target="_blank" class="fblink"><i class="fa fa-facebook"></i></a>&nbsp;
				<a href="//twitter.com/JoydayIceCream?lang=en" target="_blank"class="twlink"><i class="fa fa-twitter"></i></a>&nbsp;
				<a href="https://www.instagram.com/joydayicecream" target="_blank" class="iglink"><i class="fa fa-instagram"></i></a>&nbsp;
				<a href="tel:+62800-1-868686" target="_blank" class="tellink"><i class="fa fa-phone"></i></a>
			</div>
		</div>
	</div>
</div>
</div>
