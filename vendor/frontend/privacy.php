<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<style type="text/css">
.coklat1{
	position: absolute;
    height: 180px;
    top: 12vh;
    right:3vw;
    z-index:10;
}
.coklat2{
	position: absolute;
    height: 180px;
    bottom: 3vh;
    right:1vw;
    z-index:5;
}
.coklat3{
	position: absolute;
    right: 0;
    bottom:20vh;
    width:200px;
}
.titlepage{
	font-size: 2rem;
	text-transform: uppercase;
	text-align: center;
	margin-bottom: 2rem;
	color:#0060af;
}
.paper p{
	text-transform: uppercase;
	letter-spacing: .2em;
	color:#0060af;
	font-weight: 300;
	font-size: .7em;
	line-height: 2;
	margin-bottom:1em;
}
p{
	text-indent:4rem;
	text-align: justify;
}
p::first-letter,p .big{
	font-size: 1.5em;
	line-height: 1;
}
@media (max-width: 769px){
	.paper{
		margin: 0 0px;
	}
	.coklat1,.coklat2{
		right:-6vw;
	}
	.paper > .wrappadding{
		min-height: 75vh;
	}
	.nanas8 {
	    height: 80px;
	    top: 18vh;
	    left: 20vw;
	}
}
@media (max-width: 740px)
{
	.coklat1,.coklat2{
		right:0vw;
		max-height: 100px;
	}
	.coklat3 {
	    max-width: 120px;
	}
	.nanas1{
		max-height: 100px;
	}
	.nanas2{
		max-height: 100px;
	}
	.nanas2{
		max-height: 120px;
		bottom: 10vh;
	}
	.contentterms{
		height: 70vh;
	}
}@media (max-width: 500px)
{
	p{
		text-indent:2rem;
		text-align: left;
	}
	.contentterms{
		height: 80vh;
	}
	.paper > .wrappadding{
		padding: 1.5rem 1.5rem 4rem 1.5rem;
	}
	.titlepage{
		font-size:1.3rem;
		margin-bottom: 1rem;
	}
	.coklat2{
		bottom:10vh;
	}
	.coklat1, .coklat2{
		right: -7vw;
	}
	.coklat3{
		bottom: 30vh;
	}
}
</style>
<?php
		if($this->input->get('content') != 'inload'){
	?>
	<section id="contentsection" class="contentsection">
		<div id="load" class="contenthomeslide contenview">
	<?php }?>
			<div id="wrappingshow">
				<div class="nanas">
					<img class="img-fluid nanas1 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas1.png');?>">
					<img class="img-fluid nanas2 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas2.png');?>">
					<img class="img-fluid nanas8 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas6.png');?>">
					<img class="img-fluid coklat1 naanimation animated opacity0" src="<?php echo base_url('asset/static/coklat/coklat1.png');?>">
					<img class="img-fluid nanas5 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas5.png');?>">
					<img class="img-fluid coklat2 naanimation animated opacity0" src="<?php echo base_url('asset/static/coklat/coklat2.png');?>">
					<img class="img-fluid coklat3 naanimation animated opacity0" src="<?php echo base_url('asset/static/coklat/coklat3.png');?>">
					<div class="contentterms animated opacity0">
						<div class="contentterm height100percent">
							<div class="container height100percent container1000">
								<div class="row height100percent">
									<div  class="col-md-12 height100percent ">
										<div ss-container class="paper ss-container">
											<div class="wrappadding">
												<div class="en">
													<h1 class="titlepage">Privacy Policy</h1>
													<p>PT. Green Asia Food Indonesia is committed to protecting your privacy. <span class="big">T</span>his Privacy Statement underscores the type of information collected by PT. Green Asia Food Indonesia on this site, and how we use and protect that information. <span class="big">T</span>his statement also tells you what to do if you do not want to share your personal information when visiting our site, and how you can change the information you have provided to us. <span class="big">T</span>his Privacy Statement is only valid for this site. For example, does not apply to banners, prize competitions, and advertisements or promotions on sites owned by other third parties that we sponsor or where we participate in them.</p>
													<p>(i) Your acceptance of this Privacy Statement</p>
													<p>By using our site, you agree to the terms of our Privacy Statement as we have published here. <span class="big">I</span>f you do not agree to any of the terms of this Standard, please do not provide your Personal Information (as defined below) on this site. <span class="big">Y</span>ou can not provide information about anyone else on this site without their consent. <span class="big">I</span>f you choose not to provide your Personal Information on this site, you may not be able to do certain things, such as access to certain areas of the site, participate in lottery or contest programs, submit sample requests, or email us.</p>
													<p>(ii) Collection of Personal Information</p>
													<p>As stated in the Privacy Statement, "Personal Information" means personally identifiable information such as your name, street address, telephone number, credit card information and e-mail address. <span class="big">T</span>he site you are entering may request some or all of these Personal Information types. <span class="big">R</span>est assured that PT. Green Asia Food Indonesia will not solicit any Personal Information from your visit to our site unless you provide this information voluntarily. <span class="big">W</span>hen you provide Personal Information to us, you authorize us to use the information in accordance with the terms of this Privacy Statement.</p>
													<p>The site you are entering may also request Personal Information from you about a third party (for example, the site may allow you to send an electronic 'postcard' to a friend, in which case we will need the person's name and email address). <span class="big">P</span>lease keep in mind that we use such information only for certain purposes (eg for postcards) and we will no longer contact such third parties by electronic mail unless and until such third party contacts us.</p>
													<p>(iii) How We Use Your Information</p>
													<p>When you provide Personal Information on this site, we will use it for the instantaneous purpose on which the information is requested (for example, to include you in a promotion or send you samples). <span class="big">S</span>uch information may also be used to send you information and / or offers about our merchandise and or services that we believe will be of interest to you. <span class="big">W</span>e can also ask you if we can share it with other companies in PT. Green Asia Food Indonesia and other carefully selected companies that may want to send you information and / or offers regarding their merchandise / services. <span class="big">H</span>owever, you may also choose to expel this kind of contact in the future.</p>
													<p>Sometimes we may use third parties to process or analyze the data we collect on our site, including Personal Information. <span class="big">I</span>n addition, if you order products or services from us, we may share Personal Information with suppliers and other third parties to enable delivery of products or services. <span class="big">T</span>his third party is not permitted to use your Personal Information in any other way. <span class="big">C</span>urrently, this site is managed by a manager contracted by PT. Green Asia Food Indonesia. <span class="big">T</span>his Manager is bound by the contract to implement measures to protect your personal data and process it in accordance with the approval of PT. Green Asia Food Indonesia. <span class="big">B</span>y submitting your personal data through this website, you agree that the data will be processed by the manager on behalf of PT. Green Asia Food Indonesia. <span class="big">L</span>astly, please keep in mind that we may release your Personal Information if required by law or by a search warrant, judgment or court order.</p>
													<p>(iv) Any Other Information We May Collect</p>
													<p>You may have heard of "cookies" which are files or chunks of information that your browser stores on your computer's hard drive. <span class="big">P</span>T. Green Asia Food Indonesia may use cookies to collect other information during your visit to our site, such as the specific areas of the site you visit and the services you use through our site. <span class="big">W</span>e collect this information to tailor our sites and products to better suit your interests and needs. <span class="big">C</span>ookies can also be used to help speed up your future activities on our site. <span class="big">F</span>or example, a site may recognize that you provide Personal Information to us and not ask for the same information a second time. <span class="big">M</span>ost browsers are initially set to accept cookies. <span class="big">I</span>f you prefer, you can set your browser to refuse cookies or notify you when a cookie is sent. <span class="big">T</span>he disapproval of cookies on the sites you are entering may cause inability to visit certain areas of the site or to receive personal information when you visit the site</p>
													<p>In addition we are constantly working to improve how we promote our site. <span class="big">I</span>n order to help us do so, we may measure the effectiveness of our site presence by determining when you click on to our site. <span class="big">S</span>ite measurement statistics can be used to personalize your experience in our site search, as well as to evaluate - anonymously and in an average - statistics on site usage. <span class="big">I</span>nformation about your computer, such as your IP address (the number assigned to your computer whenever you browse the internet), the type of Internet browser and the type of operating system you use may also be collected and linked to your Personal Information. <span class="big">T</span>his is to ensure our site presents the best web experiences for visitors, and that they are an effective information resource.</p>
													<p>Some of the pages you visit may also collect information through the use of pixel tags (also called clear gifs) which can be shared with third parties that directly support the promotion and development activities of PT Green Asia Food website. <span class="big">F</span>or example, site usage information about visitors to our site could be shared with our third party advertising agencies in order to better target the banner of internet ads on Joyday sites. <span class="big">I</span>nformation collected through the use of this tag pixel can not be personally identifiable, however, it may be linked to your Personal Information.</p>
													<p>Finally, we may supplement the information you provide, with any other information we have received and / or obtained from third parties. <span class="big">W</span>e do it for marketing purposes to deliver products and services that are more targeted to meet your needs.</p>
													<p>(v) Information Security</p>
													<p>We take reasonable precautions to ensure the security of your Personal Information. <span class="big">A</span>ll personally identifiable information is subject to access restriction arrangements to prevent unauthorized access, modification, or misuse.</p>
													<p>(vi) Changes to Our Statement</p>
													<p>PT. Green Asia Food Indonesia reserves the right to change or delete this Privacy Statement in accordance with our policies. <span class="big">W</span>e will publish each new Statement here, and we encourage you to continue to visit this area to keep up-to-date.</p>
												</div>
												<div class="id">
													<h1 class="titlepage">Kebijakan Privasi</h1>
													<p>PT. Green Asia Food Indonesia memiliki komitmen untuk melindungi kerahasiaan pribadi Anda. <span class="big">P</span>ernyataan Privasi ini menggaris bawahi jenis informasi yang dikumpulkan oleh PT. Green Asia Food Indonesia pada situs ini, dan bagaimana kami menggunakan dan melindungi informasi tersebut. Pernyataan ini juga memberitahukan pada Anda apa yang harus dilakukan jika Anda tidak ingin membagi informasi pribadi Anda ketika mengunjungi situs kami, dan bagaimana Anda dapat mengubah informasi yang telah Anda berikan kepada kami. <span class="big">P</span>ernyataan Privasi ini hanya berlaku bagi situs ini. Sebagai contoh, tidak berlaku untuk spanduk, kompetisi hadiah, dan iklan atau promosi pada situs yang dimiliki oleh pihak ketiga lainnya yang kami sponsori atau di mana kami berpartisipasi di dalamnya.</p>
													<p>(i) Persetujuan Anda atas Pernyataan Privasi Ini</p>
													<p>Dengan menggunakan situs kami, berarti Anda setuju dengan ketentuan Pernyataan Privasi kami seperti yang telah kami publikasikan di sini. <span class="big">J</span>ika Anda tidak setuju dengan syarat-syarat mana pun dalam Pernyataan ini, harap tidak memberikan Informasi Pribadi Anda (seperti didefinisikan di bawah) di situs ini. <span class="big">A</span>nda tidak dapat memberikan informasi tentang orang lain dalam situs ini tanpa persetujuan mereka. Jika Anda memilih untuk tidak memberikan Informasi Pribadi Anda di situs ini, Anda mungkin tidak dapat melakukan hal-hal tertentu, seperti akses pada daerah tertentu dari situs, turut serta dalam program undian atau kontes, mengajukan permintaan sampel, atau mengirim surat elektronik pada kami.</p>
													<p>(ii) Pengumpulan Informasi Pribadi</p>
													<p>Sebagaimana dinyatakan dalam Pernyataan Privasi, “Informasi Pribadi” artinya informasi yang bisa diidentifikasi secara pribadi seperti nama, alamat jalan, nomor telepon, informasi kartu kredit, dan alamat surat elektronik Anda. <span class="big">S</span>itus yang Anda masuki mungkin meminta beberapa atau semua jenis Informasi Pribadi ini. <span class="big">Y</span>akinlah bahwa PT. Green Asia Food Indonesia tidak akan meminta Informasi Pribadi apa pun dari kunjungan Anda ke situs kami kecuali Anda memberikan informasi ini secara sukarela. <span class="big">K</span>etika Anda memberi Informasi Pribadi kepada kami, Anda memberi kuasa kepada kami untuk menggunakan informasi itu sesuai dengan syarat-syarat Pernyataan Privasi ini.</p>
													<p>Situs yang Anda masuki bisa juga meminta Informasi Pribadi dari Anda tentang pihak ketiga (misalnya, situs mungkin memperbolehkan Anda mengirim ‘kartu pos’ elektronik kepada seorang teman, dalam kasus ini kami akan membutuhkan nama dan alamat surat elektronik orang tersebut). <span class="big">H</span>arap diingat bahwa kami menggunakan informasi-informasi tersebut hanya untuk tujuan tertentu (misalnya untuk mengirimkan kartu pos) dan kami tidak akan menghubungi lagi pihak ketiga tersebut lewat surat elektronik kecuali dan hingga pihak ketiga tersebut menghubungi kami.</p>
													<p>(iii) Bagaimana Kami Menggunakan Informasi Anda</p>
													<p>Ketika Anda menyediakan Informasi Pribadi di situs ini, kami akan menggunakannya untuk tujuan seketika yang menjadi alasan informasi tersebut diminta (misalnya, untuk memasukkan Anda dalam sebuah promosi atau mengirimkan Anda sampel). <span class="big">I</span>nformasi tersebut juga dapat kami gunakan untuk mengirimi Anda informasi dan atau penawaran tentang barang dagangan dan atau layanan kami yang kami yakini akan menarik minat Anda. <span class="big">K</span>ami juga bisa menanyai Anda apakah kami bisa membaginya dengan perusahaan lain di PT. Green Asia Food Indonesia dan perusahaan yang telah dipilih dengan seksama lainnya yang mungkin ingin mengirimi Anda informasi dan/atau penawaran mengenai barang dagangan/layanan mereka. <span class="big">N</span>amun, Anda juga dapat memilih untuk mengeluarkan kontak semacam ini di masa depan.</p>
													<p>Terkadang kami bisa menggunakan pihak ketiga untuk memproses atau menganalisa data yang kami kumpulkan di situs kami, termasuk Informasi Pribadi. <span class="big">S</span>ebagai tambahan, jika Anda memesan produk atau layanan dari kami, kami bisa membagi Informasi Pribadi dengan pemasok dan pihak ketiga lain untuk memungkinkan pengantaran produk atau layanan. <span class="big">P</span>ihak ketiga ini tidak kami ijinkan untuk menggunakan Informasi Pribadi Anda dengan cara lain. <span class="big">S</span>aat ini, situs ini dikelola oleh pengelola yang dikontrak oleh PT. Green Asia Food Indonesia. <span class="big">P</span>engelola ini terikat oleh kontrak untuk mengimplementasikan langkah-langkah dalam melindungi data pribadi Anda dan memprosesnya sesuai persetujuan pedoman PT. Green Asia Food Indonesia. <span class="big">D</span>engan mengirimkan data pribadi Anda lewat website ini, Anda setuju data tersebut akan diproses oleh pengelola atas nama PT. Green Asia Food Indonesia.
													<span class="big">T</span>erakhir, harap diingat bahwa kami dapat merilis Informasi Pribadi Anda jika diharuskan oleh hukum atau dengan surat izin menggeledah, keputusan atau perintah pengadilan.</p>
													<p>(iv) Informasi Lain yang Mungkin Kami Kumpulkan</p>
													<p>Anda mungkin pernah mendengar tentang “cookies” yang merupakan berkas atau potongan informasi yang disimpan oleh browser Anda pada hard drive komputer Anda. PT. Green Asia Food Indonesia mungkin menggunakan cookies untuk mengumpulkan informasi lainnya selama kunjungan Anda ke situs kami, seperti area situs tertentu yang Anda kunjungi dan layanan yang Anda gunakan melalui situs kami. <span class="big">K</span>ami mengumpulkan informasi ini untuk menyesuaikan situs dan produk kami supaya dapat lebih baik lagi menurut minat dan kebutuhan Anda. <span class="big">C</span>ookies juga dapat digunakan untuk membantu mempercepat aktivitas masa depan Anda di situs kami. <span class="big">S</span>ebagai contoh, sebuah situs bisa mengenali bahwa Anda menyediakan Informasi Pribadi untuk kami dan tidak meminta informasi yang sama untuk kedua kalinya. <span class="big">K</span>ebanyakan browser pada mulanya diatur untuk menerima cookie. <span class="big">J</span>ika Anda lebih suka, Anda bisa mengatur browser Anda untuk menolak cookie atau memberitahukan pada Anda ketika cookie dikirimkan. <span class="big">P</span>enolakan cookie di situs yang Anda masuki bisa menyebabkan ketidakmampuan mengunjungi area tertentu dalan situs atau untuk menerima informasi yang pribadi ketika Anda mengunjungi situs.</p>
													<p>Sebagai tambahan kami terus-menerus berusaha meningkatkan bagaimana kami mempromosikan situs kami. <span class="big">D</span>emi membantu kami melakukannya, kami mungkin mengukur efektivitas kehadiran situs kami dengan menentukan kapan Anda mengklik menuju situs kami. <span class="big">S</span>tatistik pengukuran situs bisa digunakan untuk mempersonalisasi pengalaman Anda dalam penelusuran situs kami, juga untuk mengevaluasi – secara anonim dan dalam rata-rata – statistik tentang penggunaan situs. <span class="big">I</span>nformasi tentang komputer Anda, seperti alamat IP Anda (angka yang ditetapkan ke komputer Anda kapan pun Anda menelusuri internet), jenis browser Internet dan jenis sistem operasi yang Anda gunakan bisa juga dikumpulkan dan dihubungkan ke Informasi Pribadi Anda. <span class="big">I</span>ni untuk memastikan situs kami menyajikan pengalaman web terbaik untuk para pengunjung, dan bahwa mereka adalah sumber daya informasi yang efektif.</p>
													<p>Beberapa halaman yang Anda kunjungi juga mungkin mengumpulkan informasi melalui penggunaan pixel tags (disebut juga clear gifs) yang bisa dibagi dengan pihak ketiga yang langsung mendukung aktivitas promosi dan pengembangan situs PT Green Asia Food. <span class="big">S</span>ebagai contoh, informasi penggunaan situs tentang pengunjung ke situs kami bisa dibagi dengan agen periklanan pihak ketiga kami demi melakukan penentuan sasaran pada spanduk iklan internet pada situs Joyday dengan lebih baik. <span class="big">I</span>nformasi yang dikumpulkan melalui penggunaan pixel tag ini tidak bisa diidentifikasi secara pribadi, bagaimana pun, walau pun bisa dikaitkan ke Informasi Pribadi Anda.</p>
													<p>Terakhir, kami dapat menambah informasi yang Anda berikan, dengan informasi lain yang kami kami terima dan atau kami miliki yang berasal dari pihak ketiga. <span class="big">K</span>ami melakukannya untuk tujuan pemasaran demi memberikan produk dan layanan yang lebih sesuai sasaran dalam memenuhi kebutuhan Anda.</p>
													<p>(v) Keamanan Informasi</p>
													<p>Kami mengambil pencegahan yang memungkinkan untuk menjaga keamanan Informasi Pribadi Anda. <span class="big">S</span>emua informasi yang bisa diidentifikasi secara pribadi tunduk pada pengaturan pembatasan akses untuk mencegah akses yang tidak sah, modifikasi, atau penyalahgunaan.</p>
													<p>(vi) Perubahan Pada Pernyataan Kami</p>
													<p>PT. Green Asia Food Indonesia memiliki hak untuk mengubah atau menghapus Pernyataan Privasi ini sesuai kebijakan kami. <span class="big">K</span>ami akan mempublikasikan tiap Pernyataan baru di sini, dan kami mendorong Anda untuk terus mengunjungi daerah ini untuk tetap mendapatkan informasi terbaru.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<script type="text/javascript">
					$(document).ready(function(){
						$('.imgfooter').animate({'opacity':1, 'bottom' : 0},100,function(){
							$('.nanas8').animate({'opacity':1},100,function(){
								<?php if($this->input->get('content') != 'inload'){?>
									$('.socmed,.socmedmobile,.headerhome').animate({'opacity':1},1000).removeClass('fadeIn');
								<?php }?>
								$('.coklat1').animate({'opacity': 1},100,function(){
									setTimeout(function(){
										$('.nanas1').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
										$('.nanas2').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
									},300);
								}).addClass('fadeInUp');
								$('.coklat2').animate({'opacity': 1},100,function(){
									$('.coklat3').animate({'opacity': 1},100,function(){
										$('.socmed').animate({'opacity':1},1000).removeClass('fadeIn');
										setTimeout(function(){
											$('.contentterms').animate({'opacity' : 1},1000,function(){

											}).addClass('fadeInUp');
										},1000);
									}).addClass('fadeInUp');
								}).addClass('fadeInUp');
							}).addClass('fadeInUp');
						}).addClass('fadeInUp').prev('.copyright').delay(1000).animate({opacity:1},1000);
					});
				</script>
			</div>
<?php
		if($this->input->get('content') != 'inload'){
	?>
			</div>
		</section>
		<div class="copyright absolute animated opacity0">
			<div class="footermenu">
				<a href="#contentsection" data-href="<?php echo base_url('home/terms');?>" data-url="home/terms" data-title="terms of use Joyday" class="firstmenu menufoot"><span class="en">Terms of Use</span><span class="id">Syarat &amp; Ketentuan</span></a>
				<a href="#contentsection" data-href="<?php echo base_url('home/privacy');?>" data-url="home/privacy" data-title="Privacy Policy Joyday" class="firstmenu menufoot"><span class="en">Privacy Policy</span><span class="id">Kebijakan Privasi</span></a>
				<a href="#contentsection" data-href="<?php echo base_url('home/contact');?>" data-url="home/contact" data-title="Contact Joyday" class="firstmenu menufoot"><span class="en">Contact Us</span><span class="id">Hubungi Kami</span></a>
			</div>
		</div>
		<div class="imgfooter imgfooter1 opacity0 animated">
			<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer.png')?>" alt="shadowyellow"/>
		</div>
		<div class="imgfooter imgfooter2 opacity0 animated">
			<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer2.png')?>" alt="shadowyellow"/>
		</div>
		<div class="imgfooter imgfooter3 opacity0 animated">
			<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer3.png')?>" alt="shadowyellow"/>
		</div>
	<?php }?>
