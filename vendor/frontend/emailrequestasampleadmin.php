<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="margin:0 auto;font-family:Arial,Helvetica,sans-serif">
    <tbody>
        <tr>
            <td align="left" valign="top" height="30">&nbsp; </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <h1 style="color:#333;font-size:1.5em">Hi, Admin</h1>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <p style="color:#7e7e7e;line-height:25px">Berikut ini data customer yang telah mengisi data formulir “Request Sample” sebagai berikut:</p>
                <ul>
                  <li style="color:#7e7e7e;line-height:1.4"><?php echo $name;?></li>
                  <li style="color:#7e7e7e;line-height:1.4"><?php echo $email;?></li>
                  <li style="color:#7e7e7e;line-height:1.4"><?php echo $address;?><br/><?php echo $kabkota;?><br/><?php echo $propinsi;?></li>
                  <li style="color:#7e7e7e;line-height:1.4"><?php echo strtoupper($phone);?></li>
                </ul>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" height="30"><hr/></td>
        </tr>
        <tr>
            <td align="left" valign="top" height="30">
              <p style="color:#7e7e7e;line-height:1.4">ORDER : </p><br/>
              <?php
              $searchString = ',';
                if( strpos($variant, $searchString) !== false ) {
                  $var = explode(",",$variant);
                  echo '<ul>';
                  for($i=0;$i<sizeof($var);$i++){
                    echo '<li style="color:#7e7e7e;line-height:1.4">'.$var[$i].'</li>';
                  }
                  echo '</ul>';
                }else{
                  echo '<p style="color:#7e7e7e;line-height:1.4">' .$variant .'</p><br/>';
                }
              ?>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
              <p style="color:#7e7e7e;line-height:1.4">Mohon data tersebut untuk segera ditindaklanjuti.</p>

            </td>
        </tr>
    </tbody>
</table>
