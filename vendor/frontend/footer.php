<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php GloadViews('mobilemenu');?>
<script type="text/javascript">
	window.addEventListener("DOMContentLoaded", function(event) {
		JavaScript.load('<?php echo GassetURL('asset/font-awesome/css/font-awesome.min.css');?>',function(){
			JavaScript.load('<?php echo GassetURL('asset/themify-icons/themify-icons.css');?>',function(){},'css');
			JavaScript.load('<?php echo GassetURL('asset/js/popper.js');?>',function(){
				JavaScript.load('<?php echo GassetURL('asset/js/bootstrap.min.js');?>',function(){
            $(document).ready(function(){
							$(document).on('click','.changelanguange',function(){
								EC('currentlang');
	              var currentlang = $(this).attr('id');
	              if(currentlang == 'id'){
	                $('.en').hide();
	                $('.id').fadeIn('slow');
	              }else {
                        $('.id').hide();
                        $('.en').fadeIn('slow');
                    }
                    if($('.propin').length > 0){
                        var propinsi = $('.propin').attr('data-'+currentlang);
                        $('.propin').text(propinsi);
                        var kota = $('.kotas').attr('data-'+currentlang);
                        $('.kotas').text(kota);
                    }
	              CC('currentlang',currentlang,20);
	            });
              $('.togglemenu').on('click',function(){
                  $('body').toggleClass('showsidebar');
                });
              $('.has-arrow').on('click',function(){
                  $(this).toggleClass('active');
                });
              $('a[href*="#"]')
                .not('[href="#"]')
                .not('[href="#0"]')
                .click(function(event) {
                  var $this = $(this);
                  if ( location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname)
                  {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                      currentScrollPosition = target.offset().top
                      event.preventDefault();
                      $('html, body').animate({
                        scrollTop: target.offset().top
                      }, 1000, function() {
                        var $target = $(target);
                        $target.focus();
                        if ($target.is(":focus")) {
                          return false;
                        } else {
                          $target.attr('tabindex','-1');
                          $target.focus();
                        };
                      });
                    }
                  }
                  if($(this)[0].hasAttribute('data-href')){
                    var $name = $(this).attr('href');
                    var $url = $(this).attr('data-url');
                    history.pushState({urlPath:'/'+$url}, $(this).attr('data-title'), $(this).attr('data-href'));
                    $('#load').load($(this).attr('data-href')+'?content=inload',function(){
                      positiontarget = $('#contentsection').offset().top;
                      if(RC('bgColor')){
												$('#contentsection').attr('style','background-color :' + RC('bgColor') + ';');
												$('#contentsection').addClass(RC('bgClass'));
                        $('#meta-theme').attr('content',RC('bgColor'));
                      }
                    });
                  }
                  if($(this).hasClass('firstmenu'))
                  {
                    $('.firstmenu').removeClass('active');
                    $(this).addClass('active');
										setTimeout(function(){
											var currlang = RC('currentlang');
											if(currlang == 'id'){
				                $('.en').css({'display':'none'});
				                $('.id').css({'display':'inline-block'});
				              }else{
												$('.id').css({'display':'none'});
				                $('.en').css({'display':'inline-block'});
											}
										},1200);
                  }
                  if($(this).hasClass('closesidebar')){
                    setTimeout(function(){
                      $('body').removeClass('showsidebar');
                    },800);
                  }
                });

                <?php
                  if($this->uri->segment(1) != ''){
                ?>
                  var contentsection = $('#contentsection').offset().top;
                  $('[islink]').on('click',function(e){
                  if(contentsection > 0){
                    $('.firstmenu').removeClass('active');
                    $(this).addClass('active');
                    var href = this.href;
                    var datatarget = $('#contentsection');
                    if($(this).hasClass('closesidebar')){
                        setTimeout(function(){
                          $('body').removeClass('showsidebar');
                        },800);
                      }
                      e.preventDefault();
                      $('html, body').animate({
                        scrollTop: datatarget.offset().top
                      }, 1000, function() {
                        window.location = href;
                      });
                      contentsection = 0;
                  }else{
                    return true;
                  }

                });
                <?php
                  }
                ?>

                $('.logo').on('click',function(e){
                  var href = this.href;
                  e.preventDefault();
                  EC('bgColor');
									EC('bgClass');
                  window.location = href;
                });
								if(RC('bgClass')){
									$('#contentsection').addClass(RC('bgClass'));
								}
              $('.share').click(function(e){
                  e.preventDefault();
                  e.stopPropagation();
                  $fb = $(this);
                  $fb.customerPopup(e);
                  return false;
                });
            });
            JavaScript.load('<?php echo GassetURL('asset/fancybox/jquery.fancybox.css');?>',function(){
                JavaScript.load('<?php echo GassetURL('asset/fancybox/jquery.fancybox.js');?>',function(){
                    $('.fancybox').fancybox({
                        fitToView : true,
                        autoSize  : true,
                        padding   : 0,
                        clickSlide: false,
                        clickOutside: false,
                        toolbar  : false,
                        smallBtn : false,
                        type    : 'iframe',
                        iframe    : {
                          css : {
                            width : '85%',
                            height : '85%',
                            maxWidth : '90%',
                            maxHeight : '90%'
                          },
                          scrolling : 'no',
                          preload   : true
                        },
                        helpers : {
                          overlay : {
                            css : { 'overlay' : 'hidden' }
                          }
                        },
                    });
                 },'js','body');
            },'css');
          },'js','body');
			},'js','body');
		},'css');
	});
</script>

<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=1386310&mt_adid=219770&s1=<?php echo base_url();?>&s2=<?php echo @$_SERVER["HTTP_REFERER"];?>&s3=<?php echo get_cookie('currentlang');?>'></script>
<script type="text/javascript">
$(document).ready(function() {

    if ((screen.width<=575.98)) {
      $(document).scroll(function () {
        var $nav = $(".logo img");
        $nav.toggleClass('hidden', $(this).scrollTop() > $nav.height());
      });
    }
    
});
$(document).ready(function() {

   
      $(document).scroll(function () {
        var $nav = $(".iden");
        $nav.toggleClass('hidden', $(this).scrollTop() > $nav.height());
      });
   
    
});
</script>
</body>
</html>
