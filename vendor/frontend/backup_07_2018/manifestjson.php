<?php
header('Content-Type: application/json');?>
{  
  "short_name": "Joyday.com",  
  "name": "Joyday",  
  "start_url":"<?php echo base_url();?>",
  "icons": [
    {  
      "src": "<?php echo GassetURL('asset/static/icon/384x384.png');?>",  
      "sizes": "384x384",   
      "type": "image/png" 
    },
    {  
      "src": "<?php echo GassetURL('asset/static/icon/192x192.png');?>",  
      "sizes": "192x192",    
      "type": "image/png"
    },  
    {  
      "src": "<?php echo GassetURL('asset/static/icon/256x256.png');?>",  
      "sizes": "256x256",    
      "type": "image/png"
    }, 
    {  
      "src": "<?php echo GassetURL('asset/static/icon/512x512.png');?>",  
      "sizes": "512x512",  
      "type": "image/png"  
    }
  ],  
  "background_color": "#ffffff",
  "theme_color": "#00aadf", 
  "display": "standalone", 
  "orientation":"portrait" 
}