<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<style type="text/css">
.coklat1{
	position: absolute;
    height: 180px;
    top: 12vh;
    right:3vw;
    z-index:10;
}
.coklat2{
	position: absolute;
    height: 180px;
    bottom: 3vh;
    right:1vw;
    z-index:5;
}
.coklat3{
	position: absolute;
    right: 0;
    bottom:20vh;
    width:200px;
}
.titlepage{
	font-size: 2rem;
	text-transform: uppercase;
	text-align: center;
	margin-bottom: 2rem;
	color:#0060af;
}
.paper p{
	text-transform: uppercase;
	letter-spacing: .2em;
	color:#0060af;
	font-weight: 300;
	font-size: .7em;
	line-height: 2;
	margin-bottom:1em;
}
p{
	text-indent:4rem;
	text-align: justify;
}
p::first-letter,p .big{
	font-size: 1.5em;
	line-height: 1;
}
@media (max-width: 769px){
	.paper{
		margin: 0 0px;
	}
	.coklat1,.coklat2{
		right:-6vw;
	}
	.paper > .wrappadding{
		min-height: 75vh;
	}
	.nanas8 {
	    height: 80px;
	    top: 18vh;
	    left: 20vw;
	}
}
@media (max-width: 740px)
{
	.coklat1,.coklat2{
		right:0vw;
		max-height: 100px;
	}
	.coklat3 {
	    max-width: 120px;
	}
	.nanas1{
		max-height: 100px;
	}
	.nanas2{
		max-height: 100px;
	}
	.nanas2{
		max-height: 120px;
		bottom: 10vh;
	}
	.contentterms{
		height: 70vh;
	}
}@media (max-width: 500px)
{
	p{
		text-indent:2rem;
		text-align: left;
	}
	.contentterms{
		height: 80vh;
	}
	.paper > .wrappadding{
		padding: 1.5rem 1.5rem 4rem 1.5rem;
	}
	.titlepage{
		font-size:1.3rem;
		margin-bottom: 1rem;
	}
	.coklat2{
		bottom:10vh;
	}
	.coklat1, .coklat2{
		right: -7vw;
	}
	.coklat3{
		bottom: 30vh;
	}

}
</style>
<?php
		if($this->input->get('content') != 'inload'){
	?>
	<section id="contentsection" class="contentsection">
		<div id="load" class="contenthomeslide contenview">
	<?php }?>
			<div id="wrappingshow">
	<div class="nanas">
		<img class="img-fluid nanas1 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas1.png');?>">
		<img class="img-fluid nanas2 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas2.png');?>">
		<img class="img-fluid nanas8 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas6.png');?>">
		<img class="img-fluid coklat1 naanimation animated opacity0" src="<?php echo base_url('asset/static/coklat/coklat1.png');?>">
		<img class="img-fluid nanas5 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas5.png');?>">
		<img class="img-fluid coklat2 naanimation animated opacity0" src="<?php echo base_url('asset/static/coklat/coklat2.png');?>">
		<img class="img-fluid coklat3 naanimation animated opacity0" src="<?php echo base_url('asset/static/coklat/coklat3.png');?>">
		<div class="contentterms animated opacity0">
			<div class="contentterm height100percent">
				<div class="container height100percent container1000">
					<div class="row height100percent">
						<div class="col-md-12 height100percent ">
							<div ss-container class="paper ss-container">
								<div class="wrappadding">
									<div class="en">
										<h1 class="titlepage">Terms of Use</h1>
											<p>These Terms and Conditions state the rules of the Joyday that every visitor and user must comply with. <span class="big">I</span>f you use our site, you agree fully to the terms and conditions therein.</p>
											<p>If you have any questions about the terms and conditions of this site please send an email to cs@joyday.com</p>
											<p>Joyday.com is the official site of Joyday. <span class="big">T</span>his website is run and managed by PT. Green Asia Food Indonesia.</p>
											<p>Joyday.com users are welcome to open this site in private or both commercial or non-commercial organizations.</p>
											<p>Using Joyday.com for the purpose of promoting any use and / or copying of content in the form of articles, photos, videos or other materials from Joyday.com shall include a logo with hyperlinks from Joyday.com as well as the materials retrieval page from Joyday.com. <span class="big">T</span>his rule refers to Intellectual Property Rights protected by applicable law in the Unitary State of the Republic of Indonesia.</p>
											<p>The technical rules of intact and / or partial retrieval (more than 2 paragraphs) of content quotations and or other material retrieval either intact or subsequently eligible shall be affixed to the logo with the hyperlink of Joyday at the beginning of the title. <span class="big">A</span>lso at the end of the article (bottom) is also required to include hyperlinks from the article and / or materials retrieval page at Joyday.com.</p>
											<p>Related to the utilization by the organization, for example tour operators promoting travel packages can then include outbound links to other sites. <span class="big">Y</span>ou are not allowed to promote anything in ways that may be categorized as SPAM (ex links that elicit comments from others).</p>
											<p>For more details please contact our administrator for material application in this website (cs@joyday.com).</p>
											<p>We are not responsible for any information or even transactions that occur outside of Joyday.com.</p>
											<p>Personal Responsibility for the use of content as a user is responsible for the content you share, including text and photos, as well as checking that the content does not indicate a problem or interfere with the rights of others.</p>
											<p>Our copyrights have the right to our copyright under the Copyright Law of the Republic of Indonesia and may also request to our users from other countries who are subject to similar copyright laws.</p>
										</div>
										<div class="id">
											<h1 class="titlepage">Syarat dan Ketentuan</h1>
												<p>Syarat dan ketentuan ini menyatakan peraturan dari pihak Joyday yang harus dipatuhi oleh setiap pengunjung dan pengguna. <span class="big">A</span>pabila Anda menggunakan situs Kami berarti Anda setuju penuh dengan syarat dan ketentuan di dalamnya.</p>
												<p>Apabila Anda mempunyai pertanyaan mengenai syarat dan ketentuan situs ini silakan kirim email ke cs@joyday.com
													<span class="big">S</span>itus Joyday.com merupakan situs resmi Joyday. <span class="big">W</span>ebsite ini dijalankan dan dikelola oleh PT. Green Asia Food Indonesia.</p>
												<p>Pengguna Joyday.com dipersilahkan membuka situs ini secara pribadi maupun organisasi baik itu komersial ataupun nonkomersial.</p>
												<p>Menggunakan Joyday.com untuk tujuan promosi setiap penggunaan dan atau penyalinan (copy) konten berupa artikel, foto, video, atau pun bahan materi lainnya dari Joyday.com wajib menyertakan logo bersama tautan (hyperlink) dari Joyday.com dan juga laman pengambilan bahan tersebut dari Joyday.com. <span class="big">A</span>turan ini merujuk pada Hak Kekayaan Intelektual yang dilindungi oleh undang-undang yang berlaku di Negara Kesatuan <span class="big">R</span>epublik <span class="big">I</span>ndonesia.</p>
												<p>Aturan teknis pengambilan utuh dan atau sebagian (lebih dari 2 paragraf) kutipan konten dan atau pengambilan bahan materi lainnya baik utuh maupun untuk disadur berikutnya maka wajib melekatkan logo bersama tautan (hyperlink) dari Joyday pada bagian awal setelah judul. Selain itu pada bagian akhir artikel (paling bawah) juga wajib menyertakan tautan (hyperlink) dari laman pengambilan artikel dan atau bahan tersebut di Joyday.com.</p>
												<p>Terkait pemanfaatan oleh organisasi, contohnya tour operator yang mempromosikan paket perjalanan wisata maka dapat menyertakan link outbound ke situs yang lain. <span class="big">A</span>nda tidak diperkenankan mempromosikan sesuatu melalui cara-cara yang mungkin dikategorikan sebagai SPAM (contoh: link yang memancing komentar orang lain).</p>
												<p>Untuk lebih jelasnya dapat menghubungi administrator kami untuk permohonan bahan materi dalam website ini. (cs@joyday.com)</p>
												<p>Kami tidak bertanggung jawab terhadap informasi atau bahkan transaksi yang terjadi di luar Joyday.com.</p>
												<p>Tanggung Jawab Pribadi terhadap penggunaan konten anda sebagai pengguna bertanggung jawab tehadap konten yang Anda bagikan, termasuk teks dan foto, juga memeriksa bahwa konten tidak mengindikasikan masalah atau menggangu hak orang lain.</p>
												<p>Hak cipta kami mempunyai hak terhadap hak cipta kami dibawah hukum Hak Cipta Republik Indonesia dan juga bisa meminta kepada pengguna kami dari negara lain yang taat terhadap hukum hak cipta serupa.</p>
											</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.imgfooter').animate({'opacity':1, 'bottom' : 0},100,function(){
				$('.nanas8').animate({'opacity':1},100,function(){
					<?php if($this->input->get('content') != 'inload'){?>
						$('.socmed,.socmedmobile,.headerhome').animate({'opacity':1},1000).removeClass('fadeIn');
					<?php }?>
					$('.coklat1').animate({'opacity': 1},100,function(){
						setTimeout(function(){
							$('.nanas1').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
							$('.nanas2').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
						},300);
					}).addClass('fadeInUp');
					$('.coklat2').animate({'opacity': 1},100,function(){
						$('.coklat3').animate({'opacity': 1},100,function(){
							$('.socmed').animate({'opacity':1},1000).removeClass('fadeIn');
							setTimeout(function(){
								$('.contentterms').animate({'opacity' : 1},1000,function(){

								}).addClass('fadeInUp');
							},1000);
						}).addClass('fadeInUp');
					}).addClass('fadeInUp');
				}).addClass('fadeInUp');
			}).addClass('fadeInUp').prev('.copyright').delay(1000).animate({opacity:1},1000);
		});
	</script>
<?php
		if($this->input->get('content') != 'inload'){
	?>
			</div>
		</section>
		<div class="copyright absolute animated opacity0">
			<div class="footermenu">
				<a href="#contentsection" data-href="<?php echo base_url('home/terms');?>" data-url="home/terms" data-title="terms of use Joyday" class="firstmenu menufoot"><span class="en">Terms of Use</span><span class="id">Syarat &amp; Ketentuan</span></a>
				<a href="#contentsection" data-href="<?php echo base_url('home/privacy');?>" data-url="home/privacy" data-title="Privacy Policy Joyday" class="firstmenu menufoot"><span class="en">Privacy Policy</span><span class="id">Kebijakan Privasi</span></a>
			</div>
		</div>
		<div class="imgfooter imgfooter1 opacity0 animated">
			<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer.png')?>" alt="shadowyellow"/>
		</div>
		<div class="imgfooter imgfooter2 opacity0 animated">
			<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer2.png')?>" alt="shadowyellow"/>
		</div>
		<div class="imgfooter imgfooter3 opacity0 animated">
			<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer3.png')?>" alt="shadowyellow"/>
		</div>
	<?php }?>
