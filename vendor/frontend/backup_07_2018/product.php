<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<style type="text/css">
.titlepage{
	font-size: 2rem;
	text-transform: uppercase;
	text-align: center;
	margin-bottom: 2rem;
	color:#0060af;
}
.paper p{
	text-transform: uppercase;
	letter-spacing: .2em;
	color:#0060af;
	font-weight: 300;
	font-size: .7em;
	line-height: 2;
	margin-bottom:1em;
}
.menusidebarblog{
	list-style: none;
	padding:0;
	margin:0;
	letter-spacing: .2em;
}
.menusidebarblog li{
	text-align: right;
}
.menusidebarblog li a{
	text-align: right;
	position: relative;
	font-weight: 300;
	font-size: 1.3rem;
	text-transform: uppercase;
	display: inline-block;
	opacity: .5;
	color: #3273dc;
}
.menusidebarblog li a span{
	float: left;
}
.menusidebarblog li a span + span{
	float: right;
}
a:not([href]):not([tabindex]):focus,
a:not([href]):not([tabindex]):hover,
.menusidebarblog li a.active,
.menusidebarblog li:hover a,
.menusidebarblog li li:hover a,
.menusidebarblog li li:hover a:hover{
	text-decoration: none;
	opacity: 1;
	color: #0060af !important;
}
.menusidebarblog > li > a:after{
	content: "";
    display: inline-block;
    position: absolute;
    right: .15em;
    z-index: 1;
    bottom: 0;
    width: 100%;
    height: 1px;
    transition: opacity .5s ease-in-out;
    -moz-transition: opacity .5s ease-in-out;
    -webkit-transition: opacity .5s ease-in-out;
    border-bottom: .1em solid transparent;
}
.menusidebarblog > li:hover > a:after,
.menusidebarblog > li > a.active:after{
    border-bottom: .1em solid #0060af;
}
.menusidebarblog li ul{
    margin-right: -30px;
    padding-right: 30px;
    max-height: 0;
    overflow: hidden;
    -webkit-transition: all .5s ease;
    -moz-transition: all .5s ease;
    -o-transition: all .5s ease;
    -ms-transition: all .5s ease;
    transition: all .5s ease;
}

.menusidebarblog li:hover ul,
.menusidebarblog li > a.active + ul{
	max-height: 1000px;
    -webkit-transition: all .5s ease;
    -moz-transition: all .5s ease;
    -o-transition: all .5s ease;
    -ms-transition: all .5s ease;
    transition: all .5s ease;
}
.menusidebarblog li li{
	position: relative;
	margin-top:0;
	padding-top:0;
}
.menusidebarblog li li a:after{
	position: absolute;
    top: 14px;
    content: "";
    display: block;
    width: 10px;
    height: 10px;
    border-radius: 50%;
    right: -20px;
    background: #0060af;
    opacity: .5;
    display: none;
}
.menusidebarblog li li:hover a:after,.menusidebarblog li li a.active:after{
	display: block;
	opacity: 1;
}
.menusidebarblog li li a{
	position: relative;
	display: inline-block;
	width: 100%;
	font-size: .85em;
	padding:10px 0;
	margin:0;
}
.menusidebarblog li:hover li a:not(.active),
.menusidebarblog li li:hover a:not(.active){
	opacity: .5;
}
.menusidebarblog li li:hover a:hover{
	opacity: 1;
}
.sidebare{
	max-height: 80%;
	padding-right:30px;
}
.sidebare .wrapmenusidebar{
	max-width: 220px;
	margin:0;
	padding-top:30px;
}
.sidebare h2{
	float: right;
    letter-spacing: .1em;
    text-transform: uppercase;
    text-align: right;
    font-size: 1.8rem;
    margin: 0 0 2rem 0;
    margin-bottom: 1.5rem;
    color: #0060af;
}
.item-product{
	position: relative;
	letter-spacing: .2em;
	line-height: 1.5;
	font-size:.78rem;
	text-transform: uppercase;
	color: #0060af;
	margin-bottom: 25px;
	background: rgba(255,255,255,.5);
}
.item-product .level{
	padding:30px;
}
.item-product:hover{
	opacity: 1;
	cursor: pointer;
}
.item-product .imgproduct img{
	position: absolute;
	width:100%;
  left: 50%;
	bottom:0;
  -webkit-transition: all .5s ease;
  -moz-transition: all .5s ease;
  -o-transition: all .5s ease;
  -ms-transition: all .5s ease;
  transition: all .5s ease;

}
.item-product .imgproduct{
	position: relative;
	width:62px;
	margin:0 auto;
	min-height: 130px;
	min-width: 61px;
	text-align: center;
	transform: translateX(-50%);
  -webkit-transition: all .5s ease;
  -moz-transition: all .5s ease;
  -o-transition: all .5s ease;
  -ms-transition: all .5s ease;
  transition: all .5s ease;

}
.item-product:hover .imgproduct{
	width:70px;
}
.title-product{
	font-size:2rem;
	margin:0;
	padding:0;
}
.meta-title-product{
	padding:5px 0 20px 0;
}
.content-item-product{
	line-height: 2;
	margin-bottom: 25px;
}
.readmore{
	display: inline-block;
	position: relative;
}
.item-product:nth-of-type(odd) {

}

.item-product:nth-of-type(even) {
}
.papaerproduct{
	padding-left: 20px;
	padding-right:20px;
	padding-bottom: 50px;
}
.fancybox-slide--iframe .fancybox-close-small{
	right:0;
}
.fancybox-slide--iframe .fancybox-content::-webkit-scrollbar {
    width: 10px;
}

/* Track */
.fancybox-slide--iframe .fancybox-content::-webkit-scrollbar-track {
	background: rgba(255,255,255,0.7);
    border: 4px solid transparent;
    background-clip: content-box;
    border-radius: 10px;
}

/* Handle */
.fancybox-slide--iframe .fancybox-content::-webkit-scrollbar-thumb {
    background: rgb(255,255,255);
    border-radius: 10px;
}
.fancybox-slide--iframe .fancybox-content::-webkit-scrollbar-thumb:vertical {
    height:15px;
}
/* Handle on hover */
.fancybox-slide--iframe .fancybox-content::-webkit-scrollbar-thumb:hover {
    background: #555;
}
.fancybox-bg{
	background: rgba(255,255,255,0.3);
}
.close-smalle{
	position: absolute;
    top: 8px;
    right: 40px;
    width: 30px;
    height: 30px;
    line-height: 1;
    padding: 0;
    margin: 0;
    border: 0;
    border-radius: 0;
    outline: none;
    background: transparent;
    z-index: 10;
    cursor: pointer;
}
.close-smalle:after {
    content: '×';
    position: absolute;
    top: 0;
    right: 0;
    width: 30px;
    height: 30px;
    font: 40px/30px Arial,"Helvetica Neue",Helvetica,sans-serif;
    color: #0060af;
    font-weight: 300;
    text-align: center;
    border-radius: 50%;
    border-width: 0;
    background: transparent;
    transition: background .25s;
    box-sizing: border-box;
    z-index: 2;
}
#wrappingshow .detail-product{
	position: absolute;
    top: 120vh;
    z-index: 10;
		height: 80vh;
		width: 1000px;
		max-width: 95vw;
		left: 50%;
    transform: translateX(-50%);
    color: #0060af;
    background-color: transparent;
    overflow-x: hidden;
    overflow-y: auto;
	-webkit-transition: all 1s ease;
    -moz-transition: all 1s ease;
    -o-transition: all 1s ease;
    -ms-transition: all 1s ease;
    transition: all 1s ease;
}
#wrappingshow.showdetail .detail-product{
	top: 20vh;
	-webkit-transition: all 1s ease;
    -moz-transition: all 1s ease;
    -o-transition: all 1s ease;
    -ms-transition: all 1s ease;
    transition: all 1s ease;
}
#wrappingshow.showdetail .nanas .blogwrap .ss-container{
	overflow-y: hidden;
}
.product_item{
	margin: 0 30px;
  border-radius: 5px;
  box-shadow: 0 10px 20px 0px #555;
  min-height: 80vh;
  background-color: rgba(255,255,255,.95);
}
.slideproduct{
	width: 300px;
    max-width: 90%;
    margin: 0 3rem 4rem 3rem;
}
.slideproduct img{
	position: relative;
	z-index:3;
}
.overlays-product-detail{
	width: 100%;
    left: 0;
    height: calc(80% - 3rem);
    position: absolute;
    bottom: 0;
    z-index: 1;
    border-radius: 10px;
    <?php
    	if(get_cookie('bgColor')){
          echo 'background-color: '. get_cookie('bgColor') .';';
      }else{
      	  echo 'background-color: rgba(238,204,91,.8);';
      }
    ?>

}
.carousel-indicators{
	bottom:-2rem;
}
.carousel-indicators li{
	border: 2px solid #005b9c;
    position: relative;
    -webkit-box-flex: 0;
    -ms-flex: 0 1 auto;
    flex: 0 1 auto;
    width: 12px;
    height: 12px;
    border-radius: 50%;
    margin-right: 10px;
    margin-left: 10px;
    text-indent: -999px;
    background-color: rgba(255,255,255,.5);
}
.carousel-indicators li:not(.active){
    background-color: #005b9c !important;
}
.carousel-indicators li:hover{
	cursor: pointer;
}
.leveltextleft{
	justify-content:left;
}
.product-info-detailed h2{
	text-transform: uppercase;
	font-size:2rem;
}
.description-product-detailed{
	font-size:1rem;
	max-width: 400px;
	letter-spacing: .25em;
	line-height: 1.5;
	padding-bottom: 3rem;
}
.requestsample{
	font-size: 1.1rem;
	text-decoration: none;
	border:.1em solid #005b9c;
	padding:.8rem 1rem;
	border-radius: 5px;
	color: #005b9c;
}
.requestsample:hover{
	text-decoration: none;
	color: #005b9c;
}
.meta-info{
	padding:.5rem 0;
}
.sosmed-product-detaile{
	padding-right:1rem;
	font-size:2rem;
}
.sosmed-product-detaile a{
	text-decoration: none;
	color: #005b9c;
	cursor: pointer;
	opacity: .9;
}
.overlay-main-product{
	position: absolute;
	width:100%;
	height: 100%;
	z-index:25;
	top:0;
	left:0;
	display: none;
}
.overlay-main-product.show{
	display: block;
}
.product_item{
	display: none;
}
.product_item.show{
	display: block;
}
.headerhome,.socmed{
	opacity: 1;
}
.titlecorn{
	min-width: 98px;
}
@media (max-width: 1056px){
	.slideproduct {
	    margin: 0 2rem 4rem 2rem;
	}
}
@media (max-width: 825px)
{
	.product-info-detailed{
		display: block;
	    margin: 0 auto;
	    text-align: center;
	}
	.sosmed-product-detaile{
		display: inline-block;
		margin:0 auto;
		padding:15px 0;
	}.sosmed-product-detaile li{
		float: left;
	}
	.sosmed-product-detaile li:not(:last-child){
		margin-right: 15px;
	}
	.sosmed-product-detaile:after{
		content: "";
		clear: both;
		display: block;
	}
	.product_item{
		padding-bottom: 15vh;
	}
	.imgtitle{
		text-align: center;
	}
	.titlecorn{
		padding-top: 1.2rem;
    padding-bottom: 1.4rem;
		min-width: auto;
	}
}
@media (max-width: 740px)
{
	.product-info-detailed h2{
		font-size: 1.5rem;
	}
	.description-product-detailed{
		font-size: .85rem;
	}
	#main-product{
		margin-top:10vh;
	}
	#wrappingshow.showdetail .detail-product{
		top:30vh;
	}
	.product_item{
		padding-bottom: 20vh;
	}
}
@media (max-width: 500px)
{
.close-smalle{
	right:20px;
}
	#wrappingshow .detail-product{
		width: 95vw;
    margin: 0 10px;
		left:0vw;
		transform: translateX(0);
	}
	#main-product{
		margin-top:0;
	}
	#wrappingshow.showdetail .detail-product{
		top:15vh;
	}
	.product_item{
		margin:0 15px;
	}
	.description-product-detailed{
		max-width: 280px;
	}
}
</style>
<?php
		if($this->input->get('content') != 'inload'){
	?>
	<section id="contentsection" class="contentsection">
		<div id="load" class="contenthomeslide contenview">
	<?php }?>
			<div id="wrappingshow">
				<div class="nanas">
					<?php
						$cat = array('ccs','mics','sics');
					?>
					<div class="overlay-sidebar animated <?php echo in_array($this->input->get('cat'), $cat) == true ? '' : 'fadeInLeft';?>"></div>
					<div class="contentterms blogwrap animated <?php echo in_array($this->input->get('cat'), $cat) == true ? '' : 'opacity0';?>">
						<div class="contentterm height100percent">
							<div class="container height100percent">
								<div class="row height100percent">
									<div ss-container class="col-md-3 ss-container sidebare height100percent ">
										<div class="wrapmenusidebar">
										<h2 class="en">Joyday<br/>Products</h2>
										<h2 class="id">Produk<br/>Joyday</h2>
										<div class="is-clearfix"></div>
											<ul class="menusidebarblog">
												<li><a href="<?php echo base_url('product');?>" class="sidemenu stickmenu active"><span>Stick</span></a></li>
												<li><a href="<?php echo base_url('product/cone');?>"  class="sidemenu conemenu"><span>Cone</span></a></li>
												<li><a href="<?php echo base_url('product/cup');?>" class="sidemenu cupmenu"><span>Cup</span></a></li>
											</ul>
										</div>
									</div>
									<div id="main-product" class="col-lg-9 col-md-12 <?php echo in_array($this->input->get('cat'), $cat) == true ? 'opacity0 animated' : '';?> height100percent ">
										<div id="overlay-main-product" class="overlay-main-product"></div>
										<div id="loadpart" ss-container class="papaerproduct ss-container">
											<div class="row row-eq-height">
												<!-- PRODUCT THUMBNAIL 1 -->
												<div class="col-lg-4 col-md-4 col-sm-6 col-12">
													<div class="item-product showproduct" id="ccm" data-target="#product_1">
														<div class="level">
															<div class="level-left">
																<div class="level-item">
																	<div class="imgproduct">
																		<img src="<?php echo base_url('asset/static/joyday-product/CrunchyChocoMaltprod.png');?>" alt="product_1" class="img-fluid"/>
																	</div>
																</div>
															</div>
															<div class="level-right">
																<div class="level-item">
																	<div class="imgtitle">
																		Chruncy<br/>
																		Chocolate<br/>
																		Malt
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!-- PRODUCT THUMBNAIL 2 -->
												<div class="col-lg-4 col-md-4 col-sm-6 col-12">
													<div class="item-product showproduct" id="ccb" data-target="#product_2">
														<div class="level">
															<div class="level-left">
																<div class="level-item">
																	<div class="imgproduct">
																		<img src="<?php echo base_url('asset/static/joyday-product/CrunchyChocoBlueberryprod.png');?>" alt="product_2" class="img-fluid"/>
																	</div>
																</div>
															</div>
															<div class="level-right">
																<div class="level-item">
																	<div class="imgtitle">
																		Crunchy<br/>
																		Chocolate<br/>
																		Blueberry
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!-- PRODUCT THUMBNAIL 3 -->
												<div class="col-lg-4 col-md-4 col-sm-6 col-12">
													<div class="item-product showproduct" id="cbc" data-target="#product_3">
														<div class="level">
															<div class="level-left">
																<div class="level-item">
																	<div class="imgproduct">
																		<img src="<?php echo base_url('asset/static/joyday-product/ClassicBelgianChocoprod.png');?>" alt="product_3" class="img-fluid"/>
																	</div>
																</div>
															</div>
															<div class="level-right">
																<div class="level-item">
																	<div class="imgtitle">
																		Classic<br/>
																		Belgian<br/>
																		Chocolate
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!-- PRODUCT THUMBNAIL 4 -->
												<div class="col-lg-4 col-md-4 col-sm-6 col-12">
													<div class="item-product showproduct" id="tcm" data-target="#product_4">
														<div class="level">
															<div class="level-left">
																<div class="level-item">
																	<div class="imgproduct">
																		<img src="<?php echo base_url('asset/static/joyday-product/TiramisuChocolateMintprod.png');?>" alt="product_4" class="img-fluid"/>
																	</div>
																</div>
															</div>
															<div class="level-right">
																<div class="level-item">
																	<div class="imgtitle">
																		Tiramisu<br/>
																		Chocolate<br/>
																		Mint
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!-- PRODUCT THUMBNAIL 5 -->
												<div class="col-lg-4 col-md-4 col-sm-6 col-12">
													<div class="item-product showproduct" id="jujube" data-target="#product_5">
														<div class="level">
															<div class="level-left">
																<div class="level-item">
																	<div class="imgproduct">
																		<img src="<?php echo base_url('asset/static/joyday-product/JujubeWhiteChocoprod.png');?>" alt="product_5" class="img-fluid"/>
																	</div>
																</div>
															</div>
															<div class="level-right">
																<div class="level-item">
																	<div class="imgtitle">
																		Jujube<br/>
																		White<br/>
																		Chocolate
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!-- PRODUCT THUMBNAIL 6 -->
												<div class="col-lg-4 col-md-4 col-sm-6 col-12">
													<div class="item-product showproduct" id="sc" data-target="#product_6">
														<div class="level">
															<div class="level-left">
																<div class="level-item">
																	<div class="imgproduct">
																		<img src="<?php echo base_url('asset/static/joyday-product/SweetCornprod.png');?>" alt="product_6" class="img-fluid"/>
																	</div>
																</div>
															</div>
															<div class="level-left">
																<div class="level-item">
																	<div class="imgtitle titlecorn">
																		Sweet<br/>
																		Corn<br/>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!-- PRODUCT THUMBNAIL 7 -->
												<div class="col-lg-4 col-md-4 col-sm-6 col-12">
													<div class="item-product showproduct" id="blueberry" data-target="#product_7">
														<div class="level">
															<div class="level-left">
																<div class="level-item">
																	<div class="imgproduct">
																		<img src="<?php echo base_url('asset/static/joyday-product/CoolBlueberryprod2018.png');?>" alt="product_7" class="img-fluid"/>
																	</div>
																</div>
															</div>
															<div class="level-right">
																<div class="level-item">
																	<div class="imgtitle">
																		Cool<br/>
																		Blueberry<br/>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!-- PRODUCT THUMBNAIL 8 -->
												<div class="col-lg-4 col-md-4 col-sm-6 col-12">
													<div class="item-product showproduct" id="pineapple" data-target="#product_8">
														<div class="level">
															<div class="level-left">
																<div class="level-item">
																	<div class="imgproduct">
																		<img src="<?php echo base_url('asset/static/joyday-product/CoolPineappleprod.png');?>" alt="product_8" class="img-fluid"/>
																	</div>
																</div>
															</div>
															<div class="level-right">
																<div class="level-item">
																	<div class="imgtitle">
																		Cool<br/>
																		Pineapple<br/>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="detail-product" ss-container class="detail-product is-clearfix">
					<div class="closecontent close-smalle"></div>
					<!-- POP UP PRODUCT 1 -->
					<div id="product_1" class="product_item ccm is-clearfix">
						<div class="level">
							<div class="level-left">
								<div class="level-item">
									<div class="slideproduct">
										<div id="carouselfade" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
											<ul class="carousel-indicators">
											    <li data-target="#carouselfade" data-slide-to="0" class="active"></li>
											    <li data-target="#carouselfade" data-slide-to="1"></li>
											  </ul>
										  <div class="carousel-inner">
										    <div class="carousel-item active" >
										    	<div class="item-product-detailed">
										    		<div class="overlays-product-detail overlaysproduct1 opacity0 animated"></div>
										    		<img class="imgbanner imgbanner1 opacity0 animated" src="<?php echo base_url('asset/static/product/big/new-product1.png');?>" alt="First slide">
										    	</div>
										    </div>
										    <div class="carousel-item" >
										      <div class="item-product-detailed">
										    		<div class="overlays-product-detail "></div>
										    		<img class="imgbanner animated " src="<?php echo base_url('asset/static/product/big/new-product1.1.png');?>" alt="First slide">
										    	</div>
										    </div>
										  </div>
										</div>
									</div>
								</div>
							</div>
							<div class="level-item leveltextleft">
								<div class="product-info-detailed">
									<h2>Crunchy<br/>Chocolate Malt</h2>
									<div class="meta-info">NETTO : 75 GR</div>
									<div class="description-product-detailed">
										VANILLA ICE CREAM COVERED WITH CRUNCHY CHOCOLATE AND MALT
									</div>
									<div>
										<a href="<?php echo base_url('home/requestsample');?>" class="requestsample">REQUEST SAMPLE</a>
									</div>
								</div>
							</div>
							<div class="level-right">
								<div class="level-item">
									<ul class="sosmed-product-detaile">
										<li><a href="javascript:void(0);" target="_blank" class="fb"><i class="fa fa-facebook-square"></i></a></li>
										<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=CRUNCHY CHOCOLATE MALT - VANILLA ICE CREAM COVERED WITH CRUNCHY CHOCOLATE AND MALT" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
										<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
										<li><a href="mailto:?subject=CRUNCHY CHOCOLATE MALT&amp;body=VANILLA ICE CREAM COVERED WITH CRUNCHY CHOCOLATE AND MALT <?php echo current_url();?>."><i class="ti-email"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- POP UP PRODUCT 2 -->
					<div id="product_2" class="product_item ccb is-clearfix">
						<div class="level">
							<div class="level-left">
								<div class="level-item">
									<div class="slideproduct">
										<div id="carouselfade_1" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
											<ul class="carousel-indicators">
											    <li data-target="#carouselfade_1" data-slide-to="0" class="active"></li>
											    <li data-target="#carouselfade_1" data-slide-to="1"></li>
											  </ul>
										  <div class="carousel-inner">
										    <div class="carousel-item active" >
										    	<div class="item-product-detailed">
										    		<div class="overlays-product-detail overlaysproduct1 opacity0 animated"></div>
										    		<img class="imgbanner imgbanner1 opacity0 animated" src="<?php echo base_url('asset/static/product/big/new-product2.png');?>" alt="First slide">
										    	</div>
										    </div>
										    <div class="carousel-item" >
										      <div class="item-product-detailed">
										    		<div class="overlays-product-detail "></div>
										    		<img class="imgbanner animated " src="<?php echo base_url('asset/static/product/big/new-product2.1.png');?>" alt="First slide">
										    	</div>
										    </div>
										  </div>
										</div>
									</div>
								</div>
							</div>
							<div class="level-item leveltextleft">
								<div class="product-info-detailed">
									<h2>Chruncy<br/>Chocolate Blueberry</h2>
									<div class="meta-info">NETTO : 75 GR</div>
									<div class="description-product-detailed">
										VANILLA ICE CREAM COVERED IN CHOCOLATE MALT WITH BLUEBERRY JAM FILLING
									</div>
									<div>
										<a href="<?php echo base_url('home/requestsample');?>" class="requestsample">REQUEST SAMPLE</a>
									</div>
								</div>
							</div>
							<div class="level-right">
								<div class="level-item">
									<ul class="sosmed-product-detaile">
										<li><a href="javascript:void(0);" target="_blank" class="fb"><i class="fa fa-facebook-square"></i></a></li>
										<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=CHRUNCY CHOCOLATE BLUEBERRY - VANILLA ICE CREAM COVERED IN CHOCOLATE MALT WITH BLUEBERRY JAM FILLING" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
										<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
										<li><a href="mailto:?subject=CHRUNCY CHOCOLATE BLUEBERRY&amp;body=VANILLA ICE CREAM COVERED IN CHOCOLATE MALT WITH BLUEBERRY JAM FILLING <?php echo current_url();?>."><i class="ti-email"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- POP UP PRODUCT 3 -->
					<div id="product_3" class="product_item ccb is-clearfix">
						<div class="level">
							<div class="level-left">
								<div class="level-item">
									<div class="slideproduct">
										<div id="carouselfade_3" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
											<ul class="carousel-indicators">
											    <li data-target="#carouselfade_3" data-slide-to="0" class="active"></li>
											    <li data-target="#carouselfade_3" data-slide-to="1"></li>
											  </ul>
										  <div class="carousel-inner">
										    <div class="carousel-item active" >
										    	<div class="item-product-detailed">
										    		<div class="overlays-product-detail overlaysproduct1 opacity0 animated"></div>
										    		<img class="imgbanner imgbanner1 opacity0 animated" src="<?php echo base_url('asset/static/product/big/new-product3.png');?>" alt="First slide">
										    	</div>
										    </div>
										    <div class="carousel-item" >
										      <div class="item-product-detailed">
										    		<div class="overlays-product-detail "></div>
										    		<img class="imgbanner animated " src="<?php echo base_url('asset/static/product/big/new-product3.1.png');?>" alt="First slide">
										    	</div>
										    </div>
										  </div>
										</div>
									</div>
								</div>
							</div>
							<div class="level-item leveltextleft">
								<div class="product-info-detailed">
									<h2>Classic<br/>Belgian Chocolate</h2>
									<div class="meta-info">NETTO : 65 GR</div>
									<div class="description-product-detailed">
										VANILLA FLAVOURED ICE CREAM COVERED IN PREMIUM BELGIAN CHOCOLATE
									</div>
									<div>
										<a href="<?php echo base_url('home/requestsample');?>" class="requestsample">REQUEST SAMPLE</a>
									</div>
								</div>
							</div>
							<div class="level-right">
								<div class="level-item">
									<ul class="sosmed-product-detaile">
										<li><a href="javascript:void(0);" target="_blank" class="fb"><i class="fa fa-facebook-square"></i></a></li>
										<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=CLASSIC BELGIAN CHOCOLATE - VANILLA FLAVOURED ICE CREAM COVERED IN PREMIUM BELGIAN CHOCOLATE" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
										<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
										<li><a href="mailto:?subject=CLASSIC BELGIAN CHOCOLATE&amp;body=VANILLA FLAVOURED ICE CREAM COVERED IN PREMIUM BELGIAN CHOCOLATE <?php echo current_url();?>."><i class="ti-email"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- POP UP PRODUCT 4 -->
					<div id="product_4" class="product_item ccm is-clearfix">
						<div class="level">
							<div class="level-left">
								<div class="level-item">
									<div class="slideproduct">
										<div id="carouselfade_4" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
											<ul class="carousel-indicators">
											    <li data-target="#carouselfade_4" data-slide-to="0" class="active"></li>
											    <li data-target="#carouselfade_4" data-slide-to="1"></li>
											  </ul>
										  <div class="carousel-inner">
										    <div class="carousel-item active" >
										    	<div class="item-product-detailed">
										    		<div class="overlays-product-detail overlaysproduct1 opacity0 animated"></div>
										    		<img class="imgbanner imgbanner1 opacity0 animated" src="<?php echo base_url('asset/static/product/big/new-product4.png');?>" alt="First slide">
										    	</div>
										    </div>
										    <div class="carousel-item" >
										      <div class="item-product-detailed">
										    		<div class="overlays-product-detail "></div>
										    		<img class="imgbanner animated " src="<?php echo base_url('asset/static/product/big/new-product4.1.png');?>" alt="First slide">
										    	</div>
										    </div>
										  </div>
										</div>
									</div>
								</div>
							</div>
							<div class="level-item leveltextleft">
								<div class="product-info-detailed">
									<h2>Tiramisu<br/>Chocolate Mint</h2>
									<div class="meta-info">NETTO : 65 GR</div>
									<div class="description-product-detailed">
										TIRAMISU ICE CREAM COVERED IN MINT CHOCOLATE
									</div>
									<div>
										<a href="<?php echo base_url('home/requestsample');?>" class="requestsample">REQUEST SAMPLE</a>
									</div>
								</div>
							</div>
							<div class="level-right">
								<div class="level-item">
									<ul class="sosmed-product-detaile">
										<li><a href="javascript:void(0);" target="_blank" class="fb"><i class="fa fa-facebook-square"></i></a></li>
										<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=TIRAMISU CHOCOLATE MINT - TIRAMISU ICE CREAM COVERED IN MINT CHOCOLATE" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
										<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
										<li><a href="mailto:?subject=TIRAMISU CHOCOLATE MINT&amp;body=TIRAMISU ICE CREAM COVERED IN MINT CHOCOLATE <?php echo current_url();?>."><i class="ti-email"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- POP UP PRODUCT 5 -->
					<div id="product_5" class="product_item ccb is-clearfix">
						<div class="level">
							<div class="level-left">
								<div class="level-item">
									<div class="slideproduct">
										<div id="carouselfade_5" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
											<ul class="carousel-indicators">
											    <li data-target="#carouselfade_5" data-slide-to="0" class="active"></li>
											    <li data-target="#carouselfade_5" data-slide-to="1"></li>
											  </ul>
										  <div class="carousel-inner">
										    <div class="carousel-item active" >
										    	<div class="item-product-detailed">
										    		<div class="overlays-product-detail overlaysproduct1 opacity0 animated"></div>
										    		<img class="imgbanner imgbanner1 opacity0 animated" src="<?php echo base_url('asset/static/product/big/new-product5.png');?>" alt="First slide">
										    	</div>
										    </div>
										    <div class="carousel-item" >
										      <div class="item-product-detailed">
										    		<div class="overlays-product-detail "></div>
										    		<img class="imgbanner animated " src="<?php echo base_url('asset/static/product/big/new-product5.1.png');?>" alt="First slide">
										    	</div>
										    </div>
										  </div>
										</div>
									</div>
								</div>
							</div>
							<div class="level-item leveltextleft">
								<div class="product-info-detailed">
									<h2>Jujube<br/>White Chocolate</h2>
									<div class="meta-info">NETTO : 80 GR</div>
									<div class="description-product-detailed">
										RED DATE FLAVOURED MILK COVERED IN WHITE CHOCOLATE
									</div>
									<div>
										<a href="<?php echo base_url('home/requestsample');?>" class="requestsample">REQUEST SAMPLE</a>
									</div>
								</div>
							</div>
							<div class="level-right">
								<div class="level-item">
									<ul class="sosmed-product-detaile">
										<li><a href="javascript:void(0);" target="_blank" class="fb"><i class="fa fa-facebook-square"></i></a></li>
										<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=JUJUBE WHITE CHOCOLATE - RED DATE FLAVOURED MILK COVERED IN WHITE CHOCOLATE" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
										<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
										<li><a href="mailto:?subject=JUJUBE WHITE CHOCOLATE&amp;body=RED DATE FLAVOURED MILK COVERED IN WHITE CHOCOLATE <?php echo current_url();?>."><i class="ti-email"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- POP UP PRODUCT 6 -->
					<div id="product_6" class="product_item ccb is-clearfix">
						<div class="level">
							<div class="level-left">
								<div class="level-item">
									<div class="slideproduct">
										<div id="carouselfade_6" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
											<ul class="carousel-indicators">
											    <li data-target="#carouselfade_6" data-slide-to="0" class="active"></li>
											    <li data-target="#carouselfade_6" data-slide-to="1"></li>
											  </ul>
										  <div class="carousel-inner">
										    <div class="carousel-item active" >
										    	<div class="item-product-detailed">
										    		<div class="overlays-product-detail overlaysproduct1 opacity0 animated"></div>
										    		<img class="imgbanner imgbanner1 opacity0 animated" src="<?php echo base_url('asset/static/product/big/new-product6.png');?>" alt="First slide">
										    	</div>
										    </div>
										    <div class="carousel-item" >
										      <div class="item-product-detailed">
										    		<div class="overlays-product-detail "></div>
										    		<img class="imgbanner animated " src="<?php echo base_url('asset/static/product/big/new-product6.1.png');?>" alt="First slide">
										    	</div>
										    </div>
										  </div>
										</div>
									</div>
								</div>
							</div>
							<div class="level-item leveltextleft">
								<div class="product-info-detailed">
									<h2>Sweet<br/>Corn </h2>
									<div class="meta-info">NETTO : 65 GR</div>
									<div class="description-product-detailed">
										MILK ICE CREAM WITH SWEET CORN FLAVOUR
									</div>
									<div>
										<a href="<?php echo base_url('home/requestsample');?>" class="requestsample">REQUEST SAMPLE</a>
									</div>
								</div>
							</div>
							<div class="level-right">
								<div class="level-item">
									<ul class="sosmed-product-detaile">
										<li><a href="javascript:void(0);" target="_blank" class="fb"><i class="fa fa-facebook-square"></i></a></li>
										<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=SWEET CORN - MILK ICE CREAM WITH SWEET CORN FLAVOUR" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
										<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
										<li><a href="mailto:?subject=SWEET CORN&amp;body=MILK ICE CREAM WITH SWEET CORN FLAVOUR <?php echo current_url();?>."><i class="ti-email"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- POP UP PRODUCT 7 -->
					<div id="product_7" class="product_item ccb is-clearfix">
						<div class="level">
							<div class="level-left">
								<div class="level-item">
									<div class="slideproduct">
										<div id="carouselfade_7" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
											<ul class="carousel-indicators">
											    <li data-target="#carouselfade_7" data-slide-to="0" class="active"></li>
											    <li data-target="#carouselfade_7" data-slide-to="1"></li>
											  </ul>
										  <div class="carousel-inner">
										    <div class="carousel-item active" >
										    	<div class="item-product-detailed">
										    		<div class="overlays-product-detail overlaysproduct1 opacity0 animated"></div>
										    		<img class="imgbanner imgbanner1 opacity0 animated" src="<?php echo base_url('asset/static/product/big/new-product7.png');?>" alt="First slide">
										    	</div>
										    </div>
										    <div class="carousel-item" >
										      <div class="item-product-detailed">
										    		<div class="overlays-product-detail "></div>
										    		<img class="imgbanner animated " src="<?php echo base_url('asset/static/product/big/new-product7.1.png');?>" alt="First slide">
										    	</div>
										    </div>
										  </div>
										</div>
									</div>
								</div>
							</div>
							<div class="level-item leveltextleft">
								<div class="product-info-detailed">
									<h2>Cool<br/>Blueberry</h2>
									<div class="meta-info">NETTO : 70 GR</div>
									<div class="description-product-detailed">
										BLUEBERRY MILK ICE CREAM WITH BLUEBERRY SAUCE INSIDE
									</div>
									<div>
										<a href="<?php echo base_url('home/requestsample');?>" class="requestsample">REQUEST SAMPLE</a>
									</div>
								</div>
							</div>
							<div class="level-right">
								<div class="level-item">
									<ul class="sosmed-product-detaile">
										<li><a href="javascript:void(0);" target="_blank" class="fb"><i class="fa fa-facebook-square"></i></a></li>
										<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=COOL BLUEBERRY - BLUEBERRY MILK ICE CREAM WITH BLUEBERRY SAUCE INSIDE" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
										<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
										<li><a href="mailto:?subject=COOL BLUEBERRY&amp;body=BLUEBERRY MILK ICE CREAM WITH BLUEBERRY SAUCE INSIDE <?php echo current_url();?>."><i class="ti-email"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- POP UP PRODUCT 8 -->
					<div id="product_8" class="product_item ccb is-clearfix">
						<div class="level">
							<div class="level-left">
								<div class="level-item">
									<div class="slideproduct">
										<div id="carouselfade_8" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
											<ul class="carousel-indicators">
											    <li data-target="#carouselfade_8" data-slide-to="0" class="active"></li>
											    <li data-target="#carouselfade_8" data-slide-to="1"></li>
											  </ul>
										  <div class="carousel-inner">
										    <div class="carousel-item active" >
										    	<div class="item-product-detailed">
										    		<div class="overlays-product-detail overlaysproduct1 opacity0 animated"></div>
										    		<img class="imgbanner imgbanner1 opacity0 animated" src="<?php echo base_url('asset/static/product/big/new-product8.png');?>" alt="First slide">
										    	</div>
										    </div>
										    <div class="carousel-item" >
										      <div class="item-product-detailed">
										    		<div class="overlays-product-detail "></div>
										    		<img class="imgbanner animated " src="<?php echo base_url('asset/static/product/big/new-product8.1.png');?>" alt="First slide">
										    	</div>
										    </div>
										  </div>
										</div>
									</div>
								</div>
							</div>
							<div class="level-item leveltextleft">
								<div class="product-info-detailed">
									<h2>Cool<br/>Pineapple</h2>
									<div class="meta-info">NETTO : 80 GR</div>
									<div class="description-product-detailed">
										PINEAPPLE ICE CREAM WITH PINEAPPLE JUICE
									</div>
									<div>
										<a href="<?php echo base_url('home/requestsample');?>" class="requestsample">REQUEST SAMPLE</a>
									</div>
								</div>
							</div>
							<div class="level-right">
								<div class="level-item">
									<ul class="sosmed-product-detaile">
										<li><a href="javascript:void(0);" target="_blank" class="fb"><i class="fa fa-facebook-square"></i></a></li>
										<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=COOL PINEAPPLE - PINEAPPLE ICE CREAM WITH PINEAPPLE JUICE" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
										<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
										<li><a href="mailto:?subject=COOL PINEAPPLE&amp;body=PINEAPPLE ICE CREAM WITH PINEAPPLE JUICE <?php echo current_url();?>."><i class="ti-email"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>

				</div>
				<script type="text/javascript">
					$(document).ready(function(){
						$('.fb').on('click',function() {
              FB.ui({
                  method: 'feed',
                  link: '<?php echo current_url();?>',
                  picture: '<?php echo base_url('asset/static/share.png');?>'
                }, function(response){
                  if (response && !response.error_code) {
                  }
                });
          });
					  $('.showproduct').on('click',function(){
					  	var ptarget = $(this).attr('data-target');
					  	var $tp = $(ptarget);
					  	if($(ptarget).length > 0){
					  		$('.overlay-main-product').addClass('show');
					  		$('#wrappingshow').addClass('showdetail');
					  		$tp.addClass('show');
					  		setTimeout(function(){
					  			$tp.find('.overlaysproduct1').animate({'opacity':1},1000).addClass('fadeInUp');
					  			setTimeout(function(){
						  			$tp.find('.imgbanner1').animate({'opacity':1},1000).addClass('fadeInUp');
						  		},500);
						  		setTimeout(function(){
						  			$tp.find('.imgbanner1').removeClass('opacity0 fadeInUp');
						  			$tp.find('.overlaysproduct1').removeClass('opacity0 fadeInUp');
						  		},1500);
					  		},500);
					  	}else{
					  		return false;
					  	}
					  });
					  $('.closecontent').on('click',function(){
					  	$('.overlay-main-product').removeClass('show');
					  	$('#wrappingshow').removeClass('showdetail');
					  	setTimeout(function(){
					  		$('.product_item').removeClass('show');
					  		$('#wrappingshow').find('.imgbanner1').addClass('opacity0').removeAttr('style');
							$('#wrappingshow').find('.overlaysproduct1').addClass('opacity0').removeAttr('style');
					  	},500);
					  });
					  $('.firstmenu').removeClass('active');
					  $('[product]').addClass('active');
						$('.imgfooter').animate({'opacity':1, 'bottom' : 0},100,function(){
							<?php if($this->input->get('content') != 'inload'){?>
								$('.socmed, .socmedmobile, .headerhome').animate({'opacity':1},1000).removeClass('fadeIn');
							<?php }?>
							setTimeout(function(){
								<?php
									if($this->input->get('p')){
								?>
								setTimeout(function(){
									var prtarget = $('#<?php echo $this->input->get('p');?>').attr('data-target');
									var $tpr = $(prtarget);
									if($(prtarget).length > 0){
										$('.overlay-main-product').addClass('show');
										$('#wrappingshow').addClass('showdetail');
										$tpr.addClass('show');
										setTimeout(function(){
											$tpr.find('.overlaysproduct1').animate({'opacity':1},1000).addClass('fadeInUp');
											setTimeout(function(){
												$tpr.find('.imgbanner1').animate({'opacity':1},1000).addClass('fadeInUp');
											},500);
											setTimeout(function(){
												$tpr.find('.imgbanner1').removeClass('opacity0 fadeInUp');
												$tpr.find('.overlaysproduct1').removeClass('opacity0 fadeInUp');
											},1500);
										},500);
									}
								},1000);
								<?php } ?>
								<?php if(in_array($this->input->get('cat'), $cat) == false){?>
								$('.contentterms').animate({'opacity' : 1},1000,function(){}).addClass('fadeInUp');
								<?php }else{?>
									$('#main-product').animate({'opacity' : 1},500,function(){}).addClass('slideInUp');
									$('.sidebare').animate({'opacity' : 1},1000,function(){}).addClass('fadeInUp');
								<?php }?>
							},1000);
						}).addClass('fadeInUp').prev('.copyright').delay(1000).animate({opacity:1},1000);
					});
				</script>
	<?php
		if($this->input->get('content') != 'inload'){
	?>
			</div>
		</section>
		<div class="copyright absolute animated opacity0">
			<div class="footermenu">
				<a href="#contentsection" data-href="<?php echo base_url('home/terms');?>" data-url="home/terms" data-title="terms of use Joyday" class="firstmenu menufoot"><span class="en">Terms of Use</span><span class="id">Syarat &amp; Ketentuan</span></a>
				<a href="#contentsection" data-href="<?php echo base_url('home/privacy');?>" data-url="home/privacy" data-title="Privacy Policy Joyday" class="firstmenu menufoot"><span class="en">Privacy Policy</span><span class="id">Kebijakan Privasi</span></a>
			</div>
		</div>
		<div class="imgfooter imgfooter1 opacity0 animated">
			<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer.png')?>" alt="shadowyellow"/>
		</div>
		<div class="imgfooter imgfooter2 opacity0 animated">
			<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer2.png')?>" alt="shadowyellow"/>
		</div>
		<div class="imgfooter imgfooter3 opacity0 animated">
			<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer3.png')?>" alt="shadowyellow"/>
		</div>
	<?php }?>
