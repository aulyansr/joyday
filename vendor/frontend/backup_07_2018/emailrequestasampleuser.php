<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="margin:0 auto;font-family:Arial,Helvetica,sans-serif">
    <tbody>
        <tr>
            <td align="left" valign="top" height="30">&nbsp; </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <h1 style="color:#333;font-size:1.5em">Hi, <?php echo $name;?></h1>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <p style="color:#7e7e7e;line-height:25px">Sudah siap mencicipi kenikmatan es krim Joyday? Berikut ini data pesanan kamu:</p>
                <ul>
                  <li style="color:#7e7e7e;line-height:1.4"><?php echo $name;?></li>
                  <li style="color:#7e7e7e;line-height:1.4"><?php echo $address;?><br/><?php echo $kabkota;?><br/><?php echo $propinsi;?></li>
                  <li style="color:#7e7e7e;line-height:1.4"><?php echo strtoupper($phone);?></li>
                </ul>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" height="30"><hr/></td>
        </tr>
        <tr>
            <td align="left" valign="top" height="30">
              <?php
              $searchString = ',';
                if( strpos($variant, $searchString) !== false ) {
                  $var = explode(",",$variant);
                  echo '<ul>';
                  for($i=0;$i<sizeof($var);$i++){
                    echo '<li style="color:#7e7e7e;line-height:1.4">'.$var[$i].'</li>';
                  }
                  echo '</ul>';
                }else{
                  echo '<p style="color:#7e7e7e;line-height:1.4">' .$variant .'</p><br/><br/>';
                }
              ?>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
              <p style="color:#7e7e7e;line-height:1.4">Saat ini pesanan kamu sedang di review oleh tim Joyday ya. Pesanan kamu akan dibalas maksimal 2x24 jam. Sambil menunggu pesanan kamu, yuk mampir ke akun social media kami di sini.</p>
              <br/>&nbsp;
              <table>
                <tbody>
                  <tr>
                    <td style="vertical-align:middle;">
                      <a href="https://www.facebook.com/JoydayIceCream/"><img style="width:40px;" src="<?php echo base_url('asset/static/fb.png');?>"></a>
                    </td>
                    <td style="padding-right:20px; vertical-align:middle;">
                      <a style="text-decoration:none;" href="https://www.facebook.com/JoydayIceCream/">JoydayIceCream</a>
                    </td>
                    <td style="vertical-align:middle;">
                      <a href="https://www.twitter.com/JoydayIceCream/"><img style="width:40px;" src="<?php echo base_url('asset/static/tw.png');?>"></a>
                    </td>
                    <td style="padding-right:20px; vertical-align:middle;">
                      <a style="text-decoration:none;" href="https://www.twitter.com/JoydayIceCream/">JoydayIceCream</a>
                    </td>
                    <td style="vertical-align:middle;">
                      <a href="https://www.instagram.com/joydayicecream/"><img style="width:40px;" src="<?php echo base_url('asset/static/ig.png');?>"></a>
                    </td>
                    <td style="vertical-align:middle;">
                      <a style="text-decoration:none;" href="https://www.instagram.com/joydayicecream/">joydayicecream</a>
                    </td>
                  </tr>
                </tbody>
              </table><br/>&nbsp;
              <p style="color:#7e7e7e;line-height:1.4">Bila ada pertanyaan lebih lanjut, kamu dapat membalas email ini. Selamat beraktivitas kembali teman - teman.</p>
            </td>
        </tr>
    </tbody>
</table>
