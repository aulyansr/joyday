<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
	<main id="mainhome">
		<section id="home" class="home animated">
			<div class="contenthome">
				<div class="container height100percent">
					<div class="row height100percent">
						<div class="col-md-12 height100percent">
							<div class="d-table mainholiday opacity0">
								<div class="d-table-cell middle height100percent">
									<div class="hero is-medium is-bold">
									  <div class="hero-body">
									    <div class="container has-text-centered">
									      <h1 class="title text-white">
									        Happy Day, JoyDay
									      </h1>
									    </div>
									  </div>
									</div>
								</div>
							</div>
							<div class="hero discoverproduct animated opacity0">
							  <div class="hero-body">
							    <div class="container has-text-centered">
							    	<a class="showdiscover" href="#contentsection" data-title="Discover Our Product" data-href="<?php echo base_url('home/discoverourproduct');?>" data-url="home/discoverourproduct">
								      <h2 class="subtitle text-white FredokaOne">
								        <span class="en">Discover Our Product</span>
												<span class="id">Pelajari Produk Selengkapnya</span>
								      </h2>
								      <h3 class="title text-white">
								        <i class="ti-angle-down"></i>
								      </h3>
								  </a>
							    </div>
							  </div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="shadowtop">
				<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/shadowtop.png')?>" alt="shadowtop"/>
			</div>
			<div class="shadowyellow shadowyellowdesktop">
				<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/yellowshadow.png')?>" alt="shadowyellow"/>
			</div>
			<div class="shadowyellow shadowyellowmobile">
				<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/yellowshadowmobile.png')?>" alt="shadowyellow"/>
			</div>
			<img class="img-fluid halal opacity0 animate" src="<?php echo GassetURL('asset/static/halal.png')?>" alt="halal"/>
		</section>
		<section id="contentsection" class="contentsection">
			<div id="load" class="contenthomeslide contenview"></div>
		</section>
		<div class="copyright absolute animated opacity0">
			<div class="footermenu">
				<a href="#contentsection" data-href="<?php echo base_url('home/terms');?>" data-url="home/terms" data-title="terms of use Joyday" class="firstmenu menufoot"><span class="en">Terms of Use</span><span class="id">Syarat &amp; Ketentuan</span></a>
				<a href="#contentsection" data-href="<?php echo base_url('home/privacy');?>" data-url="home/privacy" data-title="Privacy Policy Joyday" class="firstmenu menufoot"><span class="en">Privacy Policy</span><span class="id">Kebijakan Privasi</span></a>
			</div>
		</div>
		<div class="imgfooter imgfooter1 opacity0 animated">
			<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer.png')?>" alt="shadowyellow"/>
		</div>
		<div class="imgfooter imgfooter2 opacity0 animated">
			<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer2.png')?>" alt="shadowyellow"/>
		</div>
		<div class="imgfooter imgfooter3 opacity0 animated">
			<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer3.png')?>" alt="shadowyellow"/>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				var windowWidth = $(window).width();
				$(window).resize(function(){
					windowWidths = $(window).width();
					windowWidth = windowWidths;
				});
				$('.footermenu, .imgfooter').attr('style','z-index:-1;bottom:-100vh;');
				var contentsection = $('#contentsection').offset().top;
				var firstScroll = false;
				var lastScrollTop = 100;

				$('.home').addClass('fadeIn');
				setTimeout(function(){
			      	$('.socmed, .socmedmobile, .headerhome').animate({'opacity':1},1000).addClass('fadeIn');
			      },1200);
				setTimeout(function(){
					$('.discoverproduct').animate({'opacity':1},100,function(){
						$('.halal').animate({'opacity':1}).addClass('fadeIn');
						setTimeout(function(){
							$('.discoverproduct').removeClass('bounceInUp');
							$('.halal, .socmed, .socmedmobile').removeClass('fadeIn');
						},1800);
					}).addClass('bounceInUp');
						$('.mainholiday').delay(1500).animate({'opacity':1},1000,function(){}).addClass('fadeIn');
						$('.showdiscover').on('click',function(e){
							$('.footermenu,.imgfooter').removeAttr('style');
							$('.firstmenu').removeClass('active');
							$(this).data('clicked', true);
							if(firstScroll == false){
								if(windowWidth < 773){
									setTimeout(function(){
										$('html').css({'overflow' : 'hidden'});
									},1000);
								}else{
									$('#load').load('<?php echo base_url('home/discoverourproduct?content=inload');?>',function(){
											history.pushState({urlPath:'/home/discoverourproduct'}, 'Discover Our Product', '<?php echo base_url('home/discoverourproduct');?>');
											contentsection = 0;
											setTimeout(function(){
												$('section#home').remove();
												$('#mainhome').css({'min-height':'100vh'});
												var currlang = RC('currentlang');
												if(currlang == 'id'){
					                $('.en').css({'display':'none'});
					                $('.id').css({'display':'inline-block'});
					              }else{
													$('.id').css({'display':'none'});
					                $('.en').css({'display':'inline-block'});
												}
											},1000);
										});
								}
								firstScroll = true;
							}
						e.preventDefault();
						return false;
					});
				},1800);

				//scroll
				var $win = $(window);
				var scroll_speed = 4;
				$(window).scroll(function() {
					var st = $(this).scrollTop();
					if($('.home').length > 0){
						var bgScroll = (($win.scrollTop() - $('.home').offset().top)/ scroll_speed);
			            var bgPosition = 'center '+ bgScroll + 'px';
			            $('.home').css({ backgroundPosition: bgPosition });
						if(st > lastScrollTop){
							$('.footermenu,.imgfooter').removeAttr('style');
							target = $('#contentsection');
							$('html, body').animate({
					          scrollTop: target.offset().top
					        }, 1000, function() {
					          var $target = $(target);
					          $target.focus();
					          if ($target.is(":focus")) {
					            return false;
					          } else {
					            $target.attr('tabindex','-1');
					            $target.focus();
					          };
					            if(firstScroll == false){
												if(windowWidth < 773){
													setTimeout(function(){
														$('.showdiscover').trigger('click');
														$('html').css({'overflow' : 'hidden'});
													},500);
												}else{
						            	$('#load').load('<?php echo base_url('home/discoverourproduct?content=inload');?>',function(){
													history.pushState({urlPath:'/home/discoverourproduct'}, 'Discover Our Product', '<?php echo base_url('home/discoverourproduct');?>');
													contentsection = 0;
													setTimeout(function(){
														$('section#home').remove();
														$('#mainhome').css({'min-height':'100vh'});
														var currlang = RC('currentlang');
														if(currlang == 'id'){
							                $('.en').css({'display':'none'});
							                $('.id').css({'display':'inline-block'});
							              }else{
															$('.id').css({'display':'none'});
							                $('.en').css({'display':'inline-block'});
														}
													},1000);
												});
											}
											firstScroll = true;
										}
					        });
					        lastScrollTop = st;
						}
					}
				});
				$(window).on('resize',function(){
					if(firstScroll = false){
						targetsa = $('#home');
						$('html, body').animate({
				          scrollTop: targetsa.offset().top
				        }, 1000);
					}
				});
				$('[islink]').on('click',function(e){
		            if(contentsection > 0){
		              $('.firstmenu').removeClass('active');
		              $(this).addClass('active');
		              var href = this.href;
		              var datatarget = $('#contentsection');
		              if($(this).hasClass('closesidebar')){
		                   setTimeout(function(){
		                      $('body').removeClass('showsidebar');
		                    },800);
	                  }
		                e.preventDefault();
		                $('html, body').animate({
		                  scrollTop: datatarget.offset().top
		                }, 1000, function() {
		                  window.location = href;
		                });
		                contentsection = 0;
		            }else{
		              return true;
		            }

		          });

			});
		</script>
	</main>
