<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="id">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
  <title>Joyday - Happy day Joyday</title>
  <meta name="description" content="Es krim Joyday diproduksi oleh PT. Green Asia Food Indonesia.Joyday menggunakan susu kualitas pertama dalam industri susu murni di Cina, sebagai dasar dari setiap es krim untuk memastikan kepuasan konsumen pada setiap gigitan."/>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
  <meta id="meta-theme" name="theme-color" content="<?php echo get_cookie('bgColor') ? get_cookie('bgColor') : '#eab929';?>"/>
  <meta name="google" content="notranslate"/>
  <meta name="format-detection" content="telephone=no"/>
  <link rel="shorcut icon" type="image/x-icon" href="<?php echo GassetURL('asset/static/icon/57x57.ico'); ?>" />
  <link rel="icon" type="image/png" href="<?php echo GassetURL('asset/static/icon/160x160.png'); ?>" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo GassetURL('asset/static/icon/114x114.png'); ?>">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo GassetURL('asset/static/icon/72x72.png'); ?>">
  <link rel="apple-touch-icon-precomposed" href="<?php echo GassetURL('asset/static/icon/57x57.png'); ?>">
  <link rel="canonical" href="<?php echo current_url();?>"/>
  <link rel="manifest" href="<?php echo base_url('home/manifest');?>"/>
  <link rel="index" href="<?php echo base_url();?>">
  <meta property="fb:app_id" content="319647625233570"/>
  <meta property="og:type"   content="article" />
  <meta property="og:url" content="<?php echo current_url();?>"/>
  <meta property="og:title" content="Joyday - Happy day Joyday"/>
  <meta property="og:image" content="<?php echo base_url('asset/static/share.png');?>"/>
  <meta property="og:image:width" content="600"/>
  <meta property="og:image:height" content="315"/>
  <meta property="og:image:alt" content="<?php echo isset($title) && $title != '' ? $title : 'joyday.com'; ?>"/>
  <meta property="ia:markup_url" content="<?php echo current_url();?>"/>
  <meta property="og:description" content="Es krim Joyday diproduksi oleh PT. Green Asia Food Indonesia.Joyday menggunakan susu kualitas pertama dalam industri susu murni di Cina, sebagai dasar dari setiap es krim untuk memastikan kepuasan konsumen pada setiap gigitan.">
  <meta name="twitter:card" content="summary"/>
  <meta name="twitter:site" content="@JoydayIceCream"/>
  <meta name="twitter:creator" content="@JoydayIceCream"/>
  <link rel="stylesheet" href="<?php echo GassetURL('asset/css/bootstrap.min.css');?>"/>
  <link rel="stylesheet" href="<?php echo GassetURL('asset/css/bulma.css?v='.time());?>"/>
  <link rel="stylesheet" href="<?php echo GassetURL('asset/css/animate.css');?>"/>
  <script src="<?php echo GassetURL('asset/js/jquery.js');?>"></script>
  <script src="<?php echo GassetURL('asset/js/jquery.waypoints.js');?>"></script>
    <script type="text/javascript">

      function CC(name,value,days) {
            if (days) {
                var date = new Date();
                date.setTime(date.getTime()+(days*24*60*60*1000));
                var expires = "; expires="+date.toGMTString();
            }
            else var expires = "";
            document.cookie = name+"="+value+expires+"; path=/";
      }

      function RC(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for(var i=0;i < ca.length;i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1,c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
            }
            return null;
      }

      function EC(name) {
          CC(name,"",-1);
      }
      (function($){

      $.fn.customerPopup = function (e, intWidth, intHeight, blnResize) {
            if(this.attr('data-href') !=''){
              $href = this.attr('data-href')
            }else{
              $href = this.attr('href');
            }
            e.preventDefault();
            intWidth = intWidth || '500';
            intHeight = intHeight || '300';
            left = (screen.width/2) - (intWidth/2);
            topup = (screen.height/2) - (intHeight/2);
            strResize = (blnResize ? 'yes' : 'no');
            var strTitle = ((typeof this.attr('title') !== 'undefined') ? this.attr('title') : 'Social Share'),
                strParam = 'width=' + intWidth + ',height=' + intHeight + ',resizable=' + strResize + ',top=' +topup + ', left=' + left,
                objWindow = window.open($href, strTitle, strParam).focus();
          }
      }(jQuery));
      var JavaScript = {
          load: function (src, callback, type = null, position='head') {
              var script = document.createElement("link"),
                  loaded;
              if (type == 'css') {
                  script.setAttribute("rel", "stylesheet");
                  script.setAttribute("type", "text/css");
                  script.setAttribute("href", src)
              } else {
                  script = document.createElement('script'),
                      loaded;
                  script.setAttribute('src', src)
              }
              if (callback) {
                  script.onreadystatechange = script.onload = function () {
                      if (!loaded) {
                          callback()
                      }
                      loaded = !0
                  }
              }
              if(position == 'head'){
                  document
                  .getElementsByTagName('head')[0]
                  .appendChild(script);
                }else{
                  document
                  .body
                  .appendChild(script);
                }
            }
        }
        window.fbAsyncInit = function() {
            FB.init({
              appId      : '319647625233570',
              xfbml      : true,
              version    : 'v3.0'
            });
            FB.AppEvents.logPageView();
          };

        (function(d, s, id){
           var js, fjs = d.getElementsByTagName(s)[0];
           if (d.getElementById(id)) {return;}
           js = d.createElement(s); js.id = id;
           js.src = "https://connect.facebook.net/en_US/sdk.js";
           fjs.parentNode.insertBefore(js, fjs);
         }(document, 'script', 'facebook-jssdk'))

         $(document).ready(function(){
           $('.fbshare').on('click',function() {
             FB.ui({
                 method: 'feed',
                 link: '<?php echo current_url();?>',
                 picture: '<?php echo base_url('asset/static/share.png');?>'
               }, function(response){
                 if (response && !response.error_code) {
                 }
            });
         });
         });
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117253733-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117253733-1');
</script>
  </head>
  <body>
  <?php
    if(file_exists(ASSETPATH . 'css/style.css')){
      $stylecss =  ASSETPATH . 'css/style.css';
      $filecss = file_get_contents($stylecss, FILE_USE_INCLUDE_PATH);
      ob_start("Gminify");
      echo '<style type="text/css">';
      $resourcecss = str_replace('[srcfont]',GassetURL('asset/fonts'),$filecss);
      echo str_replace('./asset', base_url('asset'),$resourcecss );
      if(get_cookie('bgColor')){
          echo '#contentsection{background-color: '. get_cookie('bgColor') .';}#wrappslidermobile{background-color: '. get_cookie('bgColor') .';}';
      }
      if(get_cookie('currentlang')){
        if(get_cookie('currentlang') == 'id'){
          echo '.en{display:none;}';
        }else{
          echo '.id{display:none;}';
        }
      }else{
        echo '.id{display:none;}';
      }
      echo '</style>';
      ob_end_flush();
    }
  ?>
  <div class="overlay togglemenu"></div>
  <div class="headerhome headermenu animated opacity0">
    <div class="level is-mobile">
      <div class="level-item onlyinmobile">
        <ul class="iden onlyinmobile">
          <li><a href="javascript:void(0);" title="Bahasa Indonesia" class="changelanguange" id="id">ID</a></li>
          <li><a href="javascript:void(0);" title="English" class="changelanguange" id="en">EN</a></li>
        </ul>
      </div>
      <div class="level-item">
        <a class="logo" href="<?php echo base_url();?>">
          <img class="img-fluid" src="<?php echo GassetURL('asset/static/joyday-logo-new.png');?>" alt="logo"/>
        </a>
      </div>
      <div class="level-item mr-0">
        <div class="onlyinmobile width100percent">
          <div class="wrapmenumobile">
            <a class="menumobile togglemenu" href="javascript:void(0);"><i class="ti-menu"></i></a>
          </div>
        </div>
        <ul class="mainmenu clearfix">
          <li><a href="#contentsection" data-href="<?php echo base_url('home/about');?>" data-url="home/about" data-title="About Joyday" class="firstmenu"><span class="en">About</span><span class="id">Tentang</span></a></li>
          <li><a islink product href="<?php echo base_url('product');?>" class="firstmenu product-menu"><span class="en">Product</span><span class="id">Produk</span></a>
            <ul>
              <li><a islink href="<?php echo base_url('product');?>" class="thirdmenu">Sticks</a></li>
              <li><a islink href="<?php echo base_url('product/cone');?>" class="thirdmenu">Cone</a></li>
              <li><a islink href="<?php echo base_url('product/cup');?>" class="thirdmenu">Cup</a></li>
            </ul>
          </li>
          <li><a islink href="<?php echo base_url('blog');?>" blog class="firstmenu"><span class="en">Blog</span><span class="id">Blog</span></a></li>
          <li><a islink href="<?php echo base_url('home/requestsample');?>" requestsample  class="firstmenu"><span class="en">Request Sample</span><span class="id">Coba Es Krim</span></a></li>
          <li><a islink href="<?php echo base_url('home/beaseller');?>" beaseller class="firstmenu"><span class="en">Be a Seller</span><span class="id">Jadi Penjual</span></a></li>
        </ul>
        <ul class="iden onlyindesktop">
          <li><a href="javascript:void(0);" title="Bahasa Indonesia" class="changelanguange" id="id">ID</a></li>
          <li><a href="javascript:void(0);" title="English" class="changelanguange" id="en">EN</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="socmed animated opacity0 onlyindesktop">
    <div class="d-table height100percent">
      <div class="d-table-cell middle">
        <a href="//www.facebook.com/JoydayIceCream/" target="_blank"><i class="fa fa-facebook"></i></a><br/>
        <a href="//twitter.com/JoydayIceCream?lang=en" target="_blank"><i class="fa fa-twitter"></i></a><br/>
        <a href="https://www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a><br/>
        <a href="tel:+62800-1-868686"><i class="fa fa-phone"></i></a>
      </div>
    </div>
  </div>
