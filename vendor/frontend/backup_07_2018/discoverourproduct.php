<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<style type="text/css">
	<?php ob_start("Gminify");?>
	.slidemobile{
		display: none;
	}
	.slidedesktop{
		display: block;
	}
	#wrappsliderdesktop{
		position: relative;
		width: 100vw;
		height: 100vh;
	}
	#wrappsliderdesktop.bgbigccm{
		background-color: #eab929;
		background-image: url('<?php echo GassetURL('asset/static/ccm/bgbigccm.png');?>');
		-webkit-transition: all 1s ease;
			-moz-transition: all 1s ease;
			-o-transition: all 1s ease;
			-ms-transition: all 1s ease;
			transition: all 1s ease;
	}
	#wrappsliderdesktop.bgbigccb{
		background-color: #bd3990;
		background-image: url('<?php echo GassetURL('asset/static/ccb/bgbigccb.png');?>');
		-webkit-transition: all 1s ease;
			-moz-transition: all 1s ease;
			-o-transition: all 1s ease;
			-ms-transition: all 1s ease;
			transition: all 1s ease;
	}
	#wrappsliderdesktop.bgbigjujube{
		background-color: #cf6754;
		background-image: url('<?php echo GassetURL('asset/static/jujube/bgbigjujube.png');?>');
		-webkit-transition: all 1s ease;
			-moz-transition: all 1s ease;
			-o-transition: all 1s ease;
			-ms-transition: all 1s ease;
			transition: all 1s ease;
	}
	#wrappsliderdesktop.bgbighoneydew{
		background-color: #77b493;
		background-image: url('<?php echo GassetURL('asset/static/honeydew/bgbighoneydew.png');?>');
		-webkit-transition: all 1s ease;
			-moz-transition: all 1s ease;
			-o-transition: all 1s ease;
			-ms-transition: all 1s ease;
			transition: all 1s ease;
	}
	#wrappsliderdesktop.bgbigccv{
		background-color: #662021;
		background-image: url('<?php echo GassetURL('asset/static/ccv/bgbigccv.png');?>');
		-webkit-transition: all 1s ease;
			-moz-transition: all 1s ease;
			-o-transition: all 1s ease;
			-ms-transition: all 1s ease;
			transition: all 1s ease;
	}
	#wrappsliderdesktop.bgbigvms{
		background-color: #faf182;
		background-image: url('<?php echo GassetURL('asset/static/bgbigvms.png');?>');
		-webkit-transition: all 1s ease;
			-moz-transition: all 1s ease;
			-o-transition: all 1s ease;
			-ms-transition: all 1s ease;
			transition: all 1s ease;
	}
	.contenslide{
		position: absolute;
		top: 0;
		left: 0;
		width: 100vw;
		height: 100vh;
		z-index: 10;
	}
	@media (max-width: 850px){
		.slidedesktop{
			display: none;
		}
		.slidemobile{
			position: absolute;
			top:0;
			left:0;
			width:100vw;
			height: 100vh;
			display: block;
			z-index:1;
		}
		#wrappslidermobile{
			position: relative;
			width: 100vw;
			height: 100vh;
		}
		.carousel-item{
			text-align: center;
		}
		.carousel-fade img {
		    margin: 0 auto;
		    max-height: 100vh;
		    max-width: 100%;
		}
		#wrappslidermobile{
			background-color: #b57403;
			background-image: url('<?php echo GassetURL('asset/static/ccm/bgccm.png');?>');
			background-size: cover;
			background-position: center top;
			background-repeat: no-repeat;
			-webkit-transition: all 1s ease;
		    -moz-transition: all 1s ease;
		    -o-transition: all 1s ease;
		    -ms-transition: all 1s ease;
		    transition: all 1s ease;
		}
		#wrappslidermobile.bgccm{
			background-color: #b57403;
			background-image: url('<?php echo GassetURL('asset/static/ccm/bgccm.png');?>');
			-webkit-transition: all 1s ease;
		    -moz-transition: all 1s ease;
		    -o-transition: all 1s ease;
		    -ms-transition: all 1s ease;
		    transition: all 1s ease;
		}
		#wrappslidermobile.bgccb{
			background-color: #b57403;
			background-image: url('<?php echo GassetURL('asset/static/ccb/bgccb.png');?>');
			-webkit-transition: all 1s ease;
		    -moz-transition: all 1s ease;
		    -o-transition: all 1s ease;
		    -ms-transition: all 1s ease;
		    transition: all 1s ease;
		}
		#wrappslidermobile.bgjujube{
			background-color: #ed1846;
			background-image: url('<?php echo GassetURL('asset/static/jujube/bgjujube.png');?>');
			-webkit-transition: all 1s ease;
		    -moz-transition: all 1s ease;
		    -o-transition: all 1s ease;
		    -ms-transition: all 1s ease;
		    transition: all 1s ease;
		}
		#wrappslidermobile.bghoneydew{
			background-color: #9fe3a8;
			background-image: url('<?php echo GassetURL('asset/static/honeydew/bghoneydew.png');?>');
			-webkit-transition: all 1s ease;
		    -moz-transition: all 1s ease;
		    -o-transition: all 1s ease;
		    -ms-transition: all 1s ease;
		    transition: all 1s ease;
		}
		#wrappslidermobile.bgccv{
			background-color: #541d11;
			background-image: url('<?php echo GassetURL('asset/static/ccv/bgccv.png');?>');
			-webkit-transition: all 1s ease;
		    -moz-transition: all 1s ease;
		    -o-transition: all 1s ease;
		    -ms-transition: all 1s ease;
		    transition: all 1s ease;
		}
		#wrappslidermobile.bgvanilla{
			background-color: #faf284;
			background-image: url('<?php echo GassetURL('asset/static/bgvanilla.png');?>');
			-webkit-transition: all 1s ease;
		    -moz-transition: all 1s ease;
		    -o-transition: all 1s ease;
		    -ms-transition: all 1s ease;
		    transition: all 1s ease;
		}
		.contenslidemobile{
			position: absolute;
			top: 0;
			left: 0;
			width: 100vw;
			height: 100vh;
			z-index: 10;
		}
		.wrapanimate{
			position: absolute;
			top: 0;
			left: 0;
			width: 100vw;
			height: 100vh;
			z-index: 4;
		}
		.wrapanimate > div{
			position: relative;
			top:0;
			left:0;
			width: 100vw;
			height: 100vh;
		}
		[class*="mobile-"]{
			position: absolute;
		}
		.mobile-ccm1{
			top: 10vh;
			left: 0;
			height: 100px;
		}
		.mobile-ccm2{
			bottom: -7vh;
		    left: 0;
		}
		.mobile-ccm3 {
		    top: 20vh;
		    left: 20vw;
		    height: 80px;
		}
		.mobile-ccm4 {
		    bottom: 20vh;
		    right: 30vw;
		    height: 120px;
		}
		.mobile-ccm5{
		    top: 15vh;
		    height: 120px;
		    right: 0;
		}
		.mobile-ccb1{
			bottom: 0;
			left: -8vw;
			height: 300px;
		}
		.mobile-ccb2{
			top: 10vh;
			right: -10vw;
			height: 200px;
		}
		.mobile-ccb3{
			bottom: 5vh;
    		right: 0vw;
		}
		.mobile-ccb4{
			bottom: 15vh;
		    left: 25vw;
		    height: 250px;
		}
		.mobile-ccb5{
			top: 25vh;
		    left: 10vw;
		    height: 200px;
		}
		.mobile-jujube1{
			top: 15vh;
			left: 5vw;
		}
		.mobile-jujube2{
			top: 30vh;
			left: 3vw;
		}
		.mobile-jujube3{
			top: 50vh;
			left: 3vw;
		}
		.mobile-jujube4{
			bottom: 0vh;
			left: 15vw;
		}
		.mobile-jujube5{
			bottom: 0;
			right: 0;
		}
		.mobile-jujube6{
			bottom: 40vh;
    		right: 25vw;
		}
		.mobile-jujube7{
			top: 15vh;
			right: 0;
		}
		.mobile-honeydew1{
			top:0;
			left:0;
			width:200vw;
			max-width: 200vw;
		}
		.mobile-honeydew2{
			top:20vh;
			left:0;
		}
		.mobile-honeydew3{
			bottom:20vh;
			left:0;
		}
		.mobile-honeydew4{
			bottom:0;
			right:0;
		}
		.mobile-honeydew5{
			top:20vh;
			right:15vw;
		}

		.mobile-ccv1{
			top:20vh;
			left:5vw;
		}
		.mobile-ccv2{
			bottom:0vh;
			left:0
		}
		.mobile-ccv3{
			bottom:35vh;
			right:0
		}
		.mobile-ccv4{
			top:15vh;
			right:0
		}
		.mobile-vms{
			bottom:0;
			width:100vw;
		}
	}
	@media (max-width: 740px)
	{
		.mobile-ccm1{
			top: 20vh;
			left: 0;
			height: 50px;
		}
		.mobile-ccm2{
			height: 200px;
		}
		.mobile-ccm3{
			left: 35vw;
    		height: 50px;
		}
		.mobile-ccm4{
			right: 40vw;
    		height: 60px;
		}
		.mobile-ccm5{
			top: 25vh;
    		height: 70px;
		}
		.mobile-ccb1{
			height: 200px;
		}
		.mobile-ccb2{
			top: 20vh;
		    right: 0;
		    height: 100px;
		}
		.mobile-ccb3{
			right: 10vw;
    		height: 120px;
		}
		.mobile-ccb4{
			left: 35vw;
    		height: 130px;
		}
		.mobile-ccb5{
			left: 20vw;
    		height: 100px;
		}
		.mobile-jujube1{
			top: 20vh;
		    height: 50px;
		}
		.mobile-jujube2{
			top: 35vh;
		    left: 7vw;
		    height: 80px;
		}
		.mobile-jujube3{
			top: 60vh;
		    left: 7vw;
		    height: 60px;
		}
		.mobile-jujube4{
			left: 25vw;
    		height: 75px;
		}
		.mobile-jujube5{
			height: 180px;
		}
		.mobile-jujube6{
			bottom: 25vh;
		    right: 40vw;
		    height: 60px;
		}
		.mobile-jujube7{
			top: 25vh;
		    right: 0;
		    height: 70px;
		}
		.mobile-honeydew1{
			width:100vw;
			max-width: 100vw;
		}
		.mobile-honeydew2{
			left: 5vw;
   			height: 70px;
		}
		.mobile-honeydew3{
			height: 80px;
		}
		.mobile-honeydew4{
			height: 200px;
		}
		.mobile-honeydew5{
			right:5vw;
		}
		.mobile-ccv2{
			height: 180px;
		}
		.mobile-ccv3{
			bottom: 10vh;
		}
		.mobile-ccv4{
			top: 20vh;
		    right: 0;
		    height: 100px;
		}
	}
	@media (max-width: 500px)
	{
		.carousel-fade .carousel-caption h2{
			font-size: 1.8rem;
		}
		.carousel-fade .carousel-caption p,.carousel-fade .carousel-caption a{
			font-size: .85rem;
		}
		.mobile-ccm4{
			bottom: 30vh;
		    right: 30vw;
		    height: 60px;
		}
		.mobile-ccb3{
			right: 0vw;
		}
		.mobile-ccb4{
			bottom: 22vh;
		}
		.mobile-ccb5{
			left: 15vw;
    		height: 100px;
		}
		.mobile-jujube1{
			top: 20vh;
		    height: 50px;
		}
		.mobile-jujube2{
			top: 35vh;
		    left: 7vw;
		    height: 80px;
		}
		.mobile-jujube3{
			top: 60vh;
		    left: 7vw;
		    height: 60px;
		}
		.mobile-jujube4{
			bottom:8vh;
		}
		.mobile-jujube5{
			height: 250px;
		}
		.mobile-jujube6{
			bottom: 32vh;
		}
		.mobile-jujube7{
			top: 20vh;
		}
		.mobile-honeydew1{
			width:200vw;
			max-width: 200vw;
		}
		.mobile-honeydew4{
			height: 300px;
		}
		.mobile-vms{
			bottom: 10vh;
		}
		.mobile-jujube6{
			bottom: 25vh;
		}
	}
	@media (max-width: 390px){
		.mobile-ccb4{
			bottom:13vh;
		}
		.mobile-ccm4{
			bottom: 20vh;
		}
	}
	<?php ob_end_flush();?>
</style>
<style type="text/css">
.titlepage{
	font-size: 2rem;
	text-transform: uppercase;
	text-align: center;
	margin-bottom: 2rem;
	color:#0060af;
}
.paper p{
	text-transform: uppercase;
	letter-spacing: .2em;
	color:#0060af;
	font-weight: 300;
	font-size: .7em;
	line-height: 2;
	margin-bottom:1em;
}
.menusidebarblog{
	list-style: none;
	padding:0;
	margin:0;
	letter-spacing: .2em;
}
.menusidebarblog li{
	text-align: right;
}
.menusidebarblog li a{
	text-align: right;
	position: relative;
	font-weight: 300;
	font-size: 1.3rem;
	text-transform: uppercase;
	display: inline-block;
	opacity: .5;
	color: #3273dc;
}
.menusidebarblog li a span{
	float: left;
}
.menusidebarblog li a span + span{
	float: right;
}
a:not([href]):not([tabindex]):focus,
a:not([href]):not([tabindex]):hover,
.menusidebarblog li a.active,
.menusidebarblog li:hover a,
.menusidebarblog li li:hover a,
.menusidebarblog li li:hover a:hover{
	text-decoration: none;
	opacity: 1;
	color: #0060af !important;
}
.menusidebarblog > li > a:after{
	content: "";
    display: inline-block;
    position: absolute;
    right: .15em;
    z-index: 1;
    bottom: 0;
    width: 100%;
    height: 1px;
    transition: opacity .5s ease-in-out;
    -moz-transition: opacity .5s ease-in-out;
    -webkit-transition: opacity .5s ease-in-out;
    border-bottom: .1em solid transparent;
}
.menusidebarblog > li:hover > a:after,
.menusidebarblog > li > a.active:after{
    border-bottom: .1em solid #0060af;
}
.menusidebarblog li ul{
    margin-right: -30px;
    padding-right: 30px;
    max-height: 0;
    overflow: hidden;
    -webkit-transition: all .5s ease;
    -moz-transition: all .5s ease;
    -o-transition: all .5s ease;
    -ms-transition: all .5s ease;
    transition: all .5s ease;
}

.menusidebarblog li:hover ul,
.menusidebarblog li > a.active + ul{
	max-height: 1000px;
    -webkit-transition: all .5s ease;
    -moz-transition: all .5s ease;
    -o-transition: all .5s ease;
    -ms-transition: all .5s ease;
    transition: all .5s ease;
}
.menusidebarblog li li{
	position: relative;
	margin-top:0;
	padding-top:0;
}
.menusidebarblog li li a:after{
	position: absolute;
    top: 14px;
    content: "";
    display: block;
    width: 10px;
    height: 10px;
    border-radius: 50%;
    right: -20px;
    background: #0060af;
    opacity: .5;
    display: none;
}
.menusidebarblog li li:hover a:after,.menusidebarblog li li a.active:after{
	display: block;
	opacity: 1;
}
.menusidebarblog li li a{
	position: relative;
	display: inline-block;
	width: 100%;
	font-size: .85em;
	padding:10px 0;
	margin:0;
}
.menusidebarblog li:hover li a:not(.active),
.menusidebarblog li li:hover a:not(.active){
	opacity: .5;
}
.menusidebarblog li li:hover a:hover{
	opacity: 1;
}
.sidebare{
	max-height: 80%;
	padding-right:30px;
}
.sidebare .wrapmenusidebar{
	max-width: 220px;
	margin:0;
	padding-top:30px;
}
.sidebare h2{
	float: right;
    letter-spacing: .1em;
    text-transform: uppercase;
    text-align: right;
    font-size: 1.8rem;
    margin: 0 0 2rem 0;
    margin-bottom: 1.5rem;
    color: #0060af;
}
.item-product{
	position: relative;
	letter-spacing: .2em;
	line-height: 1.5;
	font-size:.78rem;
	text-transform: uppercase;
	color: #0060af;
	margin-bottom: 25px;
	background: rgba(255,255,255,.5);
}
.item-product .level{
	padding:30px;
}
.item-product:hover{
	opacity: 1;
	cursor: pointer;
}
.item-product .imgproduct img{
	position: absolute;
	width:100%;
  left: 50%;
	bottom:0;
  -webkit-transition: all .5s ease;
  -moz-transition: all .5s ease;
  -o-transition: all .5s ease;
  -ms-transition: all .5s ease;
  transition: all .5s ease;

}
.item-product .imgproduct{
	position: relative;
	width:62px;
	margin:0 auto;
	min-height: 130px;
	min-width: 61px;
	text-align: center;
	transform: translateX(-50%);
  -webkit-transition: all .5s ease;
  -moz-transition: all .5s ease;
  -o-transition: all .5s ease;
  -ms-transition: all .5s ease;
  transition: all .5s ease;

}
.item-product:hover .imgproduct{
	width:70px;
}
.title-product{
	font-size:2rem;
	margin:0;
	padding:0;
}
.meta-title-product{
	padding:5px 0 20px 0;
}
.content-item-product{
	line-height: 2;
	margin-bottom: 25px;
}
.readmore{
	display: inline-block;
	position: relative;
}
.item-product:nth-of-type(odd) {

}

.item-product:nth-of-type(even) {
}
.papaerproduct{
	padding-left: 20px;
	padding-right:20px;
	padding-bottom: 50px;
}
.fancybox-slide--iframe .fancybox-close-small{
	right:0;
}
.fancybox-slide--iframe .fancybox-content::-webkit-scrollbar {
    width: 10px;
}

/* Track */
.fancybox-slide--iframe .fancybox-content::-webkit-scrollbar-track {
	background: rgba(255,255,255,0.7);
    border: 4px solid transparent;
    background-clip: content-box;
    border-radius: 10px;
}

/* Handle */
.fancybox-slide--iframe .fancybox-content::-webkit-scrollbar-thumb {
    background: rgb(255,255,255);
    border-radius: 10px;
}
.fancybox-slide--iframe .fancybox-content::-webkit-scrollbar-thumb:vertical {
    height:15px;
}
/* Handle on hover */
.fancybox-slide--iframe .fancybox-content::-webkit-scrollbar-thumb:hover {
    background: #555;
}
.fancybox-bg{
	background: rgba(255,255,255,0.3);
}
.close-smalle{
	position: absolute;
    top: 8px;
    right: 40px;
    width: 30px;
    height: 30px;
    line-height: 1;
    padding: 0;
    margin: 0;
    border: 0;
    border-radius: 0;
    outline: none;
    background: transparent;
    z-index: 10;
    cursor: pointer;
}
.close-smalle:after {
    content: '×';
    position: absolute;
    top: 0;
    right: 0;
    width: 30px;
    height: 30px;
    font: 40px/30px Arial,"Helvetica Neue",Helvetica,sans-serif;
    color: #0060af;
    font-weight: 300;
    text-align: center;
    border-radius: 50%;
    border-width: 0;
    background: transparent;
    transition: background .25s;
    box-sizing: border-box;
    z-index: 2;
}
#wrappingshow .detail-product{
	position: absolute;
    top: 120vh;
    z-index: 10;
		height: 80vh;
		width: 1000px;
		max-width: 95vw;
		left: 50%;
    transform: translateX(-50%);
    color: #0060af;
    background-color: transparent;
    overflow-x: hidden;
    overflow-y: auto;
	-webkit-transition: all 1s ease;
    -moz-transition: all 1s ease;
    -o-transition: all 1s ease;
    -ms-transition: all 1s ease;
    transition: all 1s ease;
}
#wrappingshow.showdetail .detail-product{
	top: 20vh;
	-webkit-transition: all 1s ease;
    -moz-transition: all 1s ease;
    -o-transition: all 1s ease;
    -ms-transition: all 1s ease;
    transition: all 1s ease;
}
#wrappingshow.showdetail .nanas .blogwrap .ss-container{
	overflow-y: hidden;
}
.product_item{
	margin: 0 30px;
  border-radius: 5px;
  box-shadow: 0 10px 20px 0px #555;
  min-height: 80vh;
  background-color: rgba(255,255,255,.95);
}
.slideproduct{
	width: 300px;
    max-width: 90%;
    margin: 0 3rem 4rem 3rem;
}
.slideproduct img{
	position: relative;
	z-index:3;
}
.overlays-product-detail{
	width: 100%;
    left: 0;
    height: calc(80% - 3rem);
    position: absolute;
    bottom: 0;
    z-index: 1;
    border-radius: 10px;
    background-color: rgba(238,204,91,.8);

}
.carousel-indicators{
	bottom:-2rem;
}
.carousel-indicators li{
	border: 2px solid #005b9c;
    position: relative;
    -webkit-box-flex: 0;
    -ms-flex: 0 1 auto;
    flex: 0 1 auto;
    width: 12px;
    height: 12px;
    border-radius: 50%;
    margin-right: 10px;
    margin-left: 10px;
    text-indent: -999px;
    background-color: rgba(255,255,255,.5);
}
.carousel-indicators li:not(.active){
    background-color: #005b9c !important;
}
.carousel-indicators li:hover{
	cursor: pointer;
}
.leveltextleft{
	justify-content:left;
}
.product-info-detailed h2{
	text-transform: uppercase;
	font-size:2rem;
}
.description-product-detailed{
	font-size:1rem;
	max-width: 400px;
	letter-spacing: .25em;
	line-height: 1.5;
	padding-bottom: 3rem;
}
.requestsample{
	font-size: 1.1rem;
	text-decoration: none;
	border:.1em solid #005b9c;
	padding:.8rem 1rem;
	border-radius: 5px;
	color: #005b9c;
}
.requestsample:hover{
	text-decoration: none;
	color: #005b9c;
}
.meta-info{
	padding:.5rem 0;
}
.sosmed-product-detaile{
	padding-right:1rem;
	font-size:2rem;
}
.sosmed-product-detaile a{
	text-decoration: none;
	color: #005b9c;
	cursor: pointer;
	opacity: .9;
}
.overlay-main-product{
	position: absolute;
	width:100%;
	height: 100%;
	z-index:25;
	top:0;
	left:0;
	display: none;
}
.overlay-main-product.show{
	display: block;
}
.product_item{
	display: none;
}
.product_item.show{
	display: block;
}
.headerhome,.socmed{
	opacity: 1;
}
@media (max-width: 1056px){
	.slideproduct {
	    margin: 0 2rem 4rem 2rem;
	}
}
@media (max-width: 825px)
{
	.product-info-detailed{
		display: block;
	    margin: 0 auto;
	    text-align: center;
	}
	.sosmed-product-detaile{
		display: inline-block;
		margin:0 auto;
		padding:15px 0;
	}.sosmed-product-detaile li{
		float: left;
	}
	.sosmed-product-detaile li:not(:last-child){
		margin-right: 15px;
	}
	.sosmed-product-detaile:after{
		content: "";
		clear: both;
		display: block;
	}
	.product_item{
		padding-bottom: 15vh;
	}
	.imgtitle{
		text-align: center;
	}
}
@media (max-width: 740px)
{
	.product-info-detailed h2{
		font-size: 1.5rem;
	}
	.description-product-detailed{
		font-size: .85rem;
	}
	#main-product{
		margin-top:10vh;
	}
	#wrappingshow.showdetail .detail-product{
		top:30vh;
	}
	.product_item{
		padding-bottom: 20vh;
	}
}
@media (max-width: 500px)
{
.close-smalle{
	right:20px;
}
	#wrappingshow .detail-product{
		width: 95vw;
    margin: 0 10px;
		left:0vw;
		transform: translateX(0);
	}
	#main-product{
		margin-top:0;
	}
	#wrappingshow.showdetail .detail-product{
		top:15vh;
	}
	.product_item{
		margin:0 15px;
	}
	.description-product-detailed{
		max-width: 280px;
	}
}
</style>
	<?php
		if($this->input->get('content') != 'inload'){
	?>
	<section id="contentsection" class="contentsection">
		<div id="load" class="contenthomeslide contenview">
	<?php }?>
			<div id="wrappingshow">
				<div class="slidedesktop">
					<div id="wrappsliderdesktop" class="bgbigccm">
						<div class="wrapanimate">
							<div class="remoattr">
								<img class="img-fluid nanas1 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas1.png');?>">
								<img class="img-fluid nanas2 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas2.png');?>">
								<img class="img-fluid nanas3 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas3.png');?>">
								<img class="img-fluid nanas4 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas4.png');?>">
								<img class="img-fluid nanas5 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas5.png');?>">
								<img class="img-fluid nanas6 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas6.png');?>">
								<img class="img-fluid nanas7 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas7.png');?>">

								<img class="img-fluid jujube1 naanimation animated opacity0" src="<?php echo base_url('asset/static/jujube/jujube1.png');?>">
								<img class="img-fluid jujube2 naanimation animated opacity0" src="<?php echo base_url('asset/static/jujube/jujube2.png');?>">
								<img class="img-fluid jujube3 naanimation animated opacity0" src="<?php echo base_url('asset/static/jujube/jujube3.png');?>">
								<img class="img-fluid jujube4 naanimation animated opacity0" src="<?php echo base_url('asset/static/jujube/jujube4.png');?>">
								<img class="img-fluid jujube5 naanimation animated opacity0" src="<?php echo base_url('asset/static/jujube/jujube5.png');?>">

								<img class="img-fluid honeydew1 naanimation animated opacity0" src="<?php echo base_url('asset/static/honeydew/honeydew1.png');?>">
								<img class="img-fluid honeydew2 naanimation animated opacity0" src="<?php echo base_url('asset/static/honeydew/honeydew2.png');?>">
								<img class="img-fluid honeydew3 naanimation animated opacity0" src="<?php echo base_url('asset/static/honeydew/honeydew3.png');?>">
								<img class="img-fluid honeydew4 naanimation animated opacity0" src="<?php echo base_url('asset/static/honeydew/honeydew4.png');?>">
								<img class="img-fluid honeydew5 naanimation animated opacity0" src="<?php echo base_url('asset/static/honeydew/honeydew5.png');?>">

								<img class="img-fluid ccv1 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccv/ccv1.png');?>">
								<img class="img-fluid ccv2 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccv/ccv2.png');?>">
								<img class="img-fluid ccv3 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccv/ccv3.png');?>">
								<img class="img-fluid ccv4 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccv/ccv4.png');?>">
								<img class="img-fluid ccv5 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccv/ccv5.png');?>">
								<img class="img-fluid ccv6 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccv/ccv6.png');?>">

								<img class="img-fluid vm naanimation animated opacity0" src="<?php echo base_url('asset/static/bg-vanilla-milkshake-banner.png');?>">

								<img class="img-fluid ccm1 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccm/ccm1.png');?>">
								<img class="img-fluid ccm2 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccm/ccm2.png');?>">
								<img class="img-fluid ccm3 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccm/ccm3.png');?>">
								<img class="img-fluid ccm4 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccm/ccm4.png');?>">
								<img class="img-fluid ccm5 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccm/ccm5.png');?>">

								<img class="img-fluid ccb1 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccb/ccb1.png');?>">
								<img class="img-fluid ccb2 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccb/ccb2.png');?>">
								<img class="img-fluid ccb3 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccb/ccb3.png');?>">
								<img class="img-fluid ccb4 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccb/ccb4.png');?>">
								<img class="img-fluid ccb5 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccb/ccb5.png');?>">
							</div>
						</div>
						<div id="contenslide" class="bgbigccm">
							<div id="carouselfade" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
							  <div class="carousel-inner">

							    <div class="carousel-item active" >
							      <img class="imgbanner imgbanner1 animated opacity0" src="<?php echo base_url('asset/static/crunchy-chocolate-malt-banner-update.png');?>" alt="First slide">
							      <div class="carousel-caption captionbanner captionbanner1">
							      	<div class="relative">
							      		<div class="d-inline-block hoverimage">
												<a class="linkproduct showproduct" data-target="#product_1" href="javascript:void(0);">
											    <h2 class="FredokaOne">CRUNCHY CHOCOLATE MALT</h2>
											    <div class="COCOGOOSE p en">VANILLA ICE CREAM COVERED WITH CRUNCHY CHOCOLATE AND MALT</div>
													<div class="COCOGOOSE p id">ES MENGANDUNG SUSU RASA VANILLA DENGAN DUA LAPISAN COKELAT DAN MALT</div>
												</a>
												<br/>
										    <a href="<?php echo base_url('home/requestsample');?>" class="COCOGOOSE"><span class="en">Request Sample</span><span class="id">Coba Es Krim</span></a>
										    <br/><br/>
										</div>
									</div>
								  </div>
							    </div>

							    <div class="carousel-item" >
							      <img class="d-block imgbanner animated" src="<?php echo base_url('asset/static/img-chocoBlue.png');?>" alt="First slide">
							      <div class="carousel-caption captionbanner">
							      	<div class="relative">
							      		<div class="d-inline-block hoverimage">
													<a class="linkproduct showproduct" data-target="#product_2" href="javascript:void(0);">
												    <h2 class="FredokaOne">CRUNCHY CHOCOLATE BLUEBERRY</h2>
												    <div class="COCOGOOSE p en">CHOCOLATE COVERED VANILLA ICE CREAM WITH BLUEBERRY JAM FILLING</div>
														<div class="COCOGOOSE p id">ES MENGANDUNG SUSU RASA VANILLA DENGAN DUA LAPISAN COKELAT MALT DAN BLUEBERRY</div>
													</a>
													<br/>
										    <a href="<?php echo base_url('home/requestsample');?>" class="COCOGOOSE"><span class="en">Request Sample</span><span class="id">Coba Es Krim</span></a>
										    <br/><br/>
										</div>
									</div>
								  </div>
							    </div>

							    <div class="carousel-item" >
							      <img class="d-block imgbanner animated" src="<?php echo base_url('asset/static/jujube-banner.png');?>" alt="JUJUBE WHITE CHOCOLATE">
							      <div class="carousel-caption captionbanner">
							      	<div class="relative">
							      		<div class="d-inline-block hoverimage">
													<a class="linkproduct showproduct" data-target="#product_3" href="javascript:void(0);">
												    <h2 class="FredokaOne">JUJUBE WHITE CHOCOLATE</h2>
												    <div class="COCOGOOSE p en">CREAMY ICE CREAM WITH RED DATES, COVERED WITH WHITE CHOCOLATE</div>
														<div class="COCOGOOSE p id">ES MENGANDUNG SUSU RASA KURMA MERAH DENGAN LAPISAN COKELAT PUTIH</div>
													</a>
													<br/>
										    <a href="<?php echo base_url('home/requestsample');?>" class="COCOGOOSE"><span class="en">Request Sample</span><span class="id">Coba Es Krim</span></a>
										    <br/><br/>
										</div>
									</div>
								  </div>
							    </div>
							    <div class="carousel-item" >
							      <img class="d-block imgbanner animated" src="<?php echo base_url('asset/static/crunchy-honeydew-melon-banner-new.png');?>" alt="CRUNCHY HONEYDEW MELON">
							      <div class="carousel-caption captionbanner">
							      	<div class="relative">
							      		<div class="d-inline-block hoverimage">
													<a class="linkproduct showproduct" data-target="#product_4" href="javascript:void(0);">
												    <h2 class="FredokaOne">CRUNCHY HONEYDEW MELON</h2>
												    <div class="COCOGOOSE p en">HONEYDEW FLAVOURED ICE CREAM WITH CHOCOLATE SAUCE AND PEANUT TOPPINGS</div>
												    <div class="COCOGOOSE p id">ES MENGANDUNG SUSU CONE RASA MELON DENGAN SAUS COKELAT DAN BUTIRAN KACANG</div>
													</a>
													<br/>
										    <a href="javascript:void(0);" onclick="$('.firsmenu[requestsample]').trigger('click')" class="COCOGOOSE"><span class="en">Request Sample</span><span class="id">Coba Es Krim</span></a>
										    <br/><br/>
										</div>
									</div>
								  </div>
							    </div>
							    <div class="carousel-item" >
							      <img class="d-block imgbanner animated" src="<?php echo base_url('asset/static/crunchy-chocolate-vanilla-banner.png');?>" alt="CRUNCHY CHOCOLATE VANILLA">
							      <div class="carousel-caption captionbanner">
							      	<div class="relative">
							      		<div class="d-inline-block hoverimage">
													<a class="linkproduct showproduct" data-target="#product_5" href="javascript:void(0);">
												    <h2 class="FredokaOne">CRUNCHY CHOCOLATE VANILLA</h2>
												    <div class="COCOGOOSE p en">CHOCOLATE VANILLA FLAVOURED ICE CREAM WITH CHOCOLATE SAUCE AND PEANUT TOPPINGS</div>
												    <div class="COCOGOOSE p id">ES MENGANDUNG SUSU CONE RASA COKELAT VANILLA DENGAN SAUS COKELAT DAN BUTIRAN KACANG</div>
													</a>
														<br/>
												  <a href="<?php echo base_url('home/requestsample');?>" class="COCOGOOSE"><span class="en">Request Sample</span><span class="id">Coba Es Krim</span></a>
										    <br/><br/>
										</div>
									</div>
								  </div>
							    </div>
							    <div class="carousel-item" >
							      <img class="d-block imgbanner animated" src="<?php echo base_url('asset/static/vanilla-milkshake-banner-alt.png');?>" alt="VANILLA MILKSHAKE">
							      <div class="carousel-caption captionbanner">
							      	<div class="relative">
							      		<div class="d-inline-block hoverimage">
													<a class="linkproduct showproduct" data-target="#product_6" href="javascript:void(0);">
												    <h2 class="FredokaOne">VANILLA MILKSHAKE</h2>
												    <div class="COCOGOOSE p en">CREAMY VANILLA MILKSHAKE FLAVOUR</div>
												    <div class="COCOGOOSE p id">ES SUSU RASA VANILLA</div>
													</a>
													<br/>
										    <a href="<?php echo base_url('home/requestsample');?>" class="COCOGOOSE"><span class="en">Request Sample</span><span class="id">Coba Es Krim</span></a>
										    <br/><br/>
										</div>
									</div>
								  </div>
							    </div>
							  </div>
							  <a class="carousel-control-prev" href="#carouselfade" role="button" data-slide="prev">
							    <span class="ti-angle-left" aria-hidden="true"></span>
							    <span class="sr-only">Previous</span>
							  </a>
							  <a class="carousel-control-next" href="#carouselfade" role="button" data-slide="next">
							    <span class="ti-angle-right" aria-hidden="true"></span>
							    <span class="sr-only">Next</span>
							  </a>
							</div>
						</div>
						<script>
							$(document).ready(function(){
								var slidecomplete = false;
								if(RC('bgColor')){
										EC('bgColor');
										EC('bgClass');
									}
								function remoattr()
								{
									$('.remoattr').find('img').removeAttr('style');
								}
								function nanasAnimate()
								{
									$('#carouselfade').find('[role="button"]').css({'pointer-events':'none','cursor':'wait'});
									$('.nanas3').animate({'opacity':1},100,function(){
											$('.nanas4').animate({'opacity': 1},100,function(){
												setTimeout(function(){
													$('.nanas1').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
													$('.nanas2').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
												},300);
											}).addClass('fadeInUp');
											$('.nanas6').animate({'opacity': 1},100,function(){
												$('.nanas7').animate({'opacity': 1},100,function(){
													$('.socmed').animate({'opacity':1},1000).removeClass('fadeIn');
													setTimeout(function(){
														$('.imgbanner1').animate({'opacity' : 1},1000,function(){}).addClass('fadeInUp');
														setTimeout(function(){
															$('.imgbanner1').removeClass('fadeInUp');
															$('.imgbanner1').removeClass('opacity0');
															$('.imgbanner1').removeAttr('style');
															$('.imgbanner').removeClass('imgbanner1');
															$('.nanas3').removeClass('fadeInUp');
															$('.nanas4').removeClass('fadeInUp');
															$('.nanas6').removeClass('fadeInUp');
															$('.nanas1').removeClass('fadeInUp');
															$('.nanas2').removeClass('fadeInUp');
															$('.nanas7').removeClass('fadeInUp');
															$('#carouselfade').find('[role="button"]').removeAttr('style');
														},1100);
													},1000);
												}).addClass('fadeInUp');
											}).addClass('fadeInUp');
										}).addClass('fadeInUp');
								}

								function ccbAnimating()
								{
									$('#carouselfade').find('[role="button"]').css({'pointer-events':'none','cursor':'wait'});
									$('.ccb2').animate({'opacity':1},100,function(){
										$('.ccb5').animate({'opacity': 1},100,function(){
											setTimeout(function(){
												$('.ccb1').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
												$('.ccb3').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
											},300);
										}).addClass('fadeInUp');
										$('.ccb4').animate({'opacity': 1},100,function(){
											setTimeout(function(){
												$('.imgbanner1').animate({'opacity' : 1},1000,function(){}).addClass('fadeInUp');
												setTimeout(function(){
														$('.imgbanner1').removeClass('fadeInUp');
														$('.imgbanner1').removeClass('opacity0');
														$('.imgbanner1').removeAttr('style');
														$('.imgbanner').removeClass('imgbanner1');
														$('.socmed,.socmedmobile,.headerhome').removeClass('fadeInUp');
														$('.ccb2').removeClass('fadeInUp');
														$('.ccb5').removeClass('fadeInUp');
														$('.ccb4').removeClass('fadeInUp');
														$('.ccb1').removeClass('fadeInUp');
														$('.ccb3').removeClass('fadeInUp');
														$('#carouselfade').find('[role="button"]').removeAttr('style');
												},1100);
											},1000);
										}).addClass('fadeInUp');
									}).addClass('fadeInUp');
								}
								function nanasAnimating()
								{
									$('#carouselfade').find('[role="button"]').css({'pointer-events':'none','cursor':'wait'});
									$('.nanas3').animate({'opacity':1},100,function(){
										$('.nanas4').animate({'opacity': 1},100,function(){
											setTimeout(function(){
												$('.nanas1').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
												$('.nanas2').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
											},300);
										}).addClass('fadeInUp');
										$('.nanas6').animate({'opacity': 1},100,function(){
											$('.nanas7').animate({'opacity': 1},100,function(){
												setTimeout(function(){
														$('.nanas3').removeClass('fadeInUp');
														$('.nanas4').removeClass('fadeInUp');
														$('.nanas6').removeClass('fadeInUp');
														$('.nanas1').removeClass('fadeInUp');
														$('.nanas2').removeClass('fadeInUp');
														$('.nanas7').removeClass('fadeInUp');
														$('#carouselfade').find('[role="button"]').removeAttr('style');
												},1400);
											}).addClass('fadeInUp');
										}).addClass('fadeInUp');
									}).addClass('fadeInUp');
								}
								function jujubeAnimating()
								{
									$('#carouselfade').find('[role="button"]').css({'pointer-events':'none','cursor':'wait'});
									$('.jujube3').animate({'opacity':1},100,function(){
										$('.jujube4').animate({'opacity': 1},100,function(){
											setTimeout(function(){
												$('.jujube1').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
												$('.jujube2').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
											},300);
										}).addClass('fadeInUp');
											$('.jujube5').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
												setTimeout(function(){
														$('.jujube3').removeClass('fadeInUp');
														$('.jujube4').removeClass('fadeInUp');
														$('.jujube1').removeClass('fadeInUp');
														$('.jujube2').removeClass('fadeInUp');
														$('.jujube5').removeClass('fadeInUp');
														$('#carouselfade').find('[role="button"]').removeAttr('style');
												},1400);
									}).addClass('fadeInUp');
								}
								function honeydewAnimating()
								{
									$('#carouselfade').find('[role="button"]').css({'pointer-events':'none','cursor':'wait'});
									$('.honeydew2').animate({'opacity':1},100,function(){
										$('.honeydew3').animate({'opacity': 1},100,function(){
											setTimeout(function(){
												$('.honeydew1').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
												$('.honeydew5').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
											},300);
										}).addClass('fadeInUp');
										$('.honeydew4').animate({'opacity': 1},100,function(){
												setTimeout(function(){
														$('.honeydew2').removeClass('fadeInUp');
														$('.honeydew3').removeClass('fadeInUp');
														$('.honeydew1').removeClass('fadeInUp');
														$('.honeydew5').removeClass('fadeInUp');
														$('.honeydew4').removeClass('fadeInUp');
														$('#carouselfade').find('[role="button"]').removeAttr('style');
												},1400);
										}).addClass('fadeInUp');
									}).addClass('fadeInUp');
								}
								function ccmAnimating()
								{
									$('#carouselfade').find('[role="button"]').css({'pointer-events':'none','cursor':'wait'});
									$('.ccm2').animate({'opacity':1},100,function(){
										$('.ccm5').animate({'opacity': 1},100,function(){
												<?php if($this->input->get('content') != 'inload'){?>
													$('.socmed, .socmedmobile, .headerhome').animate({'opacity':1},1000).removeClass('fadeIn');
												<?php }?>
											setTimeout(function(){
												$('.ccm1').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
												$('.ccm3').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
											},300);
										}).addClass('fadeInUp');
										$('.ccm4').animate({'opacity': 1},100,function(){
											$('.imgbanner1').animate({'opacity' : 1},1000,function(){}).addClass('fadeInUp');
												setTimeout(function(){
														$('.imgbanner1').removeClass('fadeInUp');
														$('.imgbanner1').removeClass('opacity0');
														$('.imgbanner1').removeAttr('style');
														$('.imgbanner').removeClass('imgbanner1');
														$('.socmed,.socmedmobile,.headerhome').removeClass('fadeInUp');
														$('.ccm2').removeClass('fadeInUp');
														$('.ccm5').removeClass('fadeInUp');
														$('.ccm4').removeClass('fadeInUp');
														$('.ccm1').removeClass('fadeInUp');
														$('.ccm3').removeClass('fadeInUp');
														$('#carouselfade').find('[role="button"]').removeAttr('style');
												},1400);
										}).addClass('fadeInUp');
									}).addClass('fadeInUp');
								}
								function ccvAnimating()
								{
									$('#carouselfade').find('[role="button"]').css({'pointer-events':'none','cursor':'wait'});
									$('.ccv1').animate({'opacity':1},100,function(){
										$('.ccv2').animate({'opacity': 1},100,function(){
											setTimeout(function(){
												$('.ccv3').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
												$('.ccv5').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
											},300);
										}).addClass('fadeInUp');
										$('.ccv4').animate({'opacity': 1},100,function(){
											$('.ccv6').animate({'opacity': 1},100,function(){
												setTimeout(function(){
														$('.ccv1').removeClass('fadeInUp');
														$('.ccv2').removeClass('fadeInUp');
														$('.ccv4').removeClass('fadeInUp');
														$('.ccv6').removeClass('fadeInUp');
														$('.ccv3').removeClass('fadeInUp');
														$('.ccv5').removeClass('fadeInUp');
														$('#carouselfade').find('[role="button"]').removeAttr('style');
												},1400);
											}).addClass('fadeInUp');
										}).addClass('fadeInUp');
									}).addClass('fadeInUp');
								}
								function vmAnimating()
								{
									$('#carouselfade').find('[role="button"]').css({'pointer-events':'none','cursor':'wait'});
									$('.vm').animate({'opacity':1},100,function(){
										setTimeout(function(){
												$('.vm').removeClass('fadeInUp');
												$('#carouselfade').find('[role="button"]').removeAttr('style');
										},1400);
									}).addClass('fadeInUp');
								}
								$('.imgfooter').animate({'opacity':1, 'bottom' : 0},100,function(){
									$('#contentsection').removeAttr('class');
									ccmAnimating();
								}).addClass('fadeInUp').prev('.copyright').delay(1000).animate({opacity:1},1000);

								$('#carouselfade').on('slide.bs.carousel', function (e) {
									remoattr();
									var index = e.to;
								  if(index === 1){
								  	EC('bgColor');
										EC('bgClass');
										ccbAnimating();
										$('#wrappsliderdesktop').removeAttr('class');
										$('#wrappsliderdesktop').addClass('bgbigccb');
										$('#contentsection').removeAttr('class');
										$('#contentsection').addClass('BgColorChocolatBlueberry');
										$('.overlays-product-detail').css({'background-color' : '#bd3990'});
										CC('bgClass','BgColorChocolatBlueberry',1);
										CC('bgColor','#bd3990',1);
									}else if(index === 2){
								  		EC('bgColor');
											EC('bgClass');
										jujubeAnimating();
										$('#wrappsliderdesktop').removeAttr('class');
										$('#wrappsliderdesktop').addClass('bgbigjujube');
										$('#contentsection').removeAttr('class');
										$('#contentsection').addClass('BgColorPurlple');
										$('.overlays-product-detail').css({'background-color' : '#cf6754'});
										CC('bgClass','BgColorPurlple',1);
										CC('bgColor','#cf6754',1);
									}else if(index === 3){
								  		EC('bgColor');
											EC('bgClass');
										honeydewAnimating();
										$('#wrappsliderdesktop').removeAttr('class');
										$('#wrappsliderdesktop').addClass('bgbighoneydew');
										$('#contentsection').removeAttr('class');
										$('#contentsection').addClass('BgColorMelon');
										$('.overlays-product-detail').css({'background-color' : '#77b493'});
										CC('bgColor','#77b493',1);
										CC('bgClass','BgColorMelon',1);
									}else if(index === 4){
								  		EC('bgColor');
											EC('bgClass');
										ccvAnimating();
										$('#wrappsliderdesktop').removeAttr('class');
										$('#wrappsliderdesktop').addClass('bgbigccv');
										$('#contentsection').removeAttr('class');
										$('#contentsection').addClass('BgColorChocolate');
										$('.overlays-product-detail').css({'background-color' : '#672221'});
										CC('bgClass','BgColorChocolate',1);
										CC('bgColor','#672221',1);
									}else if(index === 5){
								  		EC('bgColor');
											EC('bgClass');
										vmAnimating();
										$('#wrappsliderdesktop').removeAttr('class');
										$('#wrappsliderdesktop').addClass('bgbigvms');
										$('#contentsection').removeAttr('class');
										$('#contentsection').addClass('BgColorMilkshake');
										$('.overlays-product-detail').css({'background-color' : '#faf182'});
										CC('bgClass','BgColorMilkshake',1);
										CC('bgColor','#faf182',1);
									}else{
								  		EC('bgColor');
											EC('bgClass');
										ccmAnimating();
										$('#wrappsliderdesktop').removeAttr('class');
										$('#wrappsliderdesktop').addClass('bgbigccm');
										$('#contentsection').removeAttr('class');
										$('#contentsection').addClass('BgColorChocolateMalt');
										$('.overlays-product-detail').css({'background-color' : '#eab929'});
										CC('bgClass','BgColorChocolateMalt',1);
										CC('bgColor','#eab929',1);
									}
								});
								$('.requessample').on('click',function(){
								  		firstScroll = true;
								  		var $this = $(this);
								  		urlload = $this.attr('data-href');
								  		$('.firstmenu,.secondmenu,.menuitem').removeClass('active');
								  		$('[requestsample]').addClass('active');
								  		history.pushState({urlPath:'/index.php'}, $(this).attr('data-title'), $(this).attr('data-url'));
								  		$('#load').load(urlload,function(){

								  		});
								  });
								$('a[data-slide="prev"],a[data-slide="next"]').on('click',function(){

								});
								$('.hoverimage').on('mouseover',function(){
									$(this).closest('.carousel-item').find('.imgbanner').addClass('onhover');
								});
								$('.hoverimage').on('mouseleave',function(){
									$(this).closest('.carousel-item').find('.imgbanner').removeClass('onhover');
								});
							});
						</script>
					</div>
				</div>
				<div class="slidemobile">
					<div id="wrappslidermobile" class="wrappslidermobile">
						<div class="wrapanimate">
							<div class="remoattr">
								<img class="img-fluid mobile-ccm1 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccm/mobile-ccm1-new.png');?>">
								<img class="img-fluid mobile-ccm2 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccm/mobile-ccm2-alt.png');?>">
								<img class="img-fluid mobile-ccm3 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccm/mobile-ccm3-emp.png');?>">
								<img class="img-fluid mobile-ccm4 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccm/mobile-ccm4-new.png');?>">
								<img class="img-fluid mobile-ccm5 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccm/mobile-ccm3-alt.png');?>">

								<img class="img-fluid mobile-ccb1 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccb/ccb1-new.png');?>">
								<img class="img-fluid mobile-ccb2 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccb/ccb2-new.png');?>">
								<img class="img-fluid mobile-ccb3 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccb/ccb3-new.png');?>">
								<img class="img-fluid mobile-ccb4 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccb/ccb4-emp.png');?>">
								<img class="img-fluid mobile-ccb5 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccb/ccb5-new.png');?>">

								<img class="img-fluid mobile-jujube1 naanimation animated opacity0" src="<?php echo base_url('asset/static/jujube/mobile-jujube1.png');?>">
								<img class="img-fluid mobile-jujube2 naanimation animated opacity0" src="<?php echo base_url('asset/static/jujube/mobile-jujube2.png');?>">
								<img class="img-fluid mobile-jujube3 naanimation animated opacity0" src="<?php echo base_url('asset/static/jujube/mobile-jujube3.png');?>">
								<img class="img-fluid mobile-jujube4 naanimation animated opacity0" src="<?php echo base_url('asset/static/jujube/mobile-jujube4-new.png');?>">
								<img class="img-fluid mobile-jujube5 naanimation animated opacity0" src="<?php echo base_url('asset/static/jujube/mobile-jujube5.png');?>">
								<img class="img-fluid mobile-jujube6 naanimation animated opacity0" src="<?php echo base_url('asset/static/jujube/mobile-jujube6-emp.png');?>">
								<img class="img-fluid mobile-jujube7 naanimation animated opacity0" src="<?php echo base_url('asset/static/jujube/mobile-jujube7.png');?>">

								<img class="img-fluid mobile-honeydew1 naanimation animated opacity0" src="<?php echo base_url('asset/static/honeydew/mobile-honeydew1-emp.png');?>">
								<img class="img-fluid mobile-honeydew2 naanimation animated opacity0" src="<?php echo base_url('asset/static/honeydew/mobile-honeydew2.png');?>">
								<img class="img-fluid mobile-honeydew3 naanimation animated opacity0" src="<?php echo base_url('asset/static/honeydew/mobile-honeydew3-new.png');?>">
								<img class="img-fluid mobile-honeydew4 naanimation animated opacity0" src="<?php echo base_url('asset/static/honeydew/mobile-honeydew4-new.png');?>">
								<img class="img-fluid mobile-honeydew5 naanimation animated opacity0" src="<?php echo base_url('asset/static/honeydew/mobile-honeydew5-new.png');?>">

								<img class="img-fluid mobile-ccv1 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccv/mobile-ccv1-new.png');?>">
								<img class="img-fluid mobile-ccv2 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccv/mobile-ccv2.png');?>">
								<img class="img-fluid mobile-ccv3 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccv/mobile-ccv3-new.png');?>">
								<img class="img-fluid mobile-ccv4 naanimation animated opacity0" src="<?php echo base_url('asset/static/ccv/mobile-ccv4.png');?>">

								<img class="img-fluid mobile-vms naanimation animated opacity0" src="<?php echo base_url('asset/static/mobile-vms-alt.png');?>">

							</div>
						</div>
						<div class="contenslidemobile">
							<div id="carouselmobile" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
							  <div class="carousel-inner">

							    <div class="carousel-item active" >
							      <img class="imgbanner imgbanner1 animated opacity0" src="<?php echo base_url('asset/static/mobile-crunchy-chocolate-malt-banner.png');?>" alt="First slide">
							      <div class="carousel-caption captionbanner captionbanner1">
							      	<div class="relative">
							      		<div class="d-inline-block hoverimage">
													<a class="linkproduct showproduct" data-target="#product_1" href="javascript:void(0);">
												    <h2 class="FredokaOne">CRUNCHY CHOCOLATE MALT</h2>
												    <div class="COCOGOOSE p en">VANILLA ICE CREAM COVERED WITH CRUNCHY CHOCOLATE AND MALT</div>
														<div class="COCOGOOSE p id">ES MENGANDUNG SUSU RASA VANILLA DENGAN DUA LAPISAN COKELAT DAN MALT</div>
													</a>
													<br/>
												    <a href="<?php echo base_url('home/requestsample');?>" class="COCOGOOSE"><span class="en">Request Sample</span><span class="id">Coba Es Krim</span></a>
										    <br/><br/>
										</div>
									</div>
								  </div>
							    </div>

							    <div class="carousel-item" >
							      <img class="d-block imgbanner animated" src="<?php echo base_url('asset/static/mobile-crunchy-chocolate-blueberry-banner.png');?>" alt="First slide">
							      <div class="carousel-caption captionbanner">
							      	<div class="relative">
							      		<div class="d-inline-block hoverimage">
													<a class="linkproduct showproduct" data-target="#product_2" href="javascript:void(0);">
												    <h2 class="FredokaOne">CRUNCHY CHOCOLATE BLUEBERRY</h2>
												    <div class="COCOGOOSE p en">CHOCOLATE COVERED VANILLA ICE CREAM WITH BLUEBERRY JAM FILLING</div>
														<div class="COCOGOOSE p id">ES MENGANDUNG SUSU RASA VANILLA DENGAN DUA LAPISAN COKELAT MALT DAN BLUEBERRY</div>
													</a>
													<br/>
										    <a href="<?php echo base_url('home/requestsample');?>" class="COCOGOOSE"><span class="en">Request Sample</span><span class="id">Coba Es Krim</span></a>
										    <br/><br/>
										</div>
									</div>
								  </div>
							    </div>

							    <div class="carousel-item" >
							      <img class="d-block imgbanner animated" src="<?php echo base_url('asset/static/mobile-jujube-banner.png');?>" alt="JUJUBE WHITE CHOCOLATE">
							      <div class="carousel-caption captionbanner">
							      	<div class="relative">
									      <div class="d-inline-block hoverimage">
													<a class="linkproduct showproduct" data-target="#product_3" href="javascript:void(0);">
												    <h2 class="FredokaOne">JUJUBE WHITE CHOCOLATE</h2>
												    <div class="COCOGOOSE p en">CREAMY ICE CREAM WITH RED DATES, COVERED WITH WHITE CHOCOLATE</div>
														<div class="COCOGOOSE p id">ES MENGANDUNG SUSU RASA KURMA MERAH DENGAN LAPISAN COKELAT PUTIH</div>
													</a>
													<br/>
										    <a href="<?php echo base_url('home/requestsample');?>" class="COCOGOOSE"><span class="en">Request Sample</span><span class="id">Coba Es Krim</span></a>
										    <br/><br/>
										</div>
									</div>
								  </div>
							    </div>
							    <div class="carousel-item" >
							      <img class="d-block imgbanner animated" src="<?php echo base_url('asset/static/mobile-crunchy-honeydew-melon-banner.png');?>" alt="CRUNCHY HONEYDEW MELON">
							      <div class="carousel-caption captionbanner">
							      	<div class="relative">
							      		<div class="d-inline-block hoverimage">
													<a class="linkproduct showproduct" data-target="#product_4" href="javascript:void(0);">
												    <h2 class="FredokaOne">CRUNCHY HONEYDEW MELON</h2>
												    <div class="COCOGOOSE p en">HONEYDEW FLAVOURED ICE CREAM WITH CHOCOLATE SAUCE AND PEANUT TOPPINGS</div>
												    <div class="COCOGOOSE p id">ES MENGANDUNG SUSU CONE RASA MELON DENGAN SAUS COKELAT DAN BUTIRAN KACANG</div>
													</a>
													<br/>
										    <a href="javascript:void(0);" onclick="$('.firsmenu[requestsample]').trigger('click')" class="COCOGOOSE"><span class="en">Request Sample</span><span class="id">Coba Es Krim</span></a>
										    <br/><br/>
										</div>
									</div>
								  </div>
							    </div>
							    <div class="carousel-item" >
							      <img class="d-block imgbanner animated" src="<?php echo base_url('asset/static/mobile-crunchy-chocolate-vanilla-banner.png');?>" alt="CRUNCHY CHOCOLATE VANILLA">
							      <div class="carousel-caption captionbanner">
							      	<div class="relative">
							      		<div class="d-inline-block hoverimage">
													<a class="linkproduct showproduct" data-target="#product_5" href="javascript:void(0);">
										    <h2 class="FredokaOne">CRUNCHY CHOCOLATE VANILLA</h2>
												<div class="COCOGOOSE p en">CHOCOLATE VANILLA FLAVOURED ICE CREAM WITH CHOCOLATE SAUCE AND PEANUT TOPPINGS</div>
												<div class="COCOGOOSE p id">ES MENGANDUNG SUSU CONE RASA COKELAT VANILLA DENGAN SAUS COKELAT DAN BUTIRAN KACANG</div>
												<br/>
										    <a href="<?php echo base_url('home/requestsample');?>" class="COCOGOOSE"><span class="en">Request Sample</span><span class="id">Coba Es Krim</span></a>
										    <br/><br/>
										</div>
									</div>
								  </div>
							    </div>
							    <div class="carousel-item" >
							      <img class="d-block imgbanner animated" src="<?php echo base_url('asset/static/mobile-vanilla-milkshake-banner-alt.png');?>" alt="VANILLA MILKSHAKE">
							      <div class="carousel-caption captionbanner">
							      	<div class="relative">
							      		<div class="d-inline-block hoverimage">
													<a class="linkproduct showproduct" data-target="#product_6" href="javascript:void(0);">
												    <h2 class="FredokaOne">VANILLA MILKSHAKE</h2>
												    <div class="COCOGOOSE p en">CREAMY VANILLA MILKSHAKE FLAVOUR</div>
												    <div class="COCOGOOSE p id">ES SUSU RASA VANILLA</div>
													</a>
													<br/>
												   <a href="<?php echo base_url('home/requestsample');?>" class="COCOGOOSE"><span class="en">Request Sample</span><span class="id">Coba Es Krim</span></a>
										    <br/><br/>
										</div>
									</div>
								  </div>
							    </div>
							  </div>
							  <a class="carousel-control-prev" href="#carouselmobile" role="button" data-slide="prev">
							    <span class="ti-angle-left" aria-hidden="true"></span>
							    <span class="sr-only">Previous</span>
							  </a>
							  <a class="carousel-control-next" href="#carouselmobile" role="button" data-slide="next">
							    <span class="ti-angle-right" aria-hidden="true"></span>
							    <span class="sr-only">Next</span>
							  </a>
							</div>
						</div>
						<script type="text/javascript">
							if(RC('bgColor')){
								EC('bgColor');
								EC('bgClass');
							}
							function removeattr()
							{
								$('.remoattr').find('img').removeAttr('style');
							}
							function mobileccmAnimating()
							{
								$('#carouselmobile').find('[role="button"]').css({'pointer-events':'none','cursor':'wait'});
								$('.mobile-ccm2').animate({'opacity':1},100,function(){
									$('.mobile-ccm5').animate({'opacity': 1},100,function(){
										setTimeout(function(){
											$('.mobile-ccm1').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
											$('.mobile-ccm3').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
										},300);
									}).addClass('fadeInUp');
									$('.mobile-ccm4').animate({'opacity': 1},100,function(){
											setTimeout(function(){
													$('.mobile-ccm2').removeClass('fadeInUp');
													$('.mobile-ccm5').removeClass('fadeInUp');
													$('.mobile-ccm4').removeClass('fadeInUp');
													$('.mobile-ccm1').removeClass('fadeInUp');
													$('.mobile-ccm3').removeClass('fadeInUp');
													$('#carouselmobile').find('[role="button"]').removeAttr('style');
											},1400);
									}).addClass('fadeInUp');
								}).addClass('fadeInUp');
							}
							function mobileccbAnimating()
							{
								$('#carouselmobile').find('[role="button"]').css({'pointer-events':'none','cursor':'wait'});
								$('.mobile-ccb2').animate({'opacity':1},100,function(){
									$('.mobile-ccb5').animate({'opacity': 1},100,function(){
										setTimeout(function(){
											$('.mobile-ccb1').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
											$('.mobile-ccb3').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
										},300);
									}).addClass('fadeInUp');
									$('.mobile-ccb4').animate({'opacity': 1},100,function(){
											setTimeout(function(){
													$('.mobile-ccb2').removeClass('fadeInUp');
													$('.mobile-ccb5').removeClass('fadeInUp');
													$('.mobile-ccb4').removeClass('fadeInUp');
													$('.mobile-ccb1').removeClass('fadeInUp');
													$('.mobile-ccb3').removeClass('fadeInUp');
													$('#carouselmobile').find('[role="button"]').removeAttr('style');
											},1400);
									}).addClass('fadeInUp');
								}).addClass('fadeInUp');
							}
							function mobilejujubeAnimating()
							{
								$('#carouselmobile').find('[role="button"]').css({'pointer-events':'none','cursor':'wait'});
								$('.mobile-jujube5').animate({'opacity':1},100,function(){
									$('.mobile-jujube1').animate({'opacity': 1},100,function(){
										setTimeout(function(){
											$('.mobile-jujube2').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
											$('.mobile-jujube3').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
										},300);
									}).addClass('fadeInUp');
									$('.mobile-jujube4').animate({'opacity': 1},100,function(){
											$('.mobile-jujube6').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
											$('.mobile-jujube7').animate({'opacity':1},100,function(){}).addClass('fadeInUp')
											setTimeout(function(){
													$('.mobile-jujube5,.mobile-jujube4,.mobile-jujube1,.mobile-jujube2,.mobile-jujube3,.mobile-jujube6,.mobile-jujube7').removeClass('fadeInUp');
													$('#carouselmobile').find('[role="button"]').removeAttr('style');
											},1400);
									}).addClass('fadeInUp');
								}).addClass('fadeInUp');
							}
							function mobilehoneydewAnimating()
							{
								$('#carouselmobile').find('[role="button"]').css({'pointer-events':'none','cursor':'wait'});
								$('.mobile-honeydew1').animate({'opacity':1},100,function(){
									$('.mobile-honeydew4').animate({'opacity': 1},100,function(){
										setTimeout(function(){
											$('.mobile-honeydew3').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
											$('.mobile-honeydew5').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
										},300);
									}).addClass('fadeInUp');
									$('.mobile-honeydew2').animate({'opacity': 1},100,function(){
											setTimeout(function(){
													$('.mobile-honeydew1').removeClass('slideInDown');
													$('.mobile-honeydew4,.mobile-honeydew2,.mobile-honeydew3,.mobile-honeydew5').removeClass('fadeInUp');
													$('#carouselmobile').find('[role="button"]').removeAttr('style');
											},1400);
									}).addClass('fadeInUp');
								}).addClass('slideInDown');
							}
							function mobileccvAnimating()
							{
								$('#carouselmobile').find('[role="button"]').css({'pointer-events':'none','cursor':'wait'});
								$('.mobile-ccv2').animate({'opacity':1},100,function(){
									$('.mobile-ccv1').animate({'opacity': 1},100,function(){
										setTimeout(function(){
											$('.mobile-ccv3').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
										},300);
									}).addClass('fadeInUp');
									$('.mobile-ccv4').animate({'opacity': 1},100,function(){
											setTimeout(function(){
													$('.mobile-ccv2,.mobile-ccv1,.mobile-ccv4,.mobile-ccv3').removeClass('fadeInUp');
													$('#carouselmobile').find('[role="button"]').removeAttr('style');
											},1400);
									}).addClass('fadeInUp');
								}).addClass('fadeInUp');
							}
							function mobilevmsAnimating()
							{
								$('.mobile-vms').animate({'opacity':1},100,function(){
									setTimeout(function(){
										$('.mobile-vms').removeClass('fadeInUp');
									},1400);
								}).addClass('fadeInUp');
							}
							$(document).ready(function(){
								mobileccmAnimating();
								$('#carouselmobile').on('slide.bs.carousel', function (e) {
									removeattr();
									var index = e.to;
								  if(index === 1){
								  		EC('bgColor');
										  EC('bgClass');
										mobileccbAnimating();
										$('#wrappslidermobile').removeAttr('class');
										$('#wrappslidermobile').addClass('bgccb');
										$('#contentsection').removeAttr('class');
										$('#contentsection').addClass('BgColorChocolatBlueberry');
										$('.overlays-product-detail').css({'background-color' : '#be4194'});
										CC('bgClass','BgColorChocolatBlueberry',1);
										CC('bgColor','#be4194',1);
										$('#meta-theme').attr('content',RC('bgColor'));
									}else if(index === 2){
								  		EC('bgColor');
											EC('bgClass');
										mobilejujubeAnimating();
										$('#wrappslidermobile').removeAttr('class');
										$('#wrappslidermobile').addClass('bgjujube');
										$('#contentsection').removeAttr('class');
										$('#contentsection').addClass('BgColorPurlple');
										$('.overlays-product-detail').css({'background-color' : '#ed1846'});
										CC('bgClass','BgColorPurlple',1);
										CC('bgColor','#ed1846',1);
										$('#meta-theme').attr('content',RC('bgColor'));
									}else if(index === 3){
								  		EC('bgColor');
											EC('bgClass');
										mobilehoneydewAnimating();
										$('#wrappslidermobile').removeAttr('class');
										$('#wrappslidermobile').addClass('bghoneydew');
										$('#contentsection').removeAttr('class');
										$('#contentsection').addClass('BgColorMelon');
										$('.overlays-product-detail').css({'background-color' : '#9fe3a8'});
										CC('bgClass','BgColorMelon',1);
										CC('bgColor','#9fe3a8',1);
										$('#meta-theme').attr('content',RC('bgColor'));
									}else if(index === 4){
								  		EC('bgColor');
											EC('bgClass');
										mobileccvAnimating();
										$('#wrappslidermobile').removeAttr('class');
										$('#wrappslidermobile').addClass('bgccv');
										$('#contentsection').removeAttr('class');
										$('#contentsection').addClass('BgColorChocolate');
										$('.overlays-product-detail').css({'background-color' : '#541d11'});
										CC('bgClass','BgColorChocolate',1);
										CC('bgColor','#541d11',1);
										$('#meta-theme').attr('content',RC('bgColor'));
									}else if(index === 5){
								  		EC('bgColor');
											EC('bgClass');
										mobilevmsAnimating();
										$('#wrappslidermobile').removeAttr('class');
										$('#wrappslidermobile').addClass('bgvanilla');
										$('#contentsection').removeAttr('class');
										$('#contentsection').addClass('BgColorMilkshake');
										$('.overlays-product-detail').css({'background-color' : '#faf284'});
										CC('bgClass','BgColorMilkshake',1);
										CC('bgColor','#faf284',1);
										$('#meta-theme').attr('content',RC('bgColor'));
									}else{
								  		EC('bgColor');
											EC('bgClass');
										mobileccmAnimating();
										$('#wrappslidermobile').removeAttr('class');
										$('#wrappslidermobile').addClass('bgccm');
										$('#contentsection').removeAttr('class');
										$('#contentsection').addClass('BgColorChocolateMalt');
										$('.overlays-product-detail').css({'background-color' : '#b57403'});
										CC('bgClass','BgColorChocolateMalt',1);
										CC('bgColor','#b57403',1);
										$('#meta-theme').attr('content',RC('bgColor'));
									}
								});
							});
						</script>
					</div>
				</div>
<div id="detail-product" ss-container class="detail-product is-clearfix">
					<div class="closecontent close-smalle"></div>
					<div id="product_1" class="product_item ccm is-clearfix">
						<div class="level">
							<div class="level-left">
								<div class="level-item">
									<div class="slideproduct">
										<div id="carouselfade_0" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
											<ul class="carousel-indicators">
											    <li data-target="#carouselfade_0" data-slide-to="0" class="active"></li>
											    <li data-target="#carouselfade_0" data-slide-to="1"></li>
											  </ul>
										  <div class="carousel-inner">
										    <div class="carousel-item active" >
										    	<div class="item-product-detailed">
										    		<div class="overlays-product-detail overlaysproduct1 opacity0 animated"></div>
										    		<img class="imgbanner imgbanner2 opacity0 animated" src="<?php echo base_url('asset/static/product/big/new-product1.png');?>" alt="First slide">
										    	</div>
										    </div>
										    <div class="carousel-item" >
										      <div class="item-product-detailed">
										    		<div class="overlays-product-detail "></div>
										    		<img class="imgbanner animated " src="<?php echo base_url('asset/static/product/big/new-product1.1.png');?>" alt="First slide">
										    	</div>
										    </div>
										  </div>
										</div>
									</div>
								</div>
							</div>
							<div class="level-item leveltextleft">
								<div class="product-info-detailed">
									<h2>Chruncy<br/>Chocolate Malt</h2>
									<div class="meta-info">NETTO : 75 GR</div>
									<div class="description-product-detailed">
										<span class="en">VANILLA ICE CREAM COVERED WITH CRUNCHY CHOCOLATE AND MALT</span>
										<span class="id">ES KRIM VANILA DENGAN TOPING COKELAT RENYAH DAN MALT</span>
									</div>
									<div>
										<a href="<?php echo base_url('home/requestsample');?>" class="requestsample"><span class="en">REQUEST SAMPLE</span><span class="id">COBA ES KRIM</span></a>
									</div>
								</div>
							</div>
							<div class="level-right">
								<div class="level-item">
									<ul class="sosmed-product-detaile">
										<li><a href="javascript:void(0);" target="_blank" class="fb"><i class="fa fa-facebook-square"></i></a></li>
										<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=CHRUNCY CHOCOLATE MALT - VANILLA ICE CREAM COVERED WITH CRUNCHY CHOCOLATE AND MALT" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
										<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
										<li><a href="mailto:?subject=CHRUNCY CHOCOLATE MALT&amp;body=VANILLA ICE CREAM COVERED WITH CRUNCHY CHOCOLATE AND MALT <?php echo current_url();?>."><i class="ti-email"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div id="product_2" class="product_item ccb is-clearfix">
						<div class="level">
							<div class="level-left">
								<div class="level-item">
									<div class="slideproduct">
										<div id="carouselfade_1" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
											<ul class="carousel-indicators">
											    <li data-target="#carouselfade_1" data-slide-to="0" class="active"></li>
											    <li data-target="#carouselfade_1" data-slide-to="1"></li>
											  </ul>
										  <div class="carousel-inner">
										    <div class="carousel-item active" >
										    	<div class="item-product-detailed">
										    		<div class="overlays-product-detail overlaysproduct1 opacity0 animated"></div>
										    		<img class="imgbanner imgbanner2 opacity0 animated" src="<?php echo base_url('asset/static/product/big/new-product2.png');?>" alt="First slide">
										    	</div>
										    </div>
										    <div class="carousel-item" >
										      <div class="item-product-detailed">
										    		<div class="overlays-product-detail "></div>
										    		<img class="imgbanner animated " src="<?php echo base_url('asset/static/product/big/new-product2.1.png');?>" alt="First slide">
										    	</div>
										    </div>
										  </div>
										</div>
									</div>
								</div>
							</div>
							<div class="level-item leveltextleft">
								<div class="product-info-detailed">
									<h2>Chruncy<br/>Chocolate Blueberry</h2>
									<div class="meta-info">NETTO : 75 GR</div>
									<div class="description-product-detailed">
										<span class="en">CHOCOLATE COVERED VANILLA ICE CREAM WITH BLUEBERRY JAM FILLING</span>
										<span class="id">ES MENGANDUNG SUSU RASA VANILLA DENGAN DUA LAPISAN COKELAT MALT DAN BLUEBERRY</span>
									</div>
									<div>
										<a href="<?php echo base_url('home/requestsample');?>" class="requestsample"><span class="en">REQUEST SAMPLE</span><span class="id">COBA ES KRIM</span></a>
									</div>
								</div>
							</div>
							<div class="level-right">
								<div class="level-item">
									<ul class="sosmed-product-detaile">
										<li><a href="javascript:void(0);" target="_blank" class="fb"><i class="fa fa-facebook-square"></i></a></li>
										<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=CHRUNCY CHOCOLATE BLUEBERRY - CHOCOLATE COVERED VANILLA ICE CREAM WITH BLUEBERRY JAM FILLING" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
										<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
										<li><a href="mailto:?subject=CHRUNCY CHOCOLATE BLUEBERRY&amp;body=CHOCOLATE COVERED VANILLA ICE CREAM WITH BLUEBERRY JAM FILLING <?php echo current_url();?>."><i class="ti-email"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div id="product_3" class="product_item ccb is-clearfix">
						<div class="level">
							<div class="level-left">
								<div class="level-item">
									<div class="slideproduct">
										<div id="carouselfade_3" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
											<ul class="carousel-indicators">
											    <li data-target="#carouselfade_3" data-slide-to="0" class="active"></li>
											    <li data-target="#carouselfade_3" data-slide-to="1"></li>
											  </ul>
										  <div class="carousel-inner">
										    <div class="carousel-item active" >
										    	<div class="item-product-detailed">
										    		<div class="overlays-product-detail overlaysproduct1 opacity0 animated"></div>
										    		<img class="imgbanner imgbanner2 opacity0 animated" src="<?php echo base_url('asset/static/product/big/new-product5.png');?>" alt="First slide">
										    	</div>
										    </div>
										    <div class="carousel-item" >
										      <div class="item-product-detailed">
										    		<div class="overlays-product-detail "></div>
										    		<img class="imgbanner animated " src="<?php echo base_url('asset/static/product/big/new-product5.1.png');?>" alt="First slide">
										    	</div>
										    </div>
										  </div>
										</div>
									</div>
								</div>
							</div>
							<div class="level-item leveltextleft">
								<div class="product-info-detailed">
									<h2>Jujube<br/>White Chocolate</h2>
									<div class="meta-info">NETTO : 75 GR</div>
									<div class="description-product-detailed">
											<span class="en">CREAMY ICE CREAM WITH RED DATES, COVERED WITH WHITE CHOCOLATE</span>
											<span class="id">ES MENGANDUNG SUSU RASA KURMA MERAH DENGAN LAPISAN COKELAT PUTIH</span>
									</div>
									<div>
										<a href="<?php echo base_url('home/requestsample');?>" class="requestsample"><span class="en">REQUEST SAMPLE</span><span class="id">COBA ES KRIM</span></a>
									</div>
								</div>
							</div>
							<div class="level-right">
								<div class="level-item">
									<ul class="sosmed-product-detaile">
										<li><a href="javascript:void(0);" target="_blank" class="fb"><i class="fa fa-facebook-square"></i></a></li>
										<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=JUJUBE WHITE CHOCOLATE - CREAMY ICE CREAM WITH RED DATES, COVERED WITH WHITE CHOCOLATE" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
										<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
										<li><a href="mailto:?subject=JUJUBE WHITE CHOCOLATE&amp;body=CREAMY ICE CREAM WITH RED DATES, COVERED WITH WHITE CHOCOLATE <?php echo current_url();?>."><i class="ti-email"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div id="product_5" class="product_item ccm is-clearfix">
						<div class="level">
							<div class="level-left">
								<div class="level-item">
									<div class="slideproduct">
										<div id="carouselfade_4" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
											<ul class="carousel-indicators">
											    <li data-target="#carouselfade_4" data-slide-to="0" class="active"></li>
											    <li data-target="#carouselfade_4" data-slide-to="1"></li>
											  </ul>
										  <div class="carousel-inner">
										    <div class="carousel-item active" >
										    	<div class="item-product-detailed">
										    		<div class="overlays-product-detail overlaysproduct1 opacity0 animated"></div>
										    		<img class="imgbanner imgbanner2 opacity0 animated" src="<?php echo base_url('asset/static/product/big/new-product9.png');?>" alt="First slide">
										    	</div>
										    </div>
										    <div class="carousel-item" >
										      <div class="item-product-detailed">
										    		<div class="overlays-product-detail "></div>
										    		<img class="imgbanner animated " src="<?php echo base_url('asset/static/product/big/new-product9.1.png');?>" alt="First slide">
										    	</div>
										    </div>
										  </div>
										</div>
									</div>
								</div>
							</div>
							<div class="level-item leveltextleft">
								<div class="product-info-detailed">
									<h2>Chruncy<br/>Chocolate Vanila</h2>
									<div class="meta-info">NETTO : 75 GR</div>
									<div class="description-product-detailed">
											<span class="en">CHOCOLATE VANILLA FLAVOURED ICE CREAM WITH CHOCOLATE SAUCE AND PEANUT TOPPINGS</span>
											<span class="id">ES MENGANDUNG SUSU CONE RASA COKELAT VANILLA DENGAN SAUS COKELAT DAN BUTIRAN KACANG</span>
									</div>
									<div>
										<a href="<?php echo base_url('home/requestsample');?>" class="requestsample"><span class="en">REQUEST SAMPLE</span><span class="id">COBA ES KRIM</span></a>
									</div>
								</div>
							</div>
							<div class="level-right">
								<div class="level-item">
									<ul class="sosmed-product-detaile">
										<li><a href="javascript:void(0);" target="_blank" class="fb"><i class="fa fa-facebook-square"></i></a></li>
										<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=CHRUNCY CHOCOLATE VANILLA - CHOCOLATE VANILLA FLAVOURED ICE CREAM WITH CHOCOLATE SAUCE AND PEANUT TOPPINGS" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
										<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
										<li><a href="mailto:?subject=CHRUNCY CHOCOLATE VANILLA&amp;body=CHOCOLATE VANILLA FLAVOURED ICE CREAM WITH CHOCOLATE SAUCE AND PEANUT TOPPINGS <?php echo current_url();?>."><i class="ti-email"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div id="product_4" class="product_item ccb is-clearfix">
						<div class="level">
							<div class="level-left">
								<div class="level-item">
									<div class="slideproduct">
										<div id="carouselfade_5" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
											<ul class="carousel-indicators">
											    <li data-target="#carouselfade_5" data-slide-to="0" class="active"></li>
											    <li data-target="#carouselfade_5" data-slide-to="1"></li>
											  </ul>
										  <div class="carousel-inner">
										    <div class="carousel-item active" >
										    	<div class="item-product-detailed">
										    		<div class="overlays-product-detail overlaysproduct1 opacity0 animated"></div>
										    		<img class="imgbanner imgbanner2 opacity0 animated" src="<?php echo base_url('asset/static/product/big/new-product10.png');?>" alt="First slide">
										    	</div>
										    </div>
										    <div class="carousel-item" >
										      <div class="item-product-detailed">
										    		<div class="overlays-product-detail "></div>
										    		<img class="imgbanner animated " src="<?php echo base_url('asset/static/product/big/new-product10.1.png');?>" alt="First slide">
										    	</div>
										    </div>
										  </div>
										</div>
									</div>
								</div>
							</div>
							<div class="level-item leveltextleft">
								<div class="product-info-detailed">
									<h2>Chruncy<br/>HONEYDEW MELON</h2>
									<div class="meta-info">NETTO : 75 GR</div>
									<div class="description-product-detailed">
												<span class="en">HONEYDEW FLAVOURED ICE CREAM WITH CHOCOLATE SAUCE AND PEANUT TOPPINGS</span>
												<span class="id">ES MENGANDUNG SUSU CONE RASA MELON DENGAN SAUS COKELAT DAN BUTIRAN KACANG</span>
									</div>
									<div>
										<a href="<?php echo base_url('home/requestsample');?>" class="requestsample"><span class="en">REQUEST SAMPLE</span><span class="id">COBA ES KRIM</span></a>
									</div>
								</div>
							</div>
							<div class="level-right">
								<div class="level-item">
									<ul class="sosmed-product-detaile">
										<li><a href="javascript:void(0);" target="_blank" class="fb"><i class="fa fa-facebook-square"></i></a></li>
										<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=CHRUNCY HONEYDEW MELON - HONEYDEW FLAVOURED ICE CREAM WITH CHOCOLATE SAUCE AND PEANUT TOPPINGS" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
										<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
										<li><a href="mailto:?subject=CHRUNCY HONEYDEW MELON&amp;body=HONEYDEW FLAVOURED ICE CREAM WITH CHOCOLATE SAUCE AND PEANUT TOPPINGS <?php echo current_url();?>."><i class="ti-email"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div id="product_6" class="product_item vms is-clearfix">
						<div class="level">
							<div class="level-left">
								<div class="level-item">
									<div class="slideproduct">
										<div id="carouselfade_6" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
											<ul class="carousel-indicators">
											    <li data-target="#carouselfade_6" data-slide-to="0" class="active"></li>
											    <li data-target="#carouselfade_6" data-slide-to="1"></li>
											  </ul>
										  <div class="carousel-inner">
										    <div class="carousel-item active" >
										    	<div class="item-product-detailed">
										    		<div class="overlays-product-detail overlaysproduct1 opacity0 animated"></div>
										    		<img class="imgbanner imgbanner2 opacity0 animated" src="<?php echo base_url('asset/static/product/big/new-product11.png');?>" alt="First slide">
										    	</div>
										    </div>
										    <div class="carousel-item" >
										      <div class="item-product-detailed">
										    		<div class="overlays-product-detail "></div>
										    		<img class="imgbanner animated " src="<?php echo base_url('asset/static/product/big/new-product11.1.png');?>" alt="First slide">
										    	</div>
										    </div>
										  </div>
										</div>
									</div>
								</div>
							</div>
							<div class="level-item leveltextleft">
								<div class="product-info-detailed">
									<h2>Vanilla<br/>MILKSHAKE</h2>
									<div class="meta-info">NETTO : 75 GR</div>
									<div class="description-product-detailed">
												<span class="en">CREAMY VANILLA MILKSHAKE FLAVOUR</span>
												<span class="id">ES SUSU RASA VANILLA</span>
									</div>
									<div>
										<a href="<?php echo base_url('home/requestsample');?>" class="requestsample"><span class="en">REQUEST SAMPLE</span><span class="id">COBA ES KRIM</span></a>
									</div>
								</div>
							</div>
							<div class="level-right">
								<div class="level-item">
									<ul class="sosmed-product-detaile">
										<li><a href="javascript:void(0);" target="_blank" class="fb"><i class="fa fa-facebook-square"></i></a></li>
										<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=VANILLA MILKSHAKE - CREAMY VANILLA MILKSHAKE FLAVOUR" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
										<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
										<li><a href="mailto:?subject=VANILLA MILKSHAKE &amp;body=CREAMY VANILLA MILKSHAKE FLAVOUR <?php echo current_url();?>."><i class="ti-email"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<script type="text/javascript">
					$(document).ready(function(){

					  $('.showproduct').on('click',function(){
					  	var ptarget = $(this).attr('data-target');
					  	var $tp = $(ptarget);
					  	if($(ptarget).length > 0){
					  		$('.overlay-main-product').addClass('show');
					  		$('#wrappingshow').addClass('showdetail');
					  		$tp.addClass('show');
					  		setTimeout(function(){
					  			$tp.find('.overlaysproduct1').animate({'opacity':1},1000).addClass('fadeInUp');
					  			setTimeout(function(){
						  			$tp.find('.imgbanner2').animate({'opacity':1},1000).addClass('fadeInUp');
						  		},500);
						  		setTimeout(function(){
						  			$tp.find('.imgbanner2').removeClass('opacity0 fadeInUp');
						  			$tp.find('.overlaysproduct1').removeClass('opacity0 fadeInUp');
						  		},1500);
					  		},500);
					  	}else{
					  		return false;
					  	}
					  });
						$('.fb').on('click',function() {
              FB.ui({
                  method: 'feed',
                  link: '<?php echo current_url();?>',
                  picture: '<?php echo base_url('asset/static/share.png');?>'
                }, function(response){
                  if (response && !response.error_code) {
                  }
                });
          });
					  $('.closecontent').on('click',function(){
					  	$('.overlay-main-product').removeClass('show');
					  	$('#wrappingshow').removeClass('showdetail');
					  	setTimeout(function(){
					  		$('.product_item').removeClass('show');
					  		$('#wrappingshow').find('.imgbanner2').addClass('opacity0').removeAttr('style');
							$('#wrappingshow').find('.overlaysproduct1').addClass('opacity0').removeAttr('style');
					  	},500);
					  });
					});
				</script>
			</div>
	<?php
		if($this->input->get('content') != 'inload'){
	?>
		</div>
	</section>
	<div class="copyright absolute animated opacity0">
		<div class="footermenu">
			<a href="#contentsection" data-href="<?php echo base_url('home/terms');?>" data-url="home/terms" data-title="terms of use Joyday" class="firstmenu menufoot"><span class="en">Terms of Use</span><span class="id">Syarat &amp; Ketentuan</span></a>
			<a href="#contentsection" data-href="<?php echo base_url('home/privacy');?>" data-url="home/privacy" data-title="Privacy Policy Joyday" class="firstmenu menufoot"><span class="en">Privacy Policy</span><span class="id">Kebijakan Privasi</span></a>
		</div>
	</div>
	<div class="imgfooter imgfooter1 opacity0 animated">
		<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer.png')?>" alt="shadowyellow"/>
	</div>
	<div class="imgfooter imgfooter2 opacity0 animated">
		<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer2.png')?>" alt="shadowyellow"/>
	</div>
	<div class="imgfooter imgfooter3 opacity0 animated">
		<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer3.png')?>" alt="shadowyellow"/>
	</div>
	<?php }?>
