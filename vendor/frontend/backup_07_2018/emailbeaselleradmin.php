<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="margin:0 auto;font-family:Arial,Helvetica,sans-serif">
    <tbody>
        <tr>
            <td align="left" valign="top" height="30">&nbsp; </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <h1 style="color:#333;font-size:1.5em">Hi, Admin</h1>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <p style="color:#7e7e7e;line-height:25px">Berikut ini data customer yang telah mengisi data formulir “Be A Seller” sebagai berikut:</p>
                <ul>
                  <li style="color:#7e7e7e;line-height:1.4"><?php echo $name;?></li>
                  <li style="color:#7e7e7e;line-height:1.4"><?php echo $email;?></li>
                  <li style="color:#7e7e7e;line-height:1.4"><?php echo $address;?><br/><?php echo $kabkota;?><br/><?php echo $propinsi;?></li>
                  <li style="color:#7e7e7e;line-height:1.4"><?php echo strtoupper($phone);?></li>
                  <li style="color:#7e7e7e;line-height:1.4">Store Area : <?php echo $variant;?></li>
                </ul>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" height="30"><hr/></td>
        </tr>
        <tr>
            <td align="left" valign="top">
              <p style="color:#7e7e7e;line-height:1.4">Mohon data tersebut untuk segera ditindaklanjuti.</p>

            </td>
        </tr>
    </tbody>
</table>
