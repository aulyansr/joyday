<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<style type="text/css">
.coklat1{
	position: absolute;
    height: 180px;
    top: 12vh;
    right:3vw;
    z-index:10;
}
.coklat2{
	position: absolute;
    height: 180px;
    bottom: 3vh;
    right:1vw;
    z-index:5;
}
.coklat3{
	position: absolute;
    right: 0;
    bottom:20vh;
    width:200px;
}
.titlepage{
	font-size: 2rem;
	text-transform: uppercase;
	text-align: center;
	margin-bottom: 2rem;
	color:#0060af;
}
.paper p{
	text-transform: uppercase;
	letter-spacing: .2em;
	color:#0060af;
	font-weight: 300;
	font-size: .7em;
	line-height: 2;
	margin-bottom:1em;
}
p{
	text-indent:4rem;
	text-align: justify;
}
p::first-letter,p .big{
	font-size: 1.5em;
	line-height: 1;
}
@media (max-width: 769px){
	.paper{
		margin: 0 0px;
	}
	.coklat1,.coklat2{
		right:-6vw;
	}
	.paper > .wrappadding{
		min-height: 75vh;
	}
	.nanas8 {
	    height: 80px;
	    top: 18vh;
	    left: 20vw;
	}
}
@media (max-width: 740px)
{
	.coklat1,.coklat2{
		right:0vw;
		max-height: 100px;
	}
	.coklat3 {
	    max-width: 120px;
	}
	.nanas1{
		max-height: 100px;
	}
	.nanas2{
		max-height: 100px;
	}
	.nanas2{
		max-height: 120px;
		bottom: 10vh;
	}
	.contentterms{
		height: 70vh;
	}
}@media (max-width: 500px)
{
	p{
		text-indent:2rem;
		text-align: left;
	}
	.contentterms{
		height: 80vh;
	}
	.paper > .wrappadding{
		padding: 1.5rem 1.5rem 4rem 1.5rem;
	}
	.titlepage{
		font-size:1.3rem;
		margin-bottom: 1rem;
	}
	.coklat2{
		bottom:10vh;
	}
	.coklat1, .coklat2{
		right: -7vw;
	}
	.coklat3{
		bottom: 30vh;
	}
}
</style>
	<?php
		if($this->input->get('content') != 'inload'){
	?>
	<section id="contentsection" class="contentsection">
		<div id="load" class="contenthomeslide contenview">
	<?php }?>
			<div id="wrappingshow">
				<div class="nanas">
					<img class="img-fluid nanas1 naanimation animated opacity0" src="<?php echo GassetURL('asset/static/nanas/nanas1.png');?>">
					<img class="img-fluid nanas2 naanimation animated opacity0" src="<?php echo GassetURL('asset/static/nanas/nanas2.png');?>">
					<img class="img-fluid nanas8 naanimation animated opacity0" src="<?php echo GassetURL('asset/static/nanas/nanas6.png');?>">
					<img class="img-fluid coklat1 naanimation animated opacity0" src="<?php echo GassetURL('asset/static/coklat/coklat1.png');?>">
					<img class="img-fluid nanas5 naanimation animated opacity0" src="<?php echo GassetURL('asset/static/nanas/nanas5.png');?>">
					<img class="img-fluid coklat2 naanimation animated opacity0" src="<?php echo GassetURL('asset/static/coklat/coklat2.png');?>">
					<img class="img-fluid coklat3 naanimation animated opacity0" src="<?php echo GassetURL('asset/static/coklat/coklat3.png');?>">
					<div class="contentterms animated opacity0">
						<div class="contentterm height100percent">
							<div class="container height100percent container1000">
								<div class="row height100percent">
									<div class="col-md-12 height100percent ">
										<div ss-container class="paper ss-container about">
											<div class="wrappadding">
												<div class="en">
													<h1 class="titlepage">About</h1>
													<p>Joyday ice cream is produced by PT. Green Asia Food Indonesia. <span class="big">J</span>oyday uses pure milk and other high quality ingredients with international standard and halal certified by LPPOM MUI to ensure there is always happiness in every bite.</p>
													<p>Joyday wants you who enjoy it always remember to be happy anytime and in any situation. <span class="big">I</span>nspired by it, Joyday comes with the tagline "Happy Day, Joyday!" on every package.</p>
													<p>Not only with the tagline, but through the selection of ice cream flavors, Joyday wants to make sure that we are all can feel the happiness directly. <span class="big">C</span>ome on and taste ice cream Joyday now!</p>
												</div>

												<div class="id">
													<h1 class="titlepage">Tentang</h1>
													<p>ES KRIM JOYDAY DIPRODUKSI OLEH PT. GREN ASIA FOOD INDONESIA. <span class="big">J</span>OYDAY
													MENGGUNAKAN SUSU MURNI DAN BAHAN KUALITAS TINGGI LAINNYA DENGAN STANDAR INTERNASIONAL DAN BERSERTIFIKAT HALAL DARI LPPOM MUI UNTUK MEMASTIKAN AKAN SELALU ADA KEBAHAGIAAN DALAM SETIAP GIGITAN.</p>
													<p>JOYDAY INGIN ANDA YANG MENIKMATINYA SELALU INGAT UNTUK BERBAHAGIA KAPAN SAJA DAN DALAM SITUASI APAPUN. <span class="big">T</span>ERINSPIRASI OLEH HAL TERSEBUT, JOYDAY DATANG DENGAN TAGLINE "HAPPY DAY, JOYDAY!" DALAM SETIAP PAKETNYA.</p>
													<p>BUKAN SAJA DENGAN TAGLINE, TETAPI MELALUI PEMILIHAN DARI RASA ES KRIM, JOYDAY INGIN MEMBUAT YAKIN BAHWA KITA SEMUA BISA MERASAKAN KEBAHAGIAAN SECARA LANGSUNG. <span class="big">A</span>YO DAN RASAKAN ICE CREAM JOYDAY SEKARANG!</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<script type="text/javascript">
					$(document).ready(function(){
						$('.imgfooter').animate({'opacity':1, 'bottom' : 0},100,function(){
							$('.nanas8').animate({'opacity':1},100,function(){
								<?php if($this->input->get('content') != 'inload'){?>
									$('.socmed,.socmedmobile,.headerhome').animate({'opacity':1},1000).removeClass('fadeIn');
								<?php }?>
								$('.coklat1').animate({'opacity': 1},100,function(){
									setTimeout(function(){
										$('.nanas1').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
										$('.nanas2').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
									},300);
								}).addClass('fadeInUp');
								$('.coklat2').animate({'opacity': 1},100,function(){
									$('.coklat3').animate({'opacity': 1},100,function(){
										$('.socmed').animate({'opacity':1},1000).removeClass('fadeIn');
										setTimeout(function(){
											$('.contentterms').animate({'opacity' : 1},1000,function(){

											}).addClass('fadeInUp');
										},1000);
									}).addClass('fadeInUp');
								}).addClass('fadeInUp');
							}).addClass('fadeInUp');
						}).addClass('fadeInUp').prev('.copyright').delay(1000).animate({opacity:1},1000);
					});
				</script>
			</div>
	<?php
		if($this->input->get('content') != 'inload'){
	?>
			</div>
		</section>
		<div class="copyright absolute animated opacity0">
			<div class="footermenu">
				<a href="/about" data-href="" data-url="home/terms" data-title="terms of use Joyday" class="firstmenu menufoot">Syarat &amp; Ketentuan</a>
				<a href="/terms-of-use" data-href="/terms-of-use" data-url="home/privacy" data-title="Privacy Policy Joyday" class="firstmenu menufoot">Kebijakan Privasi</a>
				<a href="/contact-us" data-href="/contact-us" data-url="home/contact" data-title="Contact Joyday" class="firstmenu menufoot">Hubungi Kami</a>
			</div>
		</div>
		<div class="imgfooter imgfooter1 opacity0 animated">
			<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer.png')?>" alt="shadowyellow"/>
		</div>
		<div class="imgfooter imgfooter2 opacity0 animated">
			<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer2.png')?>" alt="shadowyellow"/>
		</div>
		<div class="imgfooter imgfooter3 opacity0 animated">
			<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer3.png')?>" alt="shadowyellow"/>
		</div>
	<?php }?>
