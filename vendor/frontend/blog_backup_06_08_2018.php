<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<style type="text/css">
.coklat1{
	position: absolute;
    height: 180px;
    top: 12vh;
    right:0;
    z-index:10;
}
.coklat2{
	position: absolute;
    height: 180px;
    bottom: 3vh;
    right:1vw;
    z-index:5;
}
.coklat3{
	position: absolute;
    right: 0;
    bottom:20vh;
    width:200px;
}
.titlepage{
	font-size: 2rem;
	text-transform: uppercase;
	text-align: center;
	margin-bottom: 2rem;
	color:#0060af;
}
.paper p{
	text-transform: uppercase;
	letter-spacing: .2em;
	color:#0060af;
	font-weight: 300;
	font-size: .7em;
	line-height: 2;
	margin-bottom:1em;
}
.menusidebarblog{
	list-style: none;
	padding:0;
	margin:0;
}
.menusidebarblog li{
	padding:10px 0;
	margin:5px 0;
}
.menusidebarblog li a{
	position: relative;
	font-weight: 300;
	font-size: 13px;
	text-transform: uppercase;
	display: inline-block;
	width: 100%;
	opacity: .5;
}
.menusidebarblog li a span{
	float: left;
}
.menusidebarblog li a span + span{
	float: right;
}
.menusidebarblog li a.active,
.menusidebarblog li:hover a{
	text-decoration: none;
	opacity: 1;
	color: #0060af;
}
.sidebare{
	max-height: 80%;
}
.sidebare .wrapmenusidebar{
	max-width: 170px;
	margin:0;
}
.sidebare h2{
	font-size: 2rem;
	margin:2rem 0;
	color: #0060af;
}
.item-blog{
	position: relative;
	letter-spacing: .2em;
	line-height: 1.5;
	padding:15px 2.5rem;
	font-size:10px;
	text-transform: uppercase;
	opacity: .5;
	color: #0060af;
	-webkit-transition: all 1s ease;
    -moz-transition: all 1s ease;
    -o-transition: all 1s ease;
    -ms-transition: all 1s ease;
    transition: all 1s ease;
}
.item-blog:hover{
	padding-left: 6rem;
	opacity: 1;
	cursor: pointer;
}
.title-blog{
	font-size:2rem;
	margin:0;
	padding:0;
}
.meta-title-blog{
	padding:5px 0 20px 0;
}
.content-item-blog{
	max-width: 650px;
	line-height: 2;
	margin-bottom: 25px;
}
.readmore{
	display: inline-block;
	position: relative;
	border:1px solid #0060af;
	padding:10px 15px;
	padding-right:35px;
	border-radius: 4px;
	font-size: 12px;
	margin-bottom: 15px;
}
.readmore span{
	position: absolute;
	top:10px;
	right:10px;
	left:auto;
	font-size: 15px;
}
.item-blog:nth-of-type(odd) {

}

.item-blog:nth-of-type(even) {

}
.papaerblog{
	padding-bottom: 50px;
}
.fancybox-slide--iframe .fancybox-close-small{
	right:0;
}
.fancybox-slide--iframe .fancybox-content::-webkit-scrollbar {
    width: 10px;
}

/* Track */
.fancybox-slide--iframe .fancybox-content::-webkit-scrollbar-track {
	background: rgba(255,255,255,0.7);
    border: 4px solid transparent;
    background-clip: content-box;
    border-radius: 10px;
}

/* Handle */
.fancybox-slide--iframe .fancybox-content::-webkit-scrollbar-thumb {
    background: rgb(255,255,255);
    border-radius: 10px;
}
.fancybox-slide--iframe .fancybox-content::-webkit-scrollbar-thumb:vertical {
    height:15px;
}
/* Handle on hover */
.fancybox-slide--iframe .fancybox-content::-webkit-scrollbar-thumb:hover {
    background: #555;
}
.fancybox-bg{
	background: rgba(255,255,255,0.3);
}
.close-smalle{
	position: absolute;
	top: 0;
	right: 0;
	width: 44px;
	height: 44px;
	padding: 0;
	margin: 0;
	border: 0;
	border-radius: 0;
	outline: none;
	background: transparent;
	z-index: 10;
	cursor: pointer;
}
.close-smalle:after {
    content: '×';
    position: absolute;
    top: 5px;
    right: 5px;
    width: 30px;
    height: 30px;
    font: 40px/50px Arial,"Helvetica Neue",Helvetica,sans-serif;
    color: #0060af;
    font-weight: 300;
    text-align: center;
    border-radius: 50%;
    border-width: 0;
    background: transparent;
    transition: background .25s;
    box-sizing: border-box;
    z-index: 2;
}
.detail-blog-item{
	position: absolute;
	top:100%;
	left:0rem;
	width: 100%;
	background: #fff;
	padding-bottom:12rem;
	height: 100vh;
	overflow: hidden;
	overflow-y: auto;
	z-index: 27;
	background: transparent;
}
.detail-blog-item > .wrappadding{
	position: relative;
	padding: 4rem 2.5rem;
	padding-right:1rem;
	margin:0 3rem;
	background: rgba(255,255,255,.98);
	border-radius: 5px;
	overflow: hidden;
	overflow-y: auto;
	box-shadow:0 10px 20px 0px #555;
}
.detail-blog-item > .wrappadding{
	text-transform: uppercase;
    letter-spacing: .2em;
    color: #0060af;
    font-weight: 300;
    font-size: .7em;
    line-height: 2;
	min-height:90vh;
}
.detail-blog-item > .wrappadding p{
    margin-bottom: 1em;
}
.nanas.hasContent .blogwrap{
	z-index:20;
}
.nanas.hasContent .blogwrap .ss-container{
	overflow-y: hidden;
}
.content-description{
	max-width: 90%;
}
.content-socmed{
	max-width: 10%;
}
.sosmed-product-detaile{
	padding-right:0rem;
	font-size:2rem;
}
.sosmed-product-detaile a{
	text-decoration: none;
	color: #005b9c;
	cursor: pointer;
	opacity: .9;
}
.headerhome,.socmed{
	opacity: 1;
}
@media (max-width: 769px){
	.ss-container {
	    max-height: 78vh;
	}
	.paper{
		margin: 0 0px;
	}
	.coklat1,.coklat2{
		right:-6vw;
	}
	.paper > .wrappadding{
		min-height: 75vh;
	}
	.nanas8 {
	    height: 80px;
	    top: 18vh;
	    left: 20vw;
	}
	.detail-blog-item{
		max-height: 80vh;
		padding-bottom: 5rem;
	}
	.blogging{
		background-color: rgba(255,255,255,.9);
		margin:0 10px;
	}
	.item-blog{
		padding:2rem 2.5rem;
	}
	.title-blog {
	    font-size: 1.5rem;
	}
	.item-blog:hover{
		padding-left: 2rem;
	}
	.detail-blog-item > .wrappadding{
		padding: 2rem 2.5rem
		margin:0 1rem 0 1.5rem;
	}
	.titlepage{
		margin-bottom: 1rem;
	}
}
@media (max-width: 740px)
{
	.detail-blog-item{
		max-height: 88vh;
		padding-bottom: 5rem;
	}
	.coklat1,.coklat2{
		right:0vw;
		max-height: 100px;
	}
	.coklat3 {
	    max-width: 120px;
	}
	.nanas1{
		max-height: 100px;
	}
	.nanas2{
		max-height: 100px;
	}
	.nanas2{
		max-height: 120px;
		bottom: 10vh;
	}
	.contentterms{
		height: 70vh;
	}
	.blogging{
		background-color: rgba(255,255,255,.9);
		margin:0 10px;
	}
	.item-blog{
		padding:1rem 1.5rem;
	}
	.title-blog {
	    font-size: 1.5rem;
	}
	.item-blog:hover{
		padding-left: 2rem;
	}
	.detail-blog-item > .wrappadding{
		padding: 2rem 1.5rem;
		margin:0 1rem 0 1.5rem;
	}
	.titlepage{
		margin-bottom: 1rem;
	}
}@media (max-width: 500px)
{
	.detail-blog-item{
		max-height: 85vh;
		padding-bottom: 5rem;
	}
	.contentterms{
		height: 80vh;
	}
	.titlepage{
		font-size:1.8rem;
	}
	.coklat2{
		bottom:10vh;
	}
	.coklat1, .coklat2{
		right: -7vw;
	}
	.coklat3{
		bottom: 30vh;
	}
}
table, th, td {
    border: 1px solid black;
    text-align: center;
}
</style>
<?php
		if($this->input->get('content') != 'inload'){
	?>
	<section id="contentsection" class="contentsection">
		<div id="load" class="contenthomeslide contenview">
	<?php }?>
			<div id="wrappingshow">
				<div class="nanas">
					<div class="overlay-sidebar animated fadeInLeft"></div>
					<img class="img-fluid nanas1 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas1.png');?>">
					<img class="img-fluid nanas2 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas2.png');?>">
					<img class="img-fluid nanas8 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas6.png');?>">
					<img class="img-fluid coklat1 naanimation animated opacity0" src="<?php echo base_url('asset/static/coklat/coklat4.png');?>">
					<img class="img-fluid nanas5 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas5.png');?>">
					<img class="img-fluid coklat3 naanimation animated opacity0" src="<?php echo base_url('asset/static/coklat/coklat3.png');?>">
					<div class="contentterms blogwrap animated opacity0">
						<div class="contentterm height100percent">
							<div class="container height100percent">
								<div class="row height100percent">
									<div ss-container class="col-md-3 ss-container sidebare height100percent ">
										<div class="wrapmenusidebar">
											<h2>BLOG</h2>
											<ul class="menusidebarblog">
												<li><a data-href="blog1" data-url="group-title-1" data-title="Group Title 1" class="sidemenu active"><span class="en">May 2018</span><span class="id">Mei 2018</span></a></li>
											</ul>
										</div>
									</div>
									<div id="main-blog" class="col-lg-9 col-md-12 height100percent ">
										<div ss-container class="papaerblog ss-container">
											<div class="blogging">
												<div class="item-blog">
													<h2 class="title-blog"><span class="id">Sertifikasi & Sistem Jaminan Halal Joyday</span><span class="en">HALAL WARRANTY CERTIFICATION & SYSTEMS OF JOYDAY</span></h2>
													<div class="meta-title-blog">7 May 2018</div>
													<div class="content-item-blog">
														<div class="id">
															Selain banyak rasa dan nikmat, eskrim Joyday telah memperoleh Sertifikat Halal dan Implementasi Sistem Jaminan Halal dengan kategori nilai A (Sangat Baik) yang dikeluarkan secara resmi oleh Lembaga Pengkajian Pangan, Obat-obatan dan Kosmetika – Majelis Ulama Indonesia (LPPOM MUI).
														</div>
														<div class="en">
															ASIDE ITS VARIANTS AND DELICIOUS TASTE. JOYDAY ICE CREAM HAS ACHIEVED HALAL CERTIFICATE AND WARRANTY SYSTEM IMPLEMENTATION WITH VALUE A (VERY GOOD) WHICH IS EXPENDED BY AUTHORISED INSTITUTIONAL AGENCY OF FOOD, DRUGS AND COSMETICS - INDONESIAN MOSLEM SCHOLAR ASSEMBLY (LPPOM MUI).
														</div>
													</div>
													<a class="readmore" data-target="#readmore_1"><div class="propin" data-id="Selengkapnya" data-en="Read More">
														<?php
															if(get_cookie('currentlang') == 'id'){
																echo 'Selengkapnya';
															}else{
																echo 'Read more';
															}
														?>
													</div><span><i class="ti-arrow-right"></i></span></a>
												</div>
												<div class="item-blog">
													<h2 class="title-blog"><span class="id">Lulus Uji BPOM</span><span class="en">PASSED TEST FROM BPOM (DRUG AND FOOD CONTROL AGENCY)</span></h2>
													<div class="meta-title-blog">8 May 2018</div>
													<div class="content-item-blog">
														<div class="id">
															Kini kamu tidak perlu khawatir, karena es krim Joyday sudah terbukti lulus uji dari BPOM (Badan Pengawas Obat dan Makanan). Jadi selain nikmat, Joyday sudah terbukti aman loh untuk dikonsumsi. Yuk, tunggu apalagi? Cobain eskrim Joyday sekarang dan ceriakan harimu karena Happy Day, Joyday!
														</div>
														<div class="en">
															NOW YOU DO NOT NEED TO WORRY, BECAUSE ICE CREAM JOYDAY HAVE PROVEN PASSED TEST FROM BPOM (DRUG AND FOOD CONTROL AGENCY). SO ENJOY THIS, JOYDAY IS PROVEN SAFE FOR CONSUMPTION. COME ONE, WHAT ARE YOU WAITING FOR? LET'S TRY JOYDAY ICE CREAM NOW AND ADD HAPPINESS IN YOUR DAY BECAUSE HAPPY DAY, JOYDAY!
														</div>
													</div>
													<a class="readmore" data-target="#readmore_2"><div class="propin" data-id="Selengkapnya" data-en="Read More"><?php
														if(get_cookie('currentlang') == 'id'){
															echo 'Selengkapnya';
														}else{
															echo 'Read more';
														}
													?></div> <span><i class="ti-arrow-right"></i></span></a>
												</div>
											</div>
										</div>

										<!-- BLOG DETAIL 1 -->
										<div id="readmore_1" ss-container class="detail-blog-item animated">
											<div class="wrappadding animated fadeInUp">
												<div class="closecontent close-smalle"></div>
												<h1 class="titlepage"><span class="id">Sertifikasi & Sistem Jaminan Halal Joyday</span><span class="en">HALAL WARRANTY CERTIFICATION & SYSTEMS OF JOYDAY</span></h1>
												<div class="is-clearfix">
													<div class="is-pulled-left content-description">
														<div class="id">
														<p>Selain banyak rasa dan nikmat, es krim Joyday telah memperoleh Sertifikat Halal dan Implementasi Sistem Jaminan Halal dengan kategori nilai A (Sangat Baik) yang dikeluarkan secara resmi oleh Lembaga Pengkajian Pangan, Obat-obatan dan Kosmetika – Majelis Ulama Indonesia (LPPOM MUI). Yuk, cobain es krim Joyday sekarang dan rasakan nikmatnya. Kapan pun dimana pun, Joyday siap menambah keceriaan harimu karena Happy Day, Joyday!</p>
														<a href="https://www.joyday.com/asset/pdf/Halal-Certificate-Meizhou.pdf" target="_blank"><p>Download sertifikat halal disini.</p></a><br />
														</div>
														<div class="en">
														<p>ASIDE ITS VARIANTS AND DELICIOUS TASTE. JOYDAY ICE CREAM HAS ACHIEVED HALAL CERTIFICATE AND WARRANTY SYSTEM IMPLEMENTATION WITH VALUE A (VERY GOOD) WHICH IS EXPENDED BY AUTHORISED INSTITUTIONAL AGENCY OF FOOD, DRUGS AND COSMETICS - INDONESIAN MOSLEM SCHOLAR ASSEMBLY (LPPOM MUI). COME ON, LET’S TRY JOYDAY ICE CREAM NOW AND TASTE IT’S DELICIOUS TASTE. ANYWHERE ANYTIME, JOYDAY IS READY TO ADD HAPPINESS IN YOUR DAY BECAUSE HAPPY DAY, JOYDAY!</p>
														<a href="https://www.joyday.com/asset/pdf/Halal-Certificate-Meizhou.pdf" target="_blank"><p>DOWNLOAD HALAL CERTIFICATE HERE.</p></a><br />
														</div>
														<center>
														<img src="https://www.joyday.com/asset/static/sertifikat-halal-1.png" alt="sertifikat halal 1"><br /><br/>
														<img src="https://www.joyday.com/asset/static/sertifikat-halal-2.png" alt="sertifikat halal 2"><br /><br/><br /><br /><br />
														</center>
													</div>
													<div class="is-pulled-right content-socmed">
														<ul class="sosmed-product-detaile">
															<li><a href="//www.facebook.com/JoydayIceCream/" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
															<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=<?php echo substr('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut',0,20)?>" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
															<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
															<li><a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?php echo current_url();?>."><i class="ti-email"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>

										<!-- BLOG DETAIL 2 -->
										<div id="readmore_2" ss-container class="detail-blog-item animated">
											<div class="wrappadding animated fadeInUp">
												<div class="closecontent close-smalle"></div>
												<h1 class="titlepage"><span class="id">Lulus Uji BPOM</span><span class="en">PASSED BPOM TEST</span></h1>
												<div class="is-clearfix">
													<div class="is-pulled-left content-description">
														<table class="table">
  															<tr style="text-align:center">
    															<th>NO</th>
    															<th><span class="id">NAMA PRODUK</span><span class="en">PRODUCT</span></th>
    															<th>NO BPOM</th>
  															</tr>
  															<tr>
    															<td>1.</td>
    															<td>Joyday - Crunchy Chocolate Malt</td>
    															<td>204209024726</td>
  															</tr>
    														<tr>
    															<td>2.</td>
    															<td>Joyday - Crunchy Chocolate Blueberry </td>
    															<td>204209029726</td>
  															</tr>
    														<tr>
    															<td>3.</td>
    															<td>Joyday - Tiramisu Chocolate Mint</td>
    															<td>204109014726</td>
  															</tr>
    														<tr>
    															<td>4.</td>
    															<td>Joyday - Classic Belgian Chocolate</td>
    															<td>204209019726</td>
  															</tr>
    														<tr>
    															<td>5.</td>
    															<td>Joyday - Crunchy Chocolate Vanila</td>
    															<td>304209016726</td>
  															</tr>
    														<tr>
    															<td>6.</td>
    															<td>Joyday - Crunchy Honeydew Melon</td>
    															<td>304209017726</td>
  															</tr>
    														<tr>
    															<td>7.</td>
    															<td>Joyday - Vanilla Milkshake</td>
    															<td>304209013726</td>
  															</tr>
    														<tr>
   	 															<td>8.</td>
    															<td>Joyday - Sweet Corn </td>
    															<td>204209012726</td>
  															</tr>
    														<tr>
    															<td>9.</td>
    															<td>Joyday - Jujube White Chocolate</td>
    															<td>204209018726</td>
  															</tr>
    														<tr>
    															<td>10.</td>
    															<td>Joyday - Cool Blueberry</td>
    															<td>214209015726</td>
  															</tr>
  															<tr>
    															<td>11.</td>
    															<td>Joyday - Cool Pineapple</td>
    															<td>214209011726</td>
    														</tr>
														</table>
														<div class="id">
															<p>Kini kamu tidak perlu khawatir, karena es krim Joyday sudah terbukti lulus uji dari BPOM (Badan Pengawas Obat dan Makanan). Jadi selain nikmat, Joyday sudah terbukti aman loh untuk dikonsumsi. Yuk, tunggu apalagi? Cobain es krim Joyday sekarang dan ceriakan harimu karena Happy Day, Joyday!</p>
															<p>&nbsp;<br/></p>
															<p>&nbsp;<br/></p>
														</div>
														<div class="en">
															<p>NOW YOU DO NOT NEED TO WORRY, BECAUSE ICE CREAM JOYDAY HAVE PROVEN PASSED TEST FROM BPOM (DRUG AND FOOD CONTROL AGENCY). SO ENJOY THIS, JOYDAY IS PROVEN SAFE FOR CONSUMPTION. COME ONE, WHAT ARE YOU WAITING FOR? LET'S TRY JOYDAY ICE CREAM NOW AND ADD HAPPINESS IN YOUR DAY BECAUSE HAPPY DAY, JOYDAY!</p>
															<p>&nbsp;<br/></p>
															<p>&nbsp;<br/></p>
														</div>
													</div>
													<div class="is-pulled-right content-socmed">
														<ul class="sosmed-product-detaile">
															<li><a href="//www.facebook.com/JoydayIceCream/" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
															<li><a data-href="https://twitter.com/share?url=<?php echo current_url();?>&amp;text=<?php echo substr('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut',0,20)?>" title="Twitter share" target="_blank" class="tw share"><i class="fa fa-twitter"></i></a></li>
															<li><a href="//www.instagram.com/joydayicecream" target="_blank"><i class="fa fa-instagram"></i></a></li>
															<li><a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?php echo current_url();?>."><i class="ti-email"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<script type="text/javascript">
					$(document).ready(function(){
						$('.sidemenu').on('click',function(){
						  	var $this = $(this);
						  		//urlload = $this.attr('data-href');
						  		$('.sidemenu').removeClass('active');
						  		$this.addClass('active');
						  		$('.closecontent').trigger('click');
						  		//history.pushState({urlPath:'/index.php'}, $(this).attr('data-title'), $(this).attr('data-url'));
						  		/*setTimeout(function(){
						  			$('#main-blog').load(urlload);
						  		},1200);*/

						  });
						$(document).on('click','.readmore',function(){
							var target = $(this).attr('data-target');
							$('.nanas').addClass('hasContent')
							$(target).animate({
								'opacity' : 1,
								'top'  : '0'
							},1000,function(){
								setTimeout(function(){
									$(target).removeClass('fadeInUp');
								},1200);
							}).addClass('fadeInUp');
						});
						$(document).on('click','.closecontent',function(){
							var $this = $(this);
							$this.closest('.detail-blog-item').animate({
								'opacity' : 0,
								'top'	  : '100%'
							},1000,function(){
								$('.nanas').removeClass('hasContent');
								setTimeout(function(){
									$('.detail-blog-item').removeClass('fadeOutDown');
								},1200);
							}).addClass('fadeOutDown');
						});
						function resizeSidebarOverlay()
						{
							var locationside = $(".sidebare");
				        	var locright = ($(window).width() - (locationside.offset().left + locationside.outerWidth()));
				        	return locright;
					    }

					  $(window).resize(function(){
					  	$('.overlay-sidebar').css({'right' : resizeSidebarOverlay()});
					  });
					  $('.firstmenu').removeClass('active');
					  $('[blog]').addClass('active');
						$('.imgfooter').animate({'opacity':1, 'bottom' : 0},100,function(){
							$('.nanas8').animate({'opacity':1},100,function(){
								<?php if($this->input->get('content') != 'inload'){?>
									$('.socmed, .socmedmobile, .headerhome').animate({'opacity':1},1000).removeClass('fadeIn');
								<?php }?>
								$('.coklat1').animate({'opacity': 1},100,function(){
									setTimeout(function(){
										$('.nanas1').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
										$('.nanas2').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
									},300);
								}).addClass('fadeInUp');
									$('.coklat3').animate({'opacity': 1},100,function(){
										setTimeout(function(){
											$('.contentterms').animate({'opacity' : 1},1000,function(){

											}).addClass('fadeInUp');
										},1000);
									}).addClass('fadeInUp');
							}).addClass('fadeInUp');
						}).addClass('fadeInUp').prev('.copyright').delay(1000).animate({opacity:1},1000);
					});
				</script>
<?php
		if($this->input->get('content') != 'inload'){
	?>
			</div>
		</section>
		<div class="copyright absolute animated opacity0">
			<div class="footermenu">
				<a href="#contentsection" data-href="<?php echo base_url('home/terms');?>" data-url="home/terms" data-title="terms of use Joyday" class="firstmenu menufoot"><span class="en">Terms of Use</span><span class="id">Syarat &amp; Ketentuan</span></a>
				<a href="#contentsection" data-href="<?php echo base_url('home/privacy');?>" data-url="home/privacy" data-title="Privacy Policy Joyday" class="firstmenu menufoot"><span class="en">Privacy Policy</span><span class="id">Kebijakan Privasi</span></a>
				<a href="#contentsection" data-href="<?php echo base_url('home/contact');?>" data-url="home/contact" data-title="Contact Joyday" class="firstmenu menufoot"><span class="en">Contact Us</span><span class="id">Hubungi Kami</span></a>
			</div>
		</div>
		<div class="imgfooter imgfooter1 opacity0 animated">
			<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer.png')?>" alt="shadowyellow"/>
		</div>
		<div class="imgfooter imgfooter2 opacity0 animated">
			<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer2.png')?>" alt="shadowyellow"/>
		</div>
		<div class="imgfooter imgfooter3 opacity0 animated">
			<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer3.png')?>" alt="shadowyellow"/>
		</div>
	<?php }?>
