<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="margin:0 auto;font-family:Arial,Helvetica,sans-serif">
    <tbody>
        <tr>
            <td align="left" valign="top" height="30">&nbsp; </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <h1 style="color:#333;font-size:1.5em">Hi, Administrator Joyday</h1>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <p style="color:#7e7e7e;line-height:25px">Berikut ini data customer yang telah mengisi data formulir <b>Request a Sample</b> sebagai berikut:</p>
                <ul>
                  <li style="color:#7e7e7e;line-height:1.4"><?php echo $name;?></li>
                  <li style="color:#7e7e7e;line-height:1.4"><?php echo $email;?></li>
                  <li style="color:#7e7e7e;line-height:1.4"><?php echo $address;?><br/><?php echo $kabkota;?><br/><?php echo $propinsi;?></li>
                  <li style="color:#7e7e7e;line-height:1.4"><?php echo strtoupper($phone);?></li>
                  <li style="color:#7e7e7e;line-height:1.4">Variant :
                    <?php
                    $searchString = ',';
                    if( strpos($variant, $searchString) !== false ) {
                      $var = explode(",",$variant);
                      echo '<ul>';
                      for($i=0;$i<sizeof($var);$i++){
                        echo '<li style="color:#7e7e7e;line-height:1.4">'.$var[$i].'</li>';
                      }
                      echo '</ul>';
                    }else{
                      echo '<p style="color:#7e7e7e;line-height:1.4">' .$variant .'</p><br/><br/>';
                    }
                  ?>
                  </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" height="30"><hr/></td>
        </tr>
        <tr>
            <td align="left" valign="top">
              <p style="color:#7e7e7e;line-height:1.4">Mohon data tersebut untuk segera ditindaklanjuti. Perlu diingat, pesan ini merupakan pesan balasan otomatis, jadi kamu tidak perlu membalas email ini.</p>
              <br/>&nbsp;
              <table>
                <tbody>
                  <tr>
                    <td style="vertical-align:middle;">
                      <a href="https://www.facebook.com/JoydayIceCream/"><img style="width:40px;" src="<?php echo base_url('asset/static/fb.png');?>"></a>
                    </td>
                    <td style="padding-right:20px; vertical-align:middle;">
                      <a style="text-decoration:none;" href="https://www.facebook.com/JoydayIceCream/">JoydayIceCream</a>
                    </td>
                    <td style="vertical-align:middle;">
                      <a href="https://www.twitter.com/JoydayIceCream/"><img style="width:40px;" src="<?php echo base_url('asset/static/tw.png');?>"></a>
                    </td>
                    <td style="padding-right:20px; vertical-align:middle;">
                      <a style="text-decoration:none;" href="https://www.twitter.com/JoydayIceCream/">JoydayIceCream</a>
                    </td>
                    <td style="vertical-align:middle;">
                      <a href="https://www.instagram.com/joydayicecream/"><img style="width:40px;" src="<?php echo base_url('asset/static/ig.png');?>"></a>
                    </td>
                    <td style="vertical-align:middle;">
                      <a style="text-decoration:none;" href="https://www.instagram.com/joydayicecream/">joydayicecream</a>
                    </td>
                  </tr>
                </tbody>
              </table><br/>&nbsp;
              <p style="color:#7e7e7e;line-height:1.4">Have a Joyday!</p>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" height="30">&nbsp;</td>
        </tr>
        <tr>
            <td align="left" valign="top" height="30"><hr/></td>
        </tr>
        <tr>
            <td align="left" valign="top" height="30">
              <table>
                <tbody>
                  <tr>
                    <td colspan="4" style="color:#7e7e7e;line-height:1.4">
                      <strong>PT Green Asia Food Indonesia</strong><br/>
                      Perwata Tower Lantai 6 Suite A<br/>
                      Jl. Pluit Selatan Raya Kav.1,<br/>
                      Penjaringan, Jakarta Utara 14440<br/>
                    </td>
                  </tr>
                  <tr style="color:#7e7e7e;line-height:1.4">
                    <td>TEL</td><td style="width:10px;">:</td><td colspan="2"><a href="tel:+622130027643">+6221 30027643 </a></td>
                  </tr>
                  <tr style="color:#7e7e7e;line-height:1.4">
                    <td>FAX</td><td style="width:10px;">:</td><td colspan="2"><a href="tel:+622130027642">+6221 30027642 </a></td>
                  </tr>
                  <tr style="color:#7e7e7e;line-height:1.4">
                    <td>HOTLINE</td><td style="width:10px;">:</td><td colspan="2"><a href="tel:08001868686">0800 1 868686 </a></td>
                  </tr>
                </tbody>
              </table>
            </td>
        </tr>
    </tbody>
</table>
