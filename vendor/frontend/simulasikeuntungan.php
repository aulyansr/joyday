<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<style type="text/css">
.coklat1{
  position: absolute;
  height: 180px;
  top: 12vh;
  right:3vw;
  z-index:10;
}
.coklat2{
  position: absolute;
  height: 180px;
  bottom: 3vh;
  right:1vw;
  z-index:5;
}
.coklat3{
  position: absolute;
  right: 0;
  bottom:20vh;
  width:200px;
}
.titlepage{
  font-size: 2rem;
  text-transform: uppercase;
  text-align: center;
  margin-bottom: 2rem;
  color:#0060af;
}
.paper p{
  text-transform: uppercase;
  letter-spacing: .2em;
  color:#0060af;
  font-weight: 300;
  font-size: .7em;
  line-height: 2;
  margin-bottom:1em;
}
p{
  text-indent:4rem;
  text-align: justify;
}
p::first-letter,p .big{
  font-size: 1.5em;
  line-height: 1;
}

.table-striped tbody tr:nth-of-type(odd) {
  background-color: rgb(179, 232, 255);
  border: 0;
}
.table-striped tbody tr:nth-of-type(even) {
  
  background: #c4e7f6;
  border: 0;
}

.tabel td, .table th{
  font-family: 'Fredoka one';
  font-weight: 100;
  border: 0;
}

.txt-ket{
    font-size: 15px;
}
@media (max-width: 769px){
  .paper{
    margin: 0 0px;
  }
  .coklat1,.coklat2{
    right:-6vw;
  }
  .paper > .wrappadding{
    min-height: 75vh;
  }
  .nanas8 {
    height: 80px;
    top: 18vh;
    left: 20vw;
  }
}
@media (max-width: 740px)
{
  .coklat1,.coklat2{
    right:0vw;
    max-height: 100px;
  }
  .coklat3 {
    max-width: 120px;
  }
  .nanas1{
    max-height: 100px;
  }
  .nanas2{
    max-height: 100px;
  }
  .nanas2{
    max-height: 120px;
    bottom: 10vh;
  }
  .contentterms{
    height: 70vh;
  }
  }@media (max-width: 500px)
  {
    p{
      text-indent:2rem;
      text-align: left;
    }
    .contentterms{
      height: 80vh;
    }
    .paper > .wrappadding{
      padding: 1.5rem 1.5rem 4rem 1.5rem;
    }
    .titlepage{
      font-size:1.3rem;
      margin-bottom: 1rem;
    }
    .coklat2{
      bottom:10vh;
    }
    .coklat1, .coklat2{
      right: -7vw;
    }
    .coklat3{
      bottom: 30vh;
    }
    .txt-ket{
    font-size: 11px;
}
  }
</style>
<?php
if($this->input->get('content') != 'inload'){
  ?>
  <section id="contentsection" class="contentsection" style="background-color:#C4E7F6">
    <div id="load" class="contenthomeslide contenview">
    <?php }?>
    <div id="wrappingshow">
      <div class="d-block" style="margin-top:25vh;padding-bottom:25vh">
        <div class="container">
          <h1 class="titlepage">Simulasi Keuntungan</h1>
          <div class="d-sm-block d-md-none" style="background: #c5e7f6;padding-top: 20px;">
        		  <img class="w-100 " src="<?php echo base_url('asset/static/simulasi-1.png');?>" >
        	</div>
          <div class="d-none d-md-block">
            <div class="p-2 text-center text-white" style="background-color:#2784C4;">
              Simulasi 1
            </div>
            <div class="p-2 text-center text-white" style="background-color:#2784C4;">
              Belanja 7 Karton es krim Joyday per minggu
            </div>    
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr class="text-white" style="background-color:#20A7DE">
                    <th scope="col" class="text-white" style="font-weight:100">Simulasi</th>
                    <th scope="col" class="text-white" style="font-weight:100">Belanja (Karton)</th>
                    <th scope="col" class="text-white" style="font-weight:100">Modal</th>
                    <th scope="col" class="text-white" style="font-weight:100">Jual</th>
                    <th scope="col" class="text-white" style="font-weight:100">Untung Berlipat</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">Minggu 1</th>
                    <td>10</td>
                    <td>Rp. 1.300.000</td>
                    <td>Rp. 1.690.000</td>
                    <td>Rp. 390.000</td>
                  </tr>
                  <tr>
                    <th scope="row">Minggu 2</th>
                    <td>7</td>
                    <td>Rp. 910.000</td>
                    <td>Rp. 1.183.000</td>
                    <td>Rp. 273.000</td>
                  </tr>
                  <tr>
                    <th scope="row">Minggu 2</th>
                    <td>7</td>
                    <td>Rp. 910.000</td>
                    <td>Rp. 1.183.000</td>
                    <td>Rp. 273.000</td>
                  </tr>
                  <tr>
                    <th scope="row">Minggu 2</th>
                    <td>7</td>
                    <td>Rp. 910.000</td>
                    <td>Rp. 1.183.000</td>
                    <td>Rp. 273.000</td>
                  </tr>
                  <tr>
                    <th scope="row">Biaya Listrik</th>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Rp. 120.000</td>
                  </tr>
                  <tr>
                    <th scope="row">Untung Bersih</th>
                    <td>31</td>
                    <td>Rp. 4.030.000</td>
                    <td>Rp. 5.239.000</td>
                    <td>Rp. 1.089.000</td>
                  </tr>
                </tbody>
              </table>
            </div>  
          </div>

          <div class="d-sm-block d-md-none" style="background: #c5e7f6;padding-top: 20px;">
        		  <img class="w-100 " src="<?php echo base_url('asset/static/simulasi-2.png');?>" >
        	</div>
          <div class="d-none d-md-block">
            <div class="p-2 text-center text-white" style="background-color:#2784C4;">
              Simulasi 2
            </div>
            <div class="p-2 text-center text-white" style="background-color:#2784C4;">
              Belanja 5 Karton es krim Joyday per minggu
            </div>  
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr class="text-white" style="background-color:#20A7DE">
                    <th scope="col" class="text-white" style="font-weight:100">Simulasi</th>
                    <th scope="col" class="text-white" style="font-weight:100">Belanja (Karton)</th>
                    <th scope="col" class="text-white" style="font-weight:100">Modal</th>
                    <th scope="col" class="text-white" style="font-weight:100">Jual</th>
                    <th scope="col" class="text-white" style="font-weight:100">Untung Berlipat</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">Minggu 1</th>
                    <td>10</td>
                    <td>Rp. 1.300.000</td>
                    <td>Rp. 1.690.000</td>
                    <td>Rp. 390.000</td>
                  </tr>
                  <tr>
                    <th scope="row">Minggu 2</th>
                    <td>3</td>
                    <td>Rp. 390.000</td>
                    <td>Rp. 507.000</td>
                    <td>Rp. 117.000</td>
                  </tr>
                  <tr>
                    <th scope="row">Minggu 2</th>
                    <td>3</td>
                    <td>Rp. 390.000</td>
                    <td>Rp. 507.000</td>
                    <td>Rp. 117.000</td>
                  </tr>
                  <tr>
                    <th scope="row">Minggu 2</th>
                    <td>3</td>
                    <td>Rp. 390.000</td>
                    <td>Rp. 507.000</td>
                    <td>Rp. 117.000</td>
                  </tr>
                  <tr>
                    <th scope="row">Biaya Listrik</th>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Rp. 120.000</td>
                  </tr>
                  <tr>
                    <th scope="row">Untung Bersih</th>
                    <td>25</td>
                    <td>Rp. 3.250.000</td>
                    <td>Rp. 4.225.000</td>
                    <td>Rp. 855.000</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

          <div class="d-sm-block d-md-none" style="background: #c5e7f6;padding-top: 20px;">
        		  <img class="w-100 " src="<?php echo base_url('asset/static/simualsi-3-fix.png');?>" >
        	</div>
          <div class="d-none d-md-block">
            <div class="p-2 text-center text-white" style="background-color:#2784C4;">
              Simulasi 3
            </div>
            <div class="p-2 text-center text-white" style="background-color:#2784C4;">
              Belanja 3 Karton es krim Joyday per minggu
            </div>  
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr class="text-white" style="background-color:#20A7DE">
                    <th scope="col" class="text-white" style="font-weight:100">Simulasi</th>
                    <th scope="col" class="text-white" style="font-weight:100">Belanja (Karton)</th>
                    <th scope="col" class="text-white" style="font-weight:100">Modal</th>
                    <th scope="col" class="text-white" style="font-weight:100">Jual</th>
                    <th scope="col" class="text-white" style="font-weight:100">Untung Berlipat</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">Minggu 1</th>
                    <td>10</td>
                    <td>Rp. 1.300.000</td>
                    <td>Rp. 1.690.000</td>
                    <td>Rp. 390.000</td>
                  </tr>
                  <tr>
                    <th scope="row">Minggu 2</th>
                    <td>3</td>
                    <td>Rp. 390.000</td>
                    <td>Rp. 507.000</td>
                    <td>Rp. 117.000</td>
                  </tr>
                  <tr>
                    <th scope="row">Minggu 2</th>
                    <td>3</td>
                    <td>Rp. 390.000</td>
                    <td>Rp. 507.000</td>
                    <td>Rp. 117.000</td>
                  </tr>
                  <tr>
                    <th scope="row">Minggu 2</th>
                    <td>3</td>
                    <td>Rp. 390.000</td>
                    <td>Rp. 507.000</td>
                    <td>Rp. 117.000</td>
                  </tr>
                  <tr>
                    <th scope="row">Biaya Listrik</th>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Rp. 120.000</td>
                  </tr>
                  <tr>
                    <th scope="row">Untung Bersih</th>
                    <td>19</td>
                    <td>Rp. 2.470.000</td>
                    <td>Rp. 3.211.000</td>
                    <td>Rp. 621.000</td>
                  </tr>
                </tbody>
              </table>
  
             
            </div>    
          </div>
           <p style="text-indent:0; margin-top:20px;" class="txt-ket">
                Keterangan: 
                <br>
                  1. Harga 1 karton es krim Joyday berkisar Rp 130.000,- <br>
                  2. Untung berlipat jual es krim Joyday sebesar 25-30%
                </ol>
              </p>    
          
          
        </div>
        
      </div>
      <script type="text/javascript">
      $(document).ready(function(){
        $('.imgfooter').animate({'opacity':1, 'bottom' : 0},100,function(){
          $('.nanas8').animate({'opacity':1},100,function(){
            <?php if($this->input->get('content') != 'inload'){?>
              $('.socmed,.socmedmobile,.headerhome').animate({'opacity':1},1000).removeClass('fadeIn');
              <?php }?>
              $('.coklat1').animate({'opacity': 1},100,function(){
                setTimeout(function(){
                  $('.nanas1').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
                  $('.nanas2').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
                },300);
              }).addClass('fadeInUp');
              $('.coklat2').animate({'opacity': 1},100,function(){
                $('.coklat3').animate({'opacity': 1},100,function(){
                  $('.socmed').animate({'opacity':1},1000).removeClass('fadeIn');
                  setTimeout(function(){
                    $('.contentterms').animate({'opacity' : 1},1000,function(){
                      
                    }).addClass('fadeInUp');
                  },1000);
                }).addClass('fadeInUp');
              }).addClass('fadeInUp');
            }).addClass('fadeInUp');
          }).addClass('fadeInUp').prev('.copyright').delay(1000).animate({opacity:1},1000);
        });
        </script>
      </div>
      <?php
      if($this->input->get('content') != 'inload'){
        ?>
      </div>
      <div class="position-absolute w-100 d-none d-md-block" style="bottom:0;left:70%">
        <img src="<?php echo base_url('asset/static/eskrem-bawah.png');?>" alt="resmi 1" style="width:35vw;">
      </div>
    </section>
    <div class="copyright absolute animated opacity0">
      <div class="footermenu">
        <a href="#contentsection" data-href="<?php echo base_url('home/terms');?>" data-url="home/terms" data-title="terms of use Joyday" class="firstmenu menufoot"><span class="en">Terms of Use</span><span class="id">Syarat &amp; Ketentuan</span></a>
        <a href="#contentsection" data-href="<?php echo base_url('home/privacy');?>" data-url="home/privacy" data-title="Privacy Policy Joyday" class="firstmenu menufoot"><span class="en">Privacy Policy</span><span class="id">Kebijakan Privasi</span></a>
        <a href="#contentsection" data-href="<?php echo base_url('home/contact');?>" data-url="home/contact" data-title="Contact Joyday" class="firstmenu menufoot"><span class="en">Contact Us</span><span class="id">Hubungi Kami</span></a>
      </div>
    </div>
    <div class="imgfooter imgfooter1 opacity0 animated">
      <img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer.png')?>" alt="shadowyellow"/>
    </div>
    <div class="imgfooter imgfooter2 opacity0 animated">
      <img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer2.png')?>" alt="shadowyellow"/>
    </div>
    <div class="imgfooter imgfooter3 opacity0 animated">
      <img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer3.png')?>" alt="shadowyellow"/>
    </div>
  <?php }?>
