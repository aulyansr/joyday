<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<style type="text/css">
.coklat1{
	position: absolute;
    height: 180px;
    top: 12vh;
    right:3vw;
    z-index:10;
}
.coklat2{
	position: absolute;
    height: 180px;
    bottom: 3vh;
    right:1vw;
    z-index:5;
}
.coklat3{
	position: absolute;
    right: 0;
    bottom:20vh;
    width:200px;
}
.titlepage{
	font-size: 2rem;
	text-transform: uppercase;
	text-align: center;
	margin-bottom: 2rem;
	color:#0060af;
}
.paper p{
	text-transform: uppercase;
	letter-spacing: .2em;
	color:#0060af;
	font-weight: 300;
	font-size: .7em;
	line-height: 2;
	margin-bottom:1em;
}
p{
	text-align: justify;
}
p::first-letter,p .big{
	font-size: 1.5em;
	line-height: 1;
}
strong{
	color:#0060af;
}
.wrappadding > div.en,.wrappadding > div.id{
	width:100%;
}
@media (max-width: 769px){
	.paper{
		margin: 0 0px;
	}
	.coklat1,.coklat2{
		right:-6vw;
	}
	.paper > .wrappadding{
		min-height: 75vh;
	}
	.nanas8 {
	    height: 80px;
	    top: 18vh;
	    left: 20vw;
	}
}
@media (max-width: 740px)
{
	.coklat1,.coklat2{
		right:0vw;
		max-height: 100px;
	}
	.coklat3 {
	    max-width: 120px;
	}
	.nanas1{
		max-height: 100px;
	}
	.nanas2{
		max-height: 100px;
	}
	.nanas2{
		max-height: 120px;
		bottom: 10vh;
	}
	.contentterms{
		height: 70vh;
	}
}@media (max-width: 500px)
{
	p{
		text-align: left;
	}
	.contentterms{
		height: 80vh;
	}
	.paper > .wrappadding{
		padding: 1.5rem 1.5rem 4rem 1.5rem;
	}
	.titlepage{
		font-size:1.3rem;
		margin-bottom: 1rem;
	}
	.coklat2{
		bottom:10vh;
	}
	.coklat1, .coklat2{
		right: -7vw;
	}
	.coklat3{
		bottom: 30vh;
	}
}
</style>
<?php
		if($this->input->get('content') != 'inload'){
	?>
	<section id="contentsection" class="contentsection">
		<div id="load" class="contenthomeslide contenview">
	<?php }?>
			<div id="wrappingshow">
				<div class="nanas">
					<img class="img-fluid nanas1 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas1.png');?>">
					<img class="img-fluid nanas2 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas2.png');?>">
					<img class="img-fluid nanas8 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas6.png');?>">
					<img class="img-fluid coklat1 naanimation animated opacity0" src="<?php echo base_url('asset/static/coklat/coklat1.png');?>">
					<img class="img-fluid nanas5 naanimation animated opacity0" src="<?php echo base_url('asset/static/nanas/nanas5.png');?>">
					<img class="img-fluid coklat2 naanimation animated opacity0" src="<?php echo base_url('asset/static/coklat/coklat2.png');?>">
					<img class="img-fluid coklat3 naanimation animated opacity0" src="<?php echo base_url('asset/static/coklat/coklat3.png');?>">
					<div class="contentterms animated opacity0">
						<div class="contentterm height100percent">
							<div class="container height100percent container1000">
								<div class="row height100percent">
									<div  class="col-md-12 height100percent ">
										<div ss-container class="paper ss-container">
											<div class="wrappadding">
												<div class="wrp">
													<h1 class="titlepage"><span class="en">Contact Us</span><span class="id">Hubungi Kami</span></h1>
													<div class="row">
														<div class="col-lg-5 col-md-12 col-sm-12">
															<p class="text-lg-left text-center"><strong style="color:#38761D;">PT. Green Asia Food Indonesia</strong><br/><br/>
															Perwata Tower Lantai 6 Suite A<br/>
															Jl. Pluit Selatan Raya Kav.1,<br/>Penjaringan, Jakarta Utara<br/>DKI Jakarta 14440
															Indonesia<br/>
															<br/>
															<strong>Tel</strong> : +6221 30027643<br/>
															<strong>Fax</strong> :  +6221 30027642<br/>
															<strong>Hotline</strong> : 0800 1 868686<br/>
														</div>
														<div class="col-lg-7 col-md-12 col-sm-12">
															<div class="map mapsfooter" id="map_canvas" style="height:350px;"></div>
															<script>
															  function initmap()
															  {
															    var locations = [
															    ['<h5>PT. Green Asia Food Indonesia<br/></h5><p>Perwata Tower Lantai 6 Suite A <br>Jl. Pluit Selatan Raya Kav.1 Penjaringan, Jakarta Utara DKI Jakarta 14440',-6.126570, 106.788753]
															    ];
															    var infowindow = new google.maps.InfoWindow();

															    var options = {
															      zoom: 16,
															      center: new google.maps.LatLng(-6.126570, 106.788753),
															      mapTypeId: google.maps.MapTypeId.ROADMAP
															    };

															     // Pembuatan petanya
															    var map = new google.maps.Map(document.getElementById('map_canvas'), options);
															    var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
															    var marker, i;
															    // proses penambahan marker pada masing-masing lokasi yang berbeda
															    for (i = 0; i < locations.length; i++) {
															        marker = new google.maps.Marker({
															        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
															        map: map,
															        //icon: '',
															    });

															      // Menampilkan informasi pada masing-masing marker yang diklik
															      google.maps.event.addListener(marker, 'click', (function(marker, i) {
															        return function() {
															          infowindow.setContent(locations[i][0]);
															          infowindow.open(map, marker);
															        }
															      })(marker, i));
															    }
															  }
															</script>
															<script src="//maps.google.com/maps/api/js?key=AIzaSyBuNng4Tih2F5VwhbQXMcq9CwmcEnoxQjw&callback=initmap"></script>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<script type="text/javascript">
					$(document).ready(function(){
						$('.imgfooter').animate({'opacity':1, 'bottom' : 0},100,function(){
							$('.nanas8').animate({'opacity':1},100,function(){
								<?php if($this->input->get('content') != 'inload'){?>
									$('.socmed,.socmedmobile,.headerhome').animate({'opacity':1},1000).removeClass('fadeIn');
								<?php }?>
								$('.coklat1').animate({'opacity': 1},100,function(){
									setTimeout(function(){
										$('.nanas1').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
										$('.nanas2').animate({'opacity':1},100,function(){}).addClass('fadeInUp');
									},300);
								}).addClass('fadeInUp');
								$('.coklat2').animate({'opacity': 1},100,function(){
									$('.coklat3').animate({'opacity': 1},100,function(){
										$('.socmed').animate({'opacity':1},1000).removeClass('fadeIn');
										setTimeout(function(){
											$('.contentterms').animate({'opacity' : 1},1000,function(){

											}).addClass('fadeInUp');
										},1000);
									}).addClass('fadeInUp');
								}).addClass('fadeInUp');
							}).addClass('fadeInUp');
						}).addClass('fadeInUp').prev('.copyright').delay(1000).animate({opacity:1},1000);
					});
				</script>
			</div>
<?php
		if($this->input->get('content') != 'inload'){
	?>
			</div>
		</section>
		<div class="copyright absolute animated opacity0">
			<div class="footermenu">
				<a href="#contentsection" data-href="<?php echo base_url('home/terms');?>" data-url="home/terms" data-title="terms of use Joyday" class="firstmenu menufoot"><span class="en">Terms of Use</span><span class="id">Syarat &amp; Ketentuan</span></a>
				<a href="#contentsection" data-href="<?php echo base_url('home/privacy');?>" data-url="home/privacy" data-title="Privacy Policy Joyday" class="firstmenu menufoot"><span class="en">Privacy Policy</span><span class="id">Kebijakan Privasi</span></a>
				<a href="#contentsection" data-href="<?php echo base_url('home/contact');?>" data-url="home/contact" data-title="Contact Joyday" class="firstmenu menufoot"><span class="en">Contact Us</span><span class="id">Hubungi Kami</span></a>
			</div>
		</div>
		<div class="imgfooter imgfooter1 opacity0 animated">
			<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer.png')?>" alt="shadowyellow"/>
		</div>
		<div class="imgfooter imgfooter2 opacity0 animated">
			<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer2.png')?>" alt="shadowyellow"/>
		</div>
		<div class="imgfooter imgfooter3 opacity0 animated">
			<img class="img-fluid img100percent" src="<?php echo GassetURL('asset/static/footer3.png')?>" alt="shadowyellow"/>
		</div>
	<?php }?>
