SitemapGenerator::Sitemap.default_host = "https://joyday.com/"
SitemapGenerator::Sitemap.create_index = true
SitemapGenerator::Sitemap.create do
  {en: :english, id: :indonesia}.each_pair do |locale, name|
  group(:sitemaps_path => "sitemaps/#{locale}/", :filename => name) do
    add root_path(locale: locale), :changefreq => 'daily'
    add contact_us_path(locale: locale)
    add discoverourproduct_path(locale: locale)
    add simulasikeuntungan_path(locale: locale)
    add crt_beaseller_path(locale: locale)
    add crt_requestsample_path(locale: locale)
    add list_product_path(locale: locale)
    add gallery_path(locale: locale)

    Product.all.each do |x|
      add dtl_product_path(x.product_category.slug,x.slug, locale:locale), :changefreq => 'weekly', :lastmod => x.updated_at
    end
    Article.all.each do |x|
      add dtl_blog_path(x.slug, locale:locale), :changefreq => 'weekly', :lastmod => x.updated_at
    end
  end
end
end
