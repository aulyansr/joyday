Rails.application.routes.draw do



  scope "(:locale)", locale: /id|en/ do
    resources :ecommerce_products
    resources :ecommerces
    resources :discoverproducts
    resources :request_samples
    root 'pages#home'
    get 'app' => "app#index"
    get 'dashboard' => "app#index"

    get 'contact-us', to: 'pages#contact', as: :contact_us
    get 'home/discoverourproduct', to: 'pages#discoverourproduct', as: :discoverourproduct
    get 'discoverourproduct', to: 'pages#discoverourproduct'
    resources :users
    resources :setups
    resources :stores do
      collection { post :import }
    end
    resources :beasellers do
      collection { post :import }
    end
    resources :image_events
    resources :events
    resources :articles
    resources :image_products
    resources :product_categories
    resources :products
    devise_for :users,
    path:'user',
    path_names: {
      sign_in: 'login',
      sign_out: 'logout'
    }
     get 'simulasi-keuntungan', to: 'pages#simulasikeuntungan', as: :simulasikeuntungan
     get 'beaseller', to: 'pages#beaseller', as: :crt_beaseller
     get 'requestsample', to: 'pages#requestsample', as: :crt_requestsample
     get 'list-page', to: 'pages#index', as: :list_pages
     get 'store-location', to: 'pages#storelocation', as: :storelocation

     get '/product', to: 'pages#list_product', as: :list_product
     get 'product/:id', to: 'product_categories#show', as: :dtl_product_category
     get 'product/:category_name/:id/', to: 'products#show', as: :dtl_product

     get '/thankspage', to: 'pages#thankspage', as: :thankspage
     get '/news-and-event', to: 'pages#list_blog', as: :gallery
     get '/blog', to: 'pages#list_blog', as: :list_blog
     get 'blog/:id', to: 'articles#show', as: :dtl_blog
     get '/event', to: 'pages#list_event', as: :list_event
     get '/event/:id', to: 'events#show', as: :dtl_event

     resources :pages, path: ''
  end


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
