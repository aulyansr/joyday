# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_08_19_000830) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "articles", force: :cascade do |t|
    t.string "title_id"
    t.string "meta_description_id"
    t.string "meta_description_en"
    t.string "title_en"
    t.text "content_id"
    t.text "content_en"
    t.string "featured_image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status"
    t.date "publish"
    t.string "slug"
    t.string "author"
  end

  create_table "beasellers", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "phone_number"
    t.text "address"
    t.string "province"
    t.string "city"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "discoverproducts", force: :cascade do |t|
    t.string "title_id"
    t.string "title_en"
    t.text "content_id"
    t.text "content_en"
    t.string "tp_1_image_1"
    t.string "tp_1_image_2"
    t.string "tp_1_image_3"
    t.string "tp_1_image_4"
    t.string "tp_1_image_5"
    t.string "tp_image_6"
    t.string "tp_2_image_1"
    t.string "tp_2_image_3"
    t.string "tp_3_image_3"
    t.string "tp_3_image_4"
    t.string "full_image"
    t.string "link"
    t.string "custom"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "background"
    t.string "main"
    t.string "slider_type"
    t.string "templatestatus"
    t.text "custom_code"
    t.string "full_image_mobile"
  end

  create_table "districts", id: false, force: :cascade do |t|
    t.integer "code", null: false
    t.integer "regency_code", null: false
    t.string "name", null: false
    t.index ["regency_code"], name: "index_districts_on_regency_code"
  end

  create_table "ecommerce_products", force: :cascade do |t|
    t.bigint "ecommerce_id"
    t.bigint "product_id"
    t.string "link"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ecommerce_id"], name: "index_ecommerce_products_on_ecommerce_id"
    t.index ["product_id"], name: "index_ecommerce_products_on_product_id"
  end

  create_table "ecommerces", force: :cascade do |t|
    t.string "title"
    t.string "logo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "events", force: :cascade do |t|
    t.string "title_id"
    t.string "meta_description_id"
    t.string "meta_description_en"
    t.string "title_en"
    t.text "content_id"
    t.text "content_en"
    t.string "featured_image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.date "publish"
    t.string "status"
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_type", "sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_type_and_sluggable_id"
  end

  create_table "image_events", force: :cascade do |t|
    t.string "image"
    t.bigint "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_image_events_on_event_id"
  end

  create_table "image_products", force: :cascade do |t|
    t.string "image"
    t.bigint "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_image_products_on_product_id"
  end

  create_table "pages", force: :cascade do |t|
    t.string "title_id"
    t.string "title_en"
    t.text "content_id"
    t.text "content_en"
    t.string "meta_description_id"
    t.string "meta_description_en"
    t.string "background"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
  end

  create_table "product_categories", force: :cascade do |t|
    t.string "title"
    t.text "description_en"
    t.text "description_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
  end

  create_table "products", force: :cascade do |t|
    t.string "title_en"
    t.text "content_id"
    t.text "content_en"
    t.string "featured_image"
    t.string "tokopedia"
    t.string "bukalapak"
    t.string "shopee"
    t.string "sku"
    t.string "netto"
    t.bigint "product_category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.string "jdid"
    t.index ["product_category_id"], name: "index_products_on_product_category_id"
  end

  create_table "provinces", id: false, force: :cascade do |t|
    t.integer "code", null: false
    t.string "name", null: false
  end

  create_table "regencies", id: false, force: :cascade do |t|
    t.integer "code", null: false
    t.integer "province_code", null: false
    t.string "name", null: false
    t.index ["province_code"], name: "index_regencies_on_province_code"
  end

  create_table "request_samples", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "phone_number"
    t.text "address"
    t.string "province"
    t.string "city"
    t.string "variant"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "setups", force: :cascade do |t|
    t.string "site_name"
    t.string "site_title"
    t.text "site_description"
    t.string "site_keywords"
    t.string "site_url"
    t.string "site_image"
    t.string "site_favicon"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "facebook"
    t.string "instagram"
    t.string "twitter"
    t.string "phone"
    t.text "contact"
    t.text "google_map"
    t.string "image_share"
    t.text "custom_code"
    t.string "hero_image"
    t.string "hero_status"
  end

  create_table "users", force: :cascade do |t|
    t.string "username"
    t.string "name"
    t.string "slug"
    t.string "role"
    t.string "gender"
    t.text "bio"
    t.string "avatar"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["slug"], name: "index_users_on_slug", unique: true
  end

  add_foreign_key "ecommerce_products", "ecommerces"
  add_foreign_key "ecommerce_products", "products"
  add_foreign_key "image_events", "events"
  add_foreign_key "image_products", "products"
  add_foreign_key "products", "product_categories"
end
