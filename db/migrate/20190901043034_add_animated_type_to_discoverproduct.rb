class AddAnimatedTypeToDiscoverproduct < ActiveRecord::Migration[5.2]
  def change
    add_column :discoverproducts, :animated_type, :string
  end
end
