class AddBackgroundToSetup < ActiveRecord::Migration[5.2]
  def change
    add_column :setups, :background, :string
  end
end
