class AddSlugToProductCategory < ActiveRecord::Migration[5.2]
  def change
    add_column :product_categories, :slug, :string
  end
end
