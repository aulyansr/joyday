class AddCustomCodeToSetup < ActiveRecord::Migration[5.2]
  def change
    add_column :setups, :custom_code, :text
    add_column :setups, :hero_image, :string
    add_column :setups, :hero_status, :string
  end
end
