class CreateEcommerceProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :ecommerce_products do |t|
      t.references :ecommerce, foreign_key: true
      t.references :product, foreign_key: true
      t.string :link

      t.timestamps
    end
  end
end
