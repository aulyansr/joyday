class AddSliderTypeToDiscoverproducts < ActiveRecord::Migration[5.2]
  def change
    add_column :discoverproducts, :slider_type, :string
  end
end
