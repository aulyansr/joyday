class AddAuthorToEvent < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :author, :string
  end
end
