class AddCustomCodeToDiscoverproduct < ActiveRecord::Migration[5.2]
  def change
    add_column :discoverproducts, :custom_code, :text
  end
end
