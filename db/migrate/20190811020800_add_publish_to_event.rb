class AddPublishToEvent < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :publish, :date
  end
end
