class AddTranslateFullImageAnimatedToDiscoverproduct < ActiveRecord::Migration[5.2]
  def change
    add_column :discoverproducts, :main_en, :string
    add_column :discoverproducts, :background_en, :string
  end
end
