class AddImageMobileToDiscoverproduct < ActiveRecord::Migration[5.2]
  def change
    add_column :discoverproducts, :full_image_mobile, :string
  end
end
