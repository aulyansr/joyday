class AddSocialMediaToSetup < ActiveRecord::Migration[5.2]
  def change
    add_column :setups, :facebook, :string
    add_column :setups, :instagram, :string
    add_column :setups, :twitter, :string
    add_column :setups, :phone, :string
  end
end
