class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :title_en
      t.text :content_id
      t.text :content_en
      t.string :featured_image
      t.string :tokopedia
      t.string :bukalapak
      t.string :shopee
      t.string :sku
      t.string :netto
      t.references :product_category, foreign_key: true

      t.timestamps
    end
  end
end
