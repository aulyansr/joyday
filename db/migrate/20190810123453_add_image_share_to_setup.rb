class AddImageShareToSetup < ActiveRecord::Migration[5.2]
  def change
    add_column :setups, :image_share, :string
  end
end
