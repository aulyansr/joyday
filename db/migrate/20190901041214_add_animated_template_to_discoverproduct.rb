class AddAnimatedTemplateToDiscoverproduct < ActiveRecord::Migration[5.2]
  def change
    add_column :discoverproducts, :animated_1, :string
    add_column :discoverproducts, :animated_2, :string
    add_column :discoverproducts, :animated_3, :string
    add_column :discoverproducts, :animated_4, :string
    add_column :discoverproducts, :animated_5, :string
    add_column :discoverproducts, :animated_6, :string
    add_column :discoverproducts, :animated_7, :string
    add_column :discoverproducts, :animated_8, :string
    add_column :discoverproducts, :animated_9, :string
  end
end
