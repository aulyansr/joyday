class CreateBeasellers < ActiveRecord::Migration[5.2]
  def change
    create_table :beasellers do |t|
      t.string :name
      t.string :email
      t.string :phone_number
      t.text :address
      t.string :province
      t.string :city

      t.timestamps
    end
  end
end
