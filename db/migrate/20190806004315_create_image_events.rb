class CreateImageEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :image_events do |t|
      t.string :image
      t.references :event, foreign_key: true

      t.timestamps
    end
  end
end
