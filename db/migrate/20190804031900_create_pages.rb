class CreatePages < ActiveRecord::Migration[5.2]
  def change
    create_table :pages do |t|
      t.string :title_id
      t.string :title_en
      t.text :content_id
      t.text :content_en
      t.string :meta_description_id
      t.string :meta_description_en
      t.string :background
      t.string :status

      t.timestamps
    end
  end
end
