class AddDetailContatToSetup < ActiveRecord::Migration[5.2]
  def change
    add_column :setups, :contact, :text
    add_column :setups, :google_map, :text
  end
end
