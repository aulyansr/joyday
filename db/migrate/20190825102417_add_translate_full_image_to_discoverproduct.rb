class AddTranslateFullImageToDiscoverproduct < ActiveRecord::Migration[5.2]
  def change
    add_column :discoverproducts, :full_image_en, :string
    add_column :discoverproducts, :full_image_mobile_id, :string
  end
end
