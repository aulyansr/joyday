class CreateDiscoverproducts < ActiveRecord::Migration[5.2]
  def change
    create_table :discoverproducts do |t|
      t.string :title_id
      t.string :title_en
      t.text :content_id
      t.text :content_en
      t.string :tp_1_image_1
      t.string :tp_1_image_2
      t.string :tp_1_image_3
      t.string :tp_1_image_4
      t.string :tp_1_image_5
      t.string :tp_image_6
      t.string :tp_2_image_1
      t.string :tp_2_image_3
      t.string :tp_3_image_3
      t.string :tp_3_image_4
      t.string :full_image
      t.string :link
      t.string :custom

      t.timestamps
    end
  end
end
