class AddHideTitleToDiscoverproduct < ActiveRecord::Migration[5.2]
  def change
    add_column :discoverproducts, :hide_title, :boolean
  end
end
