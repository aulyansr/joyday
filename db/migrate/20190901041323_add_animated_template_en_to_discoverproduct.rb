class AddAnimatedTemplateEnToDiscoverproduct < ActiveRecord::Migration[5.2]
  def change
    add_column :discoverproducts, :animated_1_en, :string
    add_column :discoverproducts, :animated_2_en, :string
    add_column :discoverproducts, :animated_3_en, :string
    add_column :discoverproducts, :animated_4_en, :string
    add_column :discoverproducts, :animated_5_en, :string
    add_column :discoverproducts, :animated_6_en, :string
    add_column :discoverproducts, :animated_7_en, :string
    add_column :discoverproducts, :animated_8_en, :string
    add_column :discoverproducts, :animated_9_en, :string
  end
end
