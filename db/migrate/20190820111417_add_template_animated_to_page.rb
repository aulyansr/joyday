class AddTemplateAnimatedToPage < ActiveRecord::Migration[5.2]
  def change
    add_column :pages, :main_image, :string
    add_column :pages, :effek_1, :string
    add_column :pages, :effek_2, :string
    add_column :pages, :effek_3, :string
    add_column :pages, :effek_4, :string
  end
end
