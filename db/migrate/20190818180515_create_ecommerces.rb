class CreateEcommerces < ActiveRecord::Migration[5.2]
  def change
    create_table :ecommerces do |t|
      t.string :title
      t.string :logo

      t.timestamps
    end
  end
end
