class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :title_id
      t.string :meta_description_id
      t.string :meta_description_en
      t.string :title_en
      t.text :content_id
      t.text :content_en
      t.string :featured_image

      t.timestamps
    end
  end
end
