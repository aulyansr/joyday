class CreateStores < ActiveRecord::Migration[5.2]
  def change
    create_table :stores do |t|
      t.string :name
      t.text :area
      t.string :address
      t.string :lat
      t.string :long

      t.timestamps
    end
  end
end
