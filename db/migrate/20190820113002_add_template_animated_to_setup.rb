class AddTemplateAnimatedToSetup < ActiveRecord::Migration[5.2]
  def change
    add_column :setups, :main_image, :string
    add_column :setups, :effek_1, :string
    add_column :setups, :effek_2, :string
    add_column :setups, :effek_3, :string
    add_column :setups, :effek_4, :string
  end
end
