class AddTemplateStatusToDiscoverproduct < ActiveRecord::Migration[5.2]
  def change
    add_column :discoverproducts, :templatestatus, :string
  end
end
