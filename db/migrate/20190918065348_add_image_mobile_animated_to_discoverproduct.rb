class AddImageMobileAnimatedToDiscoverproduct < ActiveRecord::Migration[5.2]
  def change
    add_column :discoverproducts, :main_mbl, :string
    add_column :discoverproducts, :background_mbl, :string
    add_column :discoverproducts, :effect_1_mbl, :string
    add_column :discoverproducts, :effect_2_mbl, :string
    add_column :discoverproducts, :effect_3_mbl, :string
    add_column :discoverproducts, :effect_4_mbl, :string
    add_column :discoverproducts, :main_mbl_en, :string
    add_column :discoverproducts, :background_mbl_en, :string
    add_column :discoverproducts, :effect_1_en_mbl, :string
    add_column :discoverproducts, :effect_2_en_mbl, :string
    add_column :discoverproducts, :effect_3_en_mbl, :string
    add_column :discoverproducts, :effect_4_en_mbl, :string
  end
end
