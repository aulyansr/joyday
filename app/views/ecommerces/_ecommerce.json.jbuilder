json.extract! ecommerce, :id, :title, :logo, :created_at, :updated_at
json.url ecommerce_url(ecommerce, format: :json)
