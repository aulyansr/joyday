json.(@beasellers) do |x|
  json.register_date x.created_at.strftime("%e %B %Y @%H:%M") rescue "-"
  json.name x.name
  json.email x.email
  json.phone_number x.phone_number
  json.address x.address
  json.province x.province
  json.city x.city
end
