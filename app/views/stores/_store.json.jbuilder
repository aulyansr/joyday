json.extract! store, :id, :name, :area, :address, :lat, :long, :created_at, :updated_at
json.url store_url(store, format: :json)
