json.extract! event, :id, :title_id, :meta_description_id, :meta_description_en, :title_en, :content_id, :content_en, :featured_image, :created_at, :updated_at
json.url event_url(event, format: :json)
