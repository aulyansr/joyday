json.extract! ecommerce_product, :id, :ecommerce_id, :product_id, :link, :created_at, :updated_at
json.url ecommerce_product_url(ecommerce_product, format: :json)
