json.extract! discoverproduct, :id, :title_id, :title_en, :content_id, :content_en, :tp_1_image_1, :tp_1_image_2, :tp_1_image_3, :tp_1_image_4, :tp_1_image_5, :tp_image_6, :tp_2_image_1, :tp_2_image_3, :tp_3_image_3, :tp_3_image_4, :full_image, :link, :custom, :created_at, :updated_at
json.url discoverproduct_url(discoverproduct, format: :json)
