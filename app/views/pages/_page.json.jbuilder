json.extract! page, :id, :title_id, :title_en, :content_id, :content_en, :meta_description_id, :meta_description_en, :background, :status, :created_at, :updated_at
json.url page_url(page, format: :json)
