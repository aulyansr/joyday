json.extract! product_category, :id, :title, :description_en, :description_id, :created_at, :updated_at
json.url product_category_url(product_category, format: :json)
