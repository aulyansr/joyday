json.extract! product, :id, :title_en, :content_id, :content_en, :featured_image, :tokopedia, :bukalapak, :shopee, :sku, :netto, :product_category_id, :created_at, :updated_at
json.url product_url(product, format: :json)
