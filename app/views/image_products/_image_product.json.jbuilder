json.extract! image_product, :id, :image, :product_id, :created_at, :updated_at
json.url image_product_url(image_product, format: :json)
