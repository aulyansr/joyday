module ApplicationHelper

  def url_product(x)
    dtl_product_path(x.product_category.slug,x.slug)
  end

  def url_ctr_product(x)
    dtl_product_category_path(x.slug)
  end

  def url_blog(x)
    dtl_blog_path(x.slug)
  end
  def url_event(x)
    dtl_event_path(x.slug)
  end

end
