class Beaseller < ApplicationRecord
  def self.import(file)
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]

      new
      params =
      {beaseller:
        {
          name: row["name"],
          email: row["email"],
          address: row["address"],
          phone_number: row["phone_number"],
          province: row["province"],
          city: row["city"],
          created_at: row["date"],
        }
       }
        Beaseller.create(params[:beaseller])
      end


    end

    def self.open_spreadsheet(file)
      case File.extname(file.original_filename)
      when '.csv' then Roo::CSV.new(file.path,file_warning: :ignore)
      when '.xls' then Roo::Excel.new(file.path,file_warning: :ignore)
      when '.xlsx' then Roo::Excelx.new(file.path,file_warning: :ignore)
      else raise "Unknown file type: #{file.original_filename}"
      end
    end
end
