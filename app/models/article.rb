class Article < ApplicationRecord
  extend FriendlyId
  friendly_id :title_id
  mount_uploader :featured_image, ImageUploader
  has_many :image_events, inverse_of: :article
  accepts_nested_attributes_for :image_events, reject_if: :all_blank, allow_destroy: true
  Status = [ :publish, :draft ]
end
