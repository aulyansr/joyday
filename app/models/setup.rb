class Setup < ApplicationRecord
  mount_uploader :site_image, LogoUploader
  mount_uploader :site_favicon, IconUploader
  mount_uploader :image_share, ImageUploader
  mount_uploader :hero_image, ImageUploader
  mount_uploader :main_image, ImageUploader
  mount_uploader :background, ImageUploader
  mount_uploader :effek_1, ImageUploader
  mount_uploader :effek_2, ImageUploader
  mount_uploader :effek_3, ImageUploader
  mount_uploader :effek_4, ImageUploader
  validates :site_name, presence: true
  validates :site_title, presence: true
  validates :site_url, presence: true

  def remove_image=(val)
    image_will_change! if val
    super
  end
end
