class Page < ApplicationRecord
  extend FriendlyId
  friendly_id :title_en
  mount_uploader :background, ImageUploader
  validates :title_en, presence: true

  def remove_image=(val)
    image_will_change! if val
    super
  end
end
