class Discoverproduct < ApplicationRecord
  mount_uploader :tp_1_image_1, ImageUploader
  mount_uploader :tp_1_image_2, ImageUploader
  mount_uploader :tp_1_image_3, ImageUploader
  mount_uploader :tp_1_image_4, ImageUploader
  mount_uploader :tp_1_image_1_en, ImageUploader
  mount_uploader :tp_1_image_2_en, ImageUploader
  mount_uploader :tp_1_image_3_en, ImageUploader
  mount_uploader :tp_1_image_4_en, ImageUploader
  mount_uploader :full_image, ImageUploader
  mount_uploader :full_image_mobile, ImageUploader
  mount_uploader :background,ImageUploader
  mount_uploader :main,ImageUploader
  mount_uploader :background_en,ImageUploader
  mount_uploader :main_en,ImageUploader
  mount_uploader :animated_1, ImageUploader
  mount_uploader :animated_2, ImageUploader
  mount_uploader :animated_3, ImageUploader
  mount_uploader :animated_4, ImageUploader
  mount_uploader :animated_5, ImageUploader
  mount_uploader :animated_6, ImageUploader
  mount_uploader :animated_7, ImageUploader
  mount_uploader :animated_8, ImageUploader
  mount_uploader :animated_9, ImageUploader
  mount_uploader :main_mbl, ImageUploader
  mount_uploader :background_mbl, ImageUploader
  mount_uploader :effect_1_mbl, ImageUploader
  mount_uploader :effect_2_mbl, ImageUploader
  mount_uploader :effect_3_mbl, ImageUploader
  mount_uploader :effect_4_mbl, ImageUploader
  mount_uploader :main_mbl_en, ImageUploader
  mount_uploader :background_mbl_en, ImageUploader
  mount_uploader :effect_1_en_mbl, ImageUploader
  mount_uploader :effect_2_en_mbl, ImageUploader
  mount_uploader :effect_3_en_mbl, ImageUploader
  mount_uploader :effect_4_en_mbl, ImageUploader 

  mount_uploader :background,ImageUploader

  Templated = ["Simple", "Template 1",  "Template 11", "Template 12" ]


  # animated template 1 nanas 7
  # animated template 2 jujube 5
  # animated template 3 honeydew 5
  # animated template 4 ccv 6
  # animated template 5 ccm 5
  # animated template 6 ccb 4
  # animated template 7 sc 5
  # animated template 8 bb 6
  # animated template 9 cpa 7
  # animated template 10 milky 5
  # animated template 11 mocha 5
  # animated template 12 bean 9


  def remove_image=(val)
    image_will_change! if val
    super
  end

end
