class Product < ApplicationRecord
  include RailsSortable::Model
  set_sortable :index
  extend FriendlyId
  friendly_id :title_en
  mount_uploader :featured_image, ImageUploader
  belongs_to :product_category
  has_many :image_products, inverse_of: :product
  accepts_nested_attributes_for :image_products, reject_if: :all_blank, allow_destroy: true
  has_many :ecommerce_products, inverse_of: :product
  accepts_nested_attributes_for :ecommerce_products

  def remove_image=(val)
    image_will_change! if val
    super
  end
end
