class ProductCategory < ApplicationRecord
  extend FriendlyId
  friendly_id :title

  has_many :products
end
