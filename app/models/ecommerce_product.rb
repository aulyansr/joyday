class EcommerceProduct < ApplicationRecord
  belongs_to :ecommerce
  belongs_to :product
end
