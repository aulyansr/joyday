class EcommerceProductsController < ApplicationController
  before_action :set_ecommerce_product, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:show]
  layout 'admin', except: [:show]
  # GET /ecommerce_products
  # GET /ecommerce_products.json
  def index
    @ecommerce_products = EcommerceProduct.all
  end

  # GET /ecommerce_products/1
  # GET /ecommerce_products/1.json
  def show
  end

  # GET /ecommerce_products/new
  def new
    @ecommerce_product = EcommerceProduct.new
  end

  # GET /ecommerce_products/1/edit
  def edit
  end

  # POST /ecommerce_products
  # POST /ecommerce_products.json
  def create
    @ecommerce_product = EcommerceProduct.new(ecommerce_product_params)

    respond_to do |format|
      if @ecommerce_product.save
        format.html { redirect_to @ecommerce_product, notice: 'Ecommerce product was successfully created.' }
        format.json { render :show, status: :created, location: @ecommerce_product }
      else
        format.html { render :new }
        format.json { render json: @ecommerce_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ecommerce_products/1
  # PATCH/PUT /ecommerce_products/1.json
  def update
    respond_to do |format|
      if @ecommerce_product.update(ecommerce_product_params)
        format.html { redirect_to @ecommerce_product, notice: 'Ecommerce product was successfully updated.' }
        format.json { render :show, status: :ok, location: @ecommerce_product }
      else
        format.html { render :edit }
        format.json { render json: @ecommerce_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ecommerce_products/1
  # DELETE /ecommerce_products/1.json
  def destroy
    @ecommerce_product.destroy
    respond_to do |format|
      format.html { redirect_to ecommerce_products_url, notice: 'Ecommerce product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ecommerce_product
      @ecommerce_product = EcommerceProduct.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ecommerce_product_params
      params.require(:ecommerce_product).permit(:ecommerce_id, :product_id, :link)
    end
end
