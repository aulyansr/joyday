class AppController < ApplicationController
  before_action :authenticate_user!
  layout 'admin'
  def index
    @setup = Setup.first
  end
end
