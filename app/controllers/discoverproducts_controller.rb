class DiscoverproductsController < ApplicationController
  before_action :set_discoverproduct, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  layout 'admin'
  # GET /discoverproducts
  # GET /discoverproducts.json
  def index
    @discoverproducts = Discoverproduct.all
  end

  # GET /discoverproducts/1
  # GET /discoverproducts/1.json
  def show
  end

  # GET /discoverproducts/new
  def new
    @discoverproduct = Discoverproduct.new
  end

  # GET /discoverproducts/1/edit
  def edit
  end

  # POST /discoverproducts
  # POST /discoverproducts.json
  def create
    @discoverproduct = Discoverproduct.new(discoverproduct_params)

    respond_to do |format|
      if @discoverproduct.save
        format.html { redirect_to discoverourproduct_path, notice: 'Discoverproduct was successfully created.' }
        format.json { render :show, status: :created, location: @discoverproduct }
      else
        format.html { render :new }
        format.json { render json: @discoverproduct.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /discoverproducts/1
  # PATCH/PUT /discoverproducts/1.json
  def update
    respond_to do |format|
      if @discoverproduct.update(discoverproduct_params)
        format.html { redirect_to discoverourproduct_path, notice: 'Discoverproduct was successfully updated.' }
        format.json { render :show, status: :ok, location: @discoverproduct }
      else
        format.html { render :edit }
        format.json { render json: @discoverproduct.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /discoverproducts/1
  # DELETE /discoverproducts/1.json
  def destroy
    @discoverproduct.destroy
    respond_to do |format|
      format.html { redirect_to discoverproducts_url, notice: 'Discoverproduct was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_discoverproduct
      @discoverproduct = Discoverproduct.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def discoverproduct_params
      params.require(:discoverproduct).permit(:title_id, :title_en, :content_id, :content_en, :tp_1_image_1, :tp_1_image_2, :tp_1_image_3, :tp_1_image_4,:tp_1_image_1_en, :tp_1_image_2_en, :tp_1_image_3_en, :tp_1_image_4_en, :full_image, :full_image_mobile,  :full_image_en, :full_image_mobile_en, :link, :custom, :main, :background,:main_en, :background_en, :slider_type, :custom_code, :remove_main,:remove_main_en, :remove_background_en,:remove_tp_1_image_1, :remove_tp_1_image_2, :remove_tp_1_image_3, :remove_tp_1_image_4, :remove_tp_1_image_1_en, :remove_tp_1_image_2_en, :remove_tp_1_image_3_en, :remove_tp_1_image_4_en, :hide_title, :main_mbl, :background_mbl, :effect_1_mbl, :effect_2_mbl, :effect_3_mbl, :effect_4_mbl, :main_mbl_en, :background_mbl_en, :effect_1_en_mbl, :effect_2_en_mbl, :effect_3_en_mbl, :effect_4_en_mbl,  :animated_1,:animated_2,:animated_3,:animated_4,:animated_5,:animated_6,:animated_7,:animated_8,:animated_9,:main_mbl, :remove_background_mbl, :remove_effect_1_mbl, :remove_effect_2_mbl, :remove_effect_3_mbl, :remove_effect_4_mbl, :remove_main_mbl_en, :remove_background_mbl_en, :remove_effect_1_en_mbl, :remove_effect_2_en_mbl, :remove_effect_3_en_mbl, :remove_effect_4_en_mbl,  :remove_animated_1,:remove_animated_2,:remove_animated_3,:remove_animated_4,:remove_animated_5,:remove_animated_6,:remove_animated_7,:remove_animated_8,:remove_animated_9, :animated_type)
    end
end
