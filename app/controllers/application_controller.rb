class ApplicationController < ActionController::Base
  before_action :set_locale
  before_action :prepare_meta_tags
  before_action :detect_device_variant
  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def default_url_options
    { locale: I18n.locale }
  end

  private

  def detect_device_variant
    request.variant = :phone if browser.device.mobile?
    request.variant = :tablet if browser.device.tablet?
  end

    def prepare_meta_tags(options={})
    site_name   = Setup.first.site_name
    title       = Setup.first.site_title
    description = Setup.first.site_description.to_s
    image       = "http://joyday.com#{Setup.first.image_share}"
    current_url = "http://joyday.com"

    # Let's prepare a nice set of defaults
    defaults = {
      site:        site_name,
      title:       title,
      image:       image,
      description: description,
      reverse: true,
      twitter: {
        site_name: site_name,
        site: '@JoydayIceCream',
        creator: '@JoydayIceCream',
        card: 'summary_large_image',
        description: description,
        title: title,
        image: image
      },
      og: {
        url: current_url,
        site_name: site_name,
        title: title,
        image: image,
        description: description,
        type: 'website'
      }
    }

    options.reverse_merge!(defaults)

    set_meta_tags options

  end




end
