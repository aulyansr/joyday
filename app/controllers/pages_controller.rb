class PagesController < ApplicationController
  before_action :set_page, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:index, :new, :edit]
  layout 'admin', only: [:index, :new, :edit]
  # GET /pages
  # GET /pages.json
  def index
    @pages = Page.all

  end

  # GET /pages/1
  # GET /pages/1.json
  def show
    prepare_meta_tags(
      title: @page.title_id,
      description: @page.meta_description_id,
      twitter:{description:@page.meta_description_id},
      og:{description:@page.meta_description_id}
    )
  end

  # GET /pages/new
  def new
    @page = Page.new
  end

  # GET /pages/1/edit
  def edit
      @setup = Setup.first
  end

  # POST /pages
  # POST /pages.json
  def create
    @page = Page.new(page_params)

    respond_to do |format|
      if @page.save
        format.html { redirect_to pages_path, notice: 'Page was successfully created.' }
        format.json { render :show, status: :created, location: @page }
      else
        format.html { render :new }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pages/1
  # PATCH/PUT /pages/1.json
  def update
    respond_to do |format|
      if @page.update(page_params)
        format.html { redirect_to pages_path, notice: 'Page was successfully updated.' }
        format.json { render :show, status: :ok, location: @page }
      else
        format.html { render :edit }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pages/1
  # DELETE /pages/1.json
  def destroy
    @page.destroy
    respond_to do |format|
      format.html { redirect_to pages_url, notice: 'Page was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def discoverourproduct
    @discoverproducts = Discoverproduct.all
  end

  def home
    #code
  end

  def product
    setup = Page.where(slug: "list-product").first
    prepare_meta_tags(
    title: setup.title_id,
    description:setup.content_id,
    url: request.original_url,
    twitter: {
      site_name: "JOYDAY | #{setup.title_id}" ,
      description:   setup.meta_description_id,
      title: setup.title_id,
      url: request.original_url
    },
    og: {
      url: request.original_url,
      site_name: "JOYDAY | #{setup.title_id}" ,
      title: setup.title_id,
      type: 'article',
      description:   setup.meta_description_id,
      })
  end

  def discoverourproduct
    setup = Page.where(slug: "discover-product").first
    prepare_meta_tags(
    title: setup.title_id,
    description:setup.content_id,
    url: request.original_url,
    twitter: {
      site_name: "JOYDAY | #{setup.title_id}" ,
      description:   setup.meta_description_id,
      title: setup.title_id,
      url: request.original_url
    },
    og: {
      url: request.original_url,
      site_name: "JOYDAY | #{setup.title_id}" ,
      title: setup.title_id,
      type: 'article',
      description:   setup.meta_description_id,
      })
  end

  def list_blog
    setup = Page.where(slug: "list-blog").first
    prepare_meta_tags(
    title: setup.title_id,
    description:setup.content_id,
    url: request.original_url,
    twitter: {
      site_name: "JOYDAY | #{setup.title_id}" ,
      description:   setup.meta_description_id,
      title: setup.title_id,
      url: request.original_url
    },
    og: {
      url: request.original_url,
      site_name: "JOYDAY | #{setup.title_id}" ,
      title: setup.title_id,
      type: 'article',
      description:   setup.meta_description_id,
      })
    @articles = Article.where(status:"publish").order(publish: :desc)
  end

  def list_event
      @events = Event.where(status:"publish").order(publish: :desc)
  end

  def beaseller
    setup = Page.where(slug: "be-a-seller").first
    prepare_meta_tags(
    title: setup.title_id,
    description:setup.content_id,
    url: request.original_url,
    twitter: {
      site_name: "JOYDAY | #{setup.title_id}" ,
      description:   setup.meta_description_id,
      title: setup.title_id,
      url: request.original_url
    },
    og: {
      url: request.original_url,
      site_name: "JOYDAY | #{setup.title_id}" ,
      title: setup.title_id,
      type: 'article',
      description:   setup.meta_description_id,
      })
    @beaseller = Beaseller.new
  end

  def requestsample
    setup = Page.where(slug: "request-sample").first
    prepare_meta_tags(
    title: setup.title_id,
    description:setup.content_id,
    url: request.original_url,
    twitter: {
      site_name: "JOYDAY | #{setup.title_id}" ,
      description:   setup.meta_description_id,
      title: setup.title_id,
      url: request.original_url
    },
    og: {
      url: request.original_url,
      site_name: "JOYDAY | #{setup.title_id}" ,
      title: setup.title_id,
      type: 'article',
      description:   setup.meta_description_id,
      })
    @request_sample = RequestSample.new
  end

  def simulasikeuntungan

    setup = Page.where(slug: "simulasikeuntungan").first
    prepare_meta_tags(
    title: setup.title_id,
    description:setup.content_id,
    url: request.original_url,
    twitter: {
      site_name: "JOYDAY | #{setup.title_id}" ,
      description:   setup.meta_description_id,
      title: setup.title_id,
      url: request.original_url
    },
    og: {
      url: request.original_url,
      site_name: "JOYDAY | #{setup.title_id}" ,
      title: setup.title_id,
      type: 'article',
      description:   setup.meta_description_id,
      })
  end

  def thankspage
    @gtm = true
  end

  def storelocation
    #code
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_page
      @page = Page.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def page_params
      params.require(:page).permit(:title_id, :title_en, :content_id, :content_en, :meta_description_id, :meta_description_en, :background, :status, :main, :effek_1, :effek_2, :effek_3, :effek_4)
    end
end
