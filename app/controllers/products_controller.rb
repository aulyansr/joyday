class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:show]
  layout 'admin', except: [:show]
  # GET /products
  # GET /products.json
  def index
    @products = Product.order(:index).all
  end

  # GET /products/1
  # GET /products/1.json
  def show
    prepare_meta_tags(
    title: @product.title_en,
    description: ActionView::Base.full_sanitizer.sanitize(@product.content_id).truncate(150),
    url: request.original_url,
    image: request.protocol + request.host_with_port + @product.featured_image.url,
    twitter: {
      site_name: "JOYDAY | #{@product.title_en}" ,
      description:  ActionView::Base.full_sanitizer.sanitize(@product.content_id).truncate(150),
      title: @product.title_en,
      image: request.protocol + request.host_with_port + @product.featured_image.url,
      url: request.original_url
    },
    og: {
      url: request.original_url,
      site_name: "JOYDAY | #{@product.title_en}" ,
      title: @product.title_en,
      type: 'article',
      image: request.protocol + request.host_with_port + @product.featured_image.url,
      description:  ActionView::Base.full_sanitizer.sanitize(@product.content_id).truncate(150),
      })
  end

  # GET /products/new
  def new
    @product = Product.new
    @product.ecommerce_products.build

  end

  # GET /products/1/edit
  def edit
      x = @product.ecommerce_products.count
      if x != Ecommerce.all.count
          (Ecommerce.all.count - x).times do
            @product.ecommerce_products.build
          end
      end
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:title_en, :content_id, :content_en, :featured_image, :tokopedia, :jdid, :shopee, :sku, :netto, :product_category_id, image_products_attributes: [:id, :image, :_destroy], ecommerce_products_attributes: [:id, :ecommerce_id, :link, :_destroy])
    end
end
