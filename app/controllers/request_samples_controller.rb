class RequestSamplesController < ApplicationController
  before_action :set_request_sample, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:create]
  layout 'admin'
  # GET /request_samples
  # GET /request_samples.json
  def index
    @request_samples = RequestSample.all
    if params[:start].present?
      @request_samples = @request_samples.where(created_at: DateTime.parse(params[:start]).beginning_of_day..DateTime.parse(params[:end]).end_of_day)  
    end
  end

  # GET /request_samples/1
  # GET /request_samples/1.json
  def show
  end

  # GET /request_samples/new
  def new
    @request_sample = RequestSample.new
  end

  # GET /request_samples/1/edit
  def edit
  end

  # POST /request_samples
  # POST /request_samples.json
  def create
    @request_sample = RequestSample.new(request_sample_params)

    respond_to do |format|
      if @request_sample.save
        NotifMailer.requestsample_admin(@request_sample).deliver
        NotifMailer.requestsample_user(@request_sample).deliver
        format.html { redirect_to thankspage_path, notice: 'Beaseller was successfully created.' }
        format.json { render :show, status: :created, location: @request_sample }
      else
        format.html { render :new }
        format.json { render json: @request_sample.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /request_samples/1
  # PATCH/PUT /request_samples/1.json
  def update
    respond_to do |format|
      if @request_sample.update(request_sample_params)
        format.html { redirect_to @request_sample, notice: 'Request sample was successfully updated.' }
        format.json { render :show, status: :ok, location: @request_sample }
      else
        format.html { render :edit }
        format.json { render json: @request_sample.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /request_samples/1
  # DELETE /request_samples/1.json
  def destroy
    @request_sample.destroy
    respond_to do |format|
      format.html { redirect_to request_samples_url, notice: 'Request sample was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_request_sample
      @request_sample = RequestSample.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def request_sample_params
      params.require(:request_sample).permit(:name, :email, :phone_number, :address, :province, :city, :variant)
    end
end
