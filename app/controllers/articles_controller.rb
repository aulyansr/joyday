class ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:show]
  layout 'admin', except: [:show]
  # GET /articles
  # GET /articles.json
  def index
    @articles = Article.all.order(created_at: :desc)
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
    prepare_meta_tags(
    title: @article.title_id,
    description:@article.content_id,
    url: request.original_url,
    image: request.protocol + request.host_with_port + @article.featured_image.url,
    twitter: {
      site_name: "JOYDAY | #{@article.title_id}" ,
      description:   @article.meta_description_id,
      title: @article.title_id,
      image: request.protocol + request.host_with_port + @article.featured_image.url,
      url: request.original_url
    },
    og: {
      url: request.original_url,
      site_name: "JOYDAY | #{@article.title_id}" ,
      title: @article.title_id,
      type: 'article',
      image: request.protocol + request.host_with_port + @article.featured_image.url,
      description: @article.meta_description_id,
      }
    )
  end

  # GET /articles/new
  def new
    @article = Article.new
  end

  # GET /articles/1/edit
  def edit
  end

  # POST /articles
  # POST /articles.json
  def create
    @article = Article.new(article_params)

    respond_to do |format|
      if @article.save
        format.html { redirect_to @article, notice: 'Article was successfully created.' }
        format.json { render :show, status: :created, location: @article }
      else
        format.html { render :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to @article, notice: 'Article was successfully updated.' }
        format.json { render :show, status: :ok, location: @article }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    @article.destroy
    respond_to do |format|
      format.html { redirect_to articles_url, notice: 'Article was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:title_id,:author, :meta_description_id, :meta_description_en, :title_en, :content_id, :content_en, :featured_image, :status, :publish, image_events_attributes: [:id, :image, :_destroy, :article_id])
    end
end
