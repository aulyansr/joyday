class BeasellersController < ApplicationController
  before_action :set_beaseller, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:create]
  layout 'admin'
  # GET /beasellers
  # GET /beasellers.json
  def index
    @beasellers = Beaseller.all
    if params[:start].present?
      @beasellers = @beasellers.where(created_at: DateTime.parse(params[:start]).beginning_of_day..DateTime.parse(params[:end]).end_of_day)  
    end
  end

  # GET /beasellers/1
  # GET /beasellers/1.json
  def show
  end

  # GET /beasellers/new
  def new
    @beaseller = Beaseller.new
  end

  # GET /beasellers/1/edit
  def edit
  end

  # POST /beasellers
  # POST /beasellers.json
  def create
    @beaseller = Beaseller.new(beaseller_params)

    respond_to do |format|
      if @beaseller.save
        format.html { redirect_to thankspage_path, notice: 'Beaseller was successfully created.' }
        format.json { render :show, status: :created, location: @beaseller }
        NotifMailer.beaseleer_admin(@beaseller).deliver
        NotifMailer.beaseleer_user(@beaseller).deliver
      else
        format.html { render :new }
        format.json { render json: @beaseller.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /beasellers/1
  # PATCH/PUT /beasellers/1.json
  def update
    respond_to do |format|
      if @beaseller.update(beaseller_params)
        format.html { redirect_to @beaseller, notice: 'Beaseller was successfully updated.' }
        format.json { render :show, status: :ok, location: @beaseller }
      else
        format.html { render :edit }
        format.json { render json: @beaseller.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /beasellers/1
  # DELETE /beasellers/1.json
  def destroy
    @beaseller.destroy
    respond_to do |format|
      format.html { redirect_to beasellers_url, notice: 'Beaseller was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def import
    Beaseller.import(params[:file])
    redirect_to beasellers_url, notice: "Beaseller imported."
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_beaseller
      @beaseller = Beaseller.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def beaseller_params
      params.require(:beaseller).permit(:name, :email, :phone_number, :address, :province, :city)
    end
end
