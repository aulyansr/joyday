class NotifMailer < ApplicationMailer
  default from: "Joyday.com <request@joyday.com>"

  def beaseleer_admin(user)
    @user = user
    mail( :to => "greyadmin@joyday.com",
      :subject => 'Be a Seller',
      :display_name => "Joyday.com",bcc: ['uli.oktaria@greenasiafood.com',' yogi@greenasiafood.com','aulya.nsr@live.com'] )
  end
  def beaseleer_user(user)
    @user = user
    mail( :to => user.email,
      :subject => 'Be a Seller',
      :display_name => "Joyday.com",bcc: ['seller@joyday.com', 'greyadmin@joyday.com', 'aulya.nasir@gmail.com'] )
  end
  def requestsample_admin(user)
    @user = user
    mail( :to => "greyadmin@joyday.com",
      :subject => 'Request A Sample',
      :display_name => "Joyday.com",bcc: ['request@joyday.com', 'greyadmin@joyday.com','aulya.nsr@live.com'] )
  end
  def requestsample_user(user)
    @user = user
    mail( :to => user.email,
      :subject => 'Request A Sample',
      :display_name => "Joyday.com",bcc: ['aulya.nasir@live.com'] )
  end
end
